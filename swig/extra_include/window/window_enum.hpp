/* 
 * File:   windowenum.hpp
 * Author: Samuel - Dell
 *
 * Created on den 18 juli 2014, 12:19
 */

#ifndef WINDOWENUM_HPP
#define	WINDOWENUM_HPP

#include <SDL2/SDL_video.h>


enum WindowFlags
{
    WINDOW_FULLSCREEN = SDL_WINDOW_FULLSCREEN,         /**< fullscreen window */
    //WINDOW_OPENGL = SDL_WINDOW_OPENGL,             /**< window usable with OpenGL context */
    WINDOW_SHOWN = SDL_WINDOW_SHOWN,              /**< window is visible */
    WINDOW_HIDDEN = SDL_WINDOW_HIDDEN,             /**< window is not visible */
    WINDOW_BORDERLESS = SDL_WINDOW_BORDERLESS,         /**< no window decoration */
    WINDOW_RESIZABLE = SDL_WINDOW_RESIZABLE,          /**< window can be resized */
    WINDOW_MINIMIZED = SDL_WINDOW_MINIMIZED,          /**< window is minimized */
    WINDOW_MAXIMIZED = SDL_WINDOW_MAXIMIZED,          /**< window is maximized */
    WINDOW_INPUT_GRABBED = SDL_WINDOW_INPUT_GRABBED,      /**< window has grabbed input focus */
    WINDOW_INPUT_FOCUS = SDL_WINDOW_INPUT_FOCUS,        /**< window has input focus */
    WINDOW_MOUSE_FOCUS = SDL_WINDOW_MOUSE_FOCUS,        /**< window has mouse focus */
    WINDOW_FULLSCREEN_DESKTOP = SDL_WINDOW_FULLSCREEN_DESKTOP
    //WINDOW_FOREIGN = SDL_WINDOW_FOREIGN,            /**< window not created by SDL */
    //WINDOW_ALLOW_HIGHDPI = SDL_WINDOW_ALLOW_HIGHDPI       /**< window should be created in high-DPI mode if supported */
} ;


enum WindowPos
{
    WINDOWPOS_UNDEFINED = SDL_WINDOWPOS_UNDEFINED,
    WINDOWPOS_CENTERED  = SDL_WINDOWPOS_CENTERED
};

#endif	/* WINDOWENUM_HPP */

