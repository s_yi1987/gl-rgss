/* 
 * File:   gltexture.hpp
 * Author: Samuel - Dell
 *
 * Created on den 1 juni 2014, 17:49
 */

#ifndef GLTEXTURE_HPP
#define	GLTEXTURE_HPP

%include "blendmode.hpp"
%include "surface.hpp"
%include "color.hpp"
%include "rect.hpp"
%include "renderwindow.hpp"
%include "../system/exception.hpp"


namespace rgfw
{
%newObject Texture::getColorMod;
%newObject Texture::getRect;
%newObject Texture::toSurface;
%newObject Texture::getClipRect;

%catches(rgfw::Exception) Texture::Texture;
%catches(rgfw::Exception) Texture::setup;
%catches(rgfw::Exception) Texture::update;
%catches(rgfw::Exception) Texture::blit;
%catches(rgfw::Exception) Texture::blitScaled;
%catches(rgfw::Exception) Texture::fillRect;
%catches(rgfw::Exception) Texture::setSmooth;
%catches(rgfw::Exception) Texture::setRepeated;
%catches(rgfw::Exception) Texture::downloadTo;
%catches(rgfw::Exception) Texture::toSurface;
%catches(rgfw::Exception) Texture::getMaximumSize;

%rename(rect) Texture::getRect;
%rename("disposed?") Texture::isDisposed;
%rename(set_color_mod) Texture::setColorMod;
%rename(color_mod) Texture::getColorMod;
%rename("alpha_mod=") Texture::setAlphaMod;
%rename(alpha_mod) Texture::getAlphaMod;
%rename("blend_mode=") Texture::setBlendMode;
%rename(blend_mode) Texture::getBlendMode;
%rename("clip_rect=") Texture::setClipRect;
%rename(clip_rect) Texture::getClipRect;
%rename(blit_scaled) Texture::blitScaled;
%rename(fill_rect) Texture::fillRect;
%rename(width) Texture::getWidth;
%rename(height) Texture::getHeight;
%rename(actual_width) Texture::getActualWidth;
%rename(actual_height) Texture::getActualHeight;
%rename("smooth=") Texture::setSmooth;
%rename("smooth?") Texture::isSmooth;
%rename("repeated=") Texture::setRepeated;
%rename("repeated?") Texture::isRepeated;
%rename(download_to) Texture::downloadTo;
%rename(to_surface) Texture::toSurface;
%rename("window=") Texture::setWindow;
%rename(get_valid_size) Texture::getValidSize;
%rename(maximum_size) Texture::getMaximumSize;
%rename(NativeTexture) Texture;


#include "graphics/surface.hpp"
#include "graphics/blendmode.hpp"

class RenderWindow;
class RenderTargetModeTexture;


class Texture
{
public:
    
    Texture();
    
    Texture(const Texture& other);
    
    Texture(const std::string& filename);
    
    Texture(const Surface& surface);
    
    Texture(int width, int height);
    
    Texture& operator=(const Texture& other);
    
    ~Texture();

    void setup(const std::string& filename);
    
    void setup(const Surface& surface);
    
    void setup(int width, int height);
    
    void setup(const Texture& other);
    
    void update(const Surface& surface, int x = 0, int y = 0);
    
    void dispose();
    
    bool isDisposed() const;
    
    void setColorMod(const Color& color);
    
    Color getColorMod() const;
    
    void setAlphaMod(int alphaMod);
    
    int getAlphaMod() const;
    
    void setBlendMode(BlendMode blendMode);
    
    BlendMode getBlendMode() const;
	
    void setClipRect(const Rect* rect);
    
    Rect getClipRect() const;
    
    void blit(const Texture& src, const Rect* srcRect, const Rect* dstRect);
    
    void blitScaled(const Texture& src, const Rect* srcRect, const Rect* dstRect);
    
    void fillRect(const Rect* rect, const Color& color);
    
    void fillRect(const Rect* rect, Uint32 pixel);
    
    int getWidth() const;
    
    int getHeight() const;
    
    int getActualWidth() const;
    
    int getActualHeight() const;
    
    Rect getRect() const;
    
    void setSmooth(bool smooth);
    
    bool isSmooth() const;
    
    void setRepeated(bool repeated);
    
    bool isRepeated() const;
    
    Surface toSurface() const;
    
    void downloadTo(Surface& surface) const;
    
    void setWindow(RenderWindow* window);
    
    //unsigned int getTextureID() const;
    
    //static void bind(const Texture* texture);
    
    static int getValidSize(int size);
    
    static int getMaximumSize();
    
private:
  
    void blitMain(const Texture& src, const Rect* srcRect, const Rect* dstRect,
                  bool scaled);
    
    void upload(const Surface& surface, int x = 0, int y = 0);
    
    unsigned int mTextureID;
    int mWidth, mHeight;
    int mActualWidth, mActualHeight;
    Color mColorMod;
    BlendMode mBlendMode;
    bool mIsRepeated, mIsSmooth;
    RenderWindow* mWindow;

};

}

#endif	/* GLTEXTURE_HPP */

