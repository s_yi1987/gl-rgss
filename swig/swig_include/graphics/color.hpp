/* 
 * File:   color.hpp
 * Author: Samuel - Dell
 *
 * Created on den 22 februari 2014, 16:13
 */

#ifndef COLOR_HPP
#define	COLOR_HPP

#include <SDL2/SDL_pixels.h>

namespace rgfw
{

class PixelFormat;


class PlainColor
{
public:
    
    Uint8 r;
    Uint8 g;
    Uint8 b;
    Uint8 a;
    
    PlainColor();
    
    PlainColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha = 255);
	
	PlainColor(const PlainColor& other);
    
};

%include "../system/exception.hpp"

%newObject Color::toPlain;

%rename(to_mapped) Color::toMapped;
%rename(to_plain) Color::toPlain;

%catches(rgfw::Exception) Color::toMapped;
%catches(rgfw::Exception) Color::set;


class Color
{
public:
    
    double r;
    double g;
    double b;
    double a;
    
    Color();
	
	Color(const Color& other);
    
    Color(double red, double green, double blue, double alpha = 255);
    
    Color(const PlainColor& plainColor);
    
    Color(Uint32 pixel);//, Uint32 pixelFormatValue = SDL_PIXELFORMAT_ARGB8888);
    
    //Color(Uint32 pixel, const PixelFormat& pixelFormat);
    
    void set(const Color& other);
    
    void set(const PlainColor& plainColor);
    
    void set(double red, double green, double blue, double alpha = 255);
    
    void set(Uint32 pixel);//, Uint32 pixelFormatValue = SDL_PIXELFORMAT_ARGB8888);
    
    //void set(Uint32 pixel, const PixelFormat& pixelFormat);
    
    Uint32 toMapped() const;//Uint32 pixelFormatValue = SDL_PIXELFORMAT_ARGB8888) const;
    
    //Uint32 toMapped(const PixelFormat& pixelFormat) const;
    
    PlainColor toPlain() const;
    
    SDL_Color toSDL() const;
    
};

}

#endif	/* COLOR_HPP */

