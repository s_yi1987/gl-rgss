/* 
 * File:   renderwindow.hpp
 * Author: Samuel - Dell
 *
 * Created on den 15 juni 2014, 19:41
 */

#ifndef RENDERWINDOW_HPP
#define	RENDERWINDOW_HPP

%include "vertexdata.hpp"
%include "transformable.hpp"
%include "view.hpp"
%include "renderstates.hpp"
%include "vertexbufferobject.hpp"
%include "../window/window.hpp"

namespace rgfw
{
%newObject RenderWindow::getViewport;
%newObject RenderWindow::getView;
%newObject RenderWindow::getDefaultView;
%newObject RenderWindow::getClipRect;

%catches(rgfw::Exception) RenderWindow::RenderWindow;
%catches(rgfw::Exception) RenderWindow::setRenderTarget;
%catches(rgfw::Exception) RenderWindow::setClipRect;
%catches(rgfw::Exception) RenderWindow::draw;
%catches(rgfw::Exception) RenderWindow::getViewport;

%rename(bind_vbo) RenderWindow::bindVBO;
%rename(update_current_vbo) RenderWindow::updateCurrentVBO;
%rename(update_current_vertexbuffer) RenderWindow::updateCurrentVertexBuffer;
%rename(update_current_indexbuffer) RenderWindow::updateCurrentIndexBuffer;
%rename("view=") RenderWindow::setView;
%rename("default_view=") RenderWindow::setDefaultView;
%rename(view) RenderWindow::getView;
%rename(default_view) RenderWindow::getDefaultView;
%rename(get_viewport) RenderWindow::getViewport;
%rename("clip_rect=") RenderWindow::setClipRect;
%rename(clip_rect) RenderWindow::getClipRect;
%rename(set_render_target) RenderWindow::setRenderTarget;
%rename(set_active) RenderWindow::setActive;
%rename("active?") RenderWindow::isActive;
%rename("vertical_sync_enabled=") RenderWindow::setVerticalSyncEnabled;
%rename(NativeRenderWindow) RenderWindow;


#include "system/window.hpp"
#include "graphics/view.hpp"
#include "graphics/color.hpp"
#include "graphics/blendmode.hpp"
#include "system/mutex.hpp"

class Texture;
class VertexData;
class Transform;
class Transformable;
class RenderTargetMode;
class RenderTargetModeNone;
class RenderTargetModeTexture;


class RenderWindow : public Window
{
public:
        
    RenderWindow(const std::string& title, int x, int y, int width, int height,
                 Uint32 flags = SDL_WINDOW_SHOWN);
    
    virtual ~RenderWindow();
	
	void dispose();
	
    void bindVBO(VertexBufferObject* vbo);
    
    void updateCurrentVBO(const VertexData& vertexData,
                          unsigned int vertexBufferOffset = 0,
                          unsigned int indexBufferOffset  = 0);
						  
    void updateCurrentVertexBuffer(const Vertex* vertices, unsigned int count,
                                   unsigned int offset = 0);
    
    void updateCurrentIndexBuffer(const unsigned int* indices, unsigned int count,
                                  unsigned int offset  = 0);
    
    void draw(const VertexData& vertexData, const RenderStates& states);
    
    void draw(VertexBufferObject& vertexBufferObj, const RenderStates& states);
    
    void draw(const RenderStates& states);
    
    void setView(const View& view);
	
	void setDefaultView(const View& view);
    
    const View getView() const;
    
    const View getDefaultView() const;
    
    Rect getViewport(const View& view) const;
    
    void setClipRect(const Rect* rect);
    
    Rect getClipRect() const;
    
    void setRenderTarget(Texture* texture, bool adjustView = true);
    
    //Texture* getRenderTarget();
    
    bool setActive(bool active = true);
    
    bool isActive() const;
    
    void clear(const Color& color = Color(0, 0, 0, 255));
    
    void present();
	
	void setVerticalSyncEnabled(bool enabled);
    
protected:
    
    friend class Texture;
    
    static Mutex sPingPongTextureMutex;
    static Texture* sPingPongTexture;
    
private:

	void dispose(bool disposeWindow);
    
    void applyTransform(const Transform& transform);
    
    void applyTexture(const Texture* texture);
    
    void applyBlendMode(BlendMode mode);
    
    void applyCurrentView();
    
    void resetGLStates();
    
    void setupTargetModes();
    
    RenderWindow(const RenderWindow& other);
    
    RenderWindow& operator=(const RenderWindow& other);
    
    static SDL_GLContext createContext(SDL_Window* windowSDL);
    
    
    SDL_GLContext mContextSDL;
    View mView;
    View mDefaultView;
    bool mViewChanged;
    Rect mClipRect;
    Texture* mRenderTarget;
    RenderTargetMode* mCurrentTargetMode;
    RenderTargetModeNone* mTargetModeNone;
    RenderTargetModeTexture* mTargetModeTexture;
    
    static int sWindowCount;
    
};

}

#endif	/* RENDERWINDOW_HPP */

