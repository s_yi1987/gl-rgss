/* 
 * File:   indexvertexarray.hpp
 * Author: Samuel - Dell
 *
 * Created on den 22 augusti 2014, 22:26
 */

#ifndef INDEXVERTEXARRAY_HPP
#define	INDEXVERTEXARRAY_HPP

%include "vertexdata.hpp"
%include "transform.hpp"


#include "graphics/vertexarray.hpp"
#include <vector>

namespace rgfw
{
%rename("primitive_type=") IndexVertexArray::setPrimitiveType;
%rename(append_vertex) IndexVertexArray::appendVertex;
%rename(append_vertices) IndexVertexArray::appendVertices;
%rename(set_vertex) IndexVertexArray::setVertex;
%rename(append_index) IndexVertexArray::appendIndex;
%rename(append_indices) IndexVertexArray::appendIndices;
%rename(set_index) IndexVertexArray::setIndex;
%rename(resize_vertexarray) IndexVertexArray::resizeVertexArray;
%rename(resize_indexarray) IndexVertexArray::resizeIndexArray;
%rename(clear_vertexarray) IndexVertexArray::clearVertexArray;
%rename(clear_indexarray) IndexVertexArray::clearIndexArray;
%rename(get_vertex) IndexVertexArray::getVertex;
%rename(get_index) IndexVertexArray::getIndex;
%rename(vertex_count) IndexVertexArray::getVertexCount;
%rename(primitive_type) IndexVertexArray::getPrimitiveType;

    
class IndexVertexArray : public VertexData
{
public:
    IndexVertexArray();
    
    IndexVertexArray(PrimitiveType type, unsigned int vertexCount = 0, unsigned int indexCount = 0);
	
	IndexVertexArray(const IndexVertexArray& other);
    
    virtual ~IndexVertexArray();
    
    void setPrimitiveType(PrimitiveType type);
    
    void appendVertex(const Vertex& vertex);
    
    void appendVertex(const Vertex& vertex, const Transform& transform);
	
    void appendVertices(const Vertex* vertices, unsigned int vertexCount);
    
    void appendVertices(const Vertex* vertices, unsigned int vertexCount,
                        const Transform& transform);
    
    void setVertex(unsigned int index, const Vertex& vertex);
    
    void appendIndex(unsigned int indexValue);
	
	void appendIndices(const unsigned int* indices, unsigned int indexCount);
    
    void setIndex(unsigned int index, unsigned int value);
    
    void resizeVertexArray(unsigned int vertexCount);
    
    void resizeIndexArray(unsigned int indexCount);
    
    void clear();
    
    void clearVertexArray();
    
    void clearIndexArray();
    
    Vertex& getVertex(unsigned int index);
    
    //const Vertex& getVertex(unsigned int index) const;
    
    unsigned int getIndex(unsigned int index) const;
    
    //virtual const Vertex* getVertices() const;
     
    //virtual const unsigned int* getIndices() const;
    
    virtual unsigned int getVertexCount() const;
    
    //virtual unsigned int getIndexCount() const;
    
    virtual PrimitiveType getPrimitiveType() const;
    
private:
    VertexArray mVertices;
    IndexArray mIndices;
    
};

}


#endif	/* INDEXVERTEXARRAY_HPP */

