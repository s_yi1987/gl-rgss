/* 
 * File:   vertexarray.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 juni 2014, 00:10
 */

#ifndef VERTEXARRAY_HPP
#define	VERTEXARRAY_HPP

%include "vertexdata.hpp"
%include "rect.hpp"
%include "transform.hpp"


namespace rgfw
{
%newObject VertexArray::getBounds;

%rename(vertex_count) VertexArray::getVertexCount;
%rename("primitive_type=") VertexArray::setPrimitiveType;
%rename(primitive_type) VertexArray::getPrimitiveType;
%rename(bounds) VertexArray::getBounds;
%rename(__getitem__) VertexArray::operator[];


#include "graphics/vertexdata.hpp"
#include "graphics/rect.hpp"
#include "graphics/transform.hpp"
#include <vector>


class VertexArray : public VertexData
{
public :
    VertexArray();
	
	VertexArray(const VertexArray& other);

    explicit VertexArray(PrimitiveType type, unsigned int vertexCount = 0);

    virtual unsigned int getVertexCount() const;

    Vertex& operator [](unsigned int index);

    //const Vertex& operator [](unsigned int index) const;

    void clear();

    void resize(unsigned int vertexCount);
	
	void append(const Vertex& vertex);

    void append(const Vertex& vertex, const Transform& transform);
	
	void append(const VertexData& vertexData);
	
	void append(const VertexData& vertexData, const Transform& transform);

    void setPrimitiveType(PrimitiveType type);

    virtual PrimitiveType getPrimitiveType() const;

    FloatRect getBounds() const;
    
    //virtual const Vertex* getVertices() const;

private:

    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    std::vector<Vertex> m_vertices;      ///< Vertices contained in the array
    PrimitiveType       m_primitiveType; ///< Type of primitives to draw
};

}

#endif	/* VERTEXARRAY_HPP */

