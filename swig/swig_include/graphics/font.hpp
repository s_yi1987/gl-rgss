/* 
 * File:   font.hpp
 * Author: Samuel - Dell
 *
 * Created on den 8 mars 2014, 21:51
 */

#ifndef FONT_HPP
#define	FONT_HPP

%include "rect.hpp"
%include "color.hpp"
%include "surface.hpp"

namespace rgfw
{

%newObject Font::calculateTextSize;

%newObject Font::toSurfaceTextSolid;
%newObject Font::toSurfaceUTF8Solid;
%newObject Font::toSurfaceUNICODESolid;

%newObject Font::toSurfaceTextShaded;
%newObject Font::toSurfaceUTF8Shaded;
%newObject Font::toSurfaceUNICODEShaded;

%newObject Font::toSurfaceTextBlended;
%newObject Font::toSurfaceUTF8Blended;
%newObject Font::toSurfaceUNICODEBlended;


%catches(rgfw::Exception) Font::Font;
%catches(rgfw::Exception) Font::setup;

%catches(rgfw::Exception) Font::toSurfaceTextSolid;
%catches(rgfw::Exception) Font::toSurfaceUTF8Solid;
%catches(rgfw::Exception) Font::toSurfaceUNICODESolid;
%catches(rgfw::Exception) Font::renderTextSolidTo;
%catches(rgfw::Exception) Font::renderUTF8SolidTo;
%catches(rgfw::Exception) Font::renderUNICODESolidTo;

%catches(rgfw::Exception) Font::toSurfaceTextShaded;
%catches(rgfw::Exception) Font::toSurfaceUTF8Shaded;
%catches(rgfw::Exception) Font::toSurfaceUNICODEShaded;
%catches(rgfw::Exception) Font::renderTextShadedTo;
%catches(rgfw::Exception) Font::renderUTF8ShadedTo;
%catches(rgfw::Exception) Font::renderUNICODEShadedTo;

%catches(rgfw::Exception) Font::toSurfaceTextBlended;
%catches(rgfw::Exception) Font::toSurfaceUTF8Blended;
%catches(rgfw::Exception) Font::toSurfaceUNICODEBlended;
%catches(rgfw::Exception) Font::renderTextBlendedTo;
%catches(rgfw::Exception) Font::renderUTF8BlendedTo;
%catches(rgfw::Exception) Font::renderUNICODEBlendedTo;


%rename("disposed?") Font::isDisposed;
%rename(style) Font::getStyle;
%rename("style=") Font::setStyle;
%rename(outline) Font::getOutline;
%rename("outline=") Font::setOutline;
%rename(hinting) Font::getHinting;
%rename("hinting=") Font::setHinting;
%rename(kerning) Font::getKerning;
%rename("kerning=") Font::setKerning;
%rename(ascent) Font::getAscent;
%rename("ascent=") Font::getDescent;
%rename(line_skip) Font::getLineSkip;
%rename(family_name) Font::getFamilyName;
%rename(style_name) Font::getStyleName;
%rename(calc_text_size) Font::calculateTextSize;
%rename("fixed_face_width?") Font::fixedFaceWidth;

%rename(to_surface_text_solid) Font::toSurfaceTextSolid;
%rename(render_text_solid_to) Font::renderTextSolidTo;
%rename(to_surface_utf8_solid) Font::toSurfaceUTF8Solid;
%rename(render_utf8_solid_to) Font::renderUTF8SolidTo;
%rename(to_surface_unicode_solid) Font::toSurfaceUNICODESolid;
%rename(render_unicode_solid_to) Font::renderUNICODESolidTo;

%rename(to_surface_text_shaded) Font::toSurfaceTextShaded;
%rename(render_text_shaded_to) Font::renderTextShadedTo;
%rename(to_surface_utf8_shaded) Font::toSurfaceUTF8Shaded;
%rename(render_utf8_shaded_to) Font::renderUTF8ShadedTo;
%rename(to_surface_unicode_shaded) Font::toSurfaceUNICODEShaded;
%rename(render_unicode_shaded_to) Font::renderUNICODEShadedTo;

%rename(to_surface_text_blended) Font::toSurfaceTextBlended;
%rename(render_text_blended_to) Font::renderTextBlendedTo;
%rename(to_surface_utf8_blended) Font::toSurfaceUTF8Blended;
%rename(render_utf8_blended_to) Font::renderUTF8BlendedTo;
%rename(to_surface_unicode_blended) Font::toSurfaceUNICODEBlended;
%rename(render_unicode_blended_to) Font::renderUNICODEBlendedTo;

%rename(NativeFont) Font;


#include "graphics/rect.hpp"
#include <string>
#include <SDL2/SDL_ttf.h>


class Font
{
public:
    Font();
    
    Font(const std::string& filename, int ptsize, int index = 0);
    
    ~Font();
    
    void setup(const std::string& filename, int ptsize, int index = 0);
	
    void dispose();
	
    bool isDisposed() const;
    
    TTF_Font* get() const;
    
    int getStyle() const;
	
    int setStyle(int style);
    
    int getOutline() const;
	
    int setOutline(int outline);
    
    int getHinting() const;
	
    int setHinting(int hinting);
    
    int getKerning() const;
	
    int setKerning(int allowed);
    
    int getHeight() const;
	
    int getAscent() const;
	
    int getDescent() const;
	
    int getLineSkip() const;
    
    std::string getFamilyName() const;
	
    std::string getStyleName() const;
	
    Rect calculateTextSize(const std::string& text);
    
    bool fixedFaceWidth() const;
	
    Surface toSurfaceTextSolid(const std::string& text, const Color& fg) const;
    
    void renderTextSolidTo(Surface& surface, const std::string& text,
                           const Color& fg) const;
    
    Surface toSurfaceUTF8Solid(const std::string& text, const Color& fg) const;
    
    void renderUTF8SolidTo(Surface& surface, const std::string& text,
                           const Color& fg) const;
    
    Surface toSurfaceUNICODESolid(Uint16 text, const Color& fg) const;
    
    void renderUNICODESolidTo(Surface& surface, Uint16 text, const Color& fg) const;
    
    Surface toSurfaceTextShaded(const std::string& text, const Color& fg,
                                const Color& bg) const;
    
    void renderTextShadedTo(Surface& surface, const std::string& text,
                            const Color& fg, const Color& bg) const;
    
    Surface toSurfaceUTF8Shaded(const std::string& text, const Color& fg,
                                const Color& bg) const;
    
    void renderUTF8ShadedTo(Surface& surface, const std::string& text,
                            const Color& fg, const Color& bg) const;
    
    Surface toSurfaceUNICODEShaded(Uint16 text, const Color& fg, const Color& bg) const;
    
    void renderUNICODEShadedTo(Surface& surface, Uint16 text, const Color& fg,
                               const Color& bg) const;
							   
    Surface toSurfaceTextBlended(const std::string& text, const Color& fg) const;
    
    void renderTextBlendedTo(Surface& surface, const std::string& text,
                             const Color& fg) const;
    
    Surface toSurfaceUTF8Blended(const std::string& text, const Color& fg) const;
    
    void renderUTF8BlendedTo(Surface& surface, const std::string& text,
                             const Color& fg) const;
    
    Surface toSurfaceUNICODEBlended(Uint16 text, const Color& fg) const;
    
    void renderUNICODEBlendedTo(Surface& surface, Uint16 text, const Color& fg) const;
    
private:
    Font(const Font& other);
	
    Font& operator=(const Font& other);
    
    TTF_Font* loadFont(const std::string& filename, int ptsize, int index);
	
	
	TTF_Font* mFontSDL;
    
};

}

#endif	/* FONT_HPP */

