/* 
 * File:   vertexbufferobject.hpp
 * Author: Samuel - Dell
 *
 * Created on den 18 augusti 2014, 17:24
 */

#ifndef VERTEXBUFFEROBJECT_HPP
#define	VERTEXBUFFEROBJECT_HPP

%include "vertexdata.hpp"
%include "../system/exception.hpp"


#include "graphics/primitivetype.hpp"
#include "system/mutex.hpp"


namespace rgfw
{
%catches(rgfw::Exception) VertexBufferObject::VertexBufferObject;
%catches(rgfw::Exception) VertexBufferObject::setup;
%catches(rgfw::Exception) VertexBufferObject::update;
%catches(rgfw::Exception) VertexBufferObject::updateVertexBuffer;
%catches(rgfw::Exception) VertexBufferObject::updateIndexBuffer;

%rename("disposed?") VertexBufferObject::isDisposed;
%rename(update_vertexbuffer) VertexBufferObject::updateVertexBuffer;
%rename(update_indexbuffer) VertexBufferObject::updateIndexBuffer;
%rename(primitive_type) VertexBufferObject::getPrimitiveType;
%rename("primitive_type=") VertexBufferObject::setPrimitiveType;
%rename(vertex_capacity) VertexBufferObject::getVertexCapacity;
%rename(index_capacity) VertexBufferObject::getIndexCapacity;
%rename("vertex_read_size=") VertexBufferObject::setVertexReadSize;
%rename("index_read_size=") VertexBufferObject::setIndexReadSize;
%rename(vertex_read_size) VertexBufferObject::getVertexReadSize;
%rename(index_read_size) VertexBufferObject::getIndexReadSize;
%rename(mode) VertexBufferObject::getMode;
%rename(primitive_type) VertexBufferObject::getPrimitiveType;

%rename(NativeVertexBufferObject) VertexBufferObject;


class Vertex;
class VertexData;

enum VertexBufferMode
{
    VBO_STATIC_DRAW,
    VBO_DYNAMIC_DRAW,
    VBO_STREAM_DRAW
};


class VertexBufferObject
{
public:
	VertexBufferObject();

    VertexBufferObject(unsigned int vertexCapacitySize, VertexBufferMode mode);
    
    VertexBufferObject(unsigned int vertexCapacitySize, unsigned int indexCapacitySize,
                       VertexBufferMode mode);
    
    VertexBufferObject(const VertexData& vertexData, VertexBufferMode mode);
    
    ~VertexBufferObject();
    
    void setup(unsigned int vertexCapacitySize, VertexBufferMode mode);
    
    void setup(unsigned int vertexCapacitySize, unsigned int indexCapacitySize,
               VertexBufferMode mode);
    
    void setup(const VertexData& vertexData, VertexBufferMode mode);
    
    void dispose();
    
    bool isDisposed() const;
    
    void update(const VertexData& vertexData,
                unsigned int vertexBufferOffset = 0,
                unsigned int indexBufferOffset = 0);
				
    void updateVertexBuffer(const Vertex* vertices, unsigned int count,
                            unsigned int offset = 0);
    
    void updateIndexBuffer(const unsigned int* indices, unsigned int count,
                           unsigned int offset  = 0);
    
    void setPrimitiveType(PrimitiveType type);
    
    void setVertexReadSize(unsigned int size);
    
    void setIndexReadSize(unsigned int size);
    
    unsigned int getVertexCapacity() const;
    
    unsigned int getVertexReadSize() const;
    
    unsigned int getIndexCapacity() const;
    
    unsigned int getIndexReadSize() const;
    
    //unsigned int getVertexBufferID() const;
    
    //unsigned int getIndexBufferID() const;
    
    VertexBufferMode getMode() const;
    
    PrimitiveType getPrimitiveType() const;
    
    //static void bind(const VertexBufferObject* vertexBufferObject);
    
private:
    void setupBuffers(const Vertex* vertices, unsigned int vertexCapacitySize,
                      const unsigned int* indices, unsigned int indexCapacitySize,
                      VertexBufferMode mode);
    
    void setupVertexBuffer();
    
    void setupIndexBuffer();
    
    void disposeVertexBuffer();
    
    void disposeIndexBuffer();

    static void verifyAvailability();
    
    unsigned int mVertexBufferID;
    unsigned int mIndexBufferID;
    VertexBufferMode mMode;
    PrimitiveType mPrimitiveType;
    unsigned int mVertexCapacity;
    unsigned int mIndexCapacity;
    
    
};

}

#endif	/* VERTEXBUFFEROBJECT_HPP */

