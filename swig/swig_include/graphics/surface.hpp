/* 
 * File:   surface.hpp
 * Author: Samuel - Dell
 *
 * Created on den 10 februari 2014, 13:39
 */
#ifndef SURFACE_HPP
#define	SURFACE_HPP

%include "rect.hpp"
%include "color.hpp"
%include "blendmode.hpp"
%include "../system/outputargument.hpp"
%include "../system/exception.hpp"

namespace rgfw
{
%newObject Surface::getColorMod;
%newObject Surface::getRect;
%newObject Surface::getPixelColor;
%newObject Surface::getClipRect;

%catches(rgfw::Exception) Surface::Surface;
%catches(rgfw::Exception) Surface::setup;

%rename(rect) Surface::getRect;
%rename(width) Surface::getWidth;
%rename(height) Surface::getHeight;
%rename(color_mod) Surface::getColorMod;
%rename(set_color_mod) Surface::setColorMod;
%rename(alpha_mod) Surface::getAlphaMod;
%rename("alpha_mod=") Surface::setAlphaMod;
%rename(blend_mode) Surface::getBlendMode;
%rename("blend_mode=") Surface::setBlendMode;
%rename("disposed?") Surface::isDisposed;
%rename(blit_scaled) Surface::blitScaled;
%rename(get_pixel) Surface::getPixel;
%rename(get_pixel_color) Surface::getPixelColor;
%rename(set_pixel) Surface::setPixel;
%rename(fill_rect) Surface::fillRect;
%rename(clip_rect) Surface::getClipRect;
%rename("clip_rect=") Surface::setClipRect;


#include "graphics/rect.hpp"
#include "graphics/color.hpp"
#include "graphics/blendmode.hpp"
#include "system/outputargument.hpp"
#include <SDL2/SDL_surface.h>
#include <string>

class Font;


class Surface
{
public:
    
    Surface();
    
    Surface(const std::string& filename);//,
            //Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    Surface(int width, int height);//,
            //Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    ~Surface();
    
    Surface(const Surface& other);
    
    Surface& operator=(const Surface& other);
    
    void setup(const std::string& filename);//,
               //Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    void setup(int width, int height);//,
               //Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    void setup(const Surface& other);
    
    void dispose();
    
    bool isDisposed() const;
    
    SDL_Surface* get() const;
    
    //Uint32 getPixelFormatValue() const;
    
    int getWidth() const;
    
    int getHeight() const;
    
    Rect getRect() const;
    
    Rect getClipRect() const;
    
    int setClipRect(const Rect* clipRect);
    
    int getAlphaMod() const;
    
    int setAlphaMod(int alphaMod);
    
    BlendMode getBlendMode() const;
    
    int setBlendMode(BlendMode mode);
    
    Color getColorMod(OutputArgument<int>* output = NULL) const;
    
    int setColorMod(const Color& color);
    
    int setColorMod(int r, int g, int b);
    
    Color getPixelColor(int x , int y, OutputArgument<bool>* output = NULL) const;
    
    Uint32 getPixel(int x, int y, OutputArgument<bool>* output = NULL) const;
    
    bool setPixel(int x, int y, const Color& color);
    
    bool setPixel(int x, int y, Uint32 pixel);
    
/*     Color getColorKey(OutputArgument<bool>* output = NULL) const;
    
    int setColorKey(Color& color, bool flag = true);
    
    int setColorKey(Uint32 color, bool flag = true); */
    
    int fillRect(const Rect* rect, const Color& color);
    
    int fillRect(const Rect* rect, Uint32 pixel);
    
    int blit(Surface& src, const Rect* srcRect, const Rect* dstRect);
    
    int blitScaled(Surface& src, const Rect* srcRect, const Rect* dstRect);
    
private:
    
    virtual SDL_Surface* getSDLSurfaceCopy() const;
    
    virtual int blitMain(Surface& src, const Rect* srcRect, 
                         const Rect* dstRect, bool scaled);
    
    static SDL_Surface* createImageSurface(const std::string& filename,
                                           Uint32 format);
    
    static SDL_Surface* ensurePixelFormatValue(SDL_Surface* drawSurface,
                                               Uint32 format);
    
    static SDL_Surface* createRGBSurface(int width, int height, Uint32 format);
    
    static SDL_Surface* createNullRGBSurface(int width, int height, Uint32 format);
    
    static SDL_Surface* createRGBSurfaceMain(int width, int height, Uint32 format,
                                             bool createNull = false);
    
    
    SDL_Surface* mSurfaceSDL;
    
    static const int UTF8_SOLID   = 0;
    
    static const int UTF8_BLENDED = 1;
    
    static const int UTF8_SHADED  = 2;
    
};

}

#endif	/* SURFACE_HPP */

