/* 
 * File:   transformable.hpp
 * Author: Samuel - Dell
 *
 * Created on den 3 juni 2014, 12:16
 */

#ifndef TRANSFORMABLE_HPP
#define	TRANSFORMABLE_HPP

%include "transform.hpp"

namespace rgfw
{
%newObject Transformable::getPosition;
%newObject Transformable::getScale;
%newObject Transformable::getOrigin;
%newObject Transformable::getTransform;
%newObject Transformable::getInverseTransform;

%rename(set_position) Transformable::setPosition;
%rename("rotation=") Transformable::setRotation;
%rename(set_scale) Transformable::setScale;
%rename(set_origin) Transformable::setOrigin;
%rename(position) Transformable::getPosition;
%rename(rotation) Transformable::getRotation;
%rename(scale) Transformable::getScale;
%rename(origin) Transformable::getOrigin;
%rename(transform) Transformable::getTransform;
%rename(inverse_transform) Transformable::getInverseTransform;


#include "transform.hpp"

class Transformable
{
public :

    Transformable();
	
	Transformable(const Transformable& other);

    virtual ~Transformable();

    void setPosition(float x, float y);

    void setPosition(const Vector2f& position);

    void setRotation(float angle);

    void setScale(float factorX, float factorY);

    void setScale(const Vector2f& factors);

    void setOrigin(float x, float y);

    void setOrigin(const Vector2f& origin);

    const Vector2f getPosition() const;

    float getRotation() const;

    const Vector2f getScale() const;

    const Vector2f getOrigin() const;

    void move(float offsetX, float offsetY);

    void move(const Vector2f& offset);

    void rotate(float angle);

    void scale(float factorX, float factorY);

    void scale(const Vector2f& factor);

    const Transform getTransform() const;

    const Transform getInverseTransform() const;

private :
    
    Vector2f          m_origin;                     ///< Origin of translation/rotation/scaling of the object
    Vector2f          m_position;                   ///< Position of the object in the 2D world
    float             m_rotation;                   ///< Orientation of the object, in degrees
    Vector2f          m_scale;                      ///< Scale of the object
    mutable Transform m_transform;                  ///< Combined transformation of the object
    mutable bool      m_transformNeedUpdate;        ///< Does the transform need to be recomputed?
    mutable Transform m_inverseTransform;           ///< Combined transformation of the object
    mutable bool      m_inverseTransformNeedUpdate; ///< Does the transform need to be recomputed?
    
};

}

#endif	/* TRANSFORMABLE_HPP */

