/* 
 * File:   blendmode.hpp
 * Author: Samuel - Dell
 *
 * Created on den 10 juni 2014, 11:19
 */

#ifndef BLENDMODE_HPP
#define	BLENDMODE_HPP

namespace rgfw
{

enum BlendMode
{
    BLEND_ALPHA,    ///< Pixel = Source * Source.a + Dest * (1 - Source.a)
    BLEND_ADD,      ///< Pixel = Source + Dest
    BLEND_MULTIPLY, ///< Pixel = Source * Dest
    BLEND_MOD,
    BLEND_NONE      ///< Pixel = Source
};

}
#endif	/* BLENDMODE_HPP */

