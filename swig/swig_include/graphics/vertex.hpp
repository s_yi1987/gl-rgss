/* 
 * File:   vertex.hpp
 * Author: Samuel - Dell
 *
 * Created on den 4 juni 2014, 18:36
 */

#ifndef VERTEX_HPP
#define	VERTEX_HPP

%include "../system/vector2.hpp"
%include "color.hpp"

namespace rgfw
{
%rename(tex_coords) Vertex::texCoords;


#include "system/vector2.hpp"
#include "graphics/color.hpp"


class Vertex
{
public :

    Vertex();
	
	Vertex(const Vertex& other);

    Vertex(const Vector2f& thePosition);

    Vertex(const Vector2f& thePosition, const PlainColor& theColor);

    Vertex(const Vector2f& thePosition, const Vector2f& theTexCoords);

    Vertex(const Vector2f& thePosition, const PlainColor& theColor,
           const Vector2f& theTexCoords);

    
    Vector2f   position;  ///< 2D position of the vertex
    PlainColor color;     ///< Color of the vertex
    Vector2f   texCoords; ///< Coordinates of the texture's pixel to map to the vertex
    
};

}

#endif	/* VERTEX_HPP */

