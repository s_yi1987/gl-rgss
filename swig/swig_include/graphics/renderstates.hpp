#ifndef RENDERSTATES_HPP
#define	RENDERSTATES_HPP


#include "graphics/blendmode.hpp"
#include "graphics/transform.hpp"


%include "blendmode.hpp"
%include "transform.hpp"
%include "gltexture.hpp"


namespace rgfw
{
class Texture;
class Shader;

%rename(blend_mode) RenderStates::blendMode;
    
    
class RenderStates
{
public:
    
    RenderStates();
    
    RenderStates(BlendMode theBlendMode);
    
    RenderStates(const Transform& theTransform);
    
    RenderStates(const Texture* theTexture);
    
/*     RenderStates(const Shader* theShader); */
    
/*     RenderStates(BlendMode theBlendMode, const Transform& theTransform,
                 const Texture* theTexture, const Shader* theShader); */
    
    BlendMode blendMode;
    Transform transform;
    const Texture* texture;
/*     const Shader* shader; */
/*     bool skipShader; */
    
/*     static const RenderStates DEFAULT; */

};
    
}


#endif	/* RENDERSTATES_HPP */