/* 
 * File:   transform.hpp
 * Author: Samuel - Dell
 *
 * Created on den 2 juni 2014, 20:03
 */

#ifndef TRANSFORM_HPP
#define	TRANSFORM_HPP

%include "../system/vector2.hpp"
%include "rect.hpp"

namespace rgfw
{

%newObject Transform::getInverse;
%newObject Transform::transformPoint;
%newObject Transform::transformRect;

%rename(inverse) Transform::getInverse;
%rename(transform_point) Transform::transformPoint;
%rename(transform_rect) Transform::transformRect;

%rename(IDENTITY) Transform::Identity;


#include "system/vector2.hpp"
#include "graphics/rect.hpp"


class Transform
{
public :

    Transform();

    Transform(float a00, float a01, float a02,
              float a10, float a11, float a12,
              float a20, float a21, float a22);
			  
	Transform(const Transform& other);

    //const float* getMatrix() const;

    Transform getInverse() const;

    Vector2f transformPoint(float x, float y) const;

    Vector2f transformPoint(const Vector2f& point) const;

    FloatRect transformRect(const FloatRect& rectangle) const;

    void combine(const Transform& transform);

    void translate(float x, float y);

    void translate(const Vector2f& offset);

    void rotate(float angle);

    void rotate(float angle, float centerX, float centerY);

    void rotate(float angle, const Vector2f& center);

    void scale(float scaleX, float scaleY);

    void scale(float scaleX, float scaleY, float centerX, float centerY);

    void scale(const Vector2f& factors);

    void scale(const Vector2f& factors, const Vector2f& center);

    static const Transform Identity; ///< The identity transform (does nothing)

private:
    float m_matrix[16]; ///< 4x4 matrix defining the transformation
    
};

}

#endif	/* TRANSFORM_HPP */

