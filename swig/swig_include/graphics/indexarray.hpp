/* 
 * File:   indexarray.hpp
 * Author: Samuel - Dell
 *
 * Created on den 31 augusti 2014, 20:33
 */

#ifndef INDEXARRAY_HPP
#define	INDEXARRAY_HPP

#include <vector>


namespace rgfw
{
%rename(index_count) IndexArray::getIndexCount;
%rename(indices) IndexArray::getIndices;

    
class IndexArray
{
public :

    IndexArray();

    IndexArray(unsigned int indexCount);
	
	IndexArray(const IndexArray& other);

    //const unsigned int& operator [](unsigned int index) const;
    
    //unsigned int& operator [](unsigned int index);
    
    void set(unsigned int index, unsigned int val);
    
    unsigned int get(unsigned int index) const;

    void clear();

    void resize(unsigned int indexCount);
    
    void append(unsigned int val);
    
    void append(const unsigned int* indices, unsigned int indexCount);
    
    unsigned int getIndexCount() const;
    
    const unsigned int* getIndices() const;

private:

    std::vector<unsigned int> mIndices;
};

}


#endif	/* INDEXARRAY_HPP */

