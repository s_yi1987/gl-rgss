/* 
 * File:   spritevertexdata.hpp
 * Author: Samuel - Dell
 *
 * Created on den 25 juni 2014, 11:46
 */

#ifndef SPRITEVERTEXDATA_HPP
#define	SPRITEVERTEXDATA_HPP

%include "rect.hpp"
%include "color.hpp"
%include "vertexarray.hpp"

namespace rgfw
{
//%ignore SpriteVertexData::getVertices;

%newObject SpriteVertexData::getTextureRect;
%newObject SpriteVertexData::getColor;
%newObject SpriteVertexData::getBounds;
%newObject SpriteVertexData::toVertexArray;

%rename("texture_rect=") SpriteVertexData::setTextureRect;
%rename("color=") SpriteVertexData::setColor;
%rename(texture_rect) SpriteVertexData::getTextureRect;
%rename(color) SpriteVertexData::getColor;
%rename(to_vertex_array) SpriteVertexData::toVertexArray;
%rename(bounds) SpriteVertexData::getBounds;
%rename(vertex_count) SpriteVertexData::getVertexCount;
%rename(primitive_type) SpriteVertexData::getPrimitiveType;


#include "graphics/vertexdata.hpp"
#include "graphics/rect.hpp"


class SpriteVertexData : public VertexData
{
public:

    SpriteVertexData();
	
	SpriteVertexData(const SpriteVertexData& other);

    SpriteVertexData(const Rect& rectangle);

    void setTextureRect(const Rect& rectangle);

    void setColor(const Color& color);

    const Rect getTextureRect() const;

    Color getColor() const;
	
	VertexArray toVertexArray(bool separateTriangles) const;

    FloatRect getBounds() const;
    
    //virtual const Vertex* getVertices() const;
    
    virtual unsigned int getVertexCount() const;
    
    virtual PrimitiveType getPrimitiveType() const;

private:

    void updatePositions();

    void updateTexCoords();

    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    Vertex         mVertices[4]; ///< Vertices defining the sprite's geometry
    Rect           mTextureRect; ///< Rectangle defining the area of the source texture to display
};

}

#endif

