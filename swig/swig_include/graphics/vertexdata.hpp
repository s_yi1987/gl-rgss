/* 
 * File:   vertexdata.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 juni 2014, 18:56
 */

#ifndef VERTEXDATA_HPP
#define	VERTEXDATA_HPP

%include "vertex.hpp"
%include "primitivetype.hpp"

namespace rgfw
{
%rename(vertices) VertexData::getVertices;
%rename(vertex_count) VertexData::getVertexCount;
%rename(indices) VertexData::getIndices;
%rename(index_count) VertexData::getIndexCount;
%rename(primitive_type) VertexData::getPrimitiveType;


#include "vertex.hpp"
#include "primitivetype.hpp"

class VertexData
{
public:
    
    virtual const Vertex* getVertices() const;
    
    virtual unsigned int getVertexCount() const = 0;
	
	virtual unsigned int* getIndices() const;
	
	virtual unsigned int getIndexCount() const;
    
    virtual PrimitiveType getPrimitiveType() const = 0;
    
};

}

#endif	/* VERTEXDATA_HPP */

