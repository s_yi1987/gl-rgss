

#ifndef EVENTSTRUCTS_HPP
#define	EVENTSTRUCTS_HPP


enum SDL_Scancode
{
};


struct SDL_WindowEvent
{
    Uint32 type;        /**< ::SDL_WINDOWEVENT */
    Uint32 timestamp;
    Uint32 windowID;    /**< The associated window */
    Uint8 event;        /**< ::SDL_WindowEventID */
    //Uint8 padding1;
    //Uint8 padding2;
    //Uint8 padding3;
    Sint32 data1;       /**< event dependent data */
    Sint32 data2;       /**< event dependent data */
};


%rename(Keysym) SDL_Keysym;
struct SDL_Keysym
{
    SDL_Scancode scancode;      /**< SDL physical key code - see ::SDL_Scancode for details */
    Sint32 sym;            		/**< SDL virtual key code - see ::SDL_Keycode for details */
    Uint16 mod;                 /**< current key modifiers */
};


%rename(KeyboardEvent) SDL_KeyboardEvent;
struct SDL_KeyboardEvent
{
    Uint32 type;        /**< ::SDL_KEYDOWN or ::SDL_KEYUP */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with keyboard focus, if any */
    Uint8 state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    Uint8 repeat;       /**< Non-zero if this is a key repeat */
    SDL_Keysym keysym;  /**< The key that was pressed or released */
};


%rename(TextEditingEvent) SDL_TextEditingEvent;
struct SDL_TextEditingEvent
{
    Uint32 type;                                /**< ::SDL_TEXTEDITING */
    Uint32 timestamp;
    Uint32 windowID;                            /**< The window with keyboard focus, if any */
    //char text[SDL_TEXTEDITINGEVENT_TEXT_SIZE];  /**< The editing text */
	const std::string text;
    Sint32 start;                               /**< The start cursor of selected editing text */
    Sint32 length;                              /**< The length of selected editing text */
};


%rename(TextInputEvent) SDL_TextInputEvent;
struct SDL_TextInputEvent
{
    Uint32 type;                              /**< ::SDL_TEXTINPUT */
    Uint32 timestamp;
    Uint32 windowID;                          /**< The window with keyboard focus, if any */
    //char text[SDL_TEXTINPUTEVENT_TEXT_SIZE];  /**< The input text */
	const std::string text;
};


%rename(MouseMotionEvent) SDL_MouseMotionEvent;
struct SDL_MouseMotionEvent
{
    Uint32 type;        /**< ::SDL_MOUSEMOTION */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with mouse focus, if any */
    Uint32 which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    Uint32 state;       /**< The current button state */
    Sint32 x;           /**< X coordinate, relative to window */
    Sint32 y;           /**< Y coordinate, relative to window */
    Sint32 xrel;        /**< The relative motion in the X direction */
    Sint32 yrel;        /**< The relative motion in the Y direction */
};


%rename(MouseButtonEvent) SDL_MouseButtonEvent;
struct SDL_MouseButtonEvent
{
    Uint32 type;        /**< ::SDL_MOUSEBUTTONDOWN or ::SDL_MOUSEBUTTONUP */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with mouse focus, if any */
    Uint32 which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    Uint8 button;       /**< The mouse button index */
    Uint8 state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    Uint8 clicks;       /**< 1 for single-click, 2 for double-click, etc. */
    //Uint8 padding1;
    Sint32 x;           /**< X coordinate, relative to window */
    Sint32 y;           /**< Y coordinate, relative to window */
};


%rename(MouseWheelEvent) SDL_MouseWheelEvent;
struct SDL_MouseWheelEvent
{
    Uint32 type;        /**< ::SDL_MOUSEWHEEL */
    Uint32 timestamp;
    Uint32 windowID;    /**< The window with mouse focus, if any */
    Uint32 which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    Sint32 x;           /**< The amount scrolled horizontally, positive to the right and negative to the left */
    Sint32 y;           /**< The amount scrolled vertically, positive away from the user and negative toward the user */
};


%rename(JoyAxisEvent) SDL_JoyAxisEvent;
struct SDL_JoyAxisEvent
{
    Uint32 type;        /**< ::SDL_JOYAXISMOTION */
    Uint32 timestamp;
    //SDL_JoystickID which; /**< The joystick instance id */
	Sint32 which;
    Uint8 axis;         /**< The joystick axis index */
    //Uint8 padding1;
    //Uint8 padding2;
    //Uint8 padding3;
    Sint16 value;       /**< The axis value (range: -32768 to 32767) */
    //Uint16 padding4;
};


%rename(JoyBallEvent) SDL_JoyBallEvent;
struct SDL_JoyBallEvent
{
    Uint32 type;        /**< ::SDL_JOYBALLMOTION */
    Uint32 timestamp;
    //SDL_JoystickID which; /**< The joystick instance id */
	Sint32 which;
    Uint8 ball;         /**< The joystick trackball index */
    //Uint8 padding1;
    //Uint8 padding2;
    //Uint8 padding3;
    Sint16 xrel;        /**< The relative motion in the X direction */
    Sint16 yrel;        /**< The relative motion in the Y direction */
};


%rename(JoyHatEvent) SDL_JoyHatEvent;
struct SDL_JoyHatEvent
{
    Uint32 type;        /**< ::SDL_JOYHATMOTION */
    Uint32 timestamp;
    //SDL_JoystickID which; /**< The joystick instance id */
	Sint32 which;
    Uint8 hat;          /**< The joystick hat index */
    Uint8 value;        /**< The hat position value.
                         *   \sa ::SDL_HAT_LEFTUP ::SDL_HAT_UP ::SDL_HAT_RIGHTUP
                         *   \sa ::SDL_HAT_LEFT ::SDL_HAT_CENTERED ::SDL_HAT_RIGHT
                         *   \sa ::SDL_HAT_LEFTDOWN ::SDL_HAT_DOWN ::SDL_HAT_RIGHTDOWN
                         *
                         *   Note that zero means the POV is centered.
                         */
    //Uint8 padding1;
    //Uint8 padding2;
};


%rename(JoyButtonEvent) SDL_JoyButtonEvent;
struct SDL_JoyButtonEvent
{
    Uint32 type;        /**< ::SDL_JOYBUTTONDOWN or ::SDL_JOYBUTTONUP */
    Uint32 timestamp;
    SDL_JoystickID which; /**< The joystick instance id */
    Uint8 button;       /**< The joystick button index */
    Uint8 state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    //Uint8 padding1;
    //Uint8 padding2;
};


%rename(JoyDeviceEvent) SDL_JoyDeviceEvent;
struct SDL_JoyDeviceEvent
{
    Uint32 type;        /**< ::SDL_JOYDEVICEADDED or ::SDL_JOYDEVICEREMOVED */
    Uint32 timestamp;
    Sint32 which;       /**< The joystick device index for the ADDED event, instance id for the REMOVED event */
};


%rename(ControllerAxisEvent) SDL_ControllerAxisEvent;
struct SDL_ControllerAxisEvent
{
    Uint32 type;        /**< ::SDL_CONTROLLERAXISMOTION */
    Uint32 timestamp;
    SDL_JoystickID which; /**< The joystick instance id */
    Uint8 axis;         /**< The controller axis (SDL_GameControllerAxis) */
    //Uint8 padding1;
    //Uint8 padding2;
    //Uint8 padding3;
    Sint16 value;       /**< The axis value (range: -32768 to 32767) */
    //Uint16 padding4;
};


%rename(ControllerButtonEvent) SDL_ControllerButtonEvent;
struct SDL_ControllerButtonEvent
{
    Uint32 type;        /**< ::SDL_CONTROLLERBUTTONDOWN or ::SDL_CONTROLLERBUTTONUP */
    Uint32 timestamp;
    SDL_JoystickID which; /**< The joystick instance id */
    Uint8 button;       /**< The controller button (SDL_GameControllerButton) */
    Uint8 state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    //Uint8 padding1;
    //Uint8 padding2;
};


%rename(ControllerDeviceEvent) SDL_ControllerDeviceEvent;
struct SDL_ControllerDeviceEvent
{
    Uint32 type;        /**< ::SDL_CONTROLLERDEVICEADDED, ::SDL_CONTROLLERDEVICEREMOVED, or ::SDL_CONTROLLERDEVICEREMAPPED */
    Uint32 timestamp;
    Sint32 which;       /**< The joystick device index for the ADDED event, instance id for the REMOVED or REMAPPED event */
};


%rename(QuitEvent) SDL_QuitEvent;
struct SDL_QuitEvent
{
    Uint32 type;        /**< ::SDL_QUIT */
    Uint32 timestamp;
};


%rename(UserEvent) SDL_UserEvent;
struct SDL_UserEvent
{
    Uint32 type;        /**< ::SDL_USEREVENT through ::SDL_LASTEVENT-1 */
    Uint32 timestamp;
    Uint32 windowID;    /**< The associated window if any */
    Sint32 code;        /**< User defined event code */
    //void *data1;        /**< User defined data pointer */
    //void *data2;        /**< User defined data pointer */
};


%rename(TouchFingerEvent) SDL_TouchFingerEvent;
struct SDL_TouchFingerEvent
{
    Uint32 type;        /**< ::SDL_FINGERMOTION or ::SDL_FINGERDOWN or ::SDL_FINGERUP */
    Uint32 timestamp;
    SDL_TouchID touchId; /**< The touch device id */
    SDL_FingerID fingerId;
    float x;            /**< Normalized in the range 0...1 */
    float y;            /**< Normalized in the range 0...1 */
    float dx;           /**< Normalized in the range 0...1 */
    float dy;           /**< Normalized in the range 0...1 */
    float pressure;     /**< Normalized in the range 0...1 */
};


%rename(MultiGestureEvent) SDL_MultiGestureEvent;
struct SDL_MultiGestureEvent
{
    Uint32 type;        /**< ::SDL_MULTIGESTURE */
    Uint32 timestamp;
    //SDL_TouchID touchId; /**< The touch device index */
	Sint64 touchId;
    float dTheta;
    float dDist;
    float x;
    float y;
    Uint16 numFingers;
    //Uint16 padding;
};


%rename(DollarGestureEvent) SDL_DollarGestureEvent;
struct SDL_DollarGestureEvent
{
    Uint32 type;        /**< ::SDL_DOLLARGESTURE */
    Uint32 timestamp;
    //SDL_TouchID touchId; /**< The touch device id */
	Sint64 touchId;
    //SDL_GestureID gestureId;
	Sint64 gestureId;
    Uint32 numFingers;
    float error;
    float x;            /**< Normalized center of gesture */
    float y;            /**< Normalized center of gesture */
};

#endif	/* EVENTSTRUCTS_HPP */