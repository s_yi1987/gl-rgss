/* 
 * File:   window.hpp
 * Author: Samuel - Dell
 *
 * Created on den 28 februari 2014, 21:13
 */

#ifndef WINDOW_HPP
#define	WINDOW_HPP

%include "../graphics/rect.hpp"

namespace rgfw
{
%newObject Window::getRect;
%newObject Window::getPosition;
%newObject Window::getSize;

%rename("disposed?") Window::isDisposed;
%rename(rect) Window::getRect;
%rename(set_position) Window::setPosition;
%rename(position) Window::getPosition;
%rename(set_size) Window::setSize;
%rename(size) Window::getSize;
%rename("fullscreen_mode=") Window::setFullscreenMode;
%rename(fullscreen_mode) Window::getFullscreenMode;
%rename("bordered=") Window::setBordered;
%rename("bordered?") Window::isBordered;
%rename("grabmode=") Window::setGrabMode;
%rename("grabmode?") Window::isInGrabMode;
%rename("brightness=") Window::setBrightness;
%rename(brightness) Window::getBrightness;
%rename("title=") Window::setTitle;
%rename(title) Window::getTitle;
%rename(flags) Window::getFlags;
%rename(window_id) Window::getWindowID;


#include "graphics/rect.hpp"
#include "<SDL2/SDL_video.h>"
#include <string>

class Window
{
public:

    void dispose();
    
    bool isDisposed() const;

    SDL_Window* get() const;
    
    void setPosition(int x, int y);
	
    Rect getPosition() const;
    
    void setSize(int w, int h);
	
    Rect getSize() const;
    
    int setFullscreenMode(Uint32 flag);
	
    Uint32 getFullscreenMode() const;
    
    Rect getRect() const;
    
    void setBordered(bool bordered);
	
    bool isBordered() const;
    
    void setGrabMode(bool grabbed);
	
    bool isInGrabMode() const;
    
    int setBrightness(float brightness);
	
    float getBrightness() const;
    
    void setTitle(const std::string& title);
	
    std::string getTitle() const;
    
    Uint32 getFlags() const;
	
	Uint32 getWindowID() const;
	
protected:
	
    Window(const std::string& title, int x, int y, int width, int height,
           Uint32 flags = SDL_WINDOW_SHOWN);
		   
	virtual ~Window();
    
private:
    
    Window(const Window& other);
    
    Window& operator=(const Window& other);
    
    void dispose();
    
    static SDL_Window* createWindow(const std::string& title, int x, int y,
                                    int width, int height, Uint32 flags);
					
					
	SDL_Window* mWindowSDL;
	Rect mSize;
    
};

}

#endif	/* WINDOW_HPP */

