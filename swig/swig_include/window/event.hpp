/* 
 * File:   event.hpp
 * Author: Samuel - Dell
 *
 * Created on den 26 februari 2014, 19:29
 */

#ifndef EVENT_HPP
#define	EVENT_HPP

%include "event_structs.hpp"

namespace rgfw
{
%rename(key_name) Event::getKeyName;
%rename(poll_event) Event::pollEvent;


#include <SDL2/SDL_events.h>
#include <string>


class Event
{
public:
    Event();
	
	Event(const Event& other);
	
    const SDL_Event* get() const;
	
    std::string getKeyName() const;
    
    static bool pollEvent(Event& event);
    
private:
	SDL_Event mEventSDL;
	
public:
    Uint32 type;
    const SDL_WindowEvent& window;
    const SDL_KeyboardEvent& key;
    const SDL_TextEditingEvent& edit;
    const SDL_TextInputEvent& text;
    const SDL_MouseMotionEvent& motion;
    const SDL_MouseButtonEvent& button;
    const SDL_MouseWheelEvent& wheel;
    const SDL_JoyAxisEvent& jaxis;
    const SDL_JoyBallEvent& jball;
    const SDL_JoyHatEvent& jhat;
    const SDL_JoyButtonEvent& jbutton;
    const SDL_JoyDeviceEvent& jdevice;
    const SDL_ControllerAxisEvent& caxis;
    const SDL_ControllerButtonEvent& cbutton;
    const SDL_ControllerDeviceEvent& cdevice;
    const SDL_QuitEvent& quit;
    const SDL_UserEvent& user;
    const SDL_TouchFingerEvent& tfinger;
    const SDL_MultiGestureEvent& mgesture;
    const SDL_DollarGestureEvent& dgesture;
    
};

}

#endif	/* EVENT_HPP */

