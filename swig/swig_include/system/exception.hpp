/* 
 * File:   exception.hpp
 * Author: Samuel - Dell
 *
 * Created on den 11 februari 2014, 08:50
 */

#ifndef EXCEPTION_HPP
#define	EXCEPTION_HPP

namespace rgfw
{
%rename(message) Exception::getMessage;


#include <string>


class Exception
{
public:
    Exception(const std::string& message);
    
    const std::string& getMessage() const;

private:
    std::string mMessage;
};

}

#endif	/* EXCEPTION_HPP */

