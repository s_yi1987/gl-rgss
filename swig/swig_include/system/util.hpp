/* 
 * File:   util.hpp
 * Author: Samuel - Dell
 *
 * Created on den 22 februari 2014, 10:50
 */

#ifndef UTIL_HPP
#define	UTIL_HPP

%include "../graphics/rect.hpp"

namespace rgfw
{
%ignore util::setPixel;
%ignore util::getPixel;
%ignore util::transferPixels;
%ignore util::sameRectPos;
%ignore util::sameRectSize;
%ignore util::sameRect;
%ignore util::ensureGlewInit;
%ignore util::verifyGlContext;
%ignore util::blendModeToSDL;
%ignore util::blendModeFromSDL;

%rename(ensure_within_range) util::ensureWithinRange;
%rename(ensure_within_color_range) util::ensureWithinColorRange;
%rename("has_active_gl_context?")  util::hasActiveGlContext;


//#include "graphics/rect.hpp"
//#include "graphics/blendmode.hpp"
//#include "system/exception.hpp"
//#include <SDL2/SDL_blendmode.h>


namespace util
{
    int ensureWithinRange(int val, int min, int max);
    
    double ensureWithinRange(double val, double min, double max);
    
    int ensureWithinColorRange(int val);
    
    double ensureWithinColorRange(double val);
    
    void setPixel(void* pixels, int x, int y, int bpp, int pitch, Uint32 pixel);
    
    Uint32 getPixel(void* pixels, int x, int y, int bpp, int pitch);
    
    //Only the x and y coordinates of dstRect will be used
    void transferPixels(void* srcPixels, const Rect& srcRect, void* dstPixels,
                        const Rect& dstRect, int bpp, int pitch);
    
    void transferPixels(void* srcPixels, const Rect& srcRect, int srcBpp,
                        int srcPitch, void* dstPixels, const Rect& dstRect,
                        int dstBpp, int dstPitch);
    
    bool sameRectPos(const Rect& rect, const Rect& rect2);
    
    bool sameRectSize(const Rect& rect, const Rect& rect2);
    
    bool sameRect(const Rect& rect, const Rect& rect2);
    
    void ensureGlewInit();
    
    void verifyGlContext(bool throwExceptionIfFail = true,
                         const std::string& failMsg = "");
    
    bool hasActiveGlContext();
    
    SDL_BlendMode blendModeToSDL(BlendMode mode);
    
    BlendMode blendModeFromSDL(SDL_BlendMode modeSDL);
    
};

}

#endif	/* UTIL_HPP */

