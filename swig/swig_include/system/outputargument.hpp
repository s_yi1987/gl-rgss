/* 
 * File:   outputargument.hpp
 * Author: Samuel - Dell
 *
 * Created on den 14 mars 2014, 07:22
 */

#ifndef OUTPUTARGUMENT_HPP
#define	OUTPUTARGUMENT_HPP

namespace rgfw
{

template<typename T>
class OutputArgument
{
public:
    T val;
    
    OutputArgument(T val) : val(val) {}
        
};

}

%template(IntOutputArgument)  rgfw::OutputArgument<int>;
%template(BoolOutputArgument) rgfw::OutputArgument<bool>;

#endif	/* OUTPUTARGUMENT_HPP */

