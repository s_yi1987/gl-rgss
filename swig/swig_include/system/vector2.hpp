/* 
 * File:   point.hpp
 * Author: Samuel - Dell
 *
 * Created on den 2 juni 2014, 20:09
 */

#ifndef VECTOR2_HPP
#define	VECTOR2_HPP

#include <SDL2/SDL_rect.h>

namespace rgfw
{

template <typename T>
class Vector2
{
public:
    Vector2();
    
    Vector2(T x, T y);
	
	Vector2(const Vector2<T>& other);
    
    template <typename U>
    explicit Vector2(const Vector2<U>& vector);
    
    T x;
    T y;
    
};

typedef Vector2<int>          Vector2i;
typedef Vector2<unsigned int> Vector2u;
typedef Vector2<float>        Vector2f;

}

%template(Vector2i) rgfw::Vector2<int>;
%template(Vector2u) rgfw::Vector2<unsigned int>;
%template(Vector2f) rgfw::Vector2<float>;

#endif	/* VECTOR2_HPP */