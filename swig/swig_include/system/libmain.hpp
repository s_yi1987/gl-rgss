/* 
 * File:   libmain.hpp
 * Author: Samuel - Dell
 *
 * Created on den 24 februari 2014, 14:29
 */

#ifndef LIBMAIN_HPP
#define	LIBMAIN_HPP

namespace rgfw
{
%catches(rgfw::Exception) LibMain::LibMain;

%rename(last_error) LibMain::getLastError;
%rename(last_font_error) LibMain::getLastFontError;
%rename(ticks) LibMain::getTicks;
%rename(performance_counter) LibMain::getPerformanceCounter;
%rename(performance_frequency) LibMain::getPerformanceFrequency;
%rename("initialized?") LibMain::isInitialized;


#include "system/mutex.hpp"
#include <string>
#include <SDL_stdinc.h>


class LibMain
{
public:
    LibMain();
    
    ~LibMain();
    
    bool init();
    
    void close();
    
    std::string getLastError() const;
    
    std::string getLastFontError() const;
    
    Uint32 getTicks() const;
	
	Uint64 getPerformanceCounter() const;
	
	Uint64 getPerformanceFrequency() const;
	
	void delay(Uint32 ms) const;
    
    bool isInitialized() const;
    
private:
    static Mutex sMutex;
    static bool sInstantiated;
    bool mInitialized;

    LibMain(const LibMain& other);
    
    LibMain& operator=(const LibMain& other);
    
    bool initVideo();
    
    void setGLAtrributes();
    
    bool initImage();
    
    bool initFont();
    
};

}

#endif	/* LIBMAIN_HPP */

