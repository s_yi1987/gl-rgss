/* 
 * File:   lock.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 juli 2014, 16:12
 */

#ifndef LOCK_HPP
#define	LOCK_HPP


namespace rgfw
{
class Mutex;


class Lock
{
public:
    
    Lock(Mutex& mutex);
    
    ~Lock();
    
private:
    
    Lock(const Lock& other);
    
    Lock& operator=(const Lock& other);
    
    
    Mutex& mMutex;
    
};

}

#endif	/* LOCK_HPP */

