/* 
 * File:   graphics.hpp
 * Author: Samuel - Dell
 *
 * Created on den 11 juli 2014, 20:41
 */


%include "graphics/blendmode.hpp"
%include "graphics/primitivetype.hpp"
%include "graphics/rect.hpp"
%include "graphics/color.hpp"
%include "graphics/font.hpp"
%include "graphics/gltexture.hpp"
%include "graphics/renderwindow.hpp"
%include "graphics/spritevertexdata.hpp"
%include "graphics/surface.hpp"
%include "graphics/renderstates.hpp"
%include "graphics/transform.hpp"
%include "graphics/transformable.hpp"
%include "graphics/vertex.hpp"
%include "graphics/indexarray.hpp"
%include "graphics/vertexarray.hpp"
%include "graphics/tilevertexarray.hpp"
%include "graphics/indexvertexarray.hpp"
%include "graphics/vertexdata.hpp"
%include "graphics/vertexbufferobject.hpp"
%include "graphics/view.hpp"
%include "graphics_extra.hpp"
