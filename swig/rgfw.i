%module rGFW

%{
#include "system.hpp"
#include "window.hpp"
#include "graphics.hpp"
#include "extra_include/window.hpp"
#include "extra_include/graphics.hpp"
%}

%include "stdint.i"
%include "std_string.i"


typedef int8_t Sint8;
typedef uint8_t Uint8;

typedef int16_t Sint16;
typedef uint16_t Uint16;

typedef int32_t Sint32;
typedef uint32_t Uint32;

typedef int64_t Sint64;
typedef uint64_t Uint64;


%ignore toSDL;
%ignore get;


%include "swig_include/system.hpp"
%include "swig_include/window.hpp"
%include "swig_include/graphics.hpp"