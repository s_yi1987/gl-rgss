# GL RGSS

A Work-in-progress open source implementation of the RPG Maker VX Ace game engine. Progress on this project has been halted because Enterbrain's later iterations of the RPG Maker engines (such as MV) already have taken care of the problems of the VX Ace engine I set out to fix with this project.

## Features

- Better platform portability
- Hardware accelerated graphics using OpenGL
- Additional native bindings that enable access to more low-level functionality, which in turn greatly extends the engine’s scripting capabilities
