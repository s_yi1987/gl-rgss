#include "system/exception.hpp"


namespace rgk
{

Exception::Exception(const std::string& message) : mMessage(message) {}

const std::string& Exception::getMessage() const
{
    return mMessage;
}

}