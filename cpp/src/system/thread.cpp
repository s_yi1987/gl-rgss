#include "system/thread.hpp"
#include "system/exception.hpp"


namespace rgk
{
    
    
Thread::Thread(int(*function)(void*), void* data) :
mThreadSDL(NULL),
mFunction(function),
mName(""),
mData(data)
{

}
    
Thread::Thread(int (*function)(void*), const std::string& name, void* data) :
mThreadSDL(NULL),
mFunction(function),
mName(name),
mData(data)
{

}


Thread::Thread(const Thread& other) :
mThreadSDL(NULL),
mFunction(other.mFunction),
mName(other.mName),
mData(other.mData)
{
    
}


Thread& Thread::operator =(const Thread& other)
{
    if(mThreadSDL)
        detach();
    
    mFunction = other.mFunction;
    mName = other.mName;
    mData = other.mData;
    
    return *this;
}


Thread::~Thread()
{
    detach();
}


void Thread::launch()
{
    if(mThreadSDL)
        return;
    
    const char* name = mName.empty() ? NULL : mName.c_str();
    mThreadSDL = SDL_CreateThread(mFunction, name, mData);
    if(!mThreadSDL)
        throw Exception(std::string("Failed to create Thread: ") +=
            SDL_GetError());
}


int Thread::wait()
{
    if(!mThreadSDL)
        return -1;
    
    int result;
    SDL_WaitThread(mThreadSDL, &result);
    mThreadSDL = NULL;
    
    return result;
}


void Thread::detach()
{
    if(!mThreadSDL)
        return;
    
    SDL_DetachThread(mThreadSDL);
    mThreadSDL = NULL;
}


bool Thread::hasLaunched() const
{
    return mThreadSDL != NULL;
}

    
}
