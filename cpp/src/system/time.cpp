#include "system/time.hpp"


namespace rgk
{
    
const Time Time::ZERO;


Time::Time() :
mMicroseconds(0)
{
    
}


float Time::asSeconds() const
{
    return mMicroseconds / 1000000.f;
}


Sint32 Time::asMilliseconds() const
{
    return static_cast<Sint32>(mMicroseconds / 1000);
}


Sint64 Time::asMicroseconds() const
{
    return mMicroseconds;
}


Time::Time(Sint64 microseconds) :
mMicroseconds(microseconds)
{
    
}


Time seconds(float amount)
{
    return Time(static_cast<Sint64>(amount * 1000000));
}


Time milliseconds(Sint32 amount)
{
    return Time(static_cast<Sint64>(amount) * 1000);
}


Time microseconds(Sint64 amount)
{
    return Time(amount);
}


bool operator ==(Time left, Time right)
{
    return left.asMicroseconds() == right.asMicroseconds();
}


bool operator !=(Time left, Time right)
{
    return left.asMicroseconds() != right.asMicroseconds();
}


bool operator <(Time left, Time right)
{
    return left.asMicroseconds() < right.asMicroseconds();
}


bool operator >(Time left, Time right)
{
    return left.asMicroseconds() > right.asMicroseconds();
}


bool operator <=(Time left, Time right)
{
    return left.asMicroseconds() <= right.asMicroseconds();
}


bool operator >=(Time left, Time right)
{
    return left.asMicroseconds() >= right.asMicroseconds();
}


Time operator -(Time right)
{
    return microseconds(-right.asMicroseconds());
}


Time operator +(Time left, Time right)
{
    return microseconds(left.asMicroseconds() + right.asMicroseconds());
}


Time& operator +=(Time& left, Time right)
{
    return left = left + right;
}


Time operator -(Time left, Time right)
{
    return microseconds(left.asMicroseconds() - right.asMicroseconds());
}


Time& operator -=(Time& left, Time right)
{
    return left = left - right;
}


Time operator *(Time left, float right)
{
    return seconds(left.asSeconds() * right);
}


Time operator *(Time left, Sint64 right)
{
    return microseconds(left.asMicroseconds() * right);
}


Time operator *(float left, Time right)
{
    return right * left;
}


Time operator *(Sint64 left, Time right)
{
    return right * left;
}


Time& operator *=(Time& left, float right)
{
    return left = left * right;
}


Time& operator *=(Time& left, Sint64 right)
{
    return left = left * right;
}


Time operator /(Time left, float right)
{
    return seconds(left.asSeconds() / right);
}


Time operator /(Time left, Sint64 right)
{
    return microseconds(left.asMicroseconds() / right);
}


Time& operator /=(Time& left, float right)
{
    return left = left / right;
}


Time& operator /=(Time& left, Sint64 right)
{
    return left = left / right;
}


}