#include "system/libmain.hpp"
#include "system/exception.hpp"
#include "system/lock.hpp"
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <al.h>
#include <alc.h>


namespace rgk
{
Mutex LibMain::sMutex;
bool LibMain::sInstantiated = false;


LibMain::LibMain() :
mInitialized(false),
mAudioDevice(NULL),
mAudioContext(NULL)
{
    Lock lock(sMutex);
    if(sInstantiated)
        throw Exception("An instance of LibMain already exists");
    
    sInstantiated = true;
}


LibMain::~LibMain()
{
    close();
    
    Lock lock(sMutex);
    sInstantiated = false;
}


bool LibMain::init()
{
    if(mInitialized)
        return true;
    
    mInitialized = true;
    if(SDL_Init(0) != 0)
    {
        printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
        mInitialized = false;
        return false;
    }
    
    bool success = true;
    if(!initVideo())
        success = false;
    if(success && !initImage())
        success = false;
    if(success && !initFont())
        success = false;
    if(success && !initAudio())
        success = false;
    
    if(!success)
        close();
    else
        mInitialized = success;
    
    return success;
}


bool LibMain::initVideo()
{
    if(SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
    {
        printf("SDL Video could not initialize! SDL Error: %s\n", SDL_GetError());
        return false;
    }
    
    setGLAtrributes();
    
    return true;
}


void LibMain::setGLAtrributes()
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
}


bool LibMain::initImage()
{
    int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if(!(IMG_Init(imageFlags) & imageFlags))
    {
        printf("SDL Image could not initialize! SDL Error: %s\n", IMG_GetError());
        return false;
    }
    
    return true;
}


bool LibMain::initFont()
{
    if(TTF_Init() == -1)
    {
        printf("TTF could not initialize! SDL Error: %s\n", TTF_GetError());
        return false;
    }
    
    return true;
}


bool LibMain::initAudio()
{
    mAudioDevice = alcOpenDevice(NULL);
    if(!mAudioDevice)
    {
        //SDL_SetError("Failed to initialize an OpenAL Device");
        printf("Failed to initialize an OpenAL Device");
        return false;
    }
    
    mAudioContext = alcCreateContext(mAudioDevice, NULL);
    if(!mAudioContext)
    {
        //SDL_SetError("Failed to initialize an OpenAL Context");
        printf("Failed to initialize an OpenAL Context");
        return false;
    }
    
    alcMakeContextCurrent(mAudioContext);
    return true;
}


std::string LibMain::getLastError() const
{
    return std::string(SDL_GetError());
}


std::string LibMain::getLastFontError() const
{
    return std::string(TTF_GetError());
}


Uint32 LibMain::getTicks() const
{
    return SDL_GetTicks();
}


Uint64 LibMain::getPerformanceCounter() const
{
    return SDL_GetPerformanceCounter();
}


Uint64 LibMain::getPerformanceFrequency() const
{
    return SDL_GetPerformanceFrequency();
}


void LibMain::delay(Uint32 ms) const
{
    SDL_Delay(ms);
}


bool LibMain::isInitialized() const
{
    return mInitialized;
}


void LibMain::close()
{
    if(!mInitialized)
        return;
    
    IMG_Quit();
    TTF_Quit();
    SDL_Quit();
    
    if(mAudioContext)
    {
        alcMakeContextCurrent(NULL);
        alcDestroyContext(mAudioContext);
        mAudioContext = NULL;
    }
    
    if(mAudioDevice)
    {
        alcCloseDevice(mAudioDevice);
        mAudioDevice = NULL;
    }

    mInitialized = false;
}

}