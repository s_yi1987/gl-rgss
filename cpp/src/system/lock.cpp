#include "system/lock.hpp"
#include "system/mutex.hpp"

namespace rgk
{

Lock::Lock(Mutex& mutex) :
mMutex(mutex)
{
    mMutex.lock();
}


Lock::~Lock()
{
    mMutex.unlock();
}

}
