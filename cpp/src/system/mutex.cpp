#include "system/mutex.hpp"
#include "system/exception.hpp"


namespace rgk
{

Mutex::Mutex() :
mMutexSDL(NULL)
{
    mMutexSDL = SDL_CreateMutex();
    if(!mMutexSDL)
        throw Exception(std::string("Failed to create Mutex: ") += SDL_GetError());
}


Mutex::~Mutex()
{
    SDL_DestroyMutex(mMutexSDL);
}


void Mutex::lock()
{
    SDL_LockMutex(mMutexSDL);
}


void Mutex::unlock()
{
    SDL_UnlockMutex(mMutexSDL);
}

}