#include "system/util.hpp"
#include <cstdio>
#include <SDL2/SDL.h>
#include <gl/glew.h>
#include <SDL2/SDL_opengl.h>
#include <al.h>
#include <alc.h>


namespace rgk
{

int util::ensureWithinRange(int val, int min, int max)
{
    if(val < min)
        return min;
    else if(val > max)
        return max;
    
    return val;
}


int util::ensureWithinColorRange(int val)
{
    return ensureWithinRange(val, 0, 255);
}


double util::ensureWithinRange(double val, double min, double max)
{
    if(val < min)
        return min;
    else if(val > max)
        return max;
    
    return val;
}


double util::ensureWithinColorRange(double val)
{
    return ensureWithinRange(val, 0.0, 255.0);
}


SDL_Surface* util::ensurePixelFormatValue(SDL_Surface* surfaceSDL, Uint32 format)
{
    if(surfaceSDL->format->format == format)
        return surfaceSDL;
    
    SDL_Surface* convertedSurfaceSDL =
        SDL_ConvertSurfaceFormat(surfaceSDL, format, 0);
    
    SDL_FreeSurface(surfaceSDL);
    if(!convertedSurfaceSDL)
        throw Exception(std::string("Error converting Surface format: ")
            += SDL_GetError());
    
    return convertedSurfaceSDL;
}


void util::setPixel(void* pixels, int x, int y, int bpp, int pitch, Uint32 pixel)
{
    Uint8* p = (Uint8*)pixels + y * pitch + x * bpp;

    switch(bpp)
    {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16*)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
        {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        }
        else
        {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32*)p = pixel;
        break;
    }
}


Uint32 util::getPixel(void* pixels, int x, int y, int bpp, int pitch)
{
    Uint8 *p = (Uint8*)pixels + y * pitch + x * bpp;

    switch(bpp)
    {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16*)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32*)p;
        break;

    default:
        return 0;
    }
}


void util::transferPixels(void* srcPixels, const Rect& srcRect, void* dstPixels,
                          const Rect& dstRect, int bpp, int pitch)
{
    transferPixels(srcPixels, srcRect, bpp, pitch, dstPixels, dstRect, bpp, pitch);
}


void util::transferPixels(void* srcPixels, const Rect& srcRect, int srcBpp,
                          int srcPitch, void* dstPixels, const Rect& dstRect,
                          int dstBpp, int dstPitch)
{
    for(int x = 0; x < srcRect.width; x++)
    {
        for(int y = 0; y < srcRect.height; y++)
        {
            Uint32 pixel = util::getPixel(srcPixels, x + srcRect.x, y + srcRect.y,
            srcBpp, srcPitch);
            util::setPixel(dstPixels, x + dstRect.x, y + dstRect.y, dstBpp,
            dstPitch, pixel);
        }
    }
}


bool util::sameRectPos(const Rect& rect, const Rect& rect2)
{
    return rect.x == rect2.x && rect.y == rect2.y;
}


bool util::sameRectSize(const Rect& rect, const Rect& rect2)
{
    return rect.width == rect2.width && rect.height == rect2.height;
}


bool util::sameRect(const Rect& rect, const Rect& rect2)
{
    return sameRectSize(rect, rect2) && sameRectPos(rect, rect2);
}


void util::ensureGlewInit()
{
    GLenum glewError = glewInit();
    if(glewError != GLEW_OK)
    {
        const char* err = reinterpret_cast<const char*>(glewGetErrorString(glewError));
        throw Exception(std::string("Failed to initialize GLEW: ") += err);
    }
}


void util::verifyGlContext(bool throwExceptionIfFail)
{
    std::string msg = "No active GL Context detected";
    verifyGlContext(msg, throwExceptionIfFail);
}


void util::verifyGlContext(const std::string& failMsg, bool throwExceptionIfFail)
{
    if(!util::hasActiveGlContext())
    {
        if(throwExceptionIfFail)
            throw Exception(failMsg);
        else
            std::puts(failMsg.c_str());
    }
}


bool util::hasActiveGlContext()
{
    return SDL_GL_GetCurrentContext() ? true : false;
}


void util::verifyAlContext(bool throwExceptionIfFail)
{
    std::string msg = "No active AL Context detected";
    verifyAlContext(msg, throwExceptionIfFail);
}


void util::verifyAlContext(const std::string& failMsg, bool throwExceptionIfFail)
{
    if(!util::hasActiveAlContext())
    {
        if(throwExceptionIfFail)
            throw Exception(failMsg);
        else
            std::puts(failMsg.c_str());
    }
}


bool util::hasActiveAlContext()
{
    return alcGetCurrentContext() ? true : false;
}


int util::getFormatFromChannelCount(unsigned int channelCount)
{
    verifyAlContext();
    
    // Find the good format according to the number of channels
    int format = 0;
    switch (channelCount)
    {
        case 1  : format = AL_FORMAT_MONO16;                    break;
        case 2  : format = AL_FORMAT_STEREO16;                  break;
        case 4  : format = alGetEnumValue("AL_FORMAT_QUAD16");  break;
        case 6  : format = alGetEnumValue("AL_FORMAT_51CHN16"); break;
        case 7  : format = alGetEnumValue("AL_FORMAT_61CHN16"); break;
        case 8  : format = alGetEnumValue("AL_FORMAT_71CHN16"); break;
        default : format = 0;                                   break;
    }

    // Fixes a bug on OS X
    if (format == -1)
        format = 0;

    return format;
}


int util::glEnumFromDataType(DataType type)
{
    GLenum glType = GL_UNSIGNED_BYTE;
    
    switch(type)
    {
        case BYTE :
            glType = GL_BYTE;
            break;

        case UNSIGNED_SHORT :
            glType = GL_UNSIGNED_SHORT;
            break;
            
        case SHORT :
            glType = GL_SHORT;
            break;
            
        case UNSIGNED_INT:
            glType = GL_UNSIGNED_INT;
            break;
            
        case FLOAT:
            glType = GL_FLOAT;
            break;
            
        case INT:
            glType = GL_INT;
            break;

        case UNSIGNED_BYTE :
            break;
            
        default :
            throw std::puts("Unsupported DataType was passed as an argument");
            break;
    }
    
    return static_cast<int>(glType);
}

}