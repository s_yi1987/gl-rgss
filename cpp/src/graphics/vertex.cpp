#include "graphics/vertex.hpp"


namespace rgk
{

Vertex::Vertex() :
position (0, 0),
color    (255, 255, 255, 255),
texCoords(0, 0)
{
    
}


////////////////////////////////////////////////////////////
Vertex::Vertex(const Vector2f& thePosition) :
position (thePosition),
color    (255, 255, 255, 255),
texCoords(0, 0)
{
    
}


////////////////////////////////////////////////////////////
Vertex::Vertex(const Vector2f& thePosition, const PlainColor& theColor) :
position (thePosition),
color    (theColor),
texCoords(0, 0)
{
    
}


////////////////////////////////////////////////////////////
Vertex::Vertex(const Vector2f& thePosition, const Vector2f& theTexCoords) :
position (thePosition),
color    (255, 255, 255, 255),
texCoords(theTexCoords)
{
    
}


////////////////////////////////////////////////////////////
Vertex::Vertex(const Vector2f& thePosition, const PlainColor& theColor, const Vector2f& theTexCoords) :
position (thePosition),
color    (theColor),
texCoords(theTexCoords)
{
    
}


}
