#include "graphics/renderwindow.hpp"
#include "graphics/rendertargetmode.hpp"
#include "system/exception.hpp"
#include "graphics/transformable.hpp"
#include "graphics/gltexture.hpp"
#include "graphics/shader.hpp"
#include "graphics/vertexdata.hpp"
#include "graphics/vertexbufferobject.hpp"
#include "system/lock.hpp"
#include "system/util.hpp"
#include <gl/glew.h>
#include <SDL2/SDL_opengl.h>
#include <stdio.h>


namespace rgk
{
RenderWindow::RenderWindow(const std::string& title, int x, int y,
                           int width, int height, Uint32 flags) :
Window(title, x, y, width, height, flags | SDL_WINDOW_OPENGL),
mContextSDL(NULL),
mCache(),
mView(),
mDefaultView(),
mClipRect(),
mLastSize(width, height),
mRenderTarget(NULL),
mCurrentTargetMode(NULL),
mTargetModeNone(NULL),
mTargetModeTexture(NULL),
mVertexBufferObject(NULL),
mShader(NULL)
{
    mContextSDL = createContext(get());
    
    Rect size = getSize();
    mDefaultView.reset(FloatRect(size));
    mView = mDefaultView;
    resetGLStates();
    setupTargetModes();
    mCache.vertexCacheVBO =
        new VertexBufferObject(StatesCache::VertexCacheSize,
                               StatesCache::IndexCacheSize,
                               VBO_DYNAMIC_DRAW);
}


SDL_GLContext RenderWindow::createContext(SDL_Window* windowSDL)
{
    if(!windowSDL)
        throw Exception(std::string("Failed to create Context: Window was NULL"));
    
    SDL_GLContext contextSDL = SDL_GL_CreateContext(windowSDL);
    if(!contextSDL)
        throw Exception(std::string("Failed to create Context: ") 
            += SDL_GetError());
    
    return contextSDL;
}


RenderWindow::~RenderWindow()
{
    dispose(false);
}


void RenderWindow::dispose()
{
    dispose(true);
}


void RenderWindow::dispose(bool disposeWindow)
{
    if(isDisposed())
        return;
    
    if(!setActive())
        printf("~RenderWindow() - Failed to activate GL context: %s\n", SDL_GetError());
    
    mShader = NULL;
    mVertexBufferObject = NULL;

    mCurrentTargetMode = NULL;

    delete mTargetModeNone;
    mTargetModeNone = NULL;
    
    delete mTargetModeTexture;
    mTargetModeTexture = NULL;
    
    delete mCache.vertexCacheVBO;
    mCache.vertexCacheVBO = NULL;
    
    mCache.glStatesSet = false;
    
    if(mContextSDL)
    {
        SDL_GL_DeleteContext(mContextSDL);
        mContextSDL = NULL;
    }

    if(disposeWindow)
        Window::dispose();
}


void RenderWindow::bindVBO(const VertexBufferObject* vbo)
{
    if(vbo && vbo->isDisposed())
        vbo = NULL;
    if(!setActive() || (vbo == mCache.vertexCacheVBO && mVertexBufferObject == vbo) ||
       (!vbo && !mVertexBufferObject))
        return;

    applyVBO(vbo);
}


void RenderWindow::updateCurrentVBO(const VertexData& vertexData,
                                      unsigned int vertexBufferOffset,
                                      unsigned int indexBufferOffset)
{
    updateCurrentVertexBuffer(vertexData.getVertices(),
                              vertexData.getVertexCount(),
                              vertexBufferOffset);
    
    updateCurrentIndexBuffer(vertexData.getIndices(),                       
                             vertexData.getIndexCount(),
                             indexBufferOffset);
}


void RenderWindow::updateCurrentVertexBuffer(const Vertex* vertices, unsigned int count,
                                                 unsigned int offset)
{
    if(!mVertexBufferObject || mVertexBufferObject->isDisposed() || !vertices ||
       !count || count > mVertexBufferObject->getVertexCapacity() || !setActive())
        return;
    VertexBufferObject::updateCurrentVertexBuffer(vertices, count, offset);
}


void RenderWindow::updateCurrentIndexBuffer(const unsigned int* indices, unsigned int count,
                                            unsigned int offset)
{
    if(!mVertexBufferObject || !mVertexBufferObject->getIndexBufferID() || !indices ||
       !count || count > mVertexBufferObject->getIndexCapacity() || !setActive())
        return;
    VertexBufferObject::updateCurrentIndexBuffer(indices, count, offset);
}


void RenderWindow::bindShader(const Shader* shader, bool bindTextures)
{
    if(shader && shader->isDisposed())
        shader = NULL;
    
    applyShader(shader, bindTextures);
}


void RenderWindow::setCurrentShaderParameter(const std::string& name, float x)
{
    if(!mShader)
        return;
    
    int location = mShader->getParamLocation(name);
    Shader::setParameterCurrent(location, x);
}


void RenderWindow::setCurrentShaderParameter(const std::string& name, float x, float y)
{
    if(!mShader)
        return;
    
    int location = mShader->getParamLocation(name);
    Shader::setParameterCurrent(location, x, y);
}


void RenderWindow::setCurrentShaderParameter(const std::string& name, float x, float y, float z)
{
    if(!mShader)
        return;
    
    int location = mShader->getParamLocation(name);
    Shader::setParameterCurrent(location, x, y, z);
}


void RenderWindow::setCurrentShaderParameter(const std::string& name, float x, float y, float z, float w)
{
    if(!mShader)
        return;
    
    int location = mShader->getParamLocation(name);
    Shader::setParameterCurrent(location, x, y, z, w);
}


void RenderWindow::setCurrentShaderParameter(const std::string& name, const Transform& transform)
{
    if(!mShader)
        return;
    
    int location = mShader->getParamLocation(name);
    Shader::setParameterCurrent(location, transform);
}


void RenderWindow::bindCurrentShaderTextures()
{
    if(!mShader)
        return;
    
    mShader->bindTextures();
}


void RenderWindow::draw(const VertexData& vertexData, const RenderStates& states)
{
    const Vertex* vertices   = vertexData.getVertices();
    unsigned int vertexCount = vertexData.getVertexCount();
    
    const unsigned int* indices = vertexData.getIndices();
    unsigned int indexCount     = vertexData.getIndexCount();
    
    BlendMode blendMode        = states.blendMode;
    const Transform& transform = states.transform;
    const Texture* texture     = states.texture;
    
    if(!vertices || vertexCount == 0 || !setActive() ||
       (mRenderTarget && mRenderTarget->isDisposed()))
        return;
    
    bool useVertexCache = (vertexCount <= StatesCache::VertexCacheSize);
    if(useVertexCache && indices)
        useVertexCache = (indexCount <= StatesCache::IndexCacheSize);
    
    if(useVertexCache)
    {
        for(unsigned int i = 0; i < vertexCount; ++i)
        {
            Vertex& vertex = mCache.vertexCache[i];
            vertex.position = transform * vertices[i].position;
            vertex.color = vertices[i].color;
            vertex.texCoords = vertices[i].texCoords;
        }
        
        bindVBO(mCache.vertexCacheVBO);
        mCache.vertexCacheVBO->setPrimitiveType(vertexData.getPrimitiveType());
        
        mCache.vertexCacheVBO->setVertexReadSize(vertexCount);
        updateCurrentVertexBuffer(mCache.vertexCache, vertexCount);
        
        mCache.vertexCacheVBO->setIndexReadSize(0);
        if(indices)
        {
            mCache.vertexCacheVBO->setIndexReadSize(indexCount);
            updateCurrentIndexBuffer(indices, indexCount);
        }
        
        draw(RenderStates(blendMode, Transform::Identity, texture, states.shader));
    }
    
    else
    {
        throw Exception("Can only draw client side vertex data with 4 or less vertices");
    }
}


void RenderWindow::draw(VertexBufferObject& vertexBufferObj, const RenderStates& states)
{
    mCache.usingVertexCache = false;
    bindVBO(&vertexBufferObj);
    draw(states);
}


void RenderWindow::draw(const RenderStates& states)
{
    if(!mVertexBufferObject || mVertexBufferObject->isDisposed() || !setActive() ||
       (mRenderTarget && mRenderTarget->isDisposed()))
        return;
    
    BlendMode blendMode        = states.blendMode;
    const Transform& transform = states.transform;
    const Texture* texture     = states.texture;
    
    const Transform* transformPtr = &transform;
    bool canOptimize = (mCache.usingVertexCache && mVertexBufferObject == mCache.vertexCacheVBO);
    if(canOptimize)
        transformPtr = NULL;
    
    if(states.skipShader)
        applyStates(transformPtr, blendMode, texture);
    else
        applyStates(transformPtr, blendMode, texture, states.shader);
    
    unsigned int count = mVertexBufferObject->getVertexReadSize();
    bool useIndices = false;
    if(mVertexBufferObject->getIndexReadSize() > 0)
    {
        count = mVertexBufferObject->getIndexReadSize();
        useIndices = true;
    }
    
    bool setPointers = !canOptimize;
    drawVertices(setPointers, count, useIndices,
        mVertexBufferObject->getPrimitiveType());
    
    mCache.usingVertexCache = (mVertexBufferObject == mCache.vertexCacheVBO);
}


void RenderWindow::drawVertices(bool setPointers, unsigned int vertexCount, 
                                bool useIndices, PrimitiveType type)
{
    if(mShader)
        Shader::enableShaderAttribsCurrent(mShader->mAttribs);
    
    if(setPointers)
    {
        const char* data = static_cast<char*>(0);
        glVertexPointer(2, GL_FLOAT, sizeof(Vertex), data + 0);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Vertex), data + 8);
        glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), data + 12);
    }
    
    static const GLenum modes[] = {GL_POINTS, GL_LINES, GL_LINE_STRIP, GL_TRIANGLES,
                                   GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_QUADS};
    GLenum mode = modes[type];
    if(!useIndices)
        glDrawArrays(mode, 0, static_cast<GLsizei>(vertexCount));
    else
        glDrawElements(mode, static_cast<GLsizei>(vertexCount), GL_UNSIGNED_INT, NULL);
    
    if(mShader)
        Shader::disableShaderAttribsCurrent(mShader->mAttribs);
}


void RenderWindow::applyStates(const Transform* transform, BlendMode blendMode,
                                const Texture* texture)
{
    const Rect& size = getSize();
    if(size.width != mLastSize.x || size.height != mLastSize.y)
    {
        mLastSize.x = size.width;
        mLastSize.y = size.height;
        mCache.viewChanged = true;
    }
    
    if(!mCache.glStatesSet)
        resetGLStates();
    
    if(transform)
        applyTransform(*transform);
    
    if(mCache.viewChanged)
        applyCurrentView();
    
    if(mCache.lastBlendMode != blendMode)
        applyBlendMode(blendMode);
    
    if(mCache.clipRectChanged)
        applyCurrentClipRect();
    
    if(mCache.lastTextureCacheID != (texture && !texture->isDisposed() ? texture->getCacheID() : 0))
        applyTexture(texture);
}


void RenderWindow::applyStates(const Transform* transform, BlendMode blendMode,
                                const Texture* texture, const Shader* shader)
{
    applyStates(transform, blendMode, texture);
    
    if(shader && shader->isDisposed())
        shader = NULL;
    applyShader(shader, true);
}


void RenderWindow::clear(const Color& color)
{
    if(!setActive())
        return;
    
    if(mCache.clipRectChanged)
        applyCurrentClipRect();
    applyTexture(NULL);
    
    glClearColor(color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
    glClear(GL_COLOR_BUFFER_BIT);
}


void RenderWindow::present()
{
    if(!setActive())
        return;
    
    SDL_GL_SwapWindow(get());
}


void RenderWindow::setVerticalSyncEnabled(bool enabled)
{
    if(!setActive())
        return;
    
    SDL_GL_SetSwapInterval(enabled ? 1 : 0);
}


void RenderWindow::setView(const View& view)
{
    mView = view;
    mCache.viewChanged = true;
}


void RenderWindow::setDefaultView(const View& view)
{
    mDefaultView = view;
}


const View& RenderWindow::getView() const
{
    return mView;
}


const View& RenderWindow::getDefaultView() const
{
    return mDefaultView;
}


Rect RenderWindow::getViewport(const View& view) const
{
    if(isDisposed())
        return Rect(0, 0, 0, 0);
    
    return mCurrentTargetMode->getViewport(view);
}


void RenderWindow::setClipRect(const Rect* rect)
{
    if(isDisposed())
        return;
    
    bool updateClipRect = true;
    
    if(rect && !rect->isEmpty())
        mClipRect = *rect;
    else if(!mClipRect.isEmpty())
        mClipRect.empty();
    else
        updateClipRect = false;
    
    if(updateClipRect)
        mCache.clipRectChanged = true;
}


Rect RenderWindow::getClipRect() const
{
    return mClipRect;
}


void RenderWindow::setRenderTarget(Texture* texture, bool adjustView)
{
    if(isDisposed())
        return;
    
    if(!texture || texture->isDisposed())
    {
        if(mRenderTarget)
        {
            mTargetModeTexture->setRenderTarget(NULL);
            mRenderTarget = NULL;
            mCurrentTargetMode = mTargetModeNone;
            
            if(adjustView)
                setView(mDefaultView);
            else
                setView(mView);
            
            setClipRect(&mClipRect);
        }
    }
    
    else
    {
        bool hadRenderTarget = mRenderTarget ? true : false;
        mTargetModeTexture->setRenderTarget(texture);
        mRenderTarget = texture;
        mCurrentTargetMode = mTargetModeTexture;
        
        if(adjustView)
            setView(View(FloatRect(mRenderTarget->getRect())));
        else if(!hadRenderTarget)
            setView(mView);
        
        setClipRect(&mClipRect);
    }
}


Texture* RenderWindow::getRenderTarget()
{
    return mRenderTarget;
}


bool RenderWindow::setActive(bool active)
{
    if(isDisposed())
        return false;
    
    int result;
    if(!active)
    {
        if(!isActive())
            return true;
        result = SDL_GL_MakeCurrent(get(), NULL);
    }
    else
        result = SDL_GL_MakeCurrent(get(), mContextSDL);
    
    return result == 0;
}


bool RenderWindow::isActive() const
{
    if(isDisposed())
        return false;
    
    return SDL_GL_GetCurrentWindow()  != get() ||
           SDL_GL_GetCurrentContext() != mContextSDL;
}


void RenderWindow::applyTransform(const Transform& transform)
{
    glLoadMatrixf(transform.getMatrix());
}


void RenderWindow::applyTexture(const Texture* texture)
{
    Texture::bind(texture);
    mCache.lastTextureCacheID = texture && !texture->isDisposed() ? texture->getCacheID() : 0;
}


void RenderWindow::applyShader(const Shader* shader, bool bindTextures)
{
    if(!Shader::isAvailable())
        return;
    
    if(shader)
        Shader::bind(*shader, bindTextures);
    else
        Shader::bind(shader);
    
    mShader = shader;
}


void RenderWindow::applyBlendMode(BlendMode mode)
{
    if(GLEW_EXT_blend_func_separate)
    {
        glBlendFuncSeparateEXT(
                BlendMode::factorToGlConstant(mode.colorSrcFactor),
                BlendMode::factorToGlConstant(mode.colorDstFactor),
                BlendMode::factorToGlConstant(mode.alphaSrcFactor),
                BlendMode::factorToGlConstant(mode.alphaDstFactor));
    }
    
    else
    {
        glBlendFunc(
                BlendMode::factorToGlConstant(mode.colorSrcFactor), 
                BlendMode::factorToGlConstant(mode.colorDstFactor));
    }
    
    if (GLEW_EXT_blend_minmax && GLEW_EXT_blend_subtract)
    {
        if (GLEW_EXT_blend_equation_separate)
        {
            glBlendEquationSeparateEXT(
                BlendMode::equationToGlConstant(mode.colorEquation),
                BlendMode::equationToGlConstant(mode.alphaEquation));
        }
        
        else
        {
            glBlendEquationEXT(BlendMode::equationToGlConstant(mode.colorEquation));
        }
    }
    
    mCache.lastBlendMode = mode;
}


void RenderWindow::applyCurrentView()
{
    mCurrentTargetMode->applyView(mView);
    mCache.viewChanged = false;
}


void RenderWindow::applyCurrentClipRect()
{
    mCurrentTargetMode->setClipRect(&mClipRect);
    mCache.clipRectChanged = false;
}


void RenderWindow::applyVBO(const VertexBufferObject* vbo)
{
    VertexBufferObject::bind(vbo);
    mVertexBufferObject = vbo;
}


void RenderWindow::resetGLStates()
{
    if(!setActive())
        throw Exception("Failed to activate GL context when resetting states");
    util::ensureGlewInit();
    
    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    
    glMatrixMode(GL_MODELVIEW);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    applyBlendMode(BLEND_ALPHA);
    applyTransform(Transform::Identity);
    applyTexture(NULL);
    applyVBO(NULL);
    
    mCache.usingVertexCache = false;
    setView(mView);
    
    mCache.glStatesSet = true;
}


void RenderWindow::setupTargetModes()
{
    mTargetModeNone    = new RenderTargetModeNone(*this);
    mTargetModeTexture = new RenderTargetModeTexture(*this);
    
    mCurrentTargetMode = mTargetModeNone;
}


}