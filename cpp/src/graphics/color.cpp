#include "graphics/color.hpp"
#include "system/util.hpp"
#include "system/exception.hpp"
#include "graphics/pixelformat.hpp"


namespace rgk
{

Color::Color() :
r(0),
g(0),
b(0),
a(0)
{
    
}


Color::Color(double red, double green, double blue, double alpha) :
r(util::ensureWithinColorRange(red)),
g(util::ensureWithinColorRange(green)),
b(util::ensureWithinColorRange(blue)),
a(util::ensureWithinColorRange(alpha))
{

}


Color::Color(const PlainColor& plainColor) :
r(util::ensureWithinColorRange(plainColor.x)),
g(util::ensureWithinColorRange(plainColor.y)),
b(util::ensureWithinColorRange(plainColor.z)),
a(util::ensureWithinColorRange(plainColor.w))
{
    
}


Color::Color(Uint32 pixel, const PixelFormat& pixelFormat)
{
    set(pixel, pixelFormat);
}


Color::Color(Uint32 pixel, Uint32 pixelFormatValue)
{
    set(pixel, PixelFormat(pixelFormatValue));
}


void Color::set(const Color& other)
{
    set(other.r, other.g, other.b, other.a);
}


void Color::set(const PlainColor& plainColor)
{
    set(plainColor.x, plainColor.y, plainColor.z, plainColor.w);
}



void Color::set(double red, double green, double blue, double alpha)
{
    r = util::ensureWithinColorRange(red);
    g = util::ensureWithinColorRange(green);
    b = util::ensureWithinColorRange(blue);
    a = util::ensureWithinColorRange(alpha);
}


void Color::set(Uint32 pixel, Uint32 pixelFormatValue)
{
    set(pixel, PixelFormat(pixelFormatValue));
}


void Color::set(Uint32 pixel, const PixelFormat& pixelFormat)
{
    Uint8 u8_r = 0, u8_g = 0, u8_b = 0, u8_a = 0;
    
    SDL_PixelFormat* pixelFormatSDL = pixelFormat.get();
    if(!pixelFormatSDL)
        throw Exception("Cannot get RGBA values from NULL PixelFormat!");
    
    SDL_GetRGBA(pixel, pixelFormatSDL, &u8_r, &u8_g, &u8_b, &u8_a);
    r = u8_r, g = u8_g, b = u8_b, a = u8_a;
}


Uint32 Color::toMapped(Uint32 pixelFormatValue) const
{
    return toMapped(PixelFormat(pixelFormatValue));
}


Uint32 Color::toMapped(const PixelFormat& pixelFormat) const
{
    SDL_PixelFormat* pixelFormatSDL = pixelFormat.get();
    if(!pixelFormatSDL)
        throw Exception("Cannot map RGBA values from NULL PixelFormat!");
    
    return SDL_MapRGBA(pixelFormatSDL,
                       static_cast<Uint8>(util::ensureWithinColorRange(r)),
                       static_cast<Uint8>(util::ensureWithinColorRange(g)),
                       static_cast<Uint8>(util::ensureWithinColorRange(b)),
                       static_cast<Uint8>(util::ensureWithinColorRange(a)));
}


PlainColor Color::toPlain() const
{
    PlainColor colorPlain;
    colorPlain.x = static_cast<Uint8>(util::ensureWithinColorRange(r));
    colorPlain.y = static_cast<Uint8>(util::ensureWithinColorRange(g));
    colorPlain.z = static_cast<Uint8>(util::ensureWithinColorRange(b));
    colorPlain.w = static_cast<Uint8>(util::ensureWithinColorRange(a));
    
    return colorPlain;
}


SDL_Color Color::toSDL() const
{
    SDL_Color colorSDL;
    colorSDL.r = static_cast<Uint8>(util::ensureWithinColorRange(r));
    colorSDL.g = static_cast<Uint8>(util::ensureWithinColorRange(g));
    colorSDL.b = static_cast<Uint8>(util::ensureWithinColorRange(b));
    colorSDL.a = static_cast<Uint8>(util::ensureWithinColorRange(a));
    
    return colorSDL;
}

}