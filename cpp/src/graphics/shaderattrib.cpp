#include "graphics/shaderattrib.hpp"
#include "graphics/vertexbufferobjsaver.hpp"


namespace rgk
{
    
ShaderAttrib::ShaderAttrib() :
mElementSize(0),
mArrayBuffer()
{

}


ShaderAttrib::ShaderAttrib(unsigned int elementCapacity, unsigned int elementSize, VertexBufferMode mode) :
mElementSize(elementSize),
mArrayBuffer(elementCapacity * elementSize, mode)
{
    
}


ShaderAttrib::ShaderAttrib(const void* data, unsigned int elementCapacity, unsigned int elementSize, VertexBufferMode mode) :
mElementSize(elementSize),
mArrayBuffer(data, elementCapacity * elementSize, mode)
{
    
}


ShaderAttrib::~ShaderAttrib()
{
    
}


void ShaderAttrib::setup(unsigned int elementCapacity, unsigned int elementSize, VertexBufferMode mode)
{
    mElementSize = elementSize;
    mArrayBuffer.setup(mElementSize * elementCapacity, mode);
}


void ShaderAttrib::setup(const void* data, unsigned int elementCount, unsigned int elementSize, VertexBufferMode mode)
{
    mElementSize = elementSize;
    mArrayBuffer.setup(data, mElementSize * elementCount, mode);
}


void ShaderAttrib::update(const void* data, unsigned int elementCount, unsigned int offset)
{
    mArrayBuffer.update(data, mElementSize * elementCount, offset);
}


void ShaderAttrib::dispose()
{
    mArrayBuffer.dispose();
}


bool ShaderAttrib::isDisposed() const
{
    return mArrayBuffer.isDisposed();
}


VertexBufferMode ShaderAttrib::getMode() const
{
    return mArrayBuffer.getMode();
}


int ShaderAttrib::getElementSize() const
{
    return mElementSize;
}
    
}
