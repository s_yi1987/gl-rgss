#include "graphics/vertexarray.hpp"


namespace rgk
{

////////////////////////////////////////////////////////////
VertexArray::VertexArray() :
m_vertices     (),
m_primitiveType(POINTS)
{
}


////////////////////////////////////////////////////////////
VertexArray::VertexArray(PrimitiveType type, unsigned int vertexCount) :
m_vertices     (vertexCount),
m_primitiveType(type)
{
}


////////////////////////////////////////////////////////////
unsigned int VertexArray::getVertexCount() const
{
    return static_cast<unsigned int>(m_vertices.size());
}


////////////////////////////////////////////////////////////
Vertex& VertexArray::operator [](unsigned int index)
{
    return m_vertices[index];
}


////////////////////////////////////////////////////////////
const Vertex& VertexArray::operator [](unsigned int index) const
{
    return m_vertices[index];
}


////////////////////////////////////////////////////////////
void VertexArray::clear()
{
    m_vertices.clear();
}


////////////////////////////////////////////////////////////
void VertexArray::resize(unsigned int vertexCount)
{
    m_vertices.resize(vertexCount);
}


////////////////////////////////////////////////////////////
void VertexArray::append(const Vertex& vertex)
{
    m_vertices.push_back(vertex);
}


////////////////////////////////////////////////////////////
void VertexArray::append(const Vertex& vertex, const Transform& transform)
{
    Vertex transformedVertex(vertex);
    transformedVertex.position = transform * vertex.position;
    m_vertices.push_back(transformedVertex);
}


////////////////////////////////////////////////////////////
void VertexArray::append(const VertexData& vertexData)
{
    const Vertex* vertices = vertexData.getVertices();
    for(unsigned int i = 0; i < vertexData.getVertexCount(); ++i)
    {
        const Vertex& vertex = vertices[i];
        append(vertex);
    }
}


////////////////////////////////////////////////////////////
void VertexArray::append(const VertexData& vertexData, const Transform& transform)
{
    const Vertex* vertices = vertexData.getVertices();
    for(unsigned int i = 0; i < vertexData.getVertexCount(); ++i)
    {
        const Vertex& vertex = vertices[i];
        append(vertex, transform);
    }
}


////////////////////////////////////////////////////////////
void VertexArray::setPrimitiveType(PrimitiveType type)
{
    m_primitiveType = type;
}


////////////////////////////////////////////////////////////
PrimitiveType VertexArray::getPrimitiveType() const
{
    return m_primitiveType;
}


////////////////////////////////////////////////////////////
FloatRect VertexArray::getBounds() const
{
    if (!m_vertices.empty())
    {
        float left   = m_vertices[0].position.x;
        float top    = m_vertices[0].position.y;
        float right  = m_vertices[0].position.x;
        float bottom = m_vertices[0].position.y;

        for (std::size_t i = 1; i < m_vertices.size(); ++i)
        {
            Vector2f position = m_vertices[i].position;

            // Update left and right
            if (position.x < left)
                left = position.x;
            else if (position.x > right)
                right = position.x;

            // Update top and bottom
            if (position.y < top)
                top = position.y;
            else if (position.y > bottom)
                bottom = position.y;
        }

        return FloatRect(left, top, right - left, bottom - top);
    }
    else
    {
        // Array is empty
        return FloatRect();
    }
}


////////////////////////////////////////////////////////////
const Vertex* VertexArray::getVertices() const
{
    if(!m_vertices.empty())
        return &m_vertices[0];
    
    return NULL;
}


}