#include "graphics/blendmode.hpp"
#include <gl/glew.h>
#include <SDL2/SDL_blendmode.h>
#include <cstdio>


namespace rgk
{
    
BlendMode::BlendMode() :
colorSrcFactor(BlendMode::SRC_ALPHA),
colorDstFactor(BlendMode::ONE_MINUS_SRC_ALPHA),
colorEquation (BlendMode::FUNC_ADD),
alphaSrcFactor(BlendMode::ONE),
alphaDstFactor(BlendMode::ONE_MINUS_SRC_ALPHA),
alphaEquation (BlendMode::FUNC_ADD)
{

}


BlendMode::BlendMode(BlendModePreset preset) :
colorSrcFactor(BlendMode::SRC_ALPHA),
colorDstFactor(BlendMode::ONE_MINUS_SRC_ALPHA),
colorEquation (BlendMode::FUNC_ADD),
alphaSrcFactor(BlendMode::ONE),
alphaDstFactor(BlendMode::ONE_MINUS_SRC_ALPHA),
alphaEquation (BlendMode::FUNC_ADD)
{
    setup(preset);
}


BlendMode::BlendMode(Factor sourceFactor, Factor destinationFactor, Equation blendEquation) :
colorSrcFactor(sourceFactor),
colorDstFactor(destinationFactor),
colorEquation (blendEquation),
alphaSrcFactor(sourceFactor),
alphaDstFactor(destinationFactor),
alphaEquation (blendEquation)
{

}


BlendMode::BlendMode(Factor colorSourceFactor, Factor colorDestinationFactor,
                     Equation colorBlendEquation, Factor alphaSourceFactor,
                     Factor alphaDestinationFactor, Equation alphaBlendEquation) :
colorSrcFactor(colorSourceFactor),
colorDstFactor(colorDestinationFactor),
colorEquation (colorBlendEquation),
alphaSrcFactor(alphaSourceFactor),
alphaDstFactor(alphaDestinationFactor),
alphaEquation (alphaBlendEquation)
{

}


void BlendMode::setup(BlendModePreset preset)
{
    switch (preset)
    {
        case BLEND_ALPHA:
            colorSrcFactor = BlendMode::SRC_ALPHA;
            colorDstFactor = BlendMode::ONE_MINUS_SRC_ALPHA;
            colorEquation  = BlendMode::FUNC_ADD;
            alphaSrcFactor = BlendMode::ONE;
            alphaDstFactor = BlendMode::ONE_MINUS_SRC_ALPHA;
            alphaEquation  = BlendMode::FUNC_ADD;
            break;
            
        case BLEND_ADD:
            colorSrcFactor = BlendMode::SRC_ALPHA;
            colorDstFactor = BlendMode::ONE;
            colorEquation  = BlendMode::FUNC_ADD;
            alphaSrcFactor = BlendMode::ONE;
            alphaDstFactor = BlendMode::ONE;
            alphaEquation  = BlendMode::FUNC_ADD;
            break;
            
        case BLEND_MULTIPLY:
            colorSrcFactor = BlendMode::DST_COLOR;
            colorDstFactor = BlendMode::ZERO;
            colorEquation  = BlendMode::FUNC_ADD;
            alphaSrcFactor = BlendMode::DST_COLOR;
            alphaDstFactor = BlendMode::ZERO;
            alphaEquation  = BlendMode::FUNC_ADD;
            break;
            
        case BLEND_MOD:
            colorSrcFactor = BlendMode::ZERO;
            colorDstFactor = BlendMode::SRC_COLOR;
            colorEquation  = BlendMode::FUNC_ADD;
            alphaSrcFactor = BlendMode::ZERO;
            alphaDstFactor = BlendMode::ONE;
            alphaEquation  = BlendMode::FUNC_ADD;
            break;
            
        case BLEND_NONE:
            colorSrcFactor = BlendMode::ONE;
            colorDstFactor = BlendMode::ZERO;
            colorEquation  = BlendMode::FUNC_ADD;
            alphaSrcFactor = BlendMode::ONE;
            alphaDstFactor = BlendMode::ZERO;
            alphaEquation  = BlendMode::FUNC_ADD;
            break;
    }
}


Uint32 BlendMode::factorToGlConstant(Factor factor)
{
    switch (factor)
    {
        case BlendMode::ZERO:                     return GL_ZERO;
        case BlendMode::ONE:                      return GL_ONE;
        case BlendMode::SRC_COLOR:                return GL_SRC_COLOR;
        case BlendMode::ONE_MINUS_SRC_COLOR:      return GL_ONE_MINUS_SRC_COLOR;
        case BlendMode::DST_COLOR:                return GL_DST_COLOR;
        case BlendMode::ONE_MINUS_DST_COLOR:      return GL_ONE_MINUS_DST_COLOR;
        case BlendMode::SRC_ALPHA:                return GL_SRC_ALPHA;
        case BlendMode::ONE_MINUS_SRC_ALPHA:      return GL_ONE_MINUS_SRC_ALPHA;
        case BlendMode::DST_ALPHA:                return GL_DST_ALPHA;
        case BlendMode::ONE_MINUS_DST_ALPHA:      return GL_ONE_MINUS_DST_ALPHA;
        case BlendMode::CONSTANT_COLOR:           return GL_CONSTANT_COLOR;
        case BlendMode::ONE_MINUS_CONSTANT_COLOR: return GL_ONE_MINUS_CONSTANT_COLOR;
        case BlendMode::CONSTANT_ALPHA:           return GL_CONSTANT_ALPHA;
        case BlendMode::ONE_MINUS_CONSTANT_ALPHA: return GL_ONE_MINUS_CONSTANT_ALPHA;
        case BlendMode::SRC_ALPHA_SATURATE:       return GL_SRC_ALPHA_SATURATE;
        case BlendMode::INVALID_FACTOR:           break;
    }
    
    return BlendMode::INVALID_FACTOR;
}


Uint32 BlendMode::equationToGlConstant(Equation equation)
{
    switch (equation)
    {
        case BlendMode::FUNC_ADD:              return GL_FUNC_ADD;
        case BlendMode::FUNC_SUBTRACT:         return GL_FUNC_SUBTRACT;
        case BlendMode::FUNC_REVERSE_SUBTRACT: return GL_FUNC_REVERSE_SUBTRACT;
        case BlendMode::MIN:                   return GL_MIN;
        case BlendMode::MAX:                   return GL_MAX;
        case BlendMode::INVALID_EQUATION:      break;
    }
    
    return BlendMode::INVALID_EQUATION;
}


Uint32 BlendMode::blendModePresetToSDL(BlendModePreset preset)
{
    SDL_BlendMode modeSDL = SDL_BLENDMODE_NONE;
    switch(preset)
    {
        case BLEND_ALPHA :
            modeSDL = SDL_BLENDMODE_BLEND;
            break;

        case BLEND_ADD :
            modeSDL = SDL_BLENDMODE_ADD;
            break;
            
        case BLEND_MOD :
            modeSDL = SDL_BLENDMODE_MOD;
            break;

        case BLEND_NONE :
            break;
            
        default :
            throw std::puts("Unsupported BlendMode was passed as an argument");
            break;
    }
    
    return modeSDL;
}


Uint32 BlendMode::sdlBlendModeToPreset(Uint32 blendModeSDL)
{
    BlendModePreset mode = BLEND_NONE;
    switch(blendModeSDL)
    {
        case SDL_BLENDMODE_BLEND :
            mode = BLEND_ALPHA;
            break;

        case SDL_BLENDMODE_ADD :
            mode = BLEND_ADD;
            break;
            
        case SDL_BLENDMODE_MOD :
            mode = BLEND_MOD;
            break;

        case SDL_BLENDMODE_NONE :
            break;
            
        default :
            throw std::puts("Unsupported SDL_BlendMode was passed as an argument");
            break;
    }
    
    return mode;
}


bool operator ==(const BlendMode& left, const BlendMode& right)
{
    return (left.colorSrcFactor == right.colorSrcFactor) &&
           (left.colorDstFactor == right.colorDstFactor) &&
           (left.colorEquation  == right.colorEquation)  &&
           (left.alphaSrcFactor == right.alphaSrcFactor) &&
           (left.alphaDstFactor == right.alphaDstFactor) &&
           (left.alphaEquation  == right.alphaEquation);
}


bool operator !=(const BlendMode& left, const BlendMode& right)
{
    return !(left == right);
}
    
}