#include "graphics/vertexbufferobjsaver.hpp"
#include <iostream>


namespace rgk
{
    
ArrayBufferSaver::ArrayBufferSaver()
{
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &mBufferBinding);
}


ArrayBufferSaver::~ArrayBufferSaver()
{
    glBindBufferARB(GL_ARRAY_BUFFER, static_cast<GLuint>(mBufferBinding));
}


ElementArrayBufferSaver::ElementArrayBufferSaver()
{
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &mBufferBinding);
}


ElementArrayBufferSaver::~ElementArrayBufferSaver()
{
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLuint>(mBufferBinding));
}
    
    
VertexBufferObjSaver::VertexBufferObjSaver() :
mArrayBufferBinding(),
mElementArrayBufferBinding()
{

}


VertexBufferObjSaver::~VertexBufferObjSaver()
{

}

}
