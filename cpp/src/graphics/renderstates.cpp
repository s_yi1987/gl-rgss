#include "graphics/renderstates.hpp"


namespace rgk
{
    
const RenderStates RenderStates::DEFAULT;


RenderStates::RenderStates() :
blendMode(BLEND_ALPHA),
transform(),
texture(NULL),
shader(NULL),
skipShader(false)
{
    
}


RenderStates::RenderStates(BlendMode theBlendMode) :
blendMode(theBlendMode),
transform(),
texture(NULL),
shader(NULL),
skipShader(false)
{
    
}


RenderStates::RenderStates(const Transform& theTransform) :
blendMode(BLEND_ALPHA),
transform(theTransform),
texture (NULL),
shader(NULL),
skipShader(false)
{
    
}


RenderStates::RenderStates(const Texture* theTexture) :
blendMode(BLEND_ALPHA),
transform(),
texture(theTexture),
shader(NULL),
skipShader(false)
{
    
}


RenderStates::RenderStates(const Shader* theShader) :
blendMode(BLEND_ALPHA),
transform(),
texture(NULL),
shader(theShader),
skipShader(false)
{
    
}


RenderStates::RenderStates(BlendMode theBlendMode, const Transform& theTransform,
                             const Texture* theTexture, const Shader* theShader) :
blendMode(theBlendMode),
transform(theTransform),
texture(theTexture),
shader(theShader),
skipShader(false)
{
    
}


}