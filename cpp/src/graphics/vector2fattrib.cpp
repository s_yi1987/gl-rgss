#include "graphics/vector2fattrib.hpp"
#include <cstddef>

namespace rgk
{
    
Vector2fAttrib::Vector2fAttrib() : ShaderAttrib()
{

}


Vector2fAttrib::Vector2fAttrib(unsigned int vecCapacity, VertexBufferMode mode) :
ShaderAttrib(vecCapacity, sizeof(Vector2f), mode)
{
    
}


Vector2fAttrib::Vector2fAttrib(const Vector2f* vecs, unsigned int vecCount, VertexBufferMode mode) :
ShaderAttrib(vecs, vecCount, sizeof(Vector2f), mode)
{
    
}


Vector2fAttrib::~Vector2fAttrib()
{
    
}


void Vector2fAttrib::setup(unsigned int vecCapacity, VertexBufferMode mode)
{
    ShaderAttrib::setup(vecCapacity, sizeof(Vector2f), mode);
}


void Vector2fAttrib::setup(const Vector2f* vecs, unsigned int vecCount, VertexBufferMode mode)
{
    ShaderAttrib::setup(vecs, vecCount, sizeof(Vector2f), mode);
}


ShaderAttribPointerData Vector2fAttrib::getPointerData() const
{
    return ShaderAttribPointerData(2, FLOAT, sizeof(Vector2f), NULL);
}
    
}
