#include "graphics/indexarray.hpp"
#include <stddef.h>


namespace rgk
{
    
    
IndexArray::IndexArray() :
mIndices()
{

}


IndexArray::IndexArray(unsigned int indexCount) :
mIndices(indexCount)
{
    
}


const unsigned int& IndexArray::operator [](unsigned int index) const
{
    return mIndices[index];
}


unsigned int& IndexArray::operator [](unsigned int index)
{
    return mIndices[index];
}


void IndexArray::set(unsigned int index, unsigned int val)
{
    mIndices[index] = val;
}


unsigned int IndexArray::get(unsigned int index) const
{
    return mIndices[index];
}


void IndexArray::clear()
{
    mIndices.clear();
}


void IndexArray::resize(unsigned int indexCount)
{
    mIndices.resize(indexCount);
}


void IndexArray::append(unsigned int val)
{
    mIndices.push_back(val);
}


void IndexArray::append(const unsigned int* indices, unsigned int indexCount)
{
    if(!indices)
        return;
    
    for(unsigned int i = 0; i < indexCount; ++i)
        append(indices[i]);
}


unsigned int IndexArray::getIndexCount() const
{
    return static_cast<unsigned int>(mIndices.size());
}


const unsigned int* IndexArray::getIndices() const
{
    if(!mIndices.empty())
        return &mIndices[0];

    return NULL;
}


}
