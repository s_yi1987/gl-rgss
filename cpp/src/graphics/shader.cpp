#include "graphics/shader.hpp"
#include "graphics/vertexbufferobjsaver.hpp"
#include "graphics/programsaver.hpp"
#include "graphics/gltexture.hpp"
#include "system/util.hpp"
#include "system/exception.hpp"
#include <gl/glew.h>
#include <fstream>
#include <cstdio>


namespace rgk
{
CurrentTextureType Shader::CURRENT_TEXTURE;

    
Shader::Shader() :
mProgramID(0),
mCurrentTextureParamLocation(-1),
mTextures(),
mAttribs(),
mParams(),
mAttribParams()
{

}


Shader::~Shader()
{
    dispose();
}


void Shader::dispose()
{
    if(!mProgramID)
        return;
    
    if(util::hasActiveGlContext())
        glDeleteObjectARB(static_cast<GLhandleARB>(mProgramID));
    else
        std::puts("Failed to delete Shader due to no active GL context available");
    
    mCurrentTextureParamLocation = -1;
    mTextures.clear();
    mParams.clear();
}
    

bool Shader::isDisposed() const
{
    return !mProgramID;
}


void Shader::setup(const std::string& filename, ShaderType type)
{
    std::vector<char> shaderBuffer;
    loadFileContents(filename, shaderBuffer);
    
    const char* vertexShaderCode   = type == VERTEX_SHADER ? &shaderBuffer[0] : NULL;
    const char* fragmentShaderCode = type == VERTEX_SHADER ? NULL : &shaderBuffer[0];
    
    compile(vertexShaderCode, fragmentShaderCode);
}


void Shader::setup(const std::string& vertexShaderFilename,
                    const std::string& fragmentShaderFilename)
{
    std::vector<char> vertexShaderBuffer;
    loadFileContents(vertexShaderFilename, vertexShaderBuffer);
    
    std::vector<char> fragmentShaderBuffer;
    loadFileContents(fragmentShaderFilename, fragmentShaderBuffer);
    
    compile(&vertexShaderBuffer[0], &fragmentShaderBuffer[0]);
}


void Shader::setupFromMemory(const std::string& shaderCode, ShaderType type)
{
    const char* vertexShaderCode   = type == VERTEX_SHADER ? shaderCode.c_str() : NULL;
    const char* fragmentShaderCode = type == VERTEX_SHADER ? NULL : shaderCode.c_str();
    
    compile(vertexShaderCode, fragmentShaderCode);
}


void Shader::setupFromMemory(const std::string& vertexShaderCode,
                               const std::string& fragmentShaderCode)
{
    compile(vertexShaderCode.c_str(), fragmentShaderCode.c_str());
}


void Shader::setParameter(const std::string& name, float x)
{
    if(!mProgramID)
        return;

    util::verifyGlContext();

    // Enable program
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);

    // Get parameter location and assign it new values
    int location = getParamLocation(name);
    setParameterCurrent(location, x);
}


void Shader::setParameter(const std::string& name, float x, float y)
{
    if(!mProgramID)
        return;

    util::verifyGlContext();

    // Enable program
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);

    // Get parameter location and assign it new values
    int location = getParamLocation(name);
    setParameterCurrent(location, x, y);
}


void Shader::setParameter(const std::string& name, float x, float y, float z)
{
    if(!mProgramID)
        return;

    util::verifyGlContext();

    // Enable program
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);

    // Get parameter location and assign it new values
    int location = getParamLocation(name);
    setParameterCurrent(location, x, y, z);
}


void Shader::setParameter(const std::string& name, float x, float y, float z, float w)
{
    if(!mProgramID)
        return;

    util::verifyGlContext();

    // Enable program
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);

    // Get parameter location and assign it new values
    int location = getParamLocation(name);
    setParameterCurrent(location, x, y, z, w);
}


void Shader::setParameter(const std::string& name, const Vector2f& vector)
{
    setParameter(name, vector.x, vector.y);
}


void Shader::setParameter(const std::string& name, const Vector3f& vector)
{
    setParameter(name, vector.x, vector.y, vector.z);
}


void Shader::setParameter(const std::string& name, const Color& color)
{
    setParameter(name, color.r, color.g, color.b, color.a);
}


void Shader::setParameter(const std::string& name, const Transform& transform)
{
    if(!mProgramID)
        return;

    util::verifyGlContext();
    
    // Enable program
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);
    
    int location = getParamLocation(name);
    setParameterCurrent(location, transform);
}


void Shader::setParameter(const std::string& name, const Texture& texture)
{
    if(!mProgramID)
        return;
    
    int location = getParamLocation(name);
    if(location == -1)
        return;
    
    // Store the location -> texture mapping
    TextureTable::iterator it = mTextures.find(location);
    if(it == mTextures.end())
    {
        // New entry, make sure there are enough texture units
        GLint maxUnits = getMaxTextureUnits();
        if(mTextures.size() + 1 >= static_cast<std::size_t>(maxUnits))
            throw Exception("Unable to use Texture for Shader: all available texture units are used");

        mTextures[location] = &texture;
    }
    
    else
    {
        // Location already used, just replace the texture
        it->second = &texture;
    }
}


void Shader::setParameter(const std::string& name, CurrentTextureType)
{
    if(!mProgramID)
        return;

    mCurrentTextureParamLocation = getParamLocation(name);
}


void Shader::setAttribParameter(const std::string& name, const ShaderAttrib& shaderAttrib)
{
    if(!mProgramID || shaderAttrib.isDisposed())
        return;
    
    util::verifyGlContext();
    
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);
    
    int location = getAttribParamLocation(name);
    if(location == -1)
        return;
    
        // Store the location -> texture mapping
    AttribTable::iterator it = mAttribs.find(location);
    if(it == mAttribs.end())
    {
//        GLint maxUnits = getMaxTextureUnits();
//        if(mAttribs.size() + 1 >= static_cast<std::size_t>(maxUnits))
//            throw Exception("Unable to use ShaderAttrib for Shader: all available texture units are used");

        mAttribs[location] = &shaderAttrib;
    }
    
    else
    {
        // Location already used, just replace the texture
        it->second = &shaderAttrib;
    }
    
    setAttribParameterCurrent(location, shaderAttrib);
}


void Shader::bindTextures() const
{
    TextureTable::const_iterator it = mTextures.begin();
    for(std::size_t i = 0; i < mTextures.size(); ++i)
    {
        GLint index = static_cast<GLsizei>(i + 1);
        glUniform1iARB(it->first, index);
        glActiveTextureARB(GL_TEXTURE0_ARB + index);
        Texture::bind(it->second);
        ++it;
    }

    // Make sure that the texture unit which is left active is the number 0
    glActiveTextureARB(GL_TEXTURE0_ARB);
}


void Shader::bind(const Shader* shader)
{
    util::verifyGlContext();

    if(shader && shader->mProgramID)
    {
        // Enable the program
        glUseProgramObjectARB(shader->mProgramID);
    }
    
    else
    {
        // Bind no shader
        glUseProgramObjectARB(0);
    }
}


void Shader::bind(const Shader& shader, bool bindTextures)
{
    bind(&shader);
    
    if(bindTextures)
    {
        shader.bindTextures();
        
        if(shader.mCurrentTextureParamLocation != -1)
            glUniform1iARB(shader.mCurrentTextureParamLocation, 0);
    }
}


void Shader::setParameterCurrent(int location, float x)
{
    if(location >= 0)
        glUniform1fARB(location, x);
}


void Shader::setParameterCurrent(int location, float x, float y)
{
    if(location >= 0)
        glUniform2fARB(location, x, y);
}


void Shader::setParameterCurrent(int location, float x, float y, float z)
{
    if(location >= 0)
        glUniform3fARB(location, x, y, z);
}


void Shader::setParameterCurrent(int location, float x, float y, float z, float w)
{
    if(location >= 0)
        glUniform4fARB(location, x, y, z, w);
}


void Shader::setParameterCurrent(int location, const Transform& transform)
{
    if(location >= 0)
        glUniformMatrix4fvARB(location, 1, GL_FALSE, transform.getMatrix());
}


void Shader::setAttribParameterCurrent(int location, const ShaderAttrib& shaderAttrib)
{
    if(location >= 0)
    {
        enableVertexAttribArrayCurrent(location);
        
        ArrayBufferSaver save;
        const ArrayBufferObject& arrayBuffer = shaderAttrib.mArrayBuffer;
        ArrayBufferObject::bind(&arrayBuffer);
        
        ShaderAttribPointerData pointerData = shaderAttrib.getPointerData();
        GLint size = static_cast<GLint>(pointerData.size);
        GLsizei stride = static_cast<GLsizei>(pointerData.stride);
        GLenum type = static_cast<GLenum>(util::glEnumFromDataType(pointerData.type));
        const char* pointer = static_cast<const char*>(pointerData.pointer) + pointerData.offset;
        
        glVertexAttribPointerARB(location, size, type, GL_FALSE, stride, pointer);
        
        disableVertexAttribArrayCurrent(location);
    }
}


void Shader::loadFileContents(const std::string& filename, std::vector<char>& buffer)
{
   std::ifstream file(filename.c_str(), std::ios_base::binary);
   if(file)
   {
        file.seekg(0, std::ios_base::end);
        std::streamsize size = file.tellg();
        if(size > 0)
        {
            file.seekg(0, std::ios_base::beg);
            buffer.resize(static_cast<std::size_t>(size));
            file.read(&buffer[0], size);
        }
        
        buffer.push_back('\0');
   }
   
   else
   {
        throw Exception(std::string("Failed to load file: ") += filename);
   }
}


void Shader::compile(const char* vertexShaderCode, const char* fragmentShaderCode)
{
    dispose();
    if(!isAvailable())
        throw Exception("Failed to compile Shader: Shaders not available on this system");
    
    mProgramID = static_cast<unsigned int>(glCreateProgramObjectARB());
    if(vertexShaderCode)
        attachShader(vertexShaderCode, VERTEX_SHADER);
    if(fragmentShaderCode)
        attachShader(fragmentShaderCode, FRAGMENT_SHADER);
    
   glLinkProgramARB(mProgramID);
   
   GLint success;
   glGetObjectParameterivARB(mProgramID, GL_OBJECT_LINK_STATUS_ARB, &success);
   if(success == GL_FALSE)
   {
       char log[1024];
       glGetInfoLogARB(mProgramID, sizeof(log), 0, log);
       glDeleteObjectARB(mProgramID);
       mProgramID = 0;
       throw Exception(std::string("Failed to link Shader: ") += log);
   }
   
   glFlush();
}


void Shader::attachShader(const char* shaderCode, ShaderType type)
{
    GLenum shaderType = type == VERTEX_SHADER ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER;
    GLhandleARB shaderID = glCreateShaderObjectARB(shaderType);
    glShaderSourceARB(shaderID, 1, &shaderCode, NULL);
    glCompileShaderARB(shaderID);
    
    GLint success;
    glGetObjectParameterivARB(shaderID, GL_OBJECT_COMPILE_STATUS_ARB, &success);
    if(success == GL_FALSE)
    {
        char log[1024];
        glGetInfoLogARB(shaderID, sizeof(log), 0, log);
        glDeleteObjectARB(shaderID);
        glDeleteObjectARB(mProgramID);
        mProgramID = 0;
        throw Exception(std::string("Failed to compile Shader: ") += log);
    }
    
    glAttachObjectARB(mProgramID, shaderID);
    glDeleteObjectARB(shaderID);
}


void Shader::enableShaderAttribs()
{
    if(!mProgramID)
        return;
    
    util::verifyGlContext();
    
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);
    
    enableShaderAttribsCurrent(mAttribs);
}


void Shader::disableShaderAttribs()
{
    if(!mProgramID)
        return;
    
    util::verifyGlContext();
    
    ProgramSaver save;
    glUseProgramObjectARB(mProgramID);
    
    disableShaderAttribsCurrent(mAttribs);
}


void Shader::enableShaderAttribsCurrent(const AttribTable& attribTable)
{
    AttribTable::const_iterator it = attribTable.begin();
    for(std::size_t i = 0; i < attribTable.size(); ++i)
    {
        const ShaderAttrib& shaderAttrib = *it->second;
        if(shaderAttrib.isDisposed())
            throw Exception("Shader::enableShaderAttribsCurrent() - Discovered a disposed shader in mAttribs");
        
        int location = it->first;
        enableVertexAttribArrayCurrent(location);
        
        ++it;
    }
}


void Shader::disableShaderAttribsCurrent(const AttribTable& attribTable)
{
    AttribTable::const_iterator it = attribTable.begin();
    for(std::size_t i = 0; i < attribTable.size(); ++i)
    {
        int location = it->first;
        disableVertexAttribArrayCurrent(location);
        
        ++it;
    }
}


int Shader::getParamLocation(const std::string& name) const
{
    util::verifyGlContext();
    
    // Check the cache
    ParamTable::const_iterator it = mParams.find(name);
    if (it != mParams.end())
    {
        // Already in cache, return it
        return it->second;
    }
    
    else
    {
        // Not in cache, request the location from OpenGL
        int location = static_cast<int>(glGetUniformLocationARB(mProgramID, name.c_str()));
        Shader* self = const_cast<Shader*>(this);
        self->mParams.insert(std::make_pair(name, location));

        if(location == -1)
            throw Exception(std::string("Parameter not found in shader: ") += name);

        return location;
    }
}


int Shader::getAttribParamLocation(const std::string& name) const
{
    util::verifyGlContext();
    
    // Check the cache
    ParamTable::const_iterator it = mAttribParams.find(name);
    if (it != mAttribParams.end())
    {
        // Already in cache, return it
        return it->second;
    }
    
    else
    {
        // Not in cache, request the location from OpenGL
        int location = static_cast<int>(glGetAttribLocationARB(mProgramID, name.c_str()));
        Shader* self = const_cast<Shader*>(this);
        self->mAttribParams.insert(std::make_pair(name, location));

        if(location == -1)
            throw Exception(std::string("AttribParameter not found in shader: ") += name);

        return location;
    }
}


void Shader::enableVertexAttribArrayCurrent(int location)
{
    glEnableVertexAttribArray(static_cast<GLuint>(location));
}


void Shader::disableVertexAttribArrayCurrent(int location)
{
    glDisableVertexAttribArray(static_cast<GLuint>(location));
}


void Shader::verifyAvailability()
{
    if(!isAvailable())
        throw Exception("Shaders not available on this system");
}


bool Shader::isAvailable()
{
    if(!util::hasActiveGlContext())
    {
        std::puts("WARNING: Shader::isAvailable() called without active GL context");
        return false;
    }
    
    bool available = GLEW_ARB_shading_language_100 &&
                     GLEW_ARB_shader_objects &&
                     GLEW_ARB_vertex_shader &&
                     GLEW_ARB_fragment_shader;
   return available;
}


int Shader::getMaxTextureUnits()
{
    static int maxTextureUnits = checkMaxTextureUnits();
    return maxTextureUnits;
}


int Shader::checkMaxTextureUnits()
{
    GLint maxUnits = 0;
    glGetIntegerv(GL_MAX_TEXTURE_COORDS_ARB, &maxUnits);

    return static_cast<int>(maxUnits);
}

    
}
