#include "graphics/texturesaver.hpp"


namespace rgk
{
    
TextureSaver::TextureSaver()
{
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &mTextureBinding);
}


TextureSaver::~TextureSaver()
{
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(mTextureBinding));
}

}
