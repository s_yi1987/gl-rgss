#include "graphics/pixelformat.hpp"
#include "system/exception.hpp"
#include <SDL2/SDL_error.h>


namespace rgk
{

PixelFormat::PixelFormat(Uint32 format)
{
    setup(format);
}

void PixelFormat::setup(Uint32 format)
{
    SDL_PixelFormat* oldPixelFormatSDL = mPixelFormatSDL;
    mPixelFormatSDL = createPixelFormat(format);
    
    if(oldPixelFormatSDL)
        SDL_FreeFormat(oldPixelFormatSDL);
}

SDL_PixelFormat* PixelFormat::createPixelFormat(Uint32 format)
{
    SDL_PixelFormat* pixelFormatSDL = SDL_AllocFormat(format);
    if(!pixelFormatSDL)
        throw Exception(std::string("Failed to create PixelFormat: ")
            += SDL_GetError());
    
    return pixelFormatSDL;
}

PixelFormat::PixelFormat(const PixelFormat& other)
{
    Uint32 format = other.getPixelFormatValue();
    setup(format);
}

PixelFormat& PixelFormat::operator =(const PixelFormat& other)
{
    Uint32 format = other.getPixelFormatValue();
    setup(format);
    
    return *this;
}

PixelFormat::~PixelFormat()
{
    dispose();
}

void PixelFormat::dispose()
{
    if(mPixelFormatSDL)
    {
        SDL_FreeFormat(mPixelFormatSDL);
        mPixelFormatSDL = NULL;
    }
}

Uint32 PixelFormat::getPixelFormatValue() const
{
    if(!mPixelFormatSDL)
    {
        SDL_SetError("Tried to get pixel format value of NULL PixelFormat!");
        return SDL_PIXELFORMAT_UNKNOWN;
    }
    return mPixelFormatSDL->format;
}

SDL_PixelFormat* PixelFormat::get() const
{
    return mPixelFormatSDL;
}

}