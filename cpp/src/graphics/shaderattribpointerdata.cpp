#include <cstddef>
#include "graphics/shaderattribpointerdata.hpp"

namespace rgk
{
    
ShaderAttribPointerData::ShaderAttribPointerData() :
size(0),
type(UNSIGNED_BYTE),
stride(0),
pointer(NULL)
{

}

ShaderAttribPointerData::ShaderAttribPointerData(int s, DataType t, int str, const void* ptr, int offs) :
size(s),
type(t),
stride(str),
pointer(ptr),
offset(offs)
{
    
}
    
}
