#include "graphics/indexvertexarray.hpp"


namespace rgk
{
    
    
IndexVertexArray::IndexVertexArray() :
mVertices(),
mIndices()
{
    
}


IndexVertexArray::IndexVertexArray(PrimitiveType type, unsigned int vertexCount,
                                   unsigned int indexCount) :
mVertices(type, vertexCount),
mIndices(indexCount)
{
    
}


IndexVertexArray::~IndexVertexArray()
{
    
}


void IndexVertexArray::setPrimitiveType(PrimitiveType type)
{
    mVertices.setPrimitiveType(type);
}


void IndexVertexArray::appendVertex(const Vertex& vertex)
{
    mVertices.append(vertex);
}


void IndexVertexArray::appendVertex(const Vertex& vertex, const Transform& transform)
{
    mVertices.append(vertex, transform);
}

void IndexVertexArray::appendVertices(const Vertex* vertices, unsigned int vertexCount)
{
    if(!vertices)
        return;
    
    for(unsigned int i = 0; i < vertexCount; ++i)
        appendVertex(vertices[i]);
}


void IndexVertexArray::appendVertices(const Vertex* vertices, unsigned int vertexCount,
                                      const Transform& transform)
{
    if(!vertices)
        return;
    
    for(unsigned int i = 0; i < vertexCount; ++i)
        appendVertex(vertices[i], transform);
}


void IndexVertexArray::setVertex(unsigned int index, const Vertex& vertex)
{
    mVertices[index] = vertex;
}


void IndexVertexArray::appendIndex(unsigned int indexValue)
{
    mIndices.append(indexValue);
}


void IndexVertexArray::appendIndices(const unsigned int* indices, unsigned int indexCount)
{
    if(!indices)
        return;
    
    for(unsigned int i = 0; i < indexCount; ++i)
        appendIndex(indices[i]);
}


void IndexVertexArray::setIndex(unsigned int index, unsigned int val)
{
    mIndices[index] = val;
}


void IndexVertexArray::resizeVertexArray(unsigned int vertexCount)
{
    mVertices.resize(vertexCount);
}


void IndexVertexArray::resizeIndexArray(unsigned int indexCount)
{
    mIndices.resize(indexCount);
}


void IndexVertexArray::clear()
{
    clearVertexArray();
    clearIndexArray();
}


void IndexVertexArray::clearVertexArray()
{
    mVertices.clear();
}


void IndexVertexArray::clearIndexArray()
{
    mIndices.clear();
}


Vertex& IndexVertexArray::getVertex(unsigned int index)
{
    return mVertices[index];
}


const Vertex& IndexVertexArray::getVertex(unsigned int index) const
{
    return mVertices[index];
}


unsigned int IndexVertexArray::getIndex(unsigned int index) const
{
    return mIndices[index];
}


const Vertex* IndexVertexArray::getVertices() const
{
    return mVertices.getVertices();
}


const unsigned int* IndexVertexArray::getIndices() const
{
    return mIndices.getIndices();
}


unsigned int IndexVertexArray::getVertexCount() const
{
    return mVertices.getVertexCount();
}


unsigned int IndexVertexArray::getIndexCount() const
{
    return mIndices.getIndexCount();
}


PrimitiveType IndexVertexArray::getPrimitiveType() const
{
    return mVertices.getPrimitiveType();
}


}