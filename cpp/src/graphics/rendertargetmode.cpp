#include "graphics/rendertargetmode.hpp"
#include "system/util.hpp"
#include "system/exception.hpp"
#include "graphics/renderwindow.hpp"
#include "graphics/gltexture.hpp"
#include <gl/glew.h>
#include <cstdio>


namespace rgk
{

RenderTargetMode::RenderTargetMode()
{
    
}


RenderTargetMode::~RenderTargetMode()
{
    
}


RenderTargetModeNone::RenderTargetModeNone(RenderWindow& contextWindow) :
mContextWindow(&contextWindow)
{
    
}


void RenderTargetModeNone::setClipRect(const Rect* rect)
{
    if(!mContextWindow->setActive())
        return;
    
    if(rect && !rect->isEmpty())
    {
        glEnable(GL_SCISSOR_TEST);
        
        int top = mContextWindow->getSize().height - (rect->y + rect->height);
        glScissor(rect->x, top, rect->width, rect->height);
    }
    
    else
        glDisable(GL_SCISSOR_TEST);
}


void RenderTargetModeNone::applyView(const View& view)
{
    if(!mContextWindow->setActive())
        return;
    
    Rect viewport = getViewport(view);
    int top = mContextWindow->getSize().height - (viewport.y + viewport.height);
    glViewport(viewport.x, top, viewport.width, viewport.height);
    
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(view.getTransform().getMatrix());
    glMatrixMode(GL_MODELVIEW);
}


Rect RenderTargetModeNone::getViewport(const View& view)
{
    const Rect& size = mContextWindow->getSize();
    float width  = static_cast<float>(size.width);
    float height = static_cast<float>(size.height);
    
    const FloatRect& viewport = view.getViewport();
    return Rect(static_cast<int>(0.5f + width  * viewport.x),
                static_cast<int>(0.5f + height * viewport.y),
                static_cast<int>(width  * viewport.width),
                static_cast<int>(height * viewport.height));
}


RenderTargetModeTexture::RenderTargetModeTexture(RenderWindow& contextWindow) :
mFrameBufferObjID(0),
mContextWindow(&contextWindow),
mRenderTarget(NULL),
mView()
{
    if(!mContextWindow->setActive())
        throw Exception("Failed to activate GL context when creating FBO");
    util::ensureGlewInit();
    
    if(GLEW_EXT_framebuffer_object)
    {
        GLuint frameBufferID = 0;
        glGenFramebuffersEXT(1, &frameBufferID);
        mFrameBufferObjID = static_cast<unsigned int>(frameBufferID);
    }
    
    else
    {
        std::puts("WARNING: FBO not available on this system");
    }
}


RenderTargetModeTexture::~RenderTargetModeTexture()
{
    if(mFrameBufferObjID)
    {
        if(!mContextWindow->setActive())
            std::puts("Failed to activate GL context for deletion of FBO");
        
        GLuint frameBufferID = static_cast<GLuint>(mFrameBufferObjID);
        glDeleteFramebuffersEXT(1, &frameBufferID);
        mFrameBufferObjID = 0;
    }
}


void RenderTargetModeTexture::setClipRect(const Rect* rect)
{
    if(!mRenderTarget)
        throw Exception("setClipRect() called with no active rendertarget");
    
    if(!mContextWindow->setActive())
        return;
    
    if(rect && !rect->isEmpty())
    {
        glEnable(GL_SCISSOR_TEST);
        glScissor(rect->x, rect->y, rect->width, rect->height);
    }
    
    else
        glDisable(GL_SCISSOR_TEST);
}


void RenderTargetModeTexture::applyView(const View& view)
{
    if(!mRenderTarget)
        throw Exception("applyView() called with no active rendertarget");
    
    if(!mContextWindow->setActive())
        return;
    
    Rect viewport = getViewport(view);
    glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
    
    mView.setSize(view.getSize().x, -view.getSize().y);
    mView.setCenter(view.getCenter());
    mView.setRotation(view.getRotation());
    
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(mView.getTransform().getMatrix());
    glMatrixMode(GL_MODELVIEW);
}


Rect RenderTargetModeTexture::getViewport(const View& view)
{
    if(!mRenderTarget)
        throw Exception("getViewport() called with no active rendertarget");
    
    float width  = static_cast<float>(mRenderTarget->getWidth());
    float height = static_cast<float>(mRenderTarget->getHeight());
    
    const FloatRect& viewport = view.getViewport();
    return Rect(static_cast<int>(0.5f + width  * viewport.x),
                static_cast<int>(0.5f + height * viewport.y),
                static_cast<int>(width  * viewport.width),
                static_cast<int>(height * viewport.height));
}


void RenderTargetModeTexture::setRenderTarget(Texture* texture)
{
    if(!GLEW_EXT_framebuffer_object)
        throw Exception("FBO not available on this system");
    
    if(!mContextWindow->setActive())
        return;

    if(!texture || texture->isDisposed())
    {
        if(mRenderTarget)
        {
            glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
                                      GL_TEXTURE_2D, 0, 0);
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
            mRenderTarget = NULL;
        }
    }
    
    else if(mRenderTarget == texture)
    {
        return;
    }
    
    else
    {
        if(!mRenderTarget)
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, static_cast<GLuint>(mFrameBufferObjID));
        
        mRenderTarget = texture;
        GLuint textureID = static_cast<GLuint>(texture->getTextureID());
        glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
                                  GL_TEXTURE_2D, textureID, 0);
        
        if(glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT)
        {
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
            mRenderTarget = NULL;
            throw Exception("Failed to link the target texture to the frame buffer");
        }
    }
}

}