#include "graphics/surface.hpp"
#include "system/util.hpp"
#include "system/exception.hpp"
#include <SDL2/SDL_image.h>


namespace rgk
{

Surface::Surface() :
mSurfaceSDL(NULL)
{
    
}


Surface::Surface(const std::string& filename, Uint32 format) :
mSurfaceSDL(NULL)
{
    setup(filename, format);
}


Surface::Surface(int width, int height, Uint32 format) :
mSurfaceSDL(NULL)
{
    setup(width, height, format);
}


Surface::Surface(const Surface& other) :
mSurfaceSDL(NULL)
{
    setup(other);
}


Surface& Surface::operator =(const Surface& other)
{
    setup(other);
    return *this;
}


Surface::~Surface()
{
    dispose();
}


void Surface::setup(const std::string& filename, Uint32 format)
{
    dispose();
    mSurfaceSDL = createImageSurface(filename, format);
}


SDL_Surface* Surface::createImageSurface(const std::string& filename, Uint32 format)
{
    SDL_Surface* surfaceSDL = IMG_Load(filename.c_str());
    if(!surfaceSDL)
        throw Exception(std::string("Error loading image: ")
            += IMG_GetError());
    
    return util::ensurePixelFormatValue(surfaceSDL, format);
}


void Surface::setup(int width, int height, Uint32 format)
{
    dispose();
    mSurfaceSDL = createRGBSurface(width, height, format);
}


SDL_Surface* Surface::createRGBSurface(int width, int height, Uint32 format)
{
    return createRGBSurfaceMain(width, height, format);
}


SDL_Surface* Surface::createNullRGBSurface(int width, int height, Uint32 format)
{
    return createRGBSurfaceMain(width, height, format, true);
}


SDL_Surface* Surface::createRGBSurfaceMain(int width, int height, Uint32 format,
                                           bool createNull)
{
    int bpp;
    Uint32 rMask, gMask, bMask, aMask;
    SDL_PixelFormatEnumToMasks(format, &bpp, &rMask, &gMask, &bMask, &aMask);
    
    SDL_Surface* surfaceSDL = NULL;
    if(createNull)
        surfaceSDL = SDL_CreateRGBSurfaceFrom(NULL, width, height, bpp, 0,
                                              rMask, gMask, bMask, aMask);
    else
        surfaceSDL = SDL_CreateRGBSurface(0, width, height, bpp,
                                          rMask, gMask, bMask, aMask);
    if(!surfaceSDL)
        throw Exception(std::string("Error creating Surface: ")
            += SDL_GetError());
    
    return surfaceSDL;
}


void Surface::setup(const Surface& other)
{
    setup(other.mSurfaceSDL, true);
}


void Surface::setup(SDL_Surface* surfaceSDL, bool useCopy)
{
    dispose();
    if(surfaceSDL && useCopy)
        mSurfaceSDL = createSurfaceCopy(surfaceSDL, surfaceSDL->format);
    else
        mSurfaceSDL = surfaceSDL;
}


SDL_Surface* Surface::createSurfaceCopy(SDL_Surface* surfaceSDL, SDL_PixelFormat* format)
{
    if(!surfaceSDL)
        throw Exception("Cannot copy a NULL SDL_Surface");
    if(!format)
        throw Exception("Cannot use a NULL SDL_PixelFormat");
    
    SDL_Surface* copiedSurfaceSDL =
        SDL_ConvertSurface(surfaceSDL, format, 0);
    if(!copiedSurfaceSDL)
        throw Exception(std::string("Error copying SDL_Surface: ") += SDL_GetError());

    SDL_BlendMode modeSDL;
    SDL_GetSurfaceBlendMode(surfaceSDL, &modeSDL);
    SDL_SetSurfaceBlendMode(copiedSurfaceSDL, modeSDL);
    
    return copiedSurfaceSDL;
}


void Surface::dispose()
{
    if(mSurfaceSDL)
    {
        SDL_FreeSurface(mSurfaceSDL);
        mSurfaceSDL = NULL;
    }
}


bool Surface::isDisposed() const
{
    return mSurfaceSDL ? false : true;
}


SDL_Surface* Surface::get() const
{
    return mSurfaceSDL;
}


Rect Surface::getRect() const
{
    return Rect(0, 0, getWidth(), getHeight());
}


Rect Surface::getClipRect() const
{
    if(!mSurfaceSDL)
    {
        SDL_SetError("Tried to get clip Rect of an uninitialized Surface!");
        return Rect();
    }
    SDL_Rect clipRect;
    SDL_GetClipRect(mSurfaceSDL, &clipRect);
    
    return Rect(clipRect.x, clipRect.y, clipRect.w, clipRect.h);
}


int Surface::setClipRect(const Rect* clipRect)
{
    if(!mSurfaceSDL)
    {
        SDL_SetError("Tried to set clip Rect of an uninitialized Surface!");
        return false;
    }
    
    SDL_Rect clipRectSDL;
    SDL_Rect* clipRectSDLPtr = NULL;
    if(clipRect)
    {
        if(!clipRect->isEmpty())
        {
            clipRectSDL = clipRect->toSDL();
            clipRectSDLPtr = &clipRectSDL;
        }
    }
    
    SDL_bool boolSDL = SDL_SetClipRect(mSurfaceSDL, clipRectSDLPtr);
    int result = boolSDL == SDL_TRUE ? 0 : -1;
    return result;
}


int Surface::getWidth() const
{
    if(!mSurfaceSDL)
        return 0;
    
    return mSurfaceSDL->w;
}


int Surface::getHeight() const
{
    if(!mSurfaceSDL)
        return 0;
    
    return mSurfaceSDL->h;
}


Uint32 Surface::getPixelFormatValue() const
{
    if(!mSurfaceSDL)
        return SDL_PIXELFORMAT_UNKNOWN;
    
    return mSurfaceSDL->format->format;
}


int Surface::getAlphaMod() const
{
    if(!mSurfaceSDL)
        return -1;
    
    Uint8 alphaMod = 0;
    int result = SDL_GetSurfaceAlphaMod(mSurfaceSDL, &alphaMod);
    if(result == 0)
        return (int)alphaMod;
    
    return result;
}


int Surface::setAlphaMod(int alphaMod)
{
    if(!mSurfaceSDL)
        return -1;
    
    alphaMod = util::ensureWithinColorRange(alphaMod);
    return SDL_SetSurfaceAlphaMod(mSurfaceSDL, (Uint8)alphaMod);
}


BlendModePreset Surface::getBlendModePreset() const
{
    if(!mSurfaceSDL)
        return BLEND_NONE;
    
    SDL_BlendMode modeSDL = SDL_BLENDMODE_NONE;
    SDL_GetSurfaceBlendMode(mSurfaceSDL, &modeSDL);
    
    return static_cast<BlendModePreset>(BlendMode::sdlBlendModeToPreset(modeSDL));
}


int Surface::setBlendModePreset(BlendModePreset preset)
{
    if(!mSurfaceSDL)
        return -1;
    
    SDL_BlendMode modeSDL = static_cast<SDL_BlendMode>(BlendMode::blendModePresetToSDL(preset));
    return SDL_SetSurfaceBlendMode(mSurfaceSDL, modeSDL);
}


Color Surface::getColorMod(OutputArgument<int>* output) const
{
    if(!mSurfaceSDL)
    {
        if(output)
            output->val = -1;
        return Color();
    }
    
    Uint8 r = 0, g = 0, b = 0;
    int result = SDL_GetSurfaceColorMod(mSurfaceSDL, &r, &g, &b);
    
    if(output)
        output->val = result;
    if(result == 0)
        return Color(r, g, b, 0);
    else
        return Color();
}


int Surface::setColorMod(const Color& color)
{
    return setColorMod(color.r, color.g, color.b);
}


int Surface::setColorMod(int r, int g, int b)
{
    if(!mSurfaceSDL)
        return -1;
    
    r = util::ensureWithinColorRange(r);
    g = util::ensureWithinColorRange(g);
    b = util::ensureWithinColorRange(b);
    
    return SDL_SetSurfaceColorMod(mSurfaceSDL, (Uint8)r, (Uint8)g, (Uint8)b);
}


Color Surface::getPixelColor(int x, int y, OutputArgument<bool>* output) const
{
    Uint32 rawPixel = getPixel(x, y, output);
    return Color(rawPixel, getPixelFormatValue());
}


Uint32 Surface::getPixel(int x, int y, OutputArgument<bool>* output) const
{
    if(x > getWidth() || y > getHeight() || 0 >= getWidth() || 0 >= getHeight())
    {
        if(output)
            output->val = false;
        return 0;
    }
    
    else if(output)
        output->val = true;
    
    int bpp = mSurfaceSDL->format->BytesPerPixel;
    int pitch = mSurfaceSDL->pitch;
    
    return util::getPixel(mSurfaceSDL->pixels, x, y, bpp, pitch);
}


bool Surface::setPixel(int x, int y, const Color& color)
{
    Uint32 pixel = color.toMapped(getPixelFormatValue());
    return setPixel(x, y, pixel);
}


bool Surface::setPixel(int x, int y, Uint32 pixel)
{
    if(x > getWidth() || y > getHeight() || 0 >= getWidth() || 0 >= getHeight())
        return false;
    
    int bpp = mSurfaceSDL->format->BytesPerPixel;
    int pitch = mSurfaceSDL->pitch;
    util::setPixel(mSurfaceSDL->pixels, x, y, bpp, pitch, pixel);

    return true;
}


Color Surface::getColorKey(OutputArgument<bool>* output) const
{
    if(!mSurfaceSDL)
    {
        if(output)
            output->val = false;
        return Color(0, 0, 0, 0);
    }
    Uint32 key = 0;
    int result = SDL_GetColorKey(mSurfaceSDL, &key);
    if(output)
        output->val = result == 0 ? true : false;
    if(result != 0)
        return Color(0, 0, 0, 0);
    
    return Color(key, getPixelFormatValue());
}


int Surface::setColorKey(Color& color, bool flag)
{
    return setColorKey(color.toMapped(getPixelFormatValue()), flag);
}


int Surface::setColorKey(Uint32 color, bool flag)
{
    if(!mSurfaceSDL)
        return -1;
    
    int flagSDL = flag ? SDL_TRUE : SDL_FALSE;
    return SDL_SetColorKey(mSurfaceSDL, flagSDL, color);
}


int Surface::fillRect(const Rect* rect, const Color& color)
{
    return fillRect(rect, color.toMapped(getPixelFormatValue()));
}


int Surface::fillRect(const Rect* rect, Uint32 pixel)
{
    if(!mSurfaceSDL)
        return -1;
    
    SDL_Rect rectSDL;
    SDL_Rect* rectSDLPtr = NULL;
    if(rect)
    {
        rectSDL = rect->toSDL();
        rectSDLPtr = &rectSDL;
    }
    
    return SDL_FillRect(mSurfaceSDL, rectSDLPtr, pixel);
}


int Surface::blit(Surface& src, const Rect* srcRect,  const Rect* dstRect)
{
    return blitMain(src, srcRect, dstRect, false);
}


int Surface::blitScaled(Surface& src, const Rect* srcRect, const Rect* dstRect)
{
    return blitMain(src, srcRect, dstRect, true);
}


int Surface::blitMain(Surface& src, const Rect* srcRect, const Rect* dstRect,
                      bool scaled)
{
    SDL_Surface* srcSDL = src.mSurfaceSDL;
    SDL_Surface* dstSDL = mSurfaceSDL;
    
    if(!srcSDL)
        return -1;
    else if(!dstSDL)
        return -1;
    
    SDL_Rect srcRectSDL;
    SDL_Rect* srcRectSDLPtr = NULL;
    if(srcRect)
    {
        srcRectSDL = srcRect->toSDL();
        srcRectSDLPtr = &srcRectSDL;
    }
    
    SDL_Rect dstRectSDL;
    SDL_Rect* dstRectSDLPtr = NULL;
    if(dstRect)
    {
        dstRectSDL = dstRect->toSDL();
        dstRectSDLPtr = &dstRectSDL;
    }
    
    if(scaled)
        return SDL_BlitScaled(srcSDL, srcRectSDLPtr, dstSDL, dstRectSDLPtr);
    else
        return SDL_BlitSurface(srcSDL, srcRectSDLPtr, dstSDL, dstRectSDLPtr);
}


}
