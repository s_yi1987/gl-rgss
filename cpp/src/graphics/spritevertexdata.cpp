#include "graphics/spritevertexdata.hpp"
#include <cmath>


namespace rgk
{

SpriteVertexData::SpriteVertexData() :
mTextureRect()
{
    
}


SpriteVertexData::SpriteVertexData(const Rect& rectangle) :
mTextureRect()
{
    setTextureRect(rectangle);
}


void SpriteVertexData::setTextureRect(const Rect& rectangle)
{
    if(mTextureRect != rectangle)
    {
        mTextureRect = rectangle;
        updatePositions();
        updateTexCoords();
    }
}


void SpriteVertexData::setColor(const Color& color)
{
    PlainColor plainColor = color.toPlain();
    mVertices[0].color = plainColor;
    mVertices[1].color = plainColor;
    mVertices[2].color = plainColor;
    mVertices[3].color = plainColor;
}


const Rect& SpriteVertexData::getTextureRect() const
{
    return mTextureRect;
}


Color SpriteVertexData::getColor() const
{
    return Color(mVertices[0].color);
}


FloatRect SpriteVertexData::getBounds() const
{
    float width  = static_cast<float>(std::abs(mTextureRect.width));
    float height = static_cast<float>(std::abs(mTextureRect.height));

    return FloatRect(0.f, 0.f, width, height);
}


VertexArray SpriteVertexData::toVertexArray(bool separateTriangles) const
{
    VertexArray vertexArray;
    if(!separateTriangles)
    {
        vertexArray.setPrimitiveType(getPrimitiveType());
        vertexArray.append(*this);
    }
    
    else
    {
        vertexArray.setPrimitiveType(TRIANGLES);
        Vertex vertices[6];
         
        FloatRect bounds = getBounds();

        vertices[0].position = Vector2f(0, 0);
        vertices[1].position = Vector2f(0, bounds.height);
        vertices[2].position = Vector2f(bounds.width, 0);
        
        vertices[3].position = Vector2f(bounds.width, 0);
        vertices[4].position = Vector2f(0, bounds.height);
        vertices[5].position = Vector2f(bounds.width, bounds.height);
         
        float left   = static_cast<float>(mTextureRect.x);
        float right  = left + mTextureRect.width;
        float top    = static_cast<float>(mTextureRect.y);
        float bottom = top + mTextureRect.height;
        
        vertices[0].texCoords = Vector2f(left, top);
        vertices[1].texCoords = Vector2f(left, bottom);
        vertices[2].texCoords = Vector2f(right, top);
        
        vertices[3].texCoords = Vector2f(right, top);
        vertices[4].texCoords = Vector2f(left, bottom);
        vertices[5].texCoords = Vector2f(right, bottom);
        
        for(int i = 0; i < 6; ++i)
            vertexArray.append(vertices[i]);
    }
    
    return vertexArray;
}


const Vertex* SpriteVertexData::getVertices() const
{
    return mVertices;
}


unsigned int SpriteVertexData::getVertexCount() const
{
    return 4;
}


PrimitiveType SpriteVertexData::getPrimitiveType() const
{
    return TRIANGLES_STRIP;
}


void SpriteVertexData::updatePositions()
{
    FloatRect bounds = getBounds();

    mVertices[0].position = Vector2f(0, 0);
    mVertices[1].position = Vector2f(0, bounds.height);
    mVertices[2].position = Vector2f(bounds.width, 0);
    mVertices[3].position = Vector2f(bounds.width, bounds.height);
}


void SpriteVertexData::updateTexCoords()
{
    float left   = static_cast<float>(mTextureRect.x);
    float right  = left + mTextureRect.width;
    float top    = static_cast<float>(mTextureRect.y);
    float bottom = top + mTextureRect.height;

    mVertices[0].texCoords = Vector2f(left, top);
    mVertices[1].texCoords = Vector2f(left, bottom);
    mVertices[2].texCoords = Vector2f(right, top);
    mVertices[3].texCoords = Vector2f(right, bottom);
}

}