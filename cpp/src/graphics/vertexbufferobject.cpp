#include "graphics/vertexbufferobject.hpp"
#include "graphics/vertexbufferobjsaver.hpp"
#include "graphics/vertexdata.hpp"
#include "system/exception.hpp"
#include "system/lock.hpp"
#include "system/util.hpp"
#include <gl/glew.h>
#include <cstdio>


namespace rgk
{

VertexBufferObject::VertexBufferObject() :
mArrayBuffer(),
mIndexBufferID(0),
mPrimitiveType(POINTS),
mVertexCapacity(0),
mIndexCapacity(0),
mVertexReadSize(0),
mIndexReadSize(0)
{
    
}


VertexBufferObject::VertexBufferObject(unsigned int vertexCapacitySize, VertexBufferMode mode) :
mArrayBuffer(),
mIndexBufferID(0),
mPrimitiveType(POINTS),
mVertexCapacity(0),
mIndexCapacity(0),
mVertexReadSize(0),
mIndexReadSize(0)
{
    setup(vertexCapacitySize, mode);
}


VertexBufferObject::VertexBufferObject(unsigned int vertexCapacitySize, unsigned int indexCapacitySize,
                                       VertexBufferMode mode) :
mArrayBuffer(),
mIndexBufferID(0),
mPrimitiveType(POINTS),
mVertexCapacity(0),
mIndexCapacity(0),
mVertexReadSize(0),
mIndexReadSize(0)
{
    setup(vertexCapacitySize, indexCapacitySize, mode);
}


VertexBufferObject::VertexBufferObject(const VertexData& vertexData, VertexBufferMode mode) :
mArrayBuffer(),
mIndexBufferID(0),
mPrimitiveType(POINTS),
mVertexCapacity(0),
mIndexCapacity(0),
mVertexReadSize(0),
mIndexReadSize(0)
{
    setup(vertexData, mode);
}


void VertexBufferObject::setup(unsigned int vertexCapacitySize, VertexBufferMode mode)
{
    setupBuffers(NULL, vertexCapacitySize, NULL, 0, mode);
}


void VertexBufferObject::setup(unsigned int vertexCapacitySize, unsigned int indexCapacitySize,
                               VertexBufferMode mode)
{
    setupBuffers(NULL, vertexCapacitySize, NULL, indexCapacitySize, mode);
}


void VertexBufferObject::setup(const VertexData& vertexData, VertexBufferMode mode)
{
    mPrimitiveType = vertexData.getPrimitiveType();
    
    setupBuffers(vertexData.getVertices(), vertexData.getVertexCount(),
                 vertexData.getIndices(),  vertexData.getIndexCount(),
                 mode);
}


void VertexBufferObject::setupBuffers(const Vertex* vertices, unsigned int vertexCapacitySize,
                                      const unsigned int* indices, unsigned int indexCapacitySize,
                                      VertexBufferMode mode)
{
    if(vertexCapacitySize == 0)
        throw Exception("Size of VertexBuffer must be greater than 0");
        
    verifyAvailability();
    if(indexCapacitySize > 0)
        setupIndexBuffer();
    else
        disposeIndexBuffer();
    
    mVertexCapacity = vertexCapacitySize;
    mIndexCapacity  = indexCapacitySize;
    mVertexReadSize = vertexCapacitySize;
    mIndexReadSize  = mIndexCapacity;
    
    static const GLenum usages[] = {GL_STATIC_DRAW, GL_DYNAMIC_DRAW, GL_STREAM_DRAW};
    
    VertexBufferObjSaver save;
    mArrayBuffer.setup(vertices, mVertexCapacity * sizeof(Vertex), mode);
    
    if(mIndexBufferID)
    {
        GLsizei size = static_cast<GLsizei>(mIndexCapacity * sizeof(unsigned int));
        
        glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLuint>(mIndexBufferID));
        glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, size, indices, usages[mode]);
    }
}


void VertexBufferObject::setupIndexBuffer()
{
    if(!mIndexBufferID)
    {
        GLuint indexBufferID;
        glGenBuffersARB(1, &indexBufferID);
        mIndexBufferID = static_cast<unsigned int>(indexBufferID);
    }
}


void VertexBufferObject::verifyAvailability()
{
    util::verifyGlContext();
    
    if(!GLEW_ARB_vertex_buffer_object)
        throw Exception("VBO not supported on this system");
}

VertexBufferObject::~VertexBufferObject()
{
    dispose();
}


void VertexBufferObject::dispose()
{
    if(mArrayBuffer.isDisposed())
        return;
    
    if(!util::hasActiveGlContext())
    {
        std::puts("~VertexBufferObject() - No active GL Context detected");
        
        mVertexCapacity = 0;
        
        mVertexReadSize = 0;
        mIndexReadSize  = 0;
        
        mIndexBufferID = 0;
        mIndexCapacity = 0;
    }
    
    else
    {
        disposeVertexBuffer();
        disposeIndexBuffer();
    }
}


void VertexBufferObject::disposeVertexBuffer()
{
    if(!mArrayBuffer.isDisposed())
    {
        mArrayBuffer.dispose();
    }
}


void VertexBufferObject::disposeIndexBuffer()
{
    if(mIndexBufferID)
    {
        GLuint indexBufferID = static_cast<GLuint>(mIndexBufferID);
        glDeleteBuffersARB(1, &indexBufferID);
        
        mIndexBufferID = 0;
        mIndexCapacity = 0;
        mIndexReadSize = 0;
    }
}


bool VertexBufferObject::isDisposed() const
{
    return mArrayBuffer.isDisposed();
}


void VertexBufferObject::update(const VertexData& vertexData, 
                                unsigned int vertexBufferOffset,
                                unsigned int indexBufferOffset)
{
    updateVertexBuffer(vertexData.getVertices(), vertexData.getVertexCount(),
                       vertexBufferOffset);
    updateIndexBuffer(vertexData.getIndices(), vertexData.getIndexCount(),
                      indexBufferOffset);
}


void VertexBufferObject::updateVertexBuffer(const Vertex* vertices, unsigned int count,
                                            unsigned int offset)
{
    if(mArrayBuffer.isDisposed() || !vertices || (offset + count) > mVertexCapacity || !count)
        return;
    util::verifyGlContext();
    
    ArrayBufferSaver save;
    //glBindBufferARB(GL_ARRAY_BUFFER, static_cast<GLuint>(mVertexBufferID));
    ArrayBufferObject::bind(&mArrayBuffer);
    
    updateCurrentVertexBuffer(vertices, count, offset);
}


void VertexBufferObject::updateCurrentVertexBuffer(const Vertex* vertices, unsigned int count,
                                                   unsigned int offset)
{
//    GLsizei size   = static_cast<GLsizei>(count * sizeof(Vertex));
//    GLint offsetGL = static_cast<GLint>(offset * sizeof(Vertex));
//    glBufferSubDataARB(GL_ARRAY_BUFFER, offsetGL, size, vertices);
    
    ArrayBufferObject::updateCurrentArrayBuffer(vertices, 
            count * sizeof(Vertex), 
            offset * sizeof(Vertex));
}


void VertexBufferObject::updateIndexBuffer(const unsigned int* indices, unsigned int count,
                                           unsigned int offset)
{
    if(!mIndexBufferID || !indices || (offset + count) > mIndexCapacity || !count)
        return;
    util::verifyGlContext();
    
    ElementArrayBufferSaver save;
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLuint>(mIndexBufferID));
    updateCurrentIndexBuffer(indices, count, offset);
}


void VertexBufferObject::updateCurrentIndexBuffer(const unsigned int* indices, unsigned int count,
                                                  unsigned int offset)
{
    GLsizei size   = static_cast<GLsizei>(count * sizeof(unsigned int));
    GLint offsetGL = static_cast<GLint>(offset * sizeof(unsigned int));
    glBufferSubDataARB(GL_ELEMENT_ARRAY_BUFFER, offsetGL, size, indices);
}


void VertexBufferObject::setPrimitiveType(PrimitiveType type)
{
    mPrimitiveType = type;
}


void VertexBufferObject::setVertexReadSize(unsigned int size)
{
    if(mArrayBuffer.isDisposed() || size > mVertexCapacity)
        return;
    
    mVertexReadSize = size;
}


void VertexBufferObject::setIndexReadSize(unsigned int size)
{
    if(!mIndexBufferID || size > mIndexCapacity)
        return;
    
    mIndexReadSize = size;
}


unsigned int VertexBufferObject::getVertexCapacity() const
{
    return mVertexCapacity;
}


unsigned int VertexBufferObject::getVertexReadSize() const
{
    return mVertexReadSize;
}


unsigned int VertexBufferObject::getIndexCapacity() const
{
    return mIndexCapacity;
}


unsigned int VertexBufferObject::getIndexReadSize() const
{
    return mIndexReadSize;
}


unsigned int VertexBufferObject::getVertexBufferID() const
{
    return mArrayBuffer.getArrayBufferID();
}


unsigned int VertexBufferObject::getIndexBufferID() const
{
    return mIndexBufferID;
}


VertexBufferMode VertexBufferObject::getMode() const
{
    return mArrayBuffer.getMode();
}


PrimitiveType VertexBufferObject::getPrimitiveType() const
{
    return mPrimitiveType;
}


void VertexBufferObject::bind(const VertexBufferObject* vertexBufferObject)
{
    if(!vertexBufferObject || vertexBufferObject->isDisposed())
    {
        //glBindBufferARB(GL_ARRAY_BUFFER, 0);
        ArrayBufferObject::bind(NULL);
        glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, 0);
        return;
    }
    
    //glBindBufferARB(GL_ARRAY_BUFFER, static_cast<GLuint>(vertexBufferObject->mVertexBufferID));
    ArrayBufferObject::bind(&vertexBufferObject->mArrayBuffer);
    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLuint>(vertexBufferObject->mIndexBufferID));
}

    
}
