#include "graphics/programsaver.hpp"


ProgramSaver::ProgramSaver() :
mProgramHandle(glGetHandleARB(GL_PROGRAM_OBJECT_ARB))
{
    
}


ProgramSaver::~ProgramSaver()
{
    glUseProgramObjectARB(mProgramHandle);
}



