#include "graphics/font.hpp"
#include "system/exception.hpp"
#include "system/util.hpp"


namespace rgk
{

    
Font::Font() :
mFontSDL(NULL)
{
    
}


Font::Font(const std::string& filename, int ptsize, int index) :
mFontSDL(NULL)
{
    setup(filename, ptsize, index);
}


Font::~Font()
{
    dispose();
}


void Font::setup(const std::string& filename, int ptsize, int index)
{
    dispose();
    mFontSDL = loadFont(filename, ptsize, index);
}


TTF_Font* Font::loadFont(const std::string& filename, int ptsize, int index)
{
    TTF_Font* fontSDL = TTF_OpenFontIndex(filename.c_str(), ptsize, index);
    if(!fontSDL)
        throw Exception(std::string("Error loading font: ")
            += TTF_GetError());
    return fontSDL;
}


void Font::dispose()
{
    if(mFontSDL)
    {
        TTF_CloseFont(mFontSDL);
        mFontSDL = NULL;
    }
}


bool Font::isDisposed() const
{
    return !mFontSDL;
}


TTF_Font* Font::get() const
{
    return mFontSDL;
}


int Font::getStyle() const
{
    if(!mFontSDL)
        return -1;
    
    return TTF_GetFontStyle(mFontSDL);
}


int Font::setStyle(int style)
{
    if(!mFontSDL)
        return -1;
    
    TTF_SetFontStyle(mFontSDL, style);
    return 0;
}


int Font::getOutline() const
{
    if(!mFontSDL)
        return -1;
    
    return TTF_GetFontOutline(mFontSDL);
}


int Font::setOutline(int outline)
{
    if(!mFontSDL)
        return -1;
    
    TTF_SetFontOutline(mFontSDL, outline);
    return 0;
}


int Font::getHinting() const
{
    if(!mFontSDL)
        return -1;

    return TTF_GetFontHinting(mFontSDL);
}


int Font::setHinting(int hinting)
{
    if(!mFontSDL)
        return -1;
    
    TTF_SetFontHinting(mFontSDL, hinting);
    return 0;
}


int Font::getKerning() const
{
    if(!mFontSDL)
        return -1;
    
    return TTF_GetFontKerning(mFontSDL);
}


int Font::setKerning(int allowed)
{
    if(!mFontSDL)
        return -1;
    
    TTF_SetFontKerning(mFontSDL, allowed);
    return 0;
}


int Font::getHeight() const
{
    if(!mFontSDL)
        return -1;
    
    return TTF_FontHeight(mFontSDL);
}


int Font::getAscent() const
{
    if(!mFontSDL)
        return -1;
    
    return TTF_FontAscent(mFontSDL);
}


int Font::getDescent() const
{
    if(!mFontSDL)
        return -1;

    return TTF_FontDescent(mFontSDL);
}


int Font::getLineSkip() const
{
    if(!mFontSDL)
        return -1;

    return TTF_FontLineSkip(mFontSDL);
}


std::string Font::getFamilyName() const
{
    if(!mFontSDL)
        return "";
    
    return std::string(TTF_FontFaceFamilyName(mFontSDL));
}


std::string Font::getStyleName() const
{
    if(!mFontSDL)
        return "";

    
    return std::string(TTF_FontFaceStyleName(mFontSDL));
}


Rect Font::calculateTextSize(const std::string& text) const
{
    if(!mFontSDL)
        return Rect(0, 0, 0, 0);
    
    int w = 0, h = 0;
    if(TTF_SizeUTF8(mFontSDL, text.c_str(), &w, &h) == 0)
        return Rect(0, 0, w, h);
    
    return Rect(0, 0, 0, 0);
}


bool Font::fixedFaceWidth() const
{
    if(!mFontSDL)
        return false;
    
    return TTF_FontFaceIsFixedWidth(mFontSDL) > 0;
}


Surface Font::toSurfaceTextSolid(const std::string& text, const Color& fg) const
{
    Surface surface;
    renderTextSolidTo(surface, text, fg);
    
    return surface;
}


void Font::renderTextSolidTo(Surface& surface, const std::string& text,
                             const Color& fg) const
{
    renderFontTo(TTF_RenderText_Solid, surface, text, fg);
}


Surface Font::toSurfaceUTF8Solid(const std::string& text, const Color& fg) const
{
    Surface surface;
    renderUTF8SolidTo(surface, text, fg);
    
    return surface;
}


void Font::renderUTF8SolidTo(Surface& surface, const std::string& text,
                             const Color& fg) const
{
    renderFontTo(TTF_RenderUTF8_Solid, surface, text, fg);
}


Surface Font::toSurfaceUNICODESolid(Uint16 text, const Color& fg) const
{
    Surface surface;
    renderUNICODESolidTo(surface, text, fg);
    
    return surface;
}


void Font::renderUNICODESolidTo(Surface& surface, Uint16 text, const Color& fg) const
{
    renderFontTo(TTF_RenderUNICODE_Solid, surface, &text, fg);
}


Surface Font::toSurfaceTextShaded(const std::string& text, const Color& fg,
                                  const Color& bg) const
{
    Surface surface;
    renderTextShadedTo(surface, text, fg, bg);
    
    return surface;
}


void Font::renderTextShadedTo(Surface& surface, const std::string& text,
                              const Color& fg, const Color& bg) const
{
    renderFontTo(TTF_RenderText_Shaded, surface, text, fg, bg);
}


Surface Font::toSurfaceUTF8Shaded(const std::string& text, const Color& fg,
                                  const Color& bg) const
{
    Surface surface;
    renderUTF8ShadedTo(surface, text, fg, bg);
    
    return surface;
}


void Font::renderUTF8ShadedTo(Surface& surface, const std::string& text,
                              const Color& fg, const Color& bg) const
{
    renderFontTo(TTF_RenderUTF8_Shaded, surface, text, fg, bg);
}


Surface Font::toSurfaceUNICODEShaded(Uint16 text, const Color& fg, const Color& bg) const
{
    Surface surface;
    renderUNICODEShadedTo(surface, text, fg, bg);
    
    return surface;
}


void Font::renderUNICODEShadedTo(Surface& surface, Uint16 text, const Color& fg,
                                 const Color& bg) const
{
    renderFontTo(TTF_RenderUNICODE_Shaded, surface, &text, fg, bg);
}


Surface Font::toSurfaceTextBlended(const std::string& text, const Color& fg) const
{
    Surface surface;
    renderTextBlendedTo(surface, text, fg);
    
    return surface;
}


void Font::renderTextBlendedTo(Surface& surface, const std::string& text,
                               const Color& fg) const
{
    renderFontTo(TTF_RenderText_Blended, surface, text, fg);
}


Surface Font::toSurfaceUTF8Blended(const std::string& text, const Color& fg) const
{
    Surface surface;
    renderUTF8BlendedTo(surface, text, fg);
    
    return surface;
}


void Font::renderUTF8BlendedTo(Surface& surface, const std::string& text,
                               const Color& fg) const
{
    renderFontTo(TTF_RenderUTF8_Blended, surface, text, fg);
}


Surface Font::toSurfaceUNICODEBlended(Uint16 text, const Color& fg) const
{
    Surface surface;
    renderUNICODEBlendedTo(surface, text, fg);
    
    return surface;
}


void Font::renderUNICODEBlendedTo(Surface& surface, Uint16 text, const Color& fg) const
{
    renderFontTo(TTF_RenderUNICODE_Blended, surface, &text, fg);
}


void Font::renderFontTo(SDL_Surface* (*func)(TTF_Font*, const char*, SDL_Color),
                        Surface& surface, const std::string& text, const Color& fg) const
{
    if(!mFontSDL)
    {
        surface.dispose();
        return;
    }
    
    SDL_Surface* surfaceSDL = func(mFontSDL, text.c_str(), fg.toSDL());
    if(!surfaceSDL)
        throw Exception(std::string("Failed to render Surface from Font: ") +=
            TTF_GetError());
    
    Uint32 pixelFormatVal = surface.isDisposed() ? SDL_PIXELFORMAT_ARGB8888 : surface.getPixelFormatValue();
    surfaceSDL = util::ensurePixelFormatValue(surfaceSDL, pixelFormatVal);
    surface.setup(surfaceSDL, false);
}


void Font::renderFontTo(SDL_Surface* (*func)(TTF_Font*, const Uint16*, SDL_Color),
                        Surface& surface, Uint16* text, const Color& fg) const
{
    if(!mFontSDL)
    {
        surface.dispose();
        return;
    }
    
    SDL_Surface* surfaceSDL = func(mFontSDL, text, fg.toSDL());
    if(!surfaceSDL)
        throw Exception(std::string("Failed to render Surface from Font: ") +=
            TTF_GetError());
    
    surfaceSDL = util::ensurePixelFormatValue(surfaceSDL, surface.getPixelFormatValue());
    surface.setup(surfaceSDL, false);
}


void Font::renderFontTo(SDL_Surface*(*func)(TTF_Font*, const char*, SDL_Color, SDL_Color),
                        Surface& surface, const std::string& text,
                        const Color& fg, const Color& bg) const
{
    if(!mFontSDL)
    {
        surface.dispose();
        return;
    }
    
    SDL_Surface* surfaceSDL = func(mFontSDL, text.c_str(), fg.toSDL(), bg.toSDL());
    if(!surfaceSDL)
        throw Exception(std::string("Failed to render Surface from Font: ") +=
            TTF_GetError());
    
    surfaceSDL = util::ensurePixelFormatValue(surfaceSDL, surface.getPixelFormatValue());
    surface.setup(surfaceSDL, false);
}


void Font::renderFontTo(SDL_Surface* (*func)(TTF_Font*, const Uint16*, SDL_Color, SDL_Color),
                        Surface& surface, Uint16* text, const Color& fg, const Color& bg) const
{
    if(!mFontSDL)
    {
        surface.dispose();
        return;
    }
    
    SDL_Surface* surfaceSDL = func(mFontSDL, text, fg.toSDL(), bg.toSDL());
    if(!surfaceSDL)
        throw Exception(std::string("Failed to render Surface from Font: ") +=
            TTF_GetError());
    
    surfaceSDL = util::ensurePixelFormatValue(surfaceSDL, surface.getPixelFormatValue());
    surface.setup(surfaceSDL, false);
}


}