#include "graphics/arraybufferobject.hpp"
#include "graphics/vertexbufferobjsaver.hpp"
#include "system/exception.hpp"
#include "system/util.hpp"
#include <cstdio>

namespace rgk
{
    
ArrayBufferObject::ArrayBufferObject() :
mArrayBufferID(0),
mArrayBufferCapacity(0),
mMode(VBO_STATIC_DRAW)
{

}


ArrayBufferObject::ArrayBufferObject(unsigned int size, VertexBufferMode mode) :
mArrayBufferID(0),
mArrayBufferCapacity(size),
mMode(mode)
{
    setup(size, mode);
}


ArrayBufferObject::ArrayBufferObject(const void* data, unsigned int size, VertexBufferMode mode) :
mArrayBufferID(0),
mArrayBufferCapacity(size),
mMode(mode)
{
    setup(data, size, mode);
}


void ArrayBufferObject::setup(unsigned int size, VertexBufferMode mode)
{
    setup(NULL, size, mode);
}


void ArrayBufferObject::setup(const void* data, unsigned int size, VertexBufferMode mode)
{
    if(size == 0)
        throw Exception("Size of VertexBuffer must be greater than 0");
    createArrayBuffer();
    
    mArrayBufferCapacity = size;
    mMode = mode;
    
    static const GLenum usages[] = {GL_STATIC_DRAW, GL_DYNAMIC_DRAW, GL_STREAM_DRAW};
    
    ArrayBufferSaver save;
    GLsizei sizeGL = static_cast<GLsizei>(mArrayBufferCapacity);
    
    glBindBufferARB(GL_ARRAY_BUFFER, static_cast<GLuint>(mArrayBufferID));
    glBufferDataARB(GL_ARRAY_BUFFER, sizeGL, data, usages[mMode]);
}


void ArrayBufferObject::createArrayBuffer()
{
    if(!mArrayBufferID)
    {
        verifyAvailability();
        
        GLuint vertexBufferID;
        glGenBuffersARB(1, &vertexBufferID);
        mArrayBufferID = static_cast<unsigned int>(vertexBufferID);
    }
}


void ArrayBufferObject::update(const void* data, unsigned int count, unsigned int offset)
{
    if(!mArrayBufferID || !data || offset + count > mArrayBufferCapacity || !count)
        return;
    util::verifyGlContext();
    
    ArrayBufferSaver save;
    glBindBufferARB(GL_ARRAY_BUFFER, static_cast<GLuint>(mArrayBufferID));
    updateCurrentArrayBuffer(data, count, offset);
}


ArrayBufferObject::~ArrayBufferObject()
{
    dispose();
}


void ArrayBufferObject::dispose()
{
    if(!mArrayBufferID)
        return;
    
    if(util::hasActiveGlContext())
    {
        GLuint arrayBufferID = static_cast<GLuint>(mArrayBufferID);
        glDeleteBuffersARB(1, &arrayBufferID);
    }

    else
    {
        std::puts("ArrayBufferObject::dispose() - No active GL Context detected");
    }

    mArrayBufferID = 0;
    mArrayBufferCapacity = 0;
}


bool ArrayBufferObject::isDisposed() const
{
    return mArrayBufferID ? false : true;
}


unsigned int ArrayBufferObject::getArrayBufferID() const
{
    return mArrayBufferID;
}


unsigned int ArrayBufferObject::getArrayBufferCapacity() const
{
    return mArrayBufferCapacity;
}


VertexBufferMode ArrayBufferObject::getMode() const
{
    return mMode;
}


void ArrayBufferObject::verifyAvailability()
{
    util::verifyGlContext();
    
    if(!GLEW_ARB_vertex_buffer_object)
        throw Exception("VBO not supported on this system");
}


void ArrayBufferObject::bind(const ArrayBufferObject* arrayBufferObject)
{
    if(!arrayBufferObject || arrayBufferObject->isDisposed())
    {
        glBindBufferARB(GL_ARRAY_BUFFER, 0);
        return;
    }
    
    glBindBufferARB(GL_ARRAY_BUFFER, static_cast<GLuint>(arrayBufferObject->mArrayBufferID));
}


void ArrayBufferObject::updateCurrentArrayBuffer(const void* data, unsigned int size,
                                                 unsigned int offset)
{
    GLsizei sizeGL = static_cast<GLsizei>(size);
    GLint offsetGL = static_cast<GLint>(offset);
    glBufferSubDataARB(GL_ARRAY_BUFFER, offsetGL, sizeGL, data);
}


}
