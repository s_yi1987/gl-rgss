#include "graphics/gltexture.hpp"
#include "graphics/texturesaver.hpp"
#include "system/exception.hpp"
#include "graphics/renderwindow.hpp"
#include "graphics/transformable.hpp"
#include "graphics/spritevertexdata.hpp"
#include "graphics/renderstates.hpp"
#include "system/lock.hpp"
#include "system/util.hpp"
#include <gl/glew.h>
#include <vector>
#include <cstdio>


namespace rgk
{
Uint64 Texture::sCacheID = 1;
Mutex Texture::sCacheMutex;


Uint64 Texture::createCacheID()
{
    Lock lock(sCacheMutex);
    return sCacheID++;
}

    
Texture::Texture() :
mTextureID(0),
mCacheID(createCacheID()),
mWidth(0), mHeight(0),
mActualWidth(0), mActualHeight(0),
mColorMod(255, 255, 255),
mBlendMode(BLEND_ALPHA),
mClipRect(),
mIsRepeated(false),
mIsSmooth(false),
mWindow(NULL)
{
    
}


Texture::Texture(const std::string& filename) : 
mTextureID(0),
mCacheID(createCacheID()),
mWidth(0), mHeight(0),
mActualWidth(0), mActualHeight(0),
mColorMod(255, 255, 255),
mBlendMode(BLEND_ALPHA),
mIsRepeated(false),
mIsSmooth(false),
mWindow(NULL)
{
    setup(filename);
}


Texture::Texture(const Surface& surface) : 
mTextureID(0),
mCacheID(createCacheID()),
mWidth(0), mHeight(0),
mActualWidth(0), mActualHeight(0),
mColorMod(255, 255, 255),
mBlendMode(BLEND_ALPHA),
mIsRepeated(false),
mIsSmooth(false),
mWindow(NULL)
{
    setup(surface);
}


Texture::Texture(int width, int height) :
mTextureID(0),
mCacheID(createCacheID()),
mWidth(0), mHeight(0),
mActualWidth(0), mActualHeight(0),
mColorMod(255, 255, 255),
mBlendMode(BLEND_ALPHA),
mIsRepeated(false),
mIsSmooth(false),
mWindow(NULL)
{
    setup(width, height);
}


Texture::Texture(const Texture& other) :
mTextureID(0),
mCacheID(createCacheID()),
mWidth(0), mHeight(0),
mActualWidth(0), mActualHeight(0),
mColorMod(255, 255, 255),
mBlendMode(BLEND_ALPHA),
mIsRepeated(false),
mIsSmooth(false),
mWindow(NULL)
{
    setup(other);
}


Texture& Texture::operator =(const Texture& other)
{
    setup(other);
    
    return *this;
}


void Texture::setup(const std::string& filename)
{
    Surface surface(filename, SDL_PIXELFORMAT_ARGB8888);
    setup(surface);
}


void Texture::setup(const Surface& surface)
{
    if(surface.getPixelFormatValue() != SDL_PIXELFORMAT_ARGB8888)
        throw Exception("A Surface with wrong PixelFormat was passed as an argument");
    
    setup(surface.getWidth(), surface.getHeight());
    update(surface);
}


void Texture::setup(int width, int height)
{
    util::verifyGlContext();
    
    int maximumSize = getMaximumSize();
    if(maximumSize < width || maximumSize < height)
        throw Exception("The dimension arguments exceed the maximum Texture size limit");
    
    if(!mCacheID)
        mCacheID = createCacheID();
    
    if(!mTextureID)
    {
        GLuint textureID;
        glGenTextures(1, &textureID);
        mTextureID = static_cast<unsigned int>(textureID);
    }
    
    mWidth  = width;
    mHeight = height;
    mActualWidth  = getValidSize(width);
    mActualHeight = getValidSize(height);
    
    TextureSaver save;
    
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(mTextureID));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    mIsSmooth ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    mIsSmooth ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                    mIsRepeated ? GL_REPEAT : GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                    mIsRepeated ? GL_REPEAT : GL_CLAMP_TO_EDGE);
    
    //Create the Texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, mActualWidth, mActualHeight,
                 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, NULL);
}


void Texture::setup(const Texture& other)
{
    if(!other.mTextureID)
        return;
    
    setup(other.toSurface());
    setSmooth(other.mIsSmooth);
    setRepeated(other.mIsRepeated);
}


void Texture::update(const Surface& surface, int x, int y)
{
    if(!mTextureID)
        return;
    else if(surface.getPixelFormatValue() != SDL_PIXELFORMAT_ARGB8888)
        throw Exception("A Surface with invalid PixelFormat was passed as an argument");
    
    upload(surface, x, y);
}


void Texture::upload(const Surface& surface, int x, int y)
{
    util::verifyGlContext();
    TextureSaver save;
    
    GLint oldUnpackAlignment, oldUnpackRowLength;
    glGetIntegerv(GL_UNPACK_ALIGNMENT, &oldUnpackAlignment);
    glGetIntegerv(GL_UNPACK_ROW_LENGTH, &oldUnpackRowLength);
    
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(mTextureID));
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, surface.getWidth());
                  //surface.get()->pitch / surface.get()->format->BytesPerPixel);
    glTexSubImage2D(GL_TEXTURE_2D, 0, x, y,
                    surface.getWidth(), surface.getHeight(),
                    GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,
                    surface.get()->pixels);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, oldUnpackAlignment);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, oldUnpackRowLength);
}


Texture::~Texture()
{
    dispose();
}


void Texture::dispose()
{
    if(mTextureID)
    {
        if(!util::hasActiveGlContext())
            std::puts("~Texture() - No active GL Context detected");
        else
        {
            GLuint textureID = static_cast<GLuint>(mTextureID);
            glDeleteTextures(1, &textureID);
        }
        mCacheID   = 0;
        mTextureID = 0;

        mWidth  = 0;
        mHeight = 0;
        mActualWidth  = 0;
        mActualHeight = 0;

        mIsRepeated = false;
        mIsSmooth   = false;
    }
}


bool Texture::isDisposed() const
{
    if(mTextureID)
        return false;
    
    return true;
}


void Texture::setColorMod(const Color& color)
{
    mColorMod.r = util::ensureWithinColorRange(color.r);
    mColorMod.g = util::ensureWithinColorRange(color.g);
    mColorMod.b = util::ensureWithinColorRange(color.b);
}


Color Texture::getColorMod() const
{
    return mColorMod;
}


void Texture::setAlphaMod(int alphaMod)
{
    mColorMod.a = static_cast<double>(util::ensureWithinColorRange(alphaMod));
}


int Texture::getAlphaMod() const
{
    return static_cast<int>(mColorMod.a);
}


void Texture::setBlendMode(BlendMode blendMode)
{
    mBlendMode = blendMode;
}


BlendMode Texture::getBlendMode() const
{
    return mBlendMode;
}


void Texture::setClipRect(const Rect* rect)
{
    if(!rect)
        mClipRect.empty();
    else
        mClipRect = *rect;
}


Rect Texture::getClipRect() const
{
    return mClipRect;
}


void Texture::blit(const Texture& src, const Rect* srcRect, const Rect* dstRect)
{
    blitMain(src, srcRect, dstRect, false);
}


void Texture::blitScaled(const Texture& src, const Rect* srcRect, const Rect* dstRect)
{
    blitMain(src, srcRect, dstRect, true);
}


void Texture::blitMain(const Texture& src, const Rect* srcRect, const Rect* dstRect,
                       bool scaled)
{
    if(!mWindow || !mTextureID)
        return;
    
    Rect srcRectFinal = srcRect ? *srcRect : src.getRect();
    src.adjustBlitRect(srcRectFinal);
    
    Rect dstRectFinal = dstRect ? *dstRect : getRect();
    adjustBlitRect(dstRectFinal);
    
    if(srcRectFinal.width <= 0 || srcRectFinal.height <= 0 ||
       dstRectFinal.width <= 0 || dstRectFinal.height <= 0)
        return;
    
    float srcWidth  = static_cast<float>(srcRectFinal.width);
    float srcHeight = static_cast<float>(srcRectFinal.height);
    
    float dstWidth  = static_cast<float>(dstRectFinal.width);
    float dstHeight = static_cast<float>(dstRectFinal.height);
    
    float scaleFactorX = scaled ? dstWidth / srcWidth   : 1.f;
    float scaleFactorY = scaled ? dstHeight / srcHeight : 1.f;
    
    float posX = static_cast<float>(dstRectFinal.x);
    float posY = static_cast<float>(dstRectFinal.y);
    
    Transformable dstTransformable;
    dstTransformable.setPosition(posX, posY);
    dstTransformable.setScale(scaleFactorX, scaleFactorY);
    
    SpriteVertexData srcVertices(srcRect ? *srcRect : src.getRect());
    srcVertices.setColor(src.mColorMod);
    
    Texture* oldRenderTarget = mWindow->getRenderTarget();
    const View& oldView = mWindow->getView();
    Rect oldClipRect = mWindow->getClipRect();
    
    mWindow->setRenderTarget(this);
    mWindow->setClipRect(&mClipRect);
    
    RenderStates states(src.mBlendMode, dstTransformable.getTransform(), &src, NULL);
    mWindow->draw(srcVertices, states);
    
    mWindow->setRenderTarget(oldRenderTarget);
    mWindow->setView(oldView);
    mWindow->setClipRect(&oldClipRect);
}


void Texture::adjustBlitRect(Rect& rect) const
{
    int width  = mWidth;
    int height = mHeight;
    
    if(rect.x < 0)
    {
        rect.width += rect.x;
        rect.x = 0;
    }
    
    if(rect.y < 0)
    {
        rect.height += rect.y;
        rect.y = 0;
    }

    int maxWidth  = width  - rect.x;
    int maxHeight = height - rect.y;
    
    if(rect.width  > maxWidth)  rect.width  = maxWidth;
    if(rect.height > maxHeight) rect.height = maxHeight;
}


void Texture::fillRect(const Rect* rect, const Color& color)
{
    if(!mWindow || !mTextureID)
        return;
    
    Rect fillRect = rect ? *rect : getRect();
    Texture* oldRenderTarget = mWindow->getRenderTarget();
    const View& oldView = mWindow->getView();
    
    SpriteVertexData rectVertices(Rect(0, 0, fillRect.width, fillRect.height));
    rectVertices.setColor(color);
    Transform transform;
    transform.translate(fillRect.x, fillRect.y);
    
    mWindow->setRenderTarget(this);
    
    RenderStates states(BLEND_NONE, transform, NULL, NULL);
    mWindow->draw(rectVertices, states);
    
    mWindow->setRenderTarget(oldRenderTarget);
    mWindow->setView(oldView);
}


void Texture::fillRect(const Rect* rect, Uint32 pixel)
{
    fillRect(rect, Color(pixel, SDL_PIXELFORMAT_ARGB8888));
}


int Texture::getWidth() const
{
    return mWidth;
}


int Texture::getHeight() const
{
    return mHeight;
}


int Texture::getActualWidth() const
{
    return mActualWidth;
}


int Texture::getActualHeight() const
{
    return mActualHeight;
}


Rect Texture::getRect() const
{
    return Rect(0, 0, getWidth(), getHeight());
}


void Texture::setSmooth(bool smooth)
{
    if(!mTextureID || mIsSmooth == smooth)
        return;
    
    util::verifyGlContext();
    mIsSmooth = smooth;
    
    TextureSaver save;
    
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(mTextureID));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    mIsSmooth ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    mIsSmooth ? GL_LINEAR : GL_NEAREST);
}


bool Texture::isSmooth() const
{
    return mIsSmooth;
}


void Texture::setRepeated(bool repeated)
{
    if(!mTextureID || mIsRepeated == repeated)
        return;
    
    util::verifyGlContext();
    mIsRepeated = repeated;
    
    TextureSaver save;
    
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(mTextureID));
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                    mIsRepeated ? GL_REPEAT : GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                    mIsRepeated ? GL_REPEAT : GL_CLAMP_TO_EDGE);
}


bool Texture::isRepeated() const
{
    return mIsRepeated;
}


Surface Texture::toSurface() const
{
    Surface surface;
    if(mTextureID)
    {
        surface.setup(mWidth, mHeight, SDL_PIXELFORMAT_ARGB8888);
        downloadTo(surface);
    }
    
    return surface;
}


void Texture::downloadTo(Surface& surface) const
{
    if(!mTextureID || surface.getRect() != getRect())
        return;
    
    util::verifyGlContext();
    
    TextureSaver save;
    
    GLint oldPackAlignment, oldPackRowLength;
    glGetIntegerv(GL_PACK_ALIGNMENT,  &oldPackAlignment);
    glGetIntegerv(GL_PACK_ROW_LENGTH, &oldPackRowLength);
    
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(mTextureID));
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ROW_LENGTH, mActualWidth);
    
    Uint8* dst = reinterpret_cast<Uint8*>(surface.get()->pixels);
    if(mActualWidth == mWidth && mActualHeight == mHeight)
    {
        glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, dst);
    }
    
    else
    {
        std::vector<Uint8> allPixels(mActualWidth * mActualHeight * 4);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,
                      &allPixels[0]);
        
        const Uint8* src = &allPixels[0];
        int srcPitch = mActualWidth * 4;
        int dstPitch = mWidth * 4;

        for(int i = 0; i < mHeight; ++i)
        {
            SDL_memcpy(dst, src, dstPitch);
            src += srcPitch;
            dst += dstPitch;
        }
    }
    
    glPixelStorei(GL_PACK_ALIGNMENT,  oldPackAlignment);
    glPixelStorei(GL_PACK_ROW_LENGTH, oldPackRowLength);
}


void Texture::setWindow(RenderWindow* window)
{
    mWindow = window;
}


unsigned int Texture::getTextureID() const
{
    return mTextureID;
}


Uint64 Texture::getCacheID() const
{
    return mCacheID;
}


void Texture::bind(const Texture* texture)
{
    util::verifyGlContext();
    
    if(!texture || !texture->mTextureID)
    {
        glBindTexture(GL_TEXTURE_2D, 0);
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
        
        return;
    }
    
    glBindTexture(GL_TEXTURE_2D, static_cast<GLuint>(texture->mTextureID));
    GLfloat matrix[16] = {1.f, 0.f, 0.f, 0.f,
                          0.f, 1.f, 0.f, 0.f,
                          0.f, 0.f, 1.f, 0.f,
                          0.f, 0.f, 0.f, 1.f};
    matrix[0] = 1.f / static_cast<GLfloat>(texture->mActualWidth);
    matrix[5] = 1.f / static_cast<GLfloat>(texture->mActualHeight);
    
    glMatrixMode(GL_TEXTURE);
    glLoadMatrixf(matrix);
    glMatrixMode(GL_MODELVIEW);
}


int Texture::getValidSize(int size)
{
    if(size > 0)
    {
        size--;
        size |= (size >> 1);
        size |= (size >> 2);
        size |= (size >> 4);
        size |= (size >> 8);
        size |= (size >> 16);
        size++;
    }
    else
        size = 0;

    return size;
}


int Texture::getMaximumSize()
{
    util::verifyGlContext();
    
    GLint size;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &size);

    return static_cast<int>(size);
}


}