#include "graphics/tilevertexarray.hpp"
#include <cmath>


namespace rgk
{
    
TileVertexArray::TileVertexArray() :
mVertexData()
{

}


TileVertexArray::~TileVertexArray()
{
    
}


void TileVertexArray::append(const TileVertexArray& other, const Transform& transform)
{
    unsigned int indexStart = getVertexCount();
    unsigned int loopAmount = other.getIndexCount();
    for(unsigned int i = 0; i < loopAmount; ++i)
        mVertexData.appendIndex(indexStart + other.mVertexData.getIndex(i));
    
    loopAmount = other.getVertexCount();
    for(unsigned int i = 0; i < loopAmount; ++i)
        mVertexData.appendVertex(other.mVertexData.getVertex(i), transform);
}


void TileVertexArray::append(const VertexData& vertexData, const Transform& transform)
{
    unsigned int vertexCount = vertexData.getVertexCount();
    PrimitiveType primitiveType = vertexData.getPrimitiveType();
    
    if(primitiveType == QUADS && vertexCount >= 4)
        appendQuads(vertexData, transform);
    else if(primitiveType == TRIANGLES_STRIP && vertexCount >= 4)
        appendTriangleStrips(vertexData, transform);
}


void TileVertexArray::appendQuads(const VertexData& vertexData, const Transform& transform)
{
    unsigned int offset = 0;
    unsigned int loopAmount = vertexData.getVertexCount() / 4;
    const Vertex* quadVertices = vertexData.getVertices();
    
    Vertex vertices[4];
    unsigned int indices[6];
    
    for(unsigned int i = 0; i < loopAmount; ++i)
    {
        unsigned int indexStart = getVertexCount();
        indices[0] = indexStart;     indices[1] = indexStart + 1; indices[2] = indexStart + 2;
        indices[3] = indexStart + 2; indices[4] = indexStart + 3; indices[5] = indexStart;
        
        for(unsigned int j = 0; j < 6; ++j)
            mVertexData.appendIndex(indices[j]);
        
        vertices[0] = quadVertices[offset];
        vertices[1] = quadVertices[offset + 1];
        vertices[2] = quadVertices[offset + 2];
        vertices[3] = quadVertices[offset + 3];

        for(unsigned int j = 0; j < 4; ++j)
            mVertexData.appendVertex(vertices[j], transform);
        
        offset += 4;
    }
}


void TileVertexArray::appendTriangleStrips(const VertexData& vertexData, const Transform& transform)
{
    unsigned int indexStart = getVertexCount();
    unsigned int indices[]  = {indexStart, indexStart + 1, indexStart + 2,
                               indexStart + 2, indexStart + 3, indexStart};
    for(unsigned int i = 0; i < 6; ++i)
        mVertexData.appendIndex(indices[i]);
    
    const Vertex* triStripVertices = vertexData.getVertices();
    Vertex vertices[] = {triStripVertices[0], triStripVertices[1],
                         triStripVertices[2], triStripVertices[3]};
    
    Vector2f rightTopPos       = vertices[2].position;
    Vector2f rightTopTexCoords = vertices[2].texCoords;

    vertices[2].position  = vertices[3].position;
    vertices[2].texCoords = vertices[3].texCoords;
    
    vertices[3].position  = rightTopPos;
    vertices[3].texCoords = rightTopTexCoords;
    for(unsigned int i = 0; i < 4; ++i)
        mVertexData.appendVertex(vertices[i], transform);
    
    unsigned int offset = 4;
    unsigned int loopAmount = (vertexData.getVertexCount() - 4) / 2;
    for(unsigned int i = 0; i < loopAmount; ++i)
    {
        indexStart = getVertexCount();
        indices[0] = indexStart;     indices[1] = indexStart + 1; indices[2] = indexStart + 2;
        indices[3] = indexStart + 2; indices[4] = indexStart + 3; indices[5] = indexStart;
        
        for(unsigned int j = 0; j < 6; ++j)
            mVertexData.appendIndex(indices[j]);
        
        vertices[0] = mVertexData.getVertex(indexStart - 1); //left top
        vertices[1] = mVertexData.getVertex(indexStart - 2); //left bottom
        vertices[2] = triStripVertices[offset];              //right bottom
        vertices[3] = triStripVertices[offset + 1];          //right top
        
        rightTopPos       = vertices[2].position;
        rightTopTexCoords = vertices[2].texCoords;
        
        vertices[2].position  = transform * vertices[3].position;
        vertices[2].texCoords = vertices[3].texCoords;
        vertices[3].position  = transform * rightTopPos;
        vertices[3].texCoords = rightTopTexCoords;
        
        for(unsigned int j = 0; j < 4; ++j)
            mVertexData.appendVertex(vertices[j]);
        
        offset += 2;
    }
}


void TileVertexArray::append(const Rect& textureRect, const Transform& transform)
{
    unsigned int indexStart = getVertexCount();
    unsigned int indices[]  = {indexStart, indexStart + 1, indexStart + 2,
                               indexStart + 2, indexStart + 3, indexStart};
    for(unsigned int i = 0; i < 6; ++i)
        mVertexData.appendIndex(indices[i]);
    
    Vertex vertices[4];
    FloatRect bounds(0, 0,
                     static_cast<float>(std::abs(textureRect.width)),
                     static_cast<float>(std::abs(textureRect.height)));
    
    vertices[0].position = transform * Vector2f(0, 0);
    vertices[1].position = transform * Vector2f(0, bounds.height);
    vertices[2].position = transform * Vector2f(bounds.width, bounds.height);
    vertices[3].position = transform * Vector2f(bounds.width, 0);
    
    float left   = static_cast<float>(textureRect.x);
    float right  = left + textureRect.width;
    float top    = static_cast<float>(textureRect.y);
    float bottom = top + textureRect.height;

    vertices[0].texCoords = Vector2f(left, top);
    vertices[1].texCoords = Vector2f(left, bottom);
    vertices[2].texCoords = Vector2f(right, bottom);
    vertices[3].texCoords = Vector2f(right, top);
    
    for(unsigned int i = 0; i < 4; ++i)
        mVertexData.appendVertex(vertices[i]);
}


void TileVertexArray::clear()
{
    mVertexData.clear();
}


const Vertex* TileVertexArray::getVertices() const
{
    return mVertexData.getVertices();
}


const unsigned int* TileVertexArray::getIndices() const
{
    return mVertexData.getIndices();
}


unsigned int TileVertexArray::getVertexCount() const
{
    return mVertexData.getVertexCount();
}


unsigned int TileVertexArray::getIndexCount() const
{
    return mVertexData.getIndexCount();
}


PrimitiveType TileVertexArray::getPrimitiveType() const
{
    return TRIANGLES;
}

}