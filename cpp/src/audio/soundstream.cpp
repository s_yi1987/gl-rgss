#include "audio/soundstream.hpp"
#include "system/util.hpp"
#include "system/exception.hpp"
#include <SDL2/SDL_timer.h>
#include <al.h>
#include <alc.h>


namespace rgk
{
    
    
SoundStream::SoundStream() :
mThread          (streamData, this),
mIsStreaming     (false),
mChannelCount    (0),
mSampleRate      (0),
mFormat          (0),
mLoop            (false),
mSamplesProcessed(0)
{

}


SoundStream::~SoundStream()
{
    // Stop the sound if it was playing
    stop();
}


void SoundStream::initialize(unsigned int channelCount, unsigned int sampleRate)
{
    mChannelCount = channelCount;
    mSampleRate   = sampleRate;

    // Deduce the format from the number of channels
    mFormat = util::getFormatFromChannelCount(channelCount);

    // Check if the format is valid
    if(mFormat == 0)
    {
        mChannelCount = 0;
        mSampleRate   = 0;
        throw Exception("Unsupported number of channels");
    }
}


void SoundStream::play()
{
    // Check if the sound parameters have been set
    if(mFormat == 0)
    {
        //err() << "Failed to play audio stream: sound parameters have not been initialized (call Initialize first)" << std::endl;
        return;
    }

    // If the sound is already playing (probably paused), just resume it
    if(mIsStreaming)
    {
        alSourcePlay(mSource);
        return;
    }

    // Move to the beginning
    onSeek(Time::ZERO);

    // Start updating the stream in a separate thread to avoid blocking the application
    mSamplesProcessed = 0;
    mIsStreaming = true;
    mThread.launch();
}


void SoundStream::pause()
{
    alSourcePause(mSource);
}


void SoundStream::stop()
{
    // Wait for the thread to terminate
    mIsStreaming = false;
    mThread.wait();
}


unsigned int SoundStream::getChannelCount() const
{
    return mChannelCount;
}


unsigned int SoundStream::getSampleRate() const
{
    return mSampleRate;
}


SoundStatus SoundStream::getStatus() const
{
    SoundStatus status = getStatusSource();

    // To compensate for the lag between play() and alSourceplay()
    if((status == SOUNDSTATUS_STOPPED) && mIsStreaming)
        status = SOUNDSTATUS_PLAYING;

    return status;
}


SoundStatus SoundStream::getStatusSource() const
{
    return SoundSource::getStatus();
}


void SoundStream::setPlayingOffset(Time timeOffset)
{
    // Stop the stream
    stop();

    // Let the derived class update the current position
    onSeek(timeOffset);

    // Restart streaming
    mSamplesProcessed = static_cast<Uint64>(timeOffset.asSeconds() * mSampleRate * mChannelCount);
    mIsStreaming = true;
    mThread.launch();
}


Time SoundStream::getPlayingOffset() const
{
    if (mSampleRate && mChannelCount)
    {
        ALfloat secs = 0.f;
        alGetSourcef(mSource, AL_SEC_OFFSET, &secs);

        return Time::seconds(secs + static_cast<float>(mSamplesProcessed) / mSampleRate / mChannelCount);
    }
    
    else
    {
        return Time::ZERO;
    }
}


void SoundStream::setLoop(bool loop)
{
    mLoop = loop;
}


bool SoundStream::getLoop() const
{
    return mLoop;
}


int SoundStream::streamData(void* soundStreamVoid)
{
    SoundStream* soundStream = reinterpret_cast<SoundStream*>(soundStreamVoid);
    
    // Create the buffers
    alGenBuffers(BUFFER_COUNT, soundStream->mBuffers);
    for(int i = 0; i < BUFFER_COUNT; ++i)
        soundStream->mEndBuffers[i] = false;

    // Fill the queue
    bool requestStop = soundStream->fillQueue();

    // Play the sound
    alSourcePlay(soundStream->mSource);

    while(soundStream->mIsStreaming)
    {
        // The stream has been interrupted!
        if(soundStream->getStatusSource() == SOUNDSTATUS_STOPPED)
        {
            if(!requestStop)
            {
                // Just continue
                alSourcePlay(soundStream->mSource);
            }
            
            else
            {
                // End streaming
                soundStream->mIsStreaming = false;
            }
        }

        // Get the number of buffers that have been processed (ie. ready for reuse)
        ALint nbProcessed = 0;
        alGetSourcei(soundStream->mSource, AL_BUFFERS_PROCESSED, &nbProcessed);

        while(nbProcessed--)
        {
            // Pop the first unused buffer from the queue
            ALuint buffer;
            alSourceUnqueueBuffers(soundStream->mSource, 1, &buffer);

            // Find its number
            unsigned int bufferNum = 0;
            for(int i = 0; i < BUFFER_COUNT; ++i)
            {
                if(soundStream->mBuffers[i] == buffer)
                {
                    bufferNum = i;
                    break;
                }
            }

            // Retrieve its size and add it to the samples count
            if(soundStream->mEndBuffers[bufferNum])
            {
                // This was the last buffer: reset the sample count
                soundStream->mSamplesProcessed = 0;
                soundStream->mEndBuffers[bufferNum] = false;
            }
            
            else
            {
                ALint size, bits;
                alGetBufferi(buffer, AL_SIZE, &size);
                alGetBufferi(buffer, AL_BITS, &bits);
                soundStream->mSamplesProcessed += size / (bits / 8);
            }

            // Fill it and push it back into the playing queue
            if(!requestStop)
            {
                if(soundStream->fillAndPushBuffer(bufferNum))
                    requestStop = true;
            }
        }

        // Leave some time for the other threads if the stream is still playing
        if(soundStream->getStatusSource() != SOUNDSTATUS_STOPPED)
            SDL_Delay(static_cast<Uint32>(Time::milliseconds(10).asMilliseconds()));
    }

    // Stop the playback
    alSourceStop(soundStream->mSource);

    // Unqueue any buffer left in the queue
    soundStream->clearQueue();

    // Delete the buffers
    alSourcei(soundStream->mSource, AL_BUFFER, 0);
    alDeleteBuffers(BUFFER_COUNT, soundStream->mBuffers);
    
    return 0;
}


bool SoundStream::fillAndPushBuffer(unsigned int bufferNum)
{
    bool requestStop = false;

    // Acquire audio data
    SoundChunk data = {NULL, 0};
    if(!onGetData(data))
    {
        // Mark the buffer as the last one (so that we know when to reset the playing position)
        mEndBuffers[bufferNum] = true;

        // Check if the stream must loop or stop
        if(mLoop)
        {
            // Return to the beginning of the stream source
            onSeek(Time::ZERO);

            // If we previously had no data, try to fill the buffer once again
            if(!data.samples || (data.sampleCount == 0))
            {
                return fillAndPushBuffer(bufferNum);
            }
        }
        
        else
        {
            // Not looping: request stop
            requestStop = true;
        }
    }

    // Fill the buffer if some data was returned
    if(data.samples && data.sampleCount)
    {
        unsigned int buffer = mBuffers[bufferNum];

        // Fill the buffer
        ALsizei size = static_cast<ALsizei>(data.sampleCount) * sizeof(Sint16);
        alBufferData(buffer, mFormat, data.samples, size, mSampleRate);

        // Push it into the sound queue
        alSourceQueueBuffers(mSource, 1, &buffer);
    }

    return requestStop;
}


bool SoundStream::fillQueue()
{
    // Fill and enqueue all the available buffers
    bool requestStop = false;
    for(int i = 0; (i < BUFFER_COUNT) && !requestStop; ++i)
    {
        if(fillAndPushBuffer(i))
            requestStop = true;
    }

    return requestStop;
}


void SoundStream::clearQueue()
{
    // Get the number of buffers still in the queue
    ALint nbQueued;
    alGetSourcei(mSource, AL_BUFFERS_QUEUED, &nbQueued);

    // Unqueue them all
    ALuint buffer;
    for(ALint i = 0; i < nbQueued; ++i)
        alSourceUnqueueBuffers(mSource, 1, &buffer);
}
    
    
}