#include "audio/soundsource.hpp"
#include <al.h>
#include <alc.h>


namespace rgk
{
    
    
SoundSource::SoundSource() :
mSource(0)
{
    alGenSources(1, &mSource);
    alSourcei(mSource, AL_BUFFER, 0);
}


SoundSource::~SoundSource()
{
    alSourcei(mSource, AL_BUFFER, 0);
    alDeleteSources(1, &mSource);
}


void SoundSource::setPitch(float pitch)
{
    alSourcef(mSource, AL_PITCH, pitch);
}


void SoundSource::setVolume(float volume)
{
    alSourcef(mSource, AL_GAIN, volume * 0.01f);
}


void SoundSource::setPosition(float x, float y, float z)
{
    alSource3f(mSource, AL_POSITION, x, y, z);
}


void SoundSource::setPosition(const Vector3f& position)
{
    setPosition(position.x, position.y, position.z);
}


void SoundSource::setRelativeToListener(bool relative)
{
    alSourcei(mSource, AL_SOURCE_RELATIVE, relative);
}


void SoundSource::setMinDistance(float distance)
{
    alSourcef(mSource, AL_REFERENCE_DISTANCE, distance);
}


void SoundSource::setAttenuation(float attenuation)
{
    alSourcef(mSource, AL_ROLLOFF_FACTOR, attenuation);
}


float SoundSource::getPitch() const
{
    ALfloat pitch;
    alGetSourcef(mSource, AL_PITCH, &pitch);

    return pitch;
}


float SoundSource::getVolume() const
{
    ALfloat gain;
    alGetSourcef(mSource, AL_GAIN, &gain);

    return gain * 100.f;
}


Vector3f SoundSource::getPosition() const
{
    Vector3f position;
    alGetSource3f(mSource, AL_POSITION, &position.x, &position.y, &position.z);

    return position;
}


bool SoundSource::isRelativeToListener() const
{
    ALint relative;
    alGetSourcei(mSource, AL_SOURCE_RELATIVE, &relative);

    return relative != 0;
}


float SoundSource::getMinDistance() const
{
    ALfloat distance;
    alGetSourcef(mSource, AL_REFERENCE_DISTANCE, &distance);

    return distance;
}


float SoundSource::getAttenuation() const
{
    ALfloat attenuation;
    alGetSourcef(mSource, AL_ROLLOFF_FACTOR, &attenuation);

    return attenuation;
}


SoundStatus SoundSource::getStatus() const
{
    ALint status;
    alGetSourcei(mSource, AL_SOURCE_STATE, &status);

    switch (status)
    {
        case AL_INITIAL :
        case AL_STOPPED : return SOUNDSTATUS_STOPPED;
        case AL_PAUSED  : return SOUNDSTATUS_PAUSED;
        case AL_PLAYING : return SOUNDSTATUS_PLAYING;
    }

    return SOUNDSTATUS_STOPPED;
}

    
}
