#include "window/window.hpp"
#include "system/exception.hpp"
#include "system/util.hpp"


namespace rgk
{

Window::Window(const std::string& title, int x, int y, int width, int height,
               Uint32 flags) :
mWindowSDL(NULL),
mSize(0, 0, width, height)
{
    mWindowSDL = createWindow(title, x, y, width, height, flags);
}


SDL_Window* Window::createWindow(const std::string& title, int x, int y,
                                 int width, int height, Uint32 flags)
{
    SDL_Window* windowSDL = SDL_CreateWindow(title.c_str(), x, y,
                                             width, height, flags);
    if(!windowSDL)
        throw Exception(std::string("Failed to create Window: ") 
            += SDL_GetError());
    
    return windowSDL;
}


Window::~Window()
{
    dispose();
}


void Window::dispose()
{
    if(mWindowSDL)
    {
        SDL_DestroyWindow(mWindowSDL);
        mWindowSDL = NULL;
        mSize.empty();
    }
}


bool Window::isDisposed() const
{
    return mWindowSDL ? false : true;
}


void Window::setPosition(int x, int y)
{
    if(!mWindowSDL || x < 0 || y < 0)
        return;
    
    SDL_SetWindowPosition(mWindowSDL, x, y);
}


Rect Window::getPosition() const
{
    if(!mWindowSDL)
        return Rect(0, 0, 0, 0);
    
    int x = 0, y = 0;
    SDL_GetWindowPosition(mWindowSDL, &x, &y);
    
    return Rect(x, y, 0, 0);
}


void Window::setSize(int w, int h)
{
    if(!mWindowSDL || w < 1 || h < 1)
        return;
    
    mSize.set(0, 0, w, h);
    SDL_SetWindowSize(mWindowSDL, w, h);
}


const Rect& Window::getSize() const
{
    if(mWindowSDL)
    {
        int w, h;
        SDL_GetWindowSize(mWindowSDL, &w, &h);
        mSize.set(0, 0, w, h);
    }
    return mSize;
}


int Window::setFullscreenMode(Uint32 flag)
{
    if(!mWindowSDL)
        return -1;
    
    return SDL_SetWindowFullscreen(mWindowSDL, flag);
}


Uint32 Window::getFullscreenMode() const
{
    Uint32 flags = getFlags();
    if((flags & SDL_WINDOW_FULLSCREEN) == SDL_WINDOW_FULLSCREEN)
        return SDL_WINDOW_FULLSCREEN;
    else if((flags & SDL_WINDOW_FULLSCREEN_DESKTOP) == SDL_WINDOW_FULLSCREEN_DESKTOP)
        return SDL_WINDOW_FULLSCREEN;
    else
        return 0;
}


Rect Window::getRect() const
{
    Rect posRect  = getPosition();
    Rect sizeRect = getSize();
    Rect rect(0, 0, 0, 0);
    rect.x = posRect.x;
    rect.y = posRect.y;
    rect.width = sizeRect.width;
    rect.height = sizeRect.height;
    
    return rect;
}


void Window::setBordered(bool bordered)
{
    if(!!mWindowSDL)
        return;
    
    SDL_bool boolSDL = bordered ? SDL_TRUE : SDL_FALSE;
    SDL_SetWindowBordered(mWindowSDL, boolSDL);
}

bool Window::isBordered() const
{
    if(!mWindowSDL)
        return false;
    
    Uint32 flags = getFlags();
    return !((flags & SDL_WINDOW_BORDERLESS) == SDL_WINDOW_BORDERLESS);
}


void Window::setGrabMode(bool grabbed)
{
    if(!mWindowSDL)
        return;
    
    SDL_bool boolSDL = grabbed ? SDL_TRUE : SDL_FALSE;
    SDL_SetWindowGrab(mWindowSDL, boolSDL);
}


bool Window::isInGrabMode() const
{
    if(!mWindowSDL)
        return false;
    
    SDL_bool boolSDL = SDL_GetWindowGrab(mWindowSDL);
    bool grabbed = (boolSDL == SDL_TRUE) ? true : false;
    return grabbed;
}


int Window::setBrightness(float brightness)
{
    if(!mWindowSDL)
        return -1;
    
    brightness = util::ensureWithinRange(brightness, 0.0, 1.0);
    return SDL_SetWindowBrightness(mWindowSDL, brightness);
}


float Window::getBrightness() const
{
    if(!mWindowSDL)
        return 0.f;
    
    return SDL_GetWindowBrightness(mWindowSDL);
}


void Window::setTitle(const std::string& title)
{
    if(!mWindowSDL)
        return;
    
    SDL_SetWindowTitle(mWindowSDL, title.c_str());
}


std::string Window::getTitle() const
{
    if(!mWindowSDL)
        return "";
    
    return std::string(SDL_GetWindowTitle(mWindowSDL));
}


Uint32 Window::getWindowID() const
{
    return mWindowSDL ? SDL_GetWindowID(mWindowSDL) : 0;
}


Uint32 Window::getFlags() const
{
    if(!mWindowSDL)
        return 0;
    
    return SDL_GetWindowFlags(mWindowSDL);
}


SDL_Window* Window::get() const
{
    return mWindowSDL;
}


}
