#include "window/event.hpp"

namespace rgk
{

bool pollEvent(Event& event)
{
    if(SDL_PollEvent(&event) > 0)
    {
        return true;
    }
    
    return false;
}

}