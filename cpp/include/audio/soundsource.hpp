/* 
 * File:   soundsource.hpp
 * Author: M02A706938
 *
 * Created on den 24 september 2014, 09:55
 */

#ifndef SOUNDSOURCE_HPP
#define	SOUNDSOURCE_HPP

#include "system/vector3.hpp"


namespace rgk
{
    
enum SoundStatus
{
    SOUNDSTATUS_STOPPED, ///< Sound is not playing
    SOUNDSTATUS_PAUSED,  ///< Sound is paused
    SOUNDSTATUS_PLAYING  ///< Sound is playing
};
    
class SoundSource
{
    
public:
    
    SoundSource(const SoundSource& other);
    
    virtual ~SoundSource();

    void setPitch(float pitch);

    void setVolume(float volume);

    void setPosition(float x, float y, float z);

    void setPosition(const Vector3f& position);

    void setRelativeToListener(bool relative);

    void setMinDistance(float distance);

    void setAttenuation(float attenuation);

    float getPitch() const;

    float getVolume() const;

    Vector3f getPosition() const;

    bool isRelativeToListener() const;

    float getMinDistance() const;

    float getAttenuation() const;

protected :

    SoundSource();

    SoundStatus getStatus() const;


    unsigned int mSource; ///< OpenAL source identifier
    
};
    
}

#endif	/* SOUNDSOURCE_HPP */

