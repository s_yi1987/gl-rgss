/* 
 * File:   soundstream.hpp
 * Author: M02A706938
 *
 * Created on den 18 oktober 2014, 16:43
 */

#ifndef SOUNDSTREAM_HPP
#define	SOUNDSTREAM_HPP

#include "audio/soundsource.hpp"
#include "system/time.hpp"
#include "system/thread.hpp"
#include <SDL2/SDL_stdinc.h>


namespace rgk
{
    
    
struct SoundChunk
{
    const Sint16* samples;    ///< Pointer to the audio samples
    std::size_t  sampleCount; ///< Number of samples pointed by Samples
};

    
class SoundStream : SoundSource
{
public:
    
    virtual ~SoundStream();

    void play();

    void pause();

    void stop();

    unsigned int getChannelCount() const;

    unsigned int getSampleRate() const;

    SoundStatus getStatus() const;

    void setPlayingOffset(Time timeOffset);

    Time getPlayingOffset() const;

    void setLoop(bool loop);

    bool getLoop() const;
    
protected :

    SoundStream();

    void initialize(unsigned int channelCount, unsigned int sampleRate);

    virtual bool onGetData(SoundChunk& data) = 0;

    virtual void onSeek(Time timeOffset) = 0;
    
private :

    bool fillAndPushBuffer(unsigned int bufferNum);

    bool fillQueue();

    void clearQueue();
    
    SoundStatus getStatusSource() const;
    
    static int streamData(void* soundStreamVoid);

    enum
    {
        BUFFER_COUNT = 3 ///< Number of audio buffers used by the streaming loop
    };


    Thread        mThread;                   ///< Thread running the background tasks
    bool          mIsStreaming;              ///< Streaming state (true = playing, false = stopped)
    unsigned int  mBuffers[BUFFER_COUNT];    ///< Sound buffers used to store temporary audio data
    unsigned int  mChannelCount;             ///< Number of channels (1 = mono, 2 = stereo, ...)
    unsigned int  mSampleRate;               ///< Frequency (samples / second)
    Uint32        mFormat;                   ///< Format of the internal sound buffers
    bool          mLoop;                     ///< Loop flag (true to loop, false to play once)
    Uint64        mSamplesProcessed;         ///< Number of buffers processed since beginning of the stream
    bool          mEndBuffers[BUFFER_COUNT]; ///< Each buffer is marked as "end buffer" or not, for proper duration calculation

};


}


#endif	/* SOUNDSTREAM_HPP */

