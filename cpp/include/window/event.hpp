/* 
 * File:   event.hpp
 * Author: Samuel - Dell
 *
 * Created on den 26 februari 2014, 19:29
 */

#ifndef EVENT_HPP
#define	EVENT_HPP

#include "scancode.hpp"
#include "keycode.hpp"
#include "eventtype.hpp"
#include <SDL2/SDL_events.h>
#include <string>


namespace rgk
{
    
typedef SDL_JoystickID JoystickID;
typedef SDL_TouchID TouchID;
typedef SDL_FingerID FingerID;
typedef SDL_GestureID GestureID;

typedef SDL_Event Event;

typedef SDL_CommonEvent CommonEvent;
typedef SDL_WindowEvent WindowEvent;
typedef SDL_Keysym Keysym;
typedef SDL_KeyboardEvent KeyboardEvent;
typedef SDL_TextEditingEvent TextEditingEvent;
typedef SDL_TextInputEvent TextInputEvent;
typedef SDL_MouseMotionEvent MouseMotionEvent;
typedef SDL_MouseButtonEvent MouseButtonEvent;
typedef SDL_MouseWheelEvent MouseWheelEvent;
typedef SDL_JoyAxisEvent JoyAxisEvent;
typedef SDL_JoyBallEvent JoyBallEvent;
typedef SDL_JoyHatEvent JoyHatEvent;
typedef SDL_JoyButtonEvent JoyButtonEvent;
typedef SDL_JoyDeviceEvent JoyDeviceEvent;
typedef SDL_ControllerAxisEvent ControllerAxisEvent;
typedef SDL_ControllerButtonEvent ControllerButtonEvent;
typedef SDL_ControllerDeviceEvent ControllerDeviceEvent;
typedef SDL_QuitEvent QuitEvent;
typedef SDL_UserEvent UserEvent;
typedef SDL_TouchFingerEvent TouchFingerEvent;
typedef SDL_MultiGestureEvent MultiGestureEvent;
typedef SDL_DollarGestureEvent DollarGestureEvent;

bool pollEvent(Event& event);

}

#endif	/* EVENT_HPP */

