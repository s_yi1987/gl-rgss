/* 
 * File:   scancode.hpp
 * Author: SYi
 *
 * Created on den 26 juli 2015, 21:16
 */

#ifndef SCANCODE_HPP
#define	SCANCODE_HPP

#include "SDL2/SDL_scancode.h"

namespace rgk
{

enum Scancode
{
    SCANCODE_UNKNOWN = SDL_SCANCODE_UNKNOWN,

    /**
     *  \name Usage page 0x07
     *
     *  These values are from usage page 0x07 (USB keyboard page).
     */
    /* @{ */

    SCANCODE_A = SDL_SCANCODE_A,
    SCANCODE_B = SDL_SCANCODE_B,
    SCANCODE_C = SDL_SCANCODE_C,
    SCANCODE_D = SDL_SCANCODE_D,
    SCANCODE_E = SDL_SCANCODE_E,
    SCANCODE_F = SDL_SCANCODE_F,
    SCANCODE_G = SDL_SCANCODE_G,
    SCANCODE_H = SDL_SCANCODE_H,
    SCANCODE_I = SDL_SCANCODE_I,
    SCANCODE_J = SDL_SCANCODE_J,
    SCANCODE_K = SDL_SCANCODE_K,
    SCANCODE_L = SDL_SCANCODE_L,
    SCANCODE_M = SDL_SCANCODE_M,
    SCANCODE_N = SDL_SCANCODE_N,
    SCANCODE_O = SDL_SCANCODE_O,
    SCANCODE_P = SDL_SCANCODE_P,
    SCANCODE_Q = SDL_SCANCODE_Q,
    SCANCODE_R = SDL_SCANCODE_R,
    SCANCODE_S = SDL_SCANCODE_S,
    SCANCODE_T = SDL_SCANCODE_T,
    SCANCODE_U = SDL_SCANCODE_U,
    SCANCODE_V = SDL_SCANCODE_V,
    SCANCODE_W = SDL_SCANCODE_W,
    SCANCODE_X = SDL_SCANCODE_X,
    SCANCODE_Y = SDL_SCANCODE_Y,
    SCANCODE_Z = SDL_SCANCODE_Z,

    SCANCODE_1 = SDL_SCANCODE_1,
    SCANCODE_2 = SDL_SCANCODE_2,
    SCANCODE_3 = SDL_SCANCODE_3,
    SCANCODE_4 = SDL_SCANCODE_4,
    SCANCODE_5 = SDL_SCANCODE_5,
    SCANCODE_6 = SDL_SCANCODE_6,
    SCANCODE_7 = SDL_SCANCODE_7,
    SCANCODE_8 = SDL_SCANCODE_8,
    SCANCODE_9 = SDL_SCANCODE_9,
    SCANCODE_0 = SDL_SCANCODE_0,

    SCANCODE_RETURN = SDL_SCANCODE_RETURN,
    SCANCODE_ESCAPE = SDL_SCANCODE_ESCAPE,
    SCANCODE_BACKSPACE = SDL_SCANCODE_BACKSPACE,
    SCANCODE_TAB = SDL_SCANCODE_TAB,
    SCANCODE_SPACE = SDL_SCANCODE_SPACE,

    SCANCODE_MINUS = SDL_SCANCODE_MINUS,
    SCANCODE_EQUALS = SDL_SCANCODE_EQUALS,
    SCANCODE_LEFTBRACKET = SDL_SCANCODE_LEFTBRACKET,
    SCANCODE_RIGHTBRACKET = SDL_SCANCODE_RIGHTBRACKET,
    SCANCODE_BACKSLASH = SDL_SCANCODE_BACKSLASH,      /**< Located at the lower left of the return
                                                       *   key on ISO keyboards and at the right end
                                                       *   of the QWERTY row on ANSI keyboards.
                                                       *   Produces REVERSE SOLIDUS (backslash) and
                                                       *   VERTICAL LINE in a US layout, REVERSE
                                                       *   SOLIDUS and VERTICAL LINE in a UK Mac
                                                       *   layout, NUMBER SIGN and TILDE in a UK
                                                       *   Windows layout, DOLLAR SIGN and POUND SIGN
                                                       *   in a Swiss German layout, NUMBER SIGN and
                                                       *   APOSTROPHE in a German layout, GRAVE
                                                       *   ACCENT and POUND SIGN in a French Mac
                                                       *   layout, and ASTERISK and MICRO SIGN in a
                                                       *   French Windows layout.
                                                       */
    SCANCODE_NONUSHASH = SDL_SCANCODE_NONUSHASH, /**< ISO USB keyboards actually use this code
                                  *   instead of 49 for the same key, but all
                                  *   OSes I've seen treat the two codes
                                  *   identically. So, as an implementor, unless
                                  *   your keyboard generates both of those
                                  *   codes and your OS treats them differently,
                                  *   you should generate SCANCODE_BACKSLASH
                                  *   instead of this code. As a user, you
                                  *   should not rely on this code because SDL
                                  *   will never generate it with most (all?)
                                  *   keyboards.
                                  */
    SCANCODE_SEMICOLON = SDL_SCANCODE_SEMICOLON,
    SCANCODE_APOSTROPHE = SDL_SCANCODE_APOSTROPHE,
    SCANCODE_GRAVE = SDL_SCANCODE_GRAVE,      /**< Located in the top left corner (on both ANSI
                              *   and ISO keyboards). Produces GRAVE ACCENT and
                              *   TILDE in a US Windows layout and in US and UK
                              *   Mac layouts on ANSI keyboards, GRAVE ACCENT
                              *   and NOT SIGN in a UK Windows layout, SECTION
                              *   SIGN and PLUS-MINUS SIGN in US and UK Mac
                              *   layouts on ISO keyboards, SECTION SIGN and
                              *   DEGREE SIGN in a Swiss German layout (Mac:
                              *   only on ISO keyboards), CIRCUMFLEX ACCENT and
                              *   DEGREE SIGN in a German layout (Mac: only on
                              *   ISO keyboards), SUPERSCRIPT TWO and TILDE in a
                              *   French Windows layout, COMMERCIAL AT and
                              *   NUMBER SIGN in a French Mac layout on ISO
                              *   keyboards, and LESS-THAN SIGN and GREATER-THAN
                              *   SIGN in a Swiss German, German, or French Mac
                              *   layout on ANSI keyboards.
                              */
    SCANCODE_COMMA = SDL_SCANCODE_COMMA,
    SCANCODE_PERIOD = SDL_SCANCODE_PERIOD,
    SCANCODE_SLASH = SDL_SCANCODE_SLASH,

    SCANCODE_CAPSLOCK = SDL_SCANCODE_CAPSLOCK,

    SCANCODE_F1 = SDL_SCANCODE_F1,
    SCANCODE_F2 = SDL_SCANCODE_F2,
    SCANCODE_F3 = SDL_SCANCODE_F3,
    SCANCODE_F4 = SDL_SCANCODE_F4,
    SCANCODE_F5 = SDL_SCANCODE_F5,
    SCANCODE_F6 = SDL_SCANCODE_F6,
    SCANCODE_F7 = SDL_SCANCODE_F7,
    SCANCODE_F8 = SDL_SCANCODE_F8,
    SCANCODE_F9 = SDL_SCANCODE_F9,
    SCANCODE_F10 = SDL_SCANCODE_F10,
    SCANCODE_F11 = SDL_SCANCODE_F11,
    SCANCODE_F12 = SDL_SCANCODE_F12,

    SCANCODE_PRINTSCREEN = SDL_SCANCODE_PRINTSCREEN,
    SCANCODE_SCROLLLOCK = SDL_SCANCODE_SCROLLLOCK,
    SCANCODE_PAUSE = SDL_SCANCODE_PAUSE,
    SCANCODE_INSERT = SDL_SCANCODE_INSERT,     /**< insert on PC, help on some Mac keyboards (but
                                   does send code 73, not 117) */
    SCANCODE_HOME = SDL_SCANCODE_HOME,
    SCANCODE_PAGEUP = SDL_SCANCODE_PAGEUP,
    SCANCODE_DELETE = SDL_SCANCODE_DELETE,
    SCANCODE_END = SDL_SCANCODE_END,
    SCANCODE_PAGEDOWN = SDL_SCANCODE_PAGEDOWN,
    SCANCODE_RIGHT = SDL_SCANCODE_RIGHT,
    SCANCODE_LEFT = SDL_SCANCODE_LEFT,
    SCANCODE_DOWN = SDL_SCANCODE_DOWN,
    SCANCODE_UP = SDL_SCANCODE_UP,

    SCANCODE_NUMLOCKCLEAR = SDL_SCANCODE_NUMLOCKCLEAR, /**< num lock on PC, clear on Mac keyboards
                                     */
    SCANCODE_KP_DIVIDE = SDL_SCANCODE_KP_DIVIDE,
    SCANCODE_KP_MULTIPLY = SDL_SCANCODE_KP_MULTIPLY,
    SCANCODE_KP_MINUS = SDL_SCANCODE_KP_MINUS,
    SCANCODE_KP_PLUS = SDL_SCANCODE_KP_PLUS,
    SCANCODE_KP_ENTER = SDL_SCANCODE_KP_ENTER,
    SCANCODE_KP_1 = SDL_SCANCODE_KP_1,
    SCANCODE_KP_2 = SDL_SCANCODE_KP_2,
    SCANCODE_KP_3 = SDL_SCANCODE_KP_3,
    SCANCODE_KP_4 = SDL_SCANCODE_KP_4,
    SCANCODE_KP_5 = SDL_SCANCODE_KP_5,
    SCANCODE_KP_6 = SDL_SCANCODE_KP_6,
    SCANCODE_KP_7 = SDL_SCANCODE_KP_7,
    SCANCODE_KP_8 = SDL_SCANCODE_KP_8,
    SCANCODE_KP_9 = SDL_SCANCODE_KP_9,
    SCANCODE_KP_0 = SDL_SCANCODE_KP_0,
    SCANCODE_KP_PERIOD = SDL_SCANCODE_KP_PERIOD,

    SCANCODE_NONUSBACKSLASH = SDL_SCANCODE_NONUSBACKSLASH,     /**< This is the additional key that ISO
                                        *   keyboards have over ANSI ones,
                                        *   located between left shift and Y.
                                        *   Produces GRAVE ACCENT and TILDE in a
                                        *   US or UK Mac layout, REVERSE SOLIDUS
                                        *   (backslash) and VERTICAL LINE in a
                                        *   US or UK Windows layout, and
                                        *   LESS-THAN SIGN and GREATER-THAN SIGN
                                        *   in a Swiss German, German, or French
                                        *   layout. */
    SCANCODE_APPLICATION = SDL_SCANCODE_APPLICATION,     /**< windows contextual menu, compose */
    SCANCODE_POWER = SDL_SCANCODE_POWER,     /**< The USB document says this is a status flag,
                               *   not a physical key - but some Mac keyboards
                               *   do have a power key. */
    SCANCODE_KP_EQUALS = SDL_SCANCODE_KP_EQUALS,
    SCANCODE_F13 = SDL_SCANCODE_F13,
    SCANCODE_F14 = SDL_SCANCODE_F14,
    SCANCODE_F15 = SDL_SCANCODE_F15,
    SCANCODE_F16 = SDL_SCANCODE_F16,
    SCANCODE_F17 = SDL_SCANCODE_F17,
    SCANCODE_F18 = SDL_SCANCODE_F18,
    SCANCODE_F19 = SDL_SCANCODE_F19,
    SCANCODE_F20 = SDL_SCANCODE_F20,
    SCANCODE_F21 = SDL_SCANCODE_F21,
    SCANCODE_F22 = SDL_SCANCODE_F22,
    SCANCODE_F23 = SDL_SCANCODE_F23,
    SCANCODE_F24 = SDL_SCANCODE_F24,
    SCANCODE_EXECUTE = SDL_SCANCODE_EXECUTE,
    SCANCODE_HELP = SDL_SCANCODE_HELP,
    SCANCODE_MENU = SDL_SCANCODE_MENU,
    SCANCODE_SELECT = SDL_SCANCODE_SELECT,
    SCANCODE_STOP = SDL_SCANCODE_STOP,
    SCANCODE_AGAIN = SDL_SCANCODE_AGAIN,   /**< redo */
    SCANCODE_UNDO = SDL_SCANCODE_UNDO,
    SCANCODE_CUT = SDL_SCANCODE_CUT,
    SCANCODE_COPY = SDL_SCANCODE_COPY,
    SCANCODE_PASTE = SDL_SCANCODE_PASTE,
    SCANCODE_FIND = SDL_SCANCODE_FIND,
    SCANCODE_MUTE = SDL_SCANCODE_MUTE,
    SCANCODE_VOLUMEUP = SDL_SCANCODE_VOLUMEUP,
    SCANCODE_VOLUMEDOWN = SDL_SCANCODE_VOLUMEDOWN,
/* not sure whether there's a reason to enable these */
/*     SCANCODE_LOCKINGCAPSLOCK = 130,  */
/*     SCANCODE_LOCKINGNUMLOCK = 131, */
/*     SCANCODE_LOCKINGSCROLLLOCK = 132, */
    SCANCODE_KP_COMMA = SDL_SCANCODE_KP_COMMA,
    SCANCODE_KP_EQUALSAS400 = SDL_SCANCODE_KP_EQUALSAS400,

    SCANCODE_INTERNATIONAL1 = SDL_SCANCODE_INTERNATIONAL1, /**< used on Asian keyboards, see
                                            footnotes in USB doc */
    SCANCODE_INTERNATIONAL2 = SDL_SCANCODE_INTERNATIONAL2,
    SCANCODE_INTERNATIONAL3 = SDL_SCANCODE_INTERNATIONAL3, /**< Yen */
    SCANCODE_INTERNATIONAL4 = SDL_SCANCODE_INTERNATIONAL4,
    SCANCODE_INTERNATIONAL5 = SDL_SCANCODE_INTERNATIONAL5,
    SCANCODE_INTERNATIONAL6 = SDL_SCANCODE_INTERNATIONAL6,
    SCANCODE_INTERNATIONAL7 = SDL_SCANCODE_INTERNATIONAL7,
    SCANCODE_INTERNATIONAL8 = SDL_SCANCODE_INTERNATIONAL8,
    SCANCODE_INTERNATIONAL9 = SDL_SCANCODE_INTERNATIONAL9,
    SCANCODE_LANG1 = SDL_SCANCODE_LANG1, /**< Hangul/English toggle */
    SCANCODE_LANG2 = SDL_SCANCODE_LANG2, /**< Hanja conversion */
    SCANCODE_LANG3 = SDL_SCANCODE_LANG3, /**< Katakana */
    SCANCODE_LANG4 = SDL_SCANCODE_LANG4, /**< Hiragana */
    SCANCODE_LANG5 = SDL_SCANCODE_LANG5, /**< Zenkaku/Hankaku */
    SCANCODE_LANG6 = SDL_SCANCODE_LANG6, /**< reserved */
    SCANCODE_LANG7 = SDL_SCANCODE_LANG7, /**< reserved */
    SCANCODE_LANG8 = SDL_SCANCODE_LANG8, /**< reserved */
    SCANCODE_LANG9 = SDL_SCANCODE_LANG9, /**< reserved */

    SCANCODE_ALTERASE = SDL_SCANCODE_ALTERASE, /**< Erase-Eaze */
    SCANCODE_SYSREQ = SDL_SCANCODE_SYSREQ,
    SCANCODE_CANCEL = SDL_SCANCODE_CANCEL,
    SCANCODE_CLEAR = SDL_SCANCODE_CLEAR,
    SCANCODE_PRIOR = SDL_SCANCODE_PRIOR,
    SCANCODE_RETURN2 = SDL_SCANCODE_RETURN2,
    SCANCODE_SEPARATOR = SDL_SCANCODE_SEPARATOR,
    SCANCODE_OUT = SDL_SCANCODE_OUT,
    SCANCODE_OPER = SDL_SCANCODE_OPER,
    SCANCODE_CLEARAGAIN = SDL_SCANCODE_CLEARAGAIN,
    SCANCODE_CRSEL = SDL_SCANCODE_CRSEL,
    SCANCODE_EXSEL = SDL_SCANCODE_EXSEL,

    SCANCODE_KP_00 = SDL_SCANCODE_KP_00,
    SCANCODE_KP_000 = SDL_SCANCODE_KP_000,
    SCANCODE_THOUSANDSSEPARATOR = SDL_SCANCODE_THOUSANDSSEPARATOR,
    SCANCODE_DECIMALSEPARATOR = SDL_SCANCODE_DECIMALSEPARATOR,
    SCANCODE_CURRENCYUNIT = SDL_SCANCODE_CURRENCYUNIT,
    SCANCODE_CURRENCYSUBUNIT = SDL_SCANCODE_CURRENCYSUBUNIT,
    SCANCODE_KP_LEFTPAREN = SDL_SCANCODE_KP_LEFTPAREN,
    SCANCODE_KP_RIGHTPAREN = SDL_SCANCODE_KP_RIGHTPAREN,
    SCANCODE_KP_LEFTBRACE = SDL_SCANCODE_KP_LEFTBRACE,
    SCANCODE_KP_RIGHTBRACE = SDL_SCANCODE_KP_RIGHTBRACE,
    SCANCODE_KP_TAB = SDL_SCANCODE_KP_TAB,
    SCANCODE_KP_BACKSPACE = SDL_SCANCODE_KP_BACKSPACE,
    SCANCODE_KP_A = SDL_SCANCODE_KP_A,
    SCANCODE_KP_B = SDL_SCANCODE_KP_B,
    SCANCODE_KP_C = SDL_SCANCODE_KP_C,
    SCANCODE_KP_D = SDL_SCANCODE_KP_D,
    SCANCODE_KP_E = SDL_SCANCODE_KP_E,
    SCANCODE_KP_F = SDL_SCANCODE_KP_F,
    SCANCODE_KP_XOR = SDL_SCANCODE_KP_XOR,
    SCANCODE_KP_POWER = SDL_SCANCODE_KP_POWER,
    SCANCODE_KP_PERCENT = SDL_SCANCODE_KP_PERCENT,
    SCANCODE_KP_LESS = SDL_SCANCODE_KP_LESS,
    SCANCODE_KP_GREATER = SDL_SCANCODE_KP_GREATER,
    SCANCODE_KP_AMPERSAND = SDL_SCANCODE_KP_AMPERSAND,
    SCANCODE_KP_DBLAMPERSAND = SDL_SCANCODE_KP_DBLAMPERSAND,
    SCANCODE_KP_VERTICALBAR = SDL_SCANCODE_KP_VERTICALBAR,
    SCANCODE_KP_DBLVERTICALBAR = SDL_SCANCODE_KP_DBLVERTICALBAR,
    SCANCODE_KP_COLON = SDL_SCANCODE_KP_COLON,
    SCANCODE_KP_HASH = SDL_SCANCODE_KP_HASH,
    SCANCODE_KP_SPACE = SDL_SCANCODE_KP_SPACE,
    SCANCODE_KP_AT = SDL_SCANCODE_KP_AT,
    SCANCODE_KP_EXCLAM = SDL_SCANCODE_KP_EXCLAM,
    SCANCODE_KP_MEMSTORE = SDL_SCANCODE_KP_MEMSTORE,
    SCANCODE_KP_MEMRECALL = SDL_SCANCODE_KP_MEMRECALL,
    SCANCODE_KP_MEMCLEAR = SDL_SCANCODE_KP_MEMCLEAR,
    SCANCODE_KP_MEMADD = SDL_SCANCODE_KP_MEMADD,
    SCANCODE_KP_MEMSUBTRACT = SDL_SCANCODE_KP_MEMSUBTRACT,
    SCANCODE_KP_MEMMULTIPLY = SDL_SCANCODE_KP_MEMMULTIPLY,
    SCANCODE_KP_MEMDIVIDE = SDL_SCANCODE_KP_MEMDIVIDE,
    SCANCODE_KP_PLUSMINUS = SDL_SCANCODE_KP_PLUSMINUS,
    SCANCODE_KP_CLEAR = SDL_SCANCODE_KP_CLEAR,
    SCANCODE_KP_CLEARENTRY = SDL_SCANCODE_KP_CLEARENTRY,
    SCANCODE_KP_BINARY = SDL_SCANCODE_KP_BINARY,
    SCANCODE_KP_OCTAL = SDL_SCANCODE_KP_OCTAL,
    SCANCODE_KP_DECIMAL = SDL_SCANCODE_KP_DECIMAL,
    SCANCODE_KP_HEXADECIMAL = SDL_SCANCODE_KP_HEXADECIMAL,

    SCANCODE_LCTRL = SDL_SCANCODE_LCTRL,
    SCANCODE_LSHIFT = SDL_SCANCODE_LSHIFT,
    SCANCODE_LALT = SDL_SCANCODE_LALT,     /**< alt, option */
    SCANCODE_LGUI = SDL_SCANCODE_LGUI,     /**< windows, command (apple), meta */
    SCANCODE_RCTRL = SDL_SCANCODE_RCTRL,
    SCANCODE_RSHIFT = SDL_SCANCODE_RSHIFT,
    SCANCODE_RALT = SDL_SCANCODE_RALT,     /**< alt gr, option */
    SCANCODE_RGUI = SDL_SCANCODE_RGUI,     /**< windows, command (apple), meta */

    SCANCODE_MODE = SDL_SCANCODE_MODE,        /**< I'm not sure if this is really not covered
                                 *   by any of the above, but since there's a
                                 *   special KMOD_MODE for it I'm adding it here
                                 */

    /* @} *//* Usage page 0x07 */

    /**
     *  \name Usage page 0x0C
     *
     *  These values are mapped from usage page 0x0C (USB consumer page).
     */
    /* @{ */

    SCANCODE_AUDIONEXT = SDL_SCANCODE_AUDIONEXT,
    SCANCODE_AUDIOPREV = SDL_SCANCODE_AUDIOPREV,
    SCANCODE_AUDIOSTOP = SDL_SCANCODE_AUDIOSTOP,
    SCANCODE_AUDIOPLAY = SDL_SCANCODE_AUDIOPLAY,
    SCANCODE_AUDIOMUTE = SDL_SCANCODE_AUDIOMUTE,
    SCANCODE_MEDIASELECT = SDL_SCANCODE_MEDIASELECT,
    SCANCODE_WWW = SDL_SCANCODE_WWW,
    SCANCODE_MAIL = SDL_SCANCODE_MAIL,
    SCANCODE_CALCULATOR = SDL_SCANCODE_CALCULATOR,
    SCANCODE_COMPUTER = SDL_SCANCODE_COMPUTER,
    SCANCODE_AC_SEARCH = SDL_SCANCODE_AC_SEARCH,
    SCANCODE_AC_HOME = SDL_SCANCODE_AC_HOME,
    SCANCODE_AC_BACK = SDL_SCANCODE_AC_BACK,
    SCANCODE_AC_FORWARD = SDL_SCANCODE_AC_FORWARD,
    SCANCODE_AC_STOP = SDL_SCANCODE_AC_STOP,
    SCANCODE_AC_REFRESH = SDL_SCANCODE_AC_REFRESH,
    SCANCODE_AC_BOOKMARKS = SDL_SCANCODE_AC_BOOKMARKS,

    /* @} *//* Usage page 0x0C */

    /**
     *  \name Walther keys
     *
     *  These are values that Christian Walther added (for mac keyboard?).
     */
    /* @{ */

    SCANCODE_BRIGHTNESSDOWN = SDL_SCANCODE_BRIGHTNESSDOWN,
    SCANCODE_BRIGHTNESSUP = SDL_SCANCODE_BRIGHTNESSUP,
    SCANCODE_DISPLAYSWITCH = SDL_SCANCODE_DISPLAYSWITCH,     /**< display mirroring/dual display
                                           switch, video mode switch */
    SCANCODE_KBDILLUMTOGGLE = SDL_SCANCODE_KBDILLUMTOGGLE,
    SCANCODE_KBDILLUMDOWN = SDL_SCANCODE_KBDILLUMDOWN,
    SCANCODE_KBDILLUMUP = SDL_SCANCODE_KBDILLUMUP,
    SCANCODE_EJECT = SDL_SCANCODE_EJECT,
    SCANCODE_SLEEP = SDL_SCANCODE_SLEEP,

    SCANCODE_APP1 = SDL_SCANCODE_APP1,
    SCANCODE_APP2 = SDL_SCANCODE_APP2,

    /* @} *//* Walther keys */

    /* Add any other keys here. */

    NUM_SCANCODES = SDL_NUM_SCANCODES     /**< not a key, just marks the number of scancodes
                                 for array bounds */
};

}

#endif	/* SCANCODE_HPP */

