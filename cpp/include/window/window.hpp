/* 
 * File:   window.hpp
 * Author: Samuel - Dell
 *
 * Created on den 28 februari 2014, 21:13
 */

#ifndef WINDOW_HPP
#define	WINDOW_HPP

#include "graphics/rect.hpp"
#include <SDL2/SDL_video.h>
#include <string>

namespace rgk
{

class Window
{
public:
    
    Window(const std::string& title, int x, int y, int width, int height,
           Uint32 flags = SDL_WINDOW_SHOWN);
    
    virtual ~Window();
    
    void dispose();
    
    bool isDisposed() const;
    
    SDL_Window* get() const;
    
    void setPosition(int x, int y);
    
    Rect getPosition() const;
    
    void setSize(int w, int h);
    
    const Rect& getSize() const;
    
    int setFullscreenMode(Uint32 flag);
    
    Uint32 getFullscreenMode() const;
    
    Rect getRect() const;
    
    void setBordered(bool bordered);
    
    bool isBordered() const;
    
    void setGrabMode(bool grabbed);
    
    bool isInGrabMode() const;
    
    int setBrightness(float brightness);
    
    float getBrightness() const;
    
    void setTitle(const std::string& title);
    
    std::string getTitle() const;
    
    Uint32 getWindowID() const;
    
    Uint32 getFlags() const;
    
private:
    Window(const Window& other);
    
    Window& operator=(const Window& other);
    
    static SDL_Window* createWindow(const std::string& title, int x, int y,
                                    int width, int height, Uint32 flags);
    
    
    SDL_Window* mWindowSDL;
    mutable Rect mSize;
    
};

}

#endif	/* WINDOW_HPP */

