/* 
 * File:   window.hpp
 * Author: Samuel - Dell
 *
 * Created on den 18 juli 2014, 00:13
 */

#ifndef WINDOWHEADER_HPP
#define	WINDOWHEADER_HPP


#include "window/eventtype.hpp"
#include "window/event.hpp"
#include "window/windowenum.hpp"
#include "window/window.hpp"
#include "window/keycode.hpp"
#include "window/scancode.hpp"
 

#endif	/* WINDOWHEADER_HPP */

