////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2014 Laurent Gomila (laurent.gom@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////
#ifndef VECTOR3_HPP
#define VECTOR3_HPP


namespace rgk
{
template <typename T>
class Vector3
{
public :
    Vector3();

    Vector3(T X, T Y, T Z);

    template <typename U>
    explicit Vector3(const Vector3<U>& vector);

    T x; ///< X coordinate of the vector
    T y; ///< Y coordinate of the vector
    T z; ///< Z coordinate of the vector
};

template <typename T>
Vector3<T> operator -(const Vector3<T>& left);


template <typename T>
Vector3<T>& operator +=(Vector3<T>& left, const Vector3<T>& right);


template <typename T>
Vector3<T>& operator -=(Vector3<T>& left, const Vector3<T>& right);


template <typename T>
Vector3<T> operator +(const Vector3<T>& left, const Vector3<T>& right);


template <typename T>
Vector3<T> operator -(const Vector3<T>& left, const Vector3<T>& right);


template <typename T>
Vector3<T> operator *(const Vector3<T>& left, T right);


template <typename T>
Vector3<T> operator *(T left, const Vector3<T>& right);


template <typename T>
Vector3<T>& operator *=(Vector3<T>& left, T right);


template <typename T>
Vector3<T> operator /(const Vector3<T>& left, T right);


template <typename T>
Vector3<T>& operator /=(Vector3<T>& left, T right);


template <typename T>
bool operator ==(const Vector3<T>& left, const Vector3<T>& right);


template <typename T>
bool operator !=(const Vector3<T>& left, const Vector3<T>& right);
#include "vector3.inl"
// Define the most common types
typedef Vector3<int> Vector3i;
typedef Vector3<float> Vector3f;


} // namespace rgk


#endif // VECTOR3_HPP