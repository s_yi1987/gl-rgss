////////////////////////////////////////////////////////////
template <typename T>
inline Vector4<T>::Vector4() :
x(0),
y(0),
z(0),
w(0)
{
}
////////////////////////////////////////////////////////////
template <typename T>
inline Vector4<T>::Vector4(T X, T Y, T Z, T W) :
x(X),
y(Y),
z(Z),
w(W)
{
}
////////////////////////////////////////////////////////////
template <typename T>
template <typename U>
inline Vector4<T>::Vector4(const Vector4<U>& vector) :
x(static_cast<T>(vector.x)),
y(static_cast<T>(vector.y)),
z(static_cast<T>(vector.z)),
w(static_cast<T>(vector.w))
{
}
