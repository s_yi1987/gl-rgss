/* 
 * File:   outputargument.hpp
 * Author: Samuel - Dell
 *
 * Created on den 14 mars 2014, 07:22
 */

#ifndef OUTPUTARGUMENT_HPP
#define	OUTPUTARGUMENT_HPP

namespace rgk
{
    
template<typename T>
class OutputArgument
{
public:
    T val;
    
    OutputArgument(T val) : val(val) {}
        
};

}

#endif	/* OUTPUTARGUMENT_HPP */

