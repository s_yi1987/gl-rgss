/* 
 * File:   time.hpp
 * Author: M02A706938
 *
 * Created on den 19 oktober 2014, 02:50
 */

#ifndef TIME_HPP
#define	TIME_HPP

#include <SDL2/SDL_stdinc.h>


namespace rgk
{
    
    
class Time
{
public :
    
    Time();

    float asSeconds() const;

    Sint32 asMilliseconds() const;

    Sint64 asMicroseconds() const;

    static Time seconds(float amount);
    
    static Time milliseconds(Sint32 amount);
    
    static Time microseconds(Sint64 amount);
    
    
    static const Time ZERO; ///< Predefined "zero" time value 

private :

    explicit Time(Sint64 microseconds);
    
    friend Time seconds(float amount);
    
    friend Time milliseconds(Sint32 amount);
    
    friend Time microseconds(Sint64 amount);

    
    Sint64 mMicroseconds; ///< Time value stored as microseconds
    
};
    
bool operator ==(Time left, Time right);

bool operator !=(Time left, Time right);

bool operator <(Time left, Time right);

bool operator >(Time left, Time right);

bool operator <=(Time left, Time right);

bool operator >=(Time left, Time right);

Time operator -(Time right);

Time operator +(Time left, Time right);

Time& operator +=(Time& left, Time right);

Time operator -(Time left, Time right);

Time& operator -=(Time& left, Time right);

Time operator *(Time left, float right);

Time operator *(Time left, Sint64 right);

Time operator *(float left, Time right);

Time operator *(Sint64 left, Time right);

Time& operator *=(Time& left, float right);

Time& operator *=(Time& left, Sint64 right);

Time operator /(Time left, float right);

Time operator /(Time left, Sint64 right);

Time& operator /=(Time& left, float right);

Time& operator /=(Time& left, Sint64 right);
    

}

#endif	/* TIME_HPP */

