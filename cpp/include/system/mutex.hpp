/* 
 * File:   Mutex.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 juli 2014, 16:00
 */

#ifndef MUTEX_HPP
#define	MUTEX_HPP

#include <SDL2/SDL_mutex.h>


namespace rgk
{

class Mutex
{
    
public:
    
    Mutex();
    
    ~Mutex();
    
    void lock();
    
    void unlock();
    
private:
    
    Mutex(const Mutex& other);
    
    Mutex& operator=(const Mutex& other);
    
    SDL_mutex* mMutexSDL;
    
};

}

#endif	/* MUTEX_HPP */

