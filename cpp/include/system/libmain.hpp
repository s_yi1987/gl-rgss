/* 
 * File:   libmain.hpp
 * Author: Samuel - Dell
 *
 * Created on den 24 februari 2014, 14:29
 */

#ifndef LIBMAIN_HPP
#define	LIBMAIN_HPP

#include "system/mutex.hpp"
#include <string>
#include <SDL_stdinc.h>


struct ALCdevice_struct;
struct ALCcontext_struct;

namespace rgk
{

class LibMain
{
public:
    LibMain();
    
    ~LibMain();
    
    bool init();
    
    void close();
    
    std::string getLastError() const;
    
    std::string getLastFontError() const;
    
    Uint32 getTicks() const;
    
    Uint64 getPerformanceCounter() const;
    
    Uint64 getPerformanceFrequency() const;
    
    void delay(Uint32 ms) const;
    
    bool isInitialized() const;
    
private:
    
    static Mutex sMutex;
    static bool sInstantiated;
    
    bool mInitialized;
    ALCdevice_struct* mAudioDevice;
    ALCcontext_struct* mAudioContext;
    

    LibMain(const LibMain& other);
    
    LibMain& operator=(const LibMain& other);
    
    bool initVideo();
    
    void setGLAtrributes();
    
    bool initImage();
    
    bool initFont();
    
    bool initAudio();
    
};

}

#endif	/* LIBMAIN_HPP */

