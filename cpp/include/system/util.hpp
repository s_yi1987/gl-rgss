/* 
 * File:   util.hpp
 * Author: Samuel - Dell
 *
 * Created on den 22 februari 2014, 10:50
 */

#ifndef UTIL_HPP
#define	UTIL_HPP

#include "graphics/rect.hpp"
#include "graphics/blendmode.hpp"
#include "system/exception.hpp"
#include "system/datatypes.hpp"
#include <SDL2/SDL_blendmode.h>

struct SDL_Surface;


namespace rgk
{

namespace util
{
    int ensureWithinRange(int val, int min, int max);
    
    double ensureWithinRange(double val, double min, double max);
    
    int ensureWithinColorRange(int val);
    
    double ensureWithinColorRange(double val);
    
    SDL_Surface* ensurePixelFormatValue(SDL_Surface* drawSurface, Uint32 format);
    
    void setPixel(void* pixels, int x, int y, int bpp, int pitch, Uint32 pixel);
    
    Uint32 getPixel(void* pixels, int x, int y, int bpp, int pitch);
    
    //Only the x and y coordinates of dstRect will be used
    void transferPixels(void* srcPixels, const Rect& srcRect, void* dstPixels,
                        const Rect& dstRect, int bpp, int pitch);
    
    void transferPixels(void* srcPixels, const Rect& srcRect, int srcBpp,
                        int srcPitch, void* dstPixels, const Rect& dstRect,
                        int dstBpp, int dstPitch);
    
    bool sameRectPos(const Rect& rect, const Rect& rect2);
    
    bool sameRectSize(const Rect& rect, const Rect& rect2);
    
    bool sameRect(const Rect& rect, const Rect& rect2);
    
    void ensureGlewInit();
    
    void verifyGlContext(bool throwExceptionIfFail = true);
    
    void verifyGlContext(const std::string& failMsg, bool throwExceptionIfFail = true);
    
    bool hasActiveGlContext();
    
    void verifyAlContext(bool throwExceptionIfFail = true);
    
    void verifyAlContext(const std::string& failMsg, bool throwExceptionIfFail = true);
    
    bool hasActiveAlContext();
    
    int getFormatFromChannelCount(unsigned int channelCount);
    
    int glEnumFromDataType(DataType type);
    
}

}

#endif	/* UTIL_HPP */

