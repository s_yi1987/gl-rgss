/* 
 * File:   datatypes.hpp
 * Author: SYi
 *
 * Created on den 21 juni 2015, 23:29
 */

#ifndef DATATYPES_HPP
#define	DATATYPES_HPP

namespace rgk
{

enum DataType
{
    UNSIGNED_BYTE,
    BYTE,
    UNSIGNED_SHORT,
    SHORT,
    UNSIGNED_INT,
    INT,
    FLOAT
};

}

#endif	/* DATATYPES_HPP */

