/* 
 * File:   thread.hpp
 * Author: M02A706938
 *
 * Created on den 23 september 2014, 16:38
 */

#ifndef THREAD_HPP
#define	THREAD_HPP

#include <SDL2/SDL_thread.h>
#include <string>


namespace rgk
{
    
class Thread
{
public:
    
    Thread(int (*function)(void* data), void* data);
    
    Thread(int (*function)(void* data), const std::string& name, void* data);
    
    Thread(const Thread& other);
    
    Thread& operator =(const Thread& other);
    
    ~Thread();
    
    void launch();
    
    int wait();
    
    void detach();
    
    bool hasLaunched() const;
    
private:
    
    SDL_Thread* mThreadSDL;
    int (*mFunction)(void* data);
    std::string mName;
    void* mData;
    
};
    
}


#endif	/* THREAD_HPP */

