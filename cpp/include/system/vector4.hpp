#ifndef VECTOR4_HPP
#define	VECTOR4_HPP

namespace rgk
{
template <typename T>
class Vector4
{
public :
    Vector4();

    Vector4(T X, T Y, T Z, T W);

    template <typename U>
    explicit Vector4(const Vector4<U>& vector);

    T x; ///< X coordinate of the vector
    T y; ///< Y coordinate of the vector
    T z; ///< Z coordinate of the vector
    T w; ///< W coordinate of the vector
};

#include "vector4.inl"
typedef Vector4<int> Vector4i;
typedef Vector4<float> Vector4f;

}

#endif	/* VECTOR4_HPP */

