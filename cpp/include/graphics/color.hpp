/* 
 * File:   color.hpp
 * Author: Samuel - Dell
 *
 * Created on den 22 februari 2014, 16:13
 */

#ifndef COLOR_HPP
#define	COLOR_HPP

#include "system/vector4.hpp"
#include <SDL2/SDL_pixels.h>

namespace rgk
{
typedef Vector4<Uint8> PlainColor;
    
class PixelFormat;


class Color
{
public:
    
    double r;
    double g;
    double b;
    double a;
    
    Color();
    
    Color(double red, double green, double blue, double alpha = 255);
    
    Color(const PlainColor& plainColor);
    
    Color(Uint32 pixel, Uint32 pixelFormatValue = SDL_PIXELFORMAT_ARGB8888);
    
    Color(Uint32 pixel, const PixelFormat& pixelFormat);
    
    void set(const Color& other);
    
    void set(const PlainColor& plainColor);
    
    void set(double red, double green, double blue, double alpha = 255);
    
    void set(Uint32 pixel, Uint32 pixelFormatValue = SDL_PIXELFORMAT_ARGB8888);
    
    void set(Uint32 pixel, const PixelFormat& pixelFormat);
    
    Uint32 toMapped(Uint32 pixelFormatValue = SDL_PIXELFORMAT_ARGB8888) const;
    
    Uint32 toMapped(const PixelFormat& pixelFormat) const;
    
    PlainColor toPlain() const;
    
    SDL_Color toSDL() const;
    
};

}

#endif	/* COLOR_HPP */

