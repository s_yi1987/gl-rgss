/* 
 * File:   blendmode.hpp
 * Author: Samuel - Dell
 *
 * Created on den 10 juni 2014, 11:19
 */

#ifndef BLENDMODE_HPP
#define	BLENDMODE_HPP

#include <SDL2/SDL_stdinc.h>

namespace rgk
{

enum BlendModePreset
{
    BLEND_ALPHA,    ///< Pixel = Source * Source.a + Dest * (1 - Source.a)
    BLEND_ADD,      ///< Pixel = Source + Dest
    BLEND_MULTIPLY, ///< Pixel = Source * Dest
    BLEND_MOD,
    BLEND_NONE      ///< Pixel = Source
};

class BlendMode
{
    enum Factor
    {
        ZERO,                 ///< (0, 0, 0, 0)
        ONE,                  ///< (1, 1, 1, 1)
        SRC_COLOR,            ///< (src.r, src.g, src.b, src.a)
        ONE_MINUS_SRC_COLOR,  ///< (1, 1, 1, 1) - (src.r, src.g, src.b, src.a)
        DST_COLOR,            ///< (dst.r, dst.g, dst.b, dst.a)
        ONE_MINUS_DST_COLOR,  ///< (1, 1, 1, 1) - (dst.r, dst.g, dst.b, dst.a)
        SRC_ALPHA,            ///< (src.a, src.a, src.a, src.a)
        ONE_MINUS_SRC_ALPHA,  ///< (1, 1, 1, 1) - (src.a, src.a, src.a, src.a)
        DST_ALPHA,            ///< (dst.a, dst.a, dst.a, dst.a)
        ONE_MINUS_DST_ALPHA,  ///< (1, 1, 1, 1) - (dst.a, dst.a, dst.a, dst.a)
        CONSTANT_COLOR,
        ONE_MINUS_CONSTANT_COLOR,
        CONSTANT_ALPHA,
        ONE_MINUS_CONSTANT_ALPHA,
        SRC_ALPHA_SATURATE,
        INVALID_FACTOR
    };
    
    
    enum Equation
    {
        FUNC_ADD,     ///< Pixel = Src * SrcFactor + Dst * DstFactor
        FUNC_SUBTRACT, ///< Pixel = Src * SrcFactor - Dst * DstFactor
        FUNC_REVERSE_SUBTRACT,
        MIN,
        MAX,
        INVALID_EQUATION
    };
    
public:
    BlendMode();
    
    BlendMode(BlendModePreset preset);
    
    BlendMode(Factor sourceFactor, Factor destinationFactor, Equation blendEquation = FUNC_ADD);
    
    BlendMode(Factor colorSourceFactor, Factor colorDestinationFactor,
              Equation colorBlendEquation, Factor alphaSourceFactor,
              Factor alphaDestinationFactor, Equation alphaBlendEquation);
    
    void setup(BlendModePreset preset);
    
    Factor   colorSrcFactor; ///< Source blending factor for the color channels
    Factor   colorDstFactor; ///< Destination blending factor for the color channels
    Equation colorEquation;  ///< Blending equation for the color channels
    Factor   alphaSrcFactor; ///< Source blending factor for the alpha channel
    Factor   alphaDstFactor; ///< Destination blending factor for the alpha channel
    Equation alphaEquation;  ///< Blending equation for the alpha channel
    
    
    static Uint32 factorToGlConstant(Factor factor);
    
    static Uint32 equationToGlConstant(Equation equation);
    
    static Uint32 blendModePresetToSDL(BlendModePreset preset);
    
    static Uint32 sdlBlendModeToPreset(Uint32 blendModeSDL);
};

////////////////////////////////////////////////////////////
/// \relates BlendMode
/// \brief Overload of the == operator
///
/// \param left  Left operand
/// \param right Right operand
///
/// \return True if blending modes are equal, false if they are different
///
////////////////////////////////////////////////////////////
bool operator ==(const BlendMode& left, const BlendMode& right);

////////////////////////////////////////////////////////////
/// \relates BlendMode
/// \brief Overload of the != operator
///
/// \param left  Left operand
/// \param right Right operand
///
/// \return True if blending modes are different, false if they are equal
///
////////////////////////////////////////////////////////////
bool operator !=(const BlendMode& left, const BlendMode& right);

}
#endif	/* BLENDMODE_HPP */

