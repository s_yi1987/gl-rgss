/* 
 * File:   texturesaver.hpp
 * Author: Samuel - Dell
 *
 * Created on den 21 augusti 2014, 11:21
 */

#ifndef TEXTURESAVER_HPP
#define	TEXTURESAVER_HPP

#include <gl/glew.h>


namespace rgk
{
    
class TextureSaver
{
public :

    ////////////////////////////////////////////////////////////
    /// \brief Default constructor
    ///
    /// The current texture binding is saved.
    ///
    ////////////////////////////////////////////////////////////
    TextureSaver();

    ////////////////////////////////////////////////////////////
    /// \brief Destructor
    ///
    /// The previous texture binding is restored.
    ///
    ////////////////////////////////////////////////////////////
    ~TextureSaver();

private :

    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    GLint mTextureBinding; ///< Texture binding to restore
};

}

#endif	/* TEXTURESAVER_HPP */

