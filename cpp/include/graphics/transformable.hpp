/* 
 * File:   transformable.hpp
 * Author: Samuel - Dell
 *
 * Created on den 3 juni 2014, 12:16
 */

#ifndef TRANSFORMABLE_HPP
#define	TRANSFORMABLE_HPP

////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2013 Laurent Gomila (laurent.gom@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include "transform.hpp"

namespace rgk
{

class Transformable
{
public :

    Transformable();

    virtual ~Transformable();

    void setPosition(float x, float y);

    void setPosition(const Vector2f& position);

    void setRotation(float angle);

    void setScale(float factorX, float factorY);

    void setScale(const Vector2f& factors);

    void setOrigin(float x, float y);

    void setOrigin(const Vector2f& origin);

    const Vector2f& getPosition() const;

    float getRotation() const;

    const Vector2f& getScale() const;

    const Vector2f& getOrigin() const;

    void move(float offsetX, float offsetY);

    void move(const Vector2f& offset);

    void rotate(float angle);

    void scale(float factorX, float factorY);

    void scale(const Vector2f& factor);

    const Transform& getTransform() const;

    const Transform& getInverseTransform() const;

private :
    
    Vector2f          m_origin;                     ///< Origin of translation/rotation/scaling of the object
    Vector2f          m_position;                   ///< Position of the object in the 2D world
    float             m_rotation;                   ///< Orientation of the object, in degrees
    Vector2f          m_scale;                      ///< Scale of the object
    mutable Transform m_transform;                  ///< Combined transformation of the object
    mutable bool      m_transformNeedUpdate;        ///< Does the transform need to be recomputed?
    mutable Transform m_inverseTransform;           ///< Combined transformation of the object
    mutable bool      m_inverseTransformNeedUpdate; ///< Does the transform need to be recomputed?
    
};

}

#endif	/* TRANSFORMABLE_HPP */

