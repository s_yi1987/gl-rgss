/* 
 * File:   rendertargetmode.hpp
 * Author: Samuel - Dell
 *
 * Created on den 4 juli 2014, 00:40
 */

#ifndef RENDERTARGETMODE_HPP
#define	RENDERTARGETMODE_HPP

#include "graphics/view.hpp"
#include "graphics/rect.hpp"

namespace rgk
{
class RenderWindow;
class Texture;


class RenderTargetMode
{
public:
    
    virtual ~RenderTargetMode();

    virtual void setClipRect(const Rect* rect) = 0;
    
    virtual void applyView(const View& view) = 0;
    
    virtual Rect getViewport(const View& view) = 0;
    
protected:
    
    RenderTargetMode();
    
    RenderTargetMode(const RenderTargetMode& other);
    
    RenderTargetMode& operator=(const RenderTargetMode& other);
    
};


class RenderTargetModeNone : public RenderTargetMode
{
public:
    
    RenderTargetModeNone(RenderWindow& contextWindow);
    
    virtual void setClipRect(const Rect* rect);
    
    virtual void applyView(const View& view);
    
    virtual Rect getViewport(const View& view);
    
private:
    
    RenderWindow* mContextWindow;
    
};


class RenderTargetModeTexture : public RenderTargetMode
{
public:
    
    RenderTargetModeTexture(RenderWindow& contextWindow);
    
    virtual ~RenderTargetModeTexture();
    
    virtual void setClipRect(const Rect* rect);
    
    virtual void applyView(const View& view);
    
    virtual Rect getViewport(const View& view);
    
    void setRenderTarget(Texture* texture);
    
private:
    
    unsigned int mFrameBufferObjID;
    RenderWindow* mContextWindow;
    Texture* mRenderTarget;
    View mView;
    
};

}

#endif	/* RENDERTARGETMODE_HPP */

