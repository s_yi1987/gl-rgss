/* 
 * File:   shaderattribpointerdata.hpp
 * Author: SYi
 *
 * Created on den 22 juni 2015, 00:16
 */

#ifndef SHADERATTRIBPOINTERDATA_HPP
#define	SHADERATTRIBPOINTERDATA_HPP

#include "system/datatypes.hpp"

namespace rgk
{

class ShaderAttribPointerData
{
public:
    ShaderAttribPointerData();
    
    ShaderAttribPointerData(int s, DataType t, int str, const void* ptr, int offs = 0);
    
    //attributes should be
    //GLint size, GLenum type, GLsizei stride, const GLvoid *pointer 
    int size;
    DataType type;
    int stride;
    const void* pointer;
    int offset;
};
    
}

#endif	/* SHADERATTRIBPOINTERDATA_HPP */

