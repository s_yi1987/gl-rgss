/* 
 * File:   spritevertexdata.hpp
 * Author: Samuel - Dell
 *
 * Created on den 25 juni 2014, 11:46
 */

#ifndef SPRITEVERTEXDATA_HPP
#define	SPRITEVERTEXDATA_HPP

#include "graphics/vertexarray.hpp"
#include "graphics/rect.hpp"


namespace rgk
{

class SpriteVertexData : public VertexData
{
public:

    SpriteVertexData();

    SpriteVertexData(const Rect& rectangle);

    void setTextureRect(const Rect& rectangle);

    void setColor(const Color& color);

    const Rect& getTextureRect() const;

    Color getColor() const;

    FloatRect getBounds() const;
    
    VertexArray toVertexArray(bool separateTriangles) const;
    
    virtual const Vertex* getVertices() const;
    
    virtual unsigned int getVertexCount() const;
    
    virtual PrimitiveType getPrimitiveType() const;

private:

    void updatePositions();

    void updateTexCoords();

    ////////////////////////////////////////////////////////////
    // Member data
    ////////////////////////////////////////////////////////////
    Vertex         mVertices[4]; ///< Vertices defining the sprite's geometry
    Rect           mTextureRect; ///< Rectangle defining the area of the source texture to display
};

}

#endif	/* SPRITEVERTEXDATA_HPP */

