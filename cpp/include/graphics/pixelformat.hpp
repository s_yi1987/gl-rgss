/* 
 * File:   pixelformat.hpp
 * Author: Samuel - Dell
 *
 * Created on den 12 mars 2014, 09:36
 */

#ifndef PIXELFORMAT_HPP
#define	PIXELFORMAT_HPP

#include <SDL2/SDL_pixels.h>

namespace rgk
{

class PixelFormat
{
public:
    PixelFormat(Uint32 format);
    ~PixelFormat();
    PixelFormat(const PixelFormat& other);
    PixelFormat& operator=(const PixelFormat& other);
    
    SDL_PixelFormat* get() const;
    Uint32 getPixelFormatValue() const;
    
private:
    SDL_PixelFormat* mPixelFormatSDL = NULL;
    
    void setup(Uint32 format);
    void dispose();
    static SDL_PixelFormat* createPixelFormat(Uint32 format);
    
};

}

#endif	/* PIXELFORMAT_HPP */

