/* 
 * File:   transform.hpp
 * Author: Samuel - Dell
 *
 * Created on den 2 juni 2014, 20:03
 */

#ifndef TRANSFORM_HPP
#define	TRANSFORM_HPP

////////////////////////////////////////////////////////////
//
// SFML - Simple and Fast Multimedia Library
// Copyright (C) 2007-2013 Laurent Gomila (laurent.gom@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#include "system/vector2.hpp"
#include "graphics/rect.hpp"


namespace rgk
{

class Transform
{
public :

    Transform();

    Transform(float a00, float a01, float a02,
              float a10, float a11, float a12,
              float a20, float a21, float a22);

    const float* getMatrix() const;

    Transform getInverse() const;

    Vector2f transformPoint(float x, float y) const;

    Vector2f transformPoint(const Vector2f& point) const;

    FloatRect transformRect(const FloatRect& rectangle) const;

    Transform& combine(const Transform& transform);

    Transform& translate(float x, float y);

    Transform& translate(const Vector2f& offset);

    Transform& rotate(float angle);

    Transform& rotate(float angle, float centerX, float centerY);

    Transform& rotate(float angle, const Vector2f& center);

    Transform& scale(float scaleX, float scaleY);

    Transform& scale(float scaleX, float scaleY, float centerX, float centerY);

    Transform& scale(const Vector2f& factors);

    Transform& scale(const Vector2f& factors, const Vector2f& center);

    static const Transform Identity; ///< The identity transform (does nothing)

private:
    float m_matrix[16]; ///< 4x4 matrix defining the transformation
    
};

Transform operator *(const Transform& left, const Transform& right);

Transform& operator *=(Transform& left, const Transform& right);

Vector2f operator *(const Transform& left, const Vector2f& right);

}

#endif	/* TRANSFORM_HPP */

