/* 
 * File:   arraybufferobject.hpp
 * Author: SYi
 *
 * Created on den 10 juni 2015, 13:30
 */

#ifndef ARRAYBUFFEROBJECT_HPP
#define	ARRAYBUFFEROBJECT_HPP

namespace rgk
{
    
enum VertexBufferMode
{
    VBO_STATIC_DRAW,
    VBO_DYNAMIC_DRAW,
    VBO_STREAM_DRAW
};
    

class ArrayBufferObject
{
public:
    ArrayBufferObject();
    
    ArrayBufferObject(unsigned int size, VertexBufferMode mode);
    
    ArrayBufferObject(const void* data, unsigned int size, VertexBufferMode mode);
    
    ~ArrayBufferObject();
    
    void setup(unsigned int size, VertexBufferMode mode);
    
    void setup(const void* data, unsigned int size, VertexBufferMode mode);
    
    //offset is sizeOfClass * desired offset index
    void update(const void* data, unsigned int size, unsigned int offset = 0);
    
    void dispose();
    
    bool isDisposed() const;
    
    unsigned int getArrayBufferID() const;
    
    unsigned int getArrayBufferCapacity() const;
    
    VertexBufferMode getMode() const;
    
    static void verifyAvailability();
    
    static void bind(const ArrayBufferObject* arrayBufferObject);
    
    static void updateCurrentArrayBuffer(const void* data, unsigned int size,
                                         unsigned int offset = 0);
    
private:
    ArrayBufferObject(const ArrayBufferObject& other);
    
    ArrayBufferObject& operator =(const ArrayBufferObject& other);
    
    void createArrayBuffer();
    
    unsigned int mArrayBufferID;
    unsigned int mArrayBufferCapacity;
    VertexBufferMode mMode;
    
};
    
}

#endif	/* ARRAYBUFFEROBJECT_HPP */

