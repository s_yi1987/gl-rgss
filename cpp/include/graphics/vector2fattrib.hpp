/* 
 * File:   vector2fattrib.hpp
 * Author: SYi
 *
 * Created on den 20 juni 2015, 00:20
 */

#ifndef VECTOR2FATTRIB_HPP
#define	VECTOR2FATTRIB_HPP

#include "shaderattrib.hpp"
#include "system/vector2.hpp"

namespace rgk
{
    
class Vector2fAttrib : public ShaderAttrib
{
public:
    Vector2fAttrib();
    
    Vector2fAttrib(unsigned int vecCapacity, VertexBufferMode mode);
    
    Vector2fAttrib(const Vector2f* vecs, unsigned int vecCount, VertexBufferMode mode);
    
    virtual ~Vector2fAttrib();
    
    void setup(unsigned int vecCapacity, VertexBufferMode mode);
    
    void setup(const Vector2f* vecs, unsigned int vecCount, VertexBufferMode mode);
    
    virtual ShaderAttribPointerData getPointerData() const;
    
};
    
}

#endif	/* VECTOR2FATTRIB_HPP */

