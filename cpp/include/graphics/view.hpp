/* 
 * File:   view.hpp
 * Author: Samuel - Dell
 *
 * Created on den 6 juni 2014, 11:34
 */

#ifndef VIEW_HPP
#define	VIEW_HPP

#include "graphics/rect.hpp"
#include "graphics/transform.hpp"
#include "system/vector2.hpp"

namespace rgk
{
class View
{
public :

    View();

    explicit View(const FloatRect& rectangle);

    View(const Vector2f& center, const Vector2f& size);

    void setCenter(float x, float y);

    void setCenter(const Vector2f& center);

    void setSize(float width, float height);

    void setSize(const Vector2f& size);

    void setRotation(float angle);

    void setViewport(const FloatRect& viewport);

    void reset(const FloatRect& rectangle);

    const Vector2f& getCenter() const;

    const Vector2f& getSize() const;

    float getRotation() const;

    const FloatRect& getViewport() const;

    void move(float offsetX, float offsetY);

    void move(const Vector2f& offset);

    void rotate(float angle);

    void zoom(float factor);

    const Transform& getTransform() const;

    const Transform& getInverseTransform() const;

private :

    Vector2f          m_center;              ///< Center of the view, in scene coordinates
    Vector2f          m_size;                ///< Size of the view, in scene coordinates
    float             m_rotation;            ///< Angle of rotation of the view rectangle, in degrees
    FloatRect         m_viewport;            ///< Viewport rectangle, expressed as a factor of the render-target's size
    mutable Transform m_transform;           ///< Precomputed projection transform corresponding to the view
    mutable Transform m_inverseTransform;    ///< Precomputed inverse projection transform corresponding to the view
    mutable bool      m_transformUpdated;    ///< Internal state telling if the transform needs to be updated
    mutable bool      m_invTransformUpdated; ///< Internal state telling if the inverse transform needs to be updated
    
};

}

#endif	/* VIEW_HPP */

