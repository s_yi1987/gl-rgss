/* 
 * File:   surface.hpp
 * Author: Samuel - Dell
 *
 * Created on den 10 februari 2014, 13:39
 */
#ifndef SURFACE_HPP
#define	SURFACE_HPP

#include "graphics/rect.hpp"
#include "graphics/color.hpp"
#include "graphics/blendmode.hpp"
#include "system/outputargument.hpp"
#include <SDL2/SDL_surface.h>
#include <string>


namespace rgk
{
    
class Surface
{
public:
    
    Surface();
    
    Surface(const std::string& filename,
            Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    Surface(int width, int height,
            Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    ~Surface();
    
    Surface(const Surface& other);
    
    Surface& operator =(const Surface& other);
    
    void setup(const std::string& filename,
               Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    void setup(int width, int height,
               Uint32 format = SDL_PIXELFORMAT_ARGB8888);
    
    void setup(const Surface& other);
    
    void setup(SDL_Surface* surfaceSDL, bool useCopy = true);
    
    void dispose();
    
    bool isDisposed() const;
    
    SDL_Surface* get() const;
    
    Uint32 getPixelFormatValue() const;
    
    int getWidth() const;
    
    int getHeight() const;
    
    Rect getRect() const;
    
    Rect getClipRect() const;
    
    int setClipRect(const Rect* clipRect);
    
    int getAlphaMod() const;
    
    int setAlphaMod(int alphaMod);
    
    BlendModePreset getBlendModePreset() const;
    
    int setBlendModePreset(BlendModePreset preset);
    
    Color getColorMod(OutputArgument<int>* output = NULL) const;
    
    int setColorMod(const Color& color);
    
    int setColorMod(int r, int g, int b);
    
    Color getPixelColor(int x , int y, OutputArgument<bool>* output = NULL) const;
    
    Uint32 getPixel(int x, int y, OutputArgument<bool>* output = NULL) const;
    
    bool setPixel(int x, int y, const Color& color);
    
    bool setPixel(int x, int y, Uint32 pixel);
    
    Color getColorKey(OutputArgument<bool>* output = NULL) const;
    
    int setColorKey(Color& color, bool flag = true);
    
    int setColorKey(Uint32 color, bool flag = true);
    
    int fillRect(const Rect* rect, const Color& color);
    
    int fillRect(const Rect* rect, Uint32 pixel);
    
    int blit(Surface& src, const Rect* srcRect, const Rect* dstRect);
    
    int blitScaled(Surface& src, const Rect* srcRect, const Rect* dstRect);
    
private:
    
    int blitMain(Surface& src, const Rect* srcRect, const Rect* dstRect, bool scaled);
    
    static SDL_Surface* createImageSurface(const std::string& filename,
                                           Uint32 format);
    
    static SDL_Surface* createRGBSurface(int width, int height, Uint32 format);
    
    static SDL_Surface* createNullRGBSurface(int width, int height, Uint32 format);
    
    static SDL_Surface* createRGBSurfaceMain(int width, int height, Uint32 format,
                                             bool createNull = false);
    
    static SDL_Surface* createSurfaceCopy(SDL_Surface* surfaceSDL, SDL_PixelFormat* format);
    
    SDL_Surface* mSurfaceSDL;
    
};

}

#endif	/* SURFACE_HPP */

