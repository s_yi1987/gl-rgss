/* 
 * File:   Rect.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 februari 2014, 19:57
 */

#ifndef RECT_HPP
#define	RECT_HPP

#include "system/vector2.hpp"
#include <SDL2/SDL_rect.h>
#include <algorithm>


namespace rgk
{

template <typename T>
class BaseRect
{
public :

    BaseRect();
    
    BaseRect(const Vector2<T>& position, const Vector2<T>& size);

    BaseRect(T rectLeft, T rectTop, T rectWidth, T rectHeight);

    template <typename U>
    explicit BaseRect(const BaseRect<U>& rectangle);
    
    void set(T x, T y, T width, T height);
    
    void set(const BaseRect<T>& rectangle);
    
    template <typename U>
    void set(const BaseRect<U>& rectangle);
    
    void set(const Vector2<T>& position, const Vector2<T>& size);
    
    void empty();

    bool contains(T x, T y) const;

    bool contains(const Vector2<T>& point) const;

    bool intersects(const BaseRect<T>& rectangle) const;

    bool intersects(const BaseRect<T>& rectangle, BaseRect<T>& intersection) const;
    
    bool isEmpty() const;
    
    SDL_Rect toSDL() const;

    T x;   ///< Left coordinate of the rectangle
    T y;    ///< Top coordinate of the rectangle
    T width;  ///< Width of the rectangle
    T height; ///< Height of the rectangle
};

template <typename T>
bool operator ==(const BaseRect<T>& left, const BaseRect<T>& right);

template <typename T>
bool operator !=(const BaseRect<T>& left, const BaseRect<T>& right);

#include "rect.inl"

typedef BaseRect<int>   Rect;
typedef BaseRect<float> FloatRect;

}

#endif	/* RECT_HPP */

