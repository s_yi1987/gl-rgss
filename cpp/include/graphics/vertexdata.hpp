/* 
 * File:   vertexdata.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 juni 2014, 18:56
 */

#ifndef VERTEXDATA_HPP
#define	VERTEXDATA_HPP

#include "vertex.hpp"
#include "primitivetype.hpp"
#include "indexdata.hpp"


namespace rgk
{

class VertexData : public IndexData
{
public:
    
    virtual ~VertexData();
    
    virtual const Vertex* getVertices() const = 0;
    
    virtual const unsigned int* getIndices() const;
    
    virtual unsigned int getVertexCount() const = 0;
    
    virtual unsigned int getIndexCount() const;
    
    virtual PrimitiveType getPrimitiveType() const = 0;
    
};

}

#endif	/* VERTEXDATA_HPP */

