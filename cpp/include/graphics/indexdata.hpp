/* 
 * File:   indexdata.hpp
 * Author: SYi
 *
 * Created on den 10 juni 2015, 00:50
 */

#ifndef INDEXDATA_HPP
#define	INDEXDATA_HPP

namespace rgk
{
    
class IndexData
{
public:
    virtual ~IndexData();
    
    virtual const unsigned int* getIndices() const = 0;
    
    virtual unsigned int getIndexCount() const = 0;
    
};

}

#endif	/* INDEXDATA_HPP */

