/* 
 * File:   Shader.hpp
 * Author: Samuel
 *
 * Created on den 6 september 2014, 18:59
 */

#ifndef SHADER_HPP
#define	SHADER_HPP

#include "graphics/transform.hpp"
#include "graphics/color.hpp"
#include "graphics/shaderattrib.hpp"
#include "system/vector2.hpp"
#include "system/vector3.hpp"
#include <map>
#include <string>
#include <vector>


namespace rgk
{
class Texture;
class RenderWindow;


enum ShaderType
{
    VERTEX_SHADER,
    FRAGMENT_SHADER
};


struct CurrentTextureType {};

//Don't forget to remove mAttribs if it isn't needed
class Shader
{
public:
    
    Shader();
    
    ~Shader();
    
    void dispose();
    
    bool isDisposed() const;
    
    void setup(const std::string& filename, ShaderType type);
    
    void setup(const std::string& vertexShaderFilename,
                const std::string& fragmentShaderFilename);
    
    void setupFromMemory(const std::string& shaderCode, ShaderType type);
    
    void setupFromMemory(const std::string& vertexShaderCode,
                           const std::string& fragmentShaderCode);
    
    void setParameter(const std::string& name, float x);
    
    void setParameter(const std::string& name, float x, float y);
    
    void setParameter(const std::string& name, float x, float y, float z);
    
    void setParameter(const std::string& name, float x, float y, float z, float w);
    
    void setParameter(const std::string& name, const Vector2f& vector);
    
    void setParameter(const std::string& name, const Vector3f& vector);
    
    void setParameter(const std::string& name, const Color& color);
    
    void setParameter(const std::string& name, const Transform& transform);
    
    void setParameter(const std::string& name, const Texture& texture);
    
    void setParameter(const std::string& name, CurrentTextureType);
    
    void setAttribParameter(const std::string& name, const ShaderAttrib& shaderAttrib);
    
    int getParamLocation(const std::string& name) const;
    
    int getAttribParamLocation(const std::string& name) const;
    
    void bindTextures() const;
    
    static void bind(const Shader* shader);
    
    static void bind(const Shader& shader, bool bindTextures);
    
    static void setParameterCurrent(int location, float x);
    
    static void setParameterCurrent(int location, float x, float y);
    
    static void setParameterCurrent(int location, float x, float y, float z);
    
    static void setParameterCurrent(int location, float x, float y, float z, float w);
    
    static void setParameterCurrent(int location, const Transform& transform);
    
    static bool isAvailable();
    
    static int getMaxTextureUnits();
    
    
    static CurrentTextureType CURRENT_TEXTURE;
    
private:
    typedef std::map<int, const Texture*> TextureTable;
    typedef std::map<int, const ShaderAttrib*> AttribTable;
    typedef std::map<std::string, int> ParamTable;
    
    friend class RenderWindow;
    
    Shader(const Shader& other);
    
    Shader& operator =(const Shader& other);
    
    void compile(const char* vertexShaderCode, const char* fragmentShaderCode);
    
    void attachShader(const char* shaderCode, ShaderType type);
    
    void enableShaderAttribs();
    
    void disableShaderAttribs();
    
    static void setAttribParameterCurrent(int location, const ShaderAttrib& shaderAttrib);
    
    static void enableShaderAttribsCurrent(const AttribTable& attribTable);
    
    static void disableShaderAttribsCurrent(const AttribTable& attribTable);
    
    static void enableVertexAttribArrayCurrent(int location);
    
    static void disableVertexAttribArrayCurrent(int location);
    
    static void verifyAvailability();
    
    static void loadFileContents(const std::string& filename, std::vector<char>& buffer);

    static int checkMaxTextureUnits();

    
    unsigned int mProgramID;
    int          mCurrentTextureParamLocation;
    TextureTable mTextures;
    AttribTable  mAttribs;
    ParamTable   mParams;
    ParamTable   mAttribParams;
    
};


}

#endif	/* SHADER_HPP */

