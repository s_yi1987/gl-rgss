/* 
 * File:   vertexbufferobject.hpp
 * Author: Samuel - Dell
 *
 * Created on den 18 augusti 2014, 17:24
 */

#ifndef VERTEXBUFFEROBJECT_HPP
#define	VERTEXBUFFEROBJECT_HPP

#include "graphics/primitivetype.hpp"
#include "graphics/arraybufferobject.hpp"
#include "system/mutex.hpp"


namespace rgk
{
class Vertex;
class VertexData;


class VertexBufferObject
{
public:
    VertexBufferObject();
    
    VertexBufferObject(unsigned int vertexCapacitySize, VertexBufferMode mode);
    
    VertexBufferObject(unsigned int vertexCapacitySize, unsigned int indexCapacitySize,
                       VertexBufferMode mode);
    
    VertexBufferObject(const VertexData& vertexData, VertexBufferMode mode);
    
    ~VertexBufferObject();
    
    void setup(unsigned int vertexCapacitySize, VertexBufferMode mode);
    
    void setup(unsigned int vertexCapacitySize, unsigned int indexCapacitySize,
               VertexBufferMode mode);
    
    void setup(const VertexData& vertexData, VertexBufferMode mode);
    
    void dispose();
    
    bool isDisposed() const;
    
    void update(const VertexData& vertexData,
                unsigned int vertexBufferOffset = 0,
                unsigned int indexBufferOffset  = 0);
    
    void updateVertexBuffer(const Vertex* vertices, unsigned int count,
                            unsigned int offset = 0);
    
    void updateIndexBuffer(const unsigned int* indices, unsigned int count,
                           unsigned int offset  = 0);
    
    void setPrimitiveType(PrimitiveType type);
    
    void setVertexReadSize(unsigned int size);
    
    void setIndexReadSize(unsigned int size);
    
    unsigned int getVertexCapacity() const;
    
    unsigned int getVertexReadSize() const;
    
    unsigned int getIndexCapacity() const;
    
    unsigned int getIndexReadSize() const;
    
    unsigned int getVertexBufferID() const;
    
    unsigned int getIndexBufferID() const;

    VertexBufferMode getMode() const;
    
    PrimitiveType getPrimitiveType() const;
    
    static void bind(const VertexBufferObject* vertexBufferObject);
    
    static void updateCurrentVertexBuffer(const Vertex* vertices, unsigned int count,
                                          unsigned int offset = 0);
    
    static void updateCurrentIndexBuffer(const unsigned int* indices, unsigned int count,
                                         unsigned int offset  = 0);
    
private:
    VertexBufferObject(const VertexBufferObject& other);
    
    VertexBufferObject& operator =(const VertexBufferObject& other);
    
    void setupBuffers(const Vertex* vertices, unsigned int vertexCapacitySize,
                      const unsigned int* indices, unsigned int indexCapacitySize,
                      VertexBufferMode mode);
    
    void setupIndexBuffer();
    
    void disposeVertexBuffer();
    
    void disposeIndexBuffer();

    static void verifyAvailability();
    
    ArrayBufferObject mArrayBuffer;
    unsigned int mIndexBufferID;
    PrimitiveType mPrimitiveType;
    unsigned int mVertexCapacity;
    unsigned int mIndexCapacity;
    unsigned int mVertexReadSize;
    unsigned int mIndexReadSize;
    
};

}

#endif	/* VERTEXBUFFEROBJECT_HPP */

