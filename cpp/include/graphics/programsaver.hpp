/* 
 * File:   programsaver.hpp
 * Author: M02A706938
 *
 * Created on den 12 september 2014, 23:26
 */

#ifndef PROGRAMSAVER_HPP
#define	PROGRAMSAVER_HPP

#include <GL/glew.h>

class ProgramSaver
{
public:
    
    ProgramSaver();
    
    ~ProgramSaver();
    
private:
    
    ProgramSaver(const ProgramSaver& other);
    
    ProgramSaver& operator =(const ProgramSaver& other);
    
    GLhandleARB mProgramHandle;
};


#endif	/* PROGRAMSAVER_HPP */

