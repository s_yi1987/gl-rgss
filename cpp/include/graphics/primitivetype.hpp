/* 
 * File:   primitivetype.hpp
 * Author: Samuel - Dell
 *
 * Created on den 9 juni 2014, 00:11
 */

#ifndef PRIMITIVETYPE_HPP
#define	PRIMITIVETYPE_HPP

namespace rgk
{
    
enum PrimitiveType
{
    POINTS,         ///< List of individual points
    LINES,          ///< List of individual lines
    LINES_STRIP,     ///< List of connected lines, a point uses the previous point to form a line
    TRIANGLES,      ///< List of individual triangles
    TRIANGLES_STRIP, ///< List of connected triangles, a point uses the two previous points to form a triangle
    TRIANGLES_FAN,   ///< List of connected triangles, a point uses the common center and the previous point to form a triangle
    QUADS           ///< List of individual quads
};

}

#endif	/* PRIMITIVETYPE_HPP */

