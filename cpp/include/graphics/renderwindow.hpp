/* 
 * File:   renderwindow.hpp
 * Author: Samuel - Dell
 *
 * Created on den 15 juni 2014, 19:41
 */

#ifndef RENDERWINDOW_HPP
#define	RENDERWINDOW_HPP

#include "window/window.hpp"
#include "graphics/view.hpp"
#include "graphics/color.hpp"
#include "graphics/blendmode.hpp"
#include "graphics/primitivetype.hpp"
#include "graphics/renderstates.hpp"
#include "graphics/vertex.hpp"


namespace rgk
{
class Texture;
class Shader;
class VertexData;
class VertexBufferObject;
class RenderTargetMode;
class RenderTargetModeNone;
class RenderTargetModeTexture;


class RenderWindow : public Window
{
public:
    
    RenderWindow(const std::string& title, int x, int y, int width, int height,
                   Uint32 flags = SDL_WINDOW_SHOWN);
    
    virtual ~RenderWindow();
    
    void dispose();
    
    void bindVBO(const VertexBufferObject* vbo);
    
    void updateCurrentVBO(const VertexData& vertexData,
                            unsigned int vertexBufferOffset = 0,
                            unsigned int indexBufferOffset  = 0);
    
    void updateCurrentVertexBuffer(const Vertex* vertices, unsigned int count,
                                      unsigned int offset = 0);
    
    void updateCurrentIndexBuffer(const unsigned int* indices, unsigned int count,
                                     unsigned int offset  = 0);
    
    void bindShader(const Shader* shader, bool bindTextures = true);
    
    void setCurrentShaderParameter(const std::string& name, float x);
    
    void setCurrentShaderParameter(const std::string& name, float x, float y);
    
    void setCurrentShaderParameter(const std::string& name, float x, float y, float z);
    
    void setCurrentShaderParameter(const std::string& name, float x, float y, float z, float w);
    
    void setCurrentShaderParameter(const std::string& name, const Transform& transform);
    
    void bindCurrentShaderTextures();
    
    void draw(const VertexData& vertexData,
              const RenderStates& states = RenderStates::DEFAULT);
    
    void draw(VertexBufferObject& vertexBufferObj,
              const RenderStates& states = RenderStates::DEFAULT);
    
    void draw(const RenderStates& states = RenderStates::DEFAULT);
    
    void setView(const View& view);
    
    void setDefaultView(const View& view);
    
    const View& getView() const;
    
    const View& getDefaultView() const;
    
    Rect getViewport(const View& view) const;
    
    void setClipRect(const Rect* rect);
    
    Rect getClipRect() const;
    
    void setRenderTarget(Texture* texture, bool adjustView = true);
    
    Texture* getRenderTarget();
    
    bool setActive(bool active = true);
    
    bool isActive() const;
    
    void clear(const Color& color = Color(0, 0, 0, 255));
    
    void present();
    
    void setVerticalSyncEnabled(bool enabled);
    
private:
    
    void dispose(bool disposeWindow);
    
    void drawVertices(bool setPointers, unsigned int vertexCount, bool useIndices,
                      PrimitiveType type);
    
    void applyStates(const Transform* transform, BlendMode blendMode,
                     const Texture* texture);
    
    void applyStates(const Transform* transform, BlendMode blendMode,
                     const Texture* texture, const Shader* shader);
    
    void applyTransform(const Transform& transform);
    
    void applyTexture(const Texture* texture);
    
    void applyShader(const Shader* shader, bool bindTextures);
    
    void applyBlendMode(BlendMode mode);
    
    void applyCurrentView();
    
    void applyCurrentClipRect();
    
    void applyVBO(const VertexBufferObject* vbo);
    
    void resetGLStates();
    
    void setupTargetModes();
    
    RenderWindow(const RenderWindow& other);
    
    RenderWindow& operator=(const RenderWindow& other);
    
    static SDL_GLContext createContext(SDL_Window* windowSDL);
    
    
    struct StatesCache
    {
        enum
        {
            VertexCacheSize = 4,
            IndexCacheSize  = 6
        };
        
        bool glStatesSet;
        bool viewChanged;
        bool clipRectChanged;
        BlendMode lastBlendMode;
        Uint64 lastTextureCacheID;
        bool usingVertexCache;
        VertexBufferObject* vertexCacheVBO;
        Vertex vertexCache[VertexCacheSize];
        
        StatesCache() :
        glStatesSet(false),
        viewChanged(true),
        clipRectChanged(true),
        lastBlendMode(BLEND_NONE),
        lastTextureCacheID(0),
        usingVertexCache(false),
        vertexCacheVBO(NULL)
        {
            
        }
    };
    
    
    SDL_GLContext mContextSDL;
    StatesCache mCache;
    View mView;
    View mDefaultView;
    Rect mClipRect;
    Vector2i mLastSize;
    Texture* mRenderTarget;
    RenderTargetMode* mCurrentTargetMode;
    RenderTargetModeNone* mTargetModeNone;
    RenderTargetModeTexture* mTargetModeTexture;
    const VertexBufferObject* mVertexBufferObject;
    const Shader* mShader;
    
};

}

#endif	/* RENDERWINDOW_HPP */

