/* 
 * File:   vertexbufferobjsaver.hpp
 * Author: Samuel - Dell
 *
 * Created on den 21 augusti 2014, 19:29
 */

#ifndef VERTEXBUFFEROBJSAVER_HPP
#define	VERTEXBUFFEROBJSAVER_HPP

#include <gl/glew.h>


namespace rgk
{
    
class ArrayBufferSaver
{
public:
    ArrayBufferSaver();
    
    ~ArrayBufferSaver();
    
private:
    GLint mBufferBinding;
    
};


class ElementArrayBufferSaver
{
public:
    ElementArrayBufferSaver();
    
    ~ElementArrayBufferSaver();
    
private:
    GLint mBufferBinding;
};

    
class VertexBufferObjSaver
{
public :
    VertexBufferObjSaver();

    ~VertexBufferObjSaver();

private :
    ArrayBufferSaver mArrayBufferBinding;
    ElementArrayBufferSaver mElementArrayBufferBinding;
    
};

}

#endif	/* VERTEXBUFFEROBJSAVER_HPP */

