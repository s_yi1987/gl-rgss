#include "rect.hpp"


template <typename T>
BaseRect<T>::BaseRect() :
x     (0),
y     (0),
width (0),
height(0)
{

}


////////////////////////////////////////////////////////////
template <typename T>
BaseRect<T>::BaseRect(T rectLeft, T rectTop, T rectWidth, T rectHeight) :
x     (rectLeft),
y     (rectTop),
width (rectWidth),
height(rectHeight)
{

}


////////////////////////////////////////////////////////////
template <typename T>
BaseRect<T>::BaseRect(const Vector2<T>& position, const Vector2<T>& size) :
x     (position.x),
y     (position.y),
width (size.x),
height(size.y)
{

}


////////////////////////////////////////////////////////////
template <typename T>
template <typename U>
BaseRect<T>::BaseRect(const BaseRect<U>& rectangle) :
x     (static_cast<T>(rectangle.x)),
y     (static_cast<T>(rectangle.y)),
width (static_cast<T>(rectangle.width)),
height(static_cast<T>(rectangle.height))
{
}


////////////////////////////////////////////////////////////
template <typename T>
void BaseRect<T>::set(const BaseRect<T>& rectangle)
{
    set(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
}


////////////////////////////////////////////////////////////
template <typename T>
template <typename U>
void BaseRect<T>::set(const BaseRect<U>& rectangle)
{
    set(static_cast<T>(rectangle.x),
        static_cast<T>(rectangle.y),
        static_cast<T>(rectangle.width),
        static_cast<T>(rectangle.height));
}


////////////////////////////////////////////////////////////
template <typename T>
void BaseRect<T>::set(const Vector2<T>& position, const Vector2<T>& size)
{
    set(position.x, position.y, size.x, size.y);
}


////////////////////////////////////////////////////////////
template <typename T>
void BaseRect<T>::set(T x, T y, T width, T height)
{
    this->x = x;
    this->y = y;
    this->width  = width;
    this->height = height;
}


////////////////////////////////////////////////////////////
template <typename T>
void BaseRect<T>::empty()
{
    set(0, 0, 0, 0);
}


////////////////////////////////////////////////////////////
template <typename T>
bool BaseRect<T>::isEmpty() const
{
    return (x == 0 && y == 0 && width == 0 && height == 0);
}


////////////////////////////////////////////////////////////
template <typename T>
bool BaseRect<T>::contains(T x, T y) const
{
    // Rectangles with negative dimensions are allowed, so we must handle them correctly

    // Compute the real min and max of the rectangle on both axes
    T minX = std::min(x, x + width);
    T maxX = std::max(x, x + width);
    T minY = std::min(y, y + height);
    T maxY = std::max(y, y + height);

    return (x >= minX) && (x < maxX) && (y >= minY) && (y < maxY);
}


////////////////////////////////////////////////////////////
template <typename T>
bool BaseRect<T>::contains(const Vector2<T>& point) const
{
    return contains(point.x, point.y);
}


////////////////////////////////////////////////////////////
template <typename T>
bool BaseRect<T>::intersects(const BaseRect<T>& rectangle) const
{
    BaseRect<T> intersection;
    return intersects(rectangle, intersection);
}


////////////////////////////////////////////////////////////
template <typename T>
bool BaseRect<T>::intersects(const BaseRect<T>& rectangle, BaseRect<T>& intersection) const
{
    // Rectangles with negative dimensions are allowed, so we must handle them correctly

    // Compute the min and max of the first rectangle on both axes
    T r1MinX = std::min(x, x + width);
    T r1MaxX = std::max(x, x + width);
    T r1MinY = std::min(y, y + height);
    T r1MaxY = std::max(y, y + height);

    // Compute the min and max of the second rectangle on both axes
    T r2MinX = std::min(rectangle.x, rectangle.x + rectangle.width);
    T r2MaxX = std::max(rectangle.x, rectangle.x + rectangle.width);
    T r2MinY = std::min(rectangle.y, rectangle.y + rectangle.height);
    T r2MaxY = std::max(rectangle.y, rectangle.y + rectangle.height);

    // Compute the intersection boundaries
    T interLeft   = std::max(r1MinX, r2MinX);
    T interTop    = std::max(r1MinY, r2MinY);
    T interRight  = std::min(r1MaxX, r2MaxX);
    T interBottom = std::min(r1MaxY, r2MaxY);

    // If the intersection is valid (positive non zero area), then there is an intersection
    if ((interLeft < interRight) && (interTop < interBottom))
    {
        intersection = BaseRect<T>(interLeft, interTop, interRight - interLeft, interBottom - interTop);
        return true;
    }
    else
    {
        intersection = BaseRect<T>(0, 0, 0, 0);
        return false;
    }
}


////////////////////////////////////////////////////////////
template <typename T>
SDL_Rect BaseRect<T>::toSDL() const
{
    SDL_Rect rectSDL;
    rectSDL.x = x;
    rectSDL.y = y;
    rectSDL.w = width;
    rectSDL.h = height;
    
    return rectSDL;
}


////////////////////////////////////////////////////////////
template <typename T>
inline bool operator ==(const BaseRect<T>& left, const BaseRect<T>& right)
{
    return (left.x == right.x) && (left.width == right.width) &&
           (left.y == right.y) && (left.height == right.height);
}


////////////////////////////////////////////////////////////
template <typename T>
inline bool operator !=(const BaseRect<T>& left, const BaseRect<T>& right)
{
    return !(left == right);
}