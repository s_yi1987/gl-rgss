/* 
 * File:   shaderattrib.hpp
 * Author: SYi
 *
 * Created on den 18 juni 2015, 00:37
 */

#ifndef SHADERATTRIB_HPP
#define	SHADERATTRIB_HPP

#include "arraybufferobject.hpp"
#include "shaderattribpointerdata.hpp"

namespace rgk
{

class Shader;
    
class ShaderAttrib
{
public:
    virtual ~ShaderAttrib();
    
    void dispose();
    
    bool isDisposed() const;
    
    VertexBufferMode getMode() const;
    
    int getElementSize() const;
    
    virtual ShaderAttribPointerData getPointerData() const = 0;
    
protected:
    ShaderAttrib();
    
    ShaderAttrib(unsigned int elementCapacity, unsigned int elementSize, VertexBufferMode mode);
    
    ShaderAttrib(const void* data, unsigned int elementCapacity, unsigned int elementSize, VertexBufferMode mode);
    
    void setup(unsigned int elementCapacity, unsigned int elementSize, VertexBufferMode mode);
    
    void setup(const void* data, unsigned int elementCount, unsigned int elementSize, VertexBufferMode mode);
    
    void update(const void* data, unsigned int elementCount, unsigned int offset = 0);
    
private:
    friend class Shader;
    
    ShaderAttrib(const ShaderAttrib& other);
    
    ShaderAttrib& operator =(const ShaderAttrib& other);
    
    unsigned int mElementSize;
    ArrayBufferObject mArrayBuffer;
    
};
    
}

#endif	/* SHADERATTRIB_HPP */

