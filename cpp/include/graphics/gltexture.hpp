/* 
 * File:   gltexture.hpp
 * Author: Samuel - Dell
 *
 * Created on den 1 juni 2014, 17:49
 */

#ifndef GLTEXTURE_HPP
#define	GLTEXTURE_HPP

#include "graphics/surface.hpp"
#include "graphics/blendmode.hpp"
#include "system/mutex.hpp"


namespace rgk
{

class RenderWindow;
class RenderTargetModeTexture;


class Texture
{
public:
    Texture();
    
    Texture(const Texture& other);
    
    Texture(const std::string& filename);
    
    Texture(const Surface& surface);
    
    Texture(int width, int height);
    
    Texture& operator =(const Texture& other);
    
    ~Texture();

    void setup(const std::string& filename);
    
    void setup(const Surface& surface);
    
    void setup(int width, int height);
    
    void setup(const Texture& other);
    
    void update(const Surface& surface, int x = 0, int y = 0);
    
    void dispose();
    
    bool isDisposed() const;
    
    void setColorMod(const Color& color);
    
    Color getColorMod() const;
    
    void setAlphaMod(int alphaMod);
    
    int getAlphaMod() const;
    
    void setBlendMode(BlendMode blendMode);
    
    BlendMode getBlendMode() const;
    
    void setClipRect(const Rect* rect);
    
    Rect getClipRect() const;
    
    void blit(const Texture& src, const Rect* srcRect, const Rect* dstRect);
    
    void blitScaled(const Texture& src, const Rect* srcRect, const Rect* dstRect);
    
    void fillRect(const Rect* rect, const Color& color);
    
    void fillRect(const Rect* rect, Uint32 pixel);
    
    int getWidth() const;
    
    int getHeight() const;
    
    int getActualWidth() const;
    
    int getActualHeight() const;
    
    Rect getRect() const;
    
    void setSmooth(bool smooth);
    
    bool isSmooth() const;
    
    void setRepeated(bool repeated);
    
    bool isRepeated() const;
    
    Surface toSurface() const;
    
    void downloadTo(Surface& surface) const;
    
    void setWindow(RenderWindow* window);
    
    unsigned int getTextureID() const;
    
    Uint64 getCacheID() const;
    
    static void bind(const Texture* texture);
    
    static int getValidSize(int size);
    
    static int getMaximumSize();
    
private:
    static Uint64 createCacheID();
  
    void blitMain(const Texture& src, const Rect* srcRect, const Rect* dstRect,
                  bool scaled);
    
    void adjustBlitRect(Rect& rect) const;
    
    void upload(const Surface& surface, int x, int y);
    
    unsigned int mTextureID;
    Uint64 mCacheID;
    int mWidth, mHeight;
    int mActualWidth, mActualHeight;
    Color mColorMod;
    BlendMode mBlendMode;
    Rect mClipRect;
    bool mIsRepeated, mIsSmooth;
    RenderWindow* mWindow;
    
    static Uint64 sCacheID;
    static Mutex sCacheMutex;

};

}

#endif	/* GLTEXTURE_HPP */

