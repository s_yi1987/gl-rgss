/* 
 * File:   font.hpp
 * Author: Samuel - Dell
 *
 * Created on den 8 mars 2014, 21:51
 */

#ifndef FONT_HPP
#define	FONT_HPP

#include "graphics/rect.hpp"
#include "graphics/surface.hpp"
#include <string>
#include <SDL2/SDL_ttf.h>


namespace rgk
{

class Font
{
public:
    
    Font();
    
    Font(const std::string& filename, int ptsize, int index = 0);
    
    ~Font();
    
    void setup(const std::string& filename, int ptsize, int index = 0);
    
    void dispose();
    
    bool isDisposed() const;
    
    TTF_Font* get() const;
    
    int getStyle() const;
    
    int setStyle(int style);
    
    int getOutline() const;
    
    int setOutline(int outline);
    
    int getHinting() const;
    
    int setHinting(int hinting);
    
    int getKerning() const;
    
    int setKerning(int allowed);
    
    int getHeight() const;
    
    int getAscent() const;
    
    int getDescent() const;
    
    int getLineSkip() const;
    
    std::string getFamilyName() const;
    
    std::string getStyleName() const;
    
    Rect calculateTextSize(const std::string& text) const;
    
    bool fixedFaceWidth() const;
    
    Surface toSurfaceTextSolid(const std::string& text, const Color& fg) const;
    
    void renderTextSolidTo(Surface& surface, const std::string& text,
                           const Color& fg) const;
    
    Surface toSurfaceUTF8Solid(const std::string& text, const Color& fg) const;
    
    void renderUTF8SolidTo(Surface& surface, const std::string& text,
                           const Color& fg) const;
    
    Surface toSurfaceUNICODESolid(Uint16 text, const Color& fg) const;
    
    void renderUNICODESolidTo(Surface& surface, Uint16 text, const Color& fg) const;
    
    Surface toSurfaceTextShaded(const std::string& text, const Color& fg,
                                const Color& bg) const;
    
    void renderTextShadedTo(Surface& surface, const std::string& text,
                            const Color& fg, const Color& bg) const;
    
    Surface toSurfaceUTF8Shaded(const std::string& text, const Color& fg,
                                const Color& bg) const;
    
    void renderUTF8ShadedTo(Surface& surface, const std::string& text,
                            const Color& fg, const Color& bg) const;
    
    Surface toSurfaceUNICODEShaded(Uint16 text, const Color& fg, const Color& bg) const;
    
    void renderUNICODEShadedTo(Surface& surface, Uint16 text, const Color& fg,
                               const Color& bg) const;
    
    Surface toSurfaceTextBlended(const std::string& text, const Color& fg) const;
    
    void renderTextBlendedTo(Surface& surface, const std::string& text,
                             const Color& fg) const;
    
    Surface toSurfaceUTF8Blended(const std::string& text, const Color& fg) const;
    
    void renderUTF8BlendedTo(Surface& surface, const std::string& text,
                             const Color& fg) const;
    
    Surface toSurfaceUNICODEBlended(Uint16 text, const Color& fg) const;
    
    void renderUNICODEBlendedTo(Surface& surface, Uint16 text, const Color& fg) const;
    
private:
    
    Font(const Font& other);
    
    Font& operator=(const Font& other);
    
    void renderFontTo(
        SDL_Surface* (*func)(TTF_Font*, const char*, SDL_Color),
        Surface& surface, const std::string& text, const Color& fg) const;
    
    void renderFontTo(
        SDL_Surface* (*func)(TTF_Font*, const Uint16*, SDL_Color),
        Surface& surface, Uint16* text, const Color& fg) const;
    
    void renderFontTo(
        SDL_Surface* (*func)(TTF_Font*, const char*, SDL_Color, SDL_Color),
        Surface& surface, const std::string& text, const Color& fg, const Color& bg) const;
    
    void renderFontTo(
        SDL_Surface* (*func)(TTF_Font*, const Uint16*, SDL_Color, SDL_Color),
        Surface& surface, Uint16* text, const Color& fg, const Color& bg) const;
    
    static TTF_Font* loadFont(const std::string& filename, int ptsize, int index);
    
    
    TTF_Font* mFontSDL;
    
};

}

#endif	/* FONT_HPP */

