/* 
 * File:   tilevertexarray.hpp
 * Author: Samuel - Dell
 *
 * Created on den 14 augusti 2014, 23:54
 */

#ifndef TILEVERTEXARRAY_HPP
#define	TILEVERTEXARRAY_HPP

#include "graphics/indexvertexarray.hpp"
#include "graphics/rect.hpp"
#include "graphics/transform.hpp"
#include <vector>

namespace rgk
{
    
class TileVertexArray : public VertexData
{
public:
    TileVertexArray();
    
    virtual ~TileVertexArray();
    
    void append(const TileVertexArray& other,
                const Transform& transform = Transform::Identity);
    
    void append(const VertexData& vertexData,
                const Transform& transform = Transform::Identity);
    
    void append(const Rect& textureRect,
                const Transform& transform = Transform::Identity);
    
    void clear();
    
    virtual const Vertex* getVertices() const;
     
    virtual const unsigned int* getIndices() const;
    
    virtual unsigned int getVertexCount() const;
    
    virtual unsigned int getIndexCount() const;
    
    virtual PrimitiveType getPrimitiveType() const;
    
private:
    void appendQuads(const VertexData& vertexData, const Transform& transform);
    
    void appendTriangleStrips(const VertexData& vertexData, const Transform& transform);
    
    IndexVertexArray mVertexData;
    
};

}

#endif	/* TILEVERTEXARRAY_HPP */

