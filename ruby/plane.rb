class Plane
  include RGSS::Drawable

  attr_reader :ox, :oy
  attr_reader :bitmap
  attr_reader :opacity

 #--------------------------------------------------------------------------
 # ? initialize
 #--------------------------------------------------------------------------
  def initialize(viewport = nil)
    @bitmap = nil
    @x  = 0
    @y  = 0
    @z  = 0
    @ox = 0
    @oy = 0
    @opacity = 255
    @disposed = false
    @visible  = true

    @vertex_array  = RGFW::SpriteVertexData.new
    @texture_rect  = Rect.new
    @transformable = RGFW::Transformable.new
    @render_states = RGFW::RenderStates.new
    super(viewport)
  end

  def opacity=(val)
    @opacity = RGSS.ensure_within_color_range(val)
  end

  def bitmap=(val)
    @bitmap = val
    @src_rect = @bitmap.rect if @bitmap && !@bitmap.disposed?
  end

  #--------------------------------------------------------------------------
  # ? ox=
  #--------------------------------------------------------------------------
  def ox=(ox)
    @ox = ox
  end

  #--------------------------------------------------------------------------
  # ? oy=
  #--------------------------------------------------------------------------
  def oy=(oy)
    @oy = oy
  end

  def draw
    return unless @bitmap && !@bitmap.disposed?
    base_x = -@ox
    base_y = -@oy

    old_clip_rect = Graphics.screen.clip_rect
    revert_clip_rect = false
    if viewport
      viewport_rect = viewport.rect
      if viewport_rect != old_clip_rect
        Graphics.screen.clip_rect = viewport_rect
        revert_clip_rect = true
      end
    end

    revert_repeat = false
    if !@bitmap.texture.repeated?
      @bitmap.texture.repeated = true
      revert_repeat = true
    end

    width  = viewport ? viewport.rect.width  : Graphics.width
    height = viewport ? viewport.rect.height : Graphics.height

    w_mult = width / @bitmap.width
    w_mult += 1 if width % @bitmap.width  > 0 || w_mult < 1
    draw_width = w_mult * @bitmap.width

    h_mult = height / @bitmap.height
    h_mult += 1 if height % @bitmap.height > 0 || h_mult < 1
    draw_height = h_mult * @bitmap.height

    @texture_rect.set(0, 0, draw_width * 2, draw_height * 2)
    @vertex_array.texture_rect = @texture_rect

    x_mult = base_x.abs / draw_width
    x_mult += 1 if base_x.abs % draw_width > 0 && base_x > 0 || x_mult < 1
    if base_x > 0
      base_x -= draw_width * x_mult
    elsif base_x < 0 && base_x.abs >= draw_width
      base_x += draw_width * x_mult
    end

    y_mult = base_y.abs / draw_height
    y_mult += 1 if base_y.abs % draw_height > 0 && base_y > 0 || y_mult < 1
    if base_y > 0
      base_y -= draw_height * y_mult
    elsif base_y < 0 && base_y.abs >= draw_height
      base_y += draw_height * y_mult
    end

    if viewport
      base_x += viewport.rect.x - viewport.ox
      base_y += viewport.rect.y - viewport.oy
    end

    @transformable.set_position(base_x, base_y)
    @render_states.transform = @transformable.transform
    @render_states.texture = @bitmap.texture
    Graphics.screen.draw(@vertex_array, @render_states)

    Graphics.screen.clip_rect = old_clip_rect if revert_clip_rect
    @bitmap.texture.repeated = false if revert_repeat
  end

end

class TilemapPlane < Plane

  def draw
    return unless @bitmap && !@bitmap.disposed?
    pos_x = 0
    pos_y = 0

    old_clip_rect = Graphics.screen.clip_rect
    revert_clip_rect = false
    if viewport
      viewport_rect = viewport.rect
      if viewport_rect != old_clip_rect
        Graphics.screen.clip_rect = viewport_rect
        revert_clip_rect = true
      end
      pos_x += viewport.rect.x - viewport.ox
      pos_y += viewport.rect.y - viewport.oy
    end

    @vertex_array.texture_rect = @bitmap.rect
    @transformable.set_origin(@ox, @oy)
    @transformable.set_position(pos_x, pos_y)
    @render_states.transform = @transformable.transform
    @render_states.texture = @bitmap.texture

    Graphics.screen.draw(@vertex_array, @render_states)
    Graphics.screen.clip_rect = old_clip_rect if revert_clip_rect
  end

end