require 'zlib'
require_relative 'inifile/inifile'
require_relative 'rgfw/rgfw'

if RUBY_PLATFORM['mingw'] || RUBY_PLATFORM['mswin']
  require 'win32api'
end