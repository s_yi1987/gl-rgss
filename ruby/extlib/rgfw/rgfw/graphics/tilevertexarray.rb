class RGFW::TileVertexArray
  def dup
    RGFW::TileVertexArray.new(self)
  end

  def clone
    dup
  end
end