
class RGFW::Transform
  IDENTITY = IDENTITY()

  class << self
    remove_method(:IDENTITY)
  end

  def dup
    RGFW::Transform.new(self)
  end

  def clone
    dup
  end
end


class RGFW::Transformable
  def dup
    RGFW::Transform.new(self)
  end

  def clone
    dup
  end
end