class RGFW::RenderStates
	include RGFW::NativeObj
	
	alias native_initialize initialize
	def initialize(*args)
		case args.size
		when 1
			if args[0].respond_to?(:native_obj)
				arg = args[0]
				args[0] = arg.native_obj
				if arg.is_a? RGFW::Texture
					@texture = arg
				end
			end
		end
		native_initialize(*args)
	end
	
	alias native_transform transform
	def transform
		get_native_obj(:native_transform, false)
	end
	
	alias :native_set_texture :texture=
	def texture=(val)
		native_set_texture(val.native_obj)
		@texture = val
	end
	
	def texture
		@texture
	end
	
	private :native_transform, :native_set_texture
end