class RGFW::IndexVertexArray
  alias :native_initialize :initialize
  def initialize(*args)
    native_initialize(*args)
    @__clear_count = 0
  end

  alias :native_clear :clear
  def clear
    @__clear_count += 1
    native_clear
  end

  alias :native_resize_vertexarray :resize_vertexarray
  def resize_vertexarray(v_count)
    v_count = [v_count, 0].max
    @__clear_count += 1 if v_count < vertex_count
    native_resize_vertexarray(v_count) if vertex_count != v_count
  end

  alias :native_get_vertex :get_vertex
  def get_vertex(index)
    throw IndexError if index >= vertex_count
    vertex = native_get_vertex(index)
    _mark_vertex(vertex)
    vertex
  end

  def _mark_vertex(vertex)
    vertex.instance_variable_set(:@__ref_varray, self)
    vertex.instance_variable_set(:@__clear_stamp, @__clear_count)
  end

  def dup
    RGFW::IndexVertexArray.new(self)
  end

  def clone
    dup
  end

  private :native_initialize, :native_clear, :native_resize_vertexarray,
          :native_get_vertex, :_mark_vertex
end