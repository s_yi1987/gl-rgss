class RGFW::VertexData
  include RGFW::NativeObj

  alias native_vertices vertices
  def vertices
    verts = get_native_obj(:native_vertices, false)
    _mark_vertex(verts)
    verts
  end

  alias native_indices indices
  def indices
    get_native_obj(:native_indices, false)
  end

  def _mark_vertex(vertex)
    vertex.instance_variable_set(:@__clear_stamp, @__clear_count)
  end

  private :native_vertices, :native_indices, :_mark_vertex
end