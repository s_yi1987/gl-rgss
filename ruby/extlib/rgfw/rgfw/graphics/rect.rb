class RGFW::Rect
  alias native_initialize initialize
  def initialize(*args)
    case args.size
    when 4
      native_initialize(args[0].to_i, args[1].to_i, args[2].to_i, args[03].to_i)
    else
      native_initialize(*args)
    end
  end

  alias native_set set
  def set(*args)
    case args.size
    when 4
      native_set(args[0].to_i, args[1].to_i, args[2].to_i, args[03].to_i)
    else
      native_set(*args)
    end
  end

  alias :set_native_x :x=
  def x=(val)
    set_native_x(val.to_i)
  end

  alias :set_native_y :y=
  def y=(val)
     set_native_y(val.to_i)
  end

  alias :set_native_width :width=
  def width=(val)
    set_native_width(val.to_i)
  end

  alias :set_native_height :height=
  def height=(val)
    set_native_height(val.to_i)
  end

  def dup
    RGFW::Rect.new(self)
  end

  def clone
    dup
  end

  private :native_initialize, :native_set, :set_native_x, :set_native_y,
          :set_native_width, :set_native_height
end