class RGFW::NativeRenderWindow
  def dup
    throw NotImplementedError
  end

  def clone
    dup
  end
end

class RGFW::RenderWindow
  attr_reader :native_obj

  def initialize(*args)
    @native_obj = RGFW::NativeRenderWindow.new(*args)
    ObjectSpace.define_finalizer(self, self.class.finalize(@native_obj))
  end

  def self.finalize(native_window)
    proc{ native_window.dispose }
  end

  def dispose
    @native_obj.dispose
  end

  def disposed?
    @native_obj.disposed?
  end

  def bind_vbo(vbo)
    @native_obj.bind_vbo(vbo.native_obj)
  end

  def update_current_vbo(*args)
    @native_obj.update_current_vbo(*args)
  end

  def update_current_vertexbuffer(*args)
    @native_obj.update_current_vertexbuffer(*args)
  end

  def update_current_indexbuffer(*args)
    @native_obj.update_current_indexbuffer(*args)
  end

  def draw(*args)
    args[0] = args[0].native_obj if args[0].is_a? RGFW::VertexBufferObject
    @native_obj.draw(*args)
  end

  def view=(val)
    @native_obj.view = val
  end

  def view
    @native_obj.view
  end

  def default_view=(val)
    @native_obj.default_view = val
  end

  def default_view
    @native_obj.default_view
  end

  def get_viewport(val)
    @native_obj.get_viewport(val)
  end

  def clip_rect=(rect)
    @native_obj.clip_rect = rect
  end

  def clip_rect
    @native_obj.clip_rect
  end

  def render_target=(texture)
    set_render_target(texture)
  end

  def set_render_target(texture, adjust_view = true)
    return if @render_target == texture
    native_texture = nil
    if texture
      @render_target = texture.disposed? ? nil : texture
      native_texture = texture.disposed? ? nil : texture.native_obj
    else
      @render_target = nil
    end
    @native_obj.set_render_target(native_texture, adjust_view)
  end

  def render_target
    set_render_target(nil, false) if @render_target && @render_target.disposed?
    @render_target
  end

  def active=(active)
    set_active(active)
  end

  def set_active(active = true)
    @native_obj.set_active(active)
  end

  def active?
    @native_obj.active?
  end

  def clear(*args)
    @native_obj.clear(*args)
  end

  def present
    @native_obj.present
  end

  def vertical_sync_enabled=(val)
    @native_obj.vertical_sync_enabled = val
  end

  def set_position(*args)
    @native_obj.set_position(*args)
  end

  def position
    @native_obj.position
  end

  def set_size(*args)
    @native_obj.set_size(*args)
  end

  def size
    @native_obj.size
  end

  def fullscreen_mode=(mode)
    @native_obj.fullscreen_mode = mode
  end

  def fullscreen_mode
    @native_obj.fullscreen_mode
  end

  def rect
    @native_obj.rect
  end

  def bordered=(val)
    @native_obj.bordered = val
  end

  def bordered?
    @native_obj.bordered?
  end

  def grabmode=(val)
    @native_obj.grabmode = val
  end

  def grabmode?
    @native_obj.grabmode?
  end

  def brightness=(val)
    @native_obj.brightness = val
  end

  def brightness
    @native_obj.brightness
  end

  def title=(str)
    @native_obj.title = str
  end

  def title
    @native_obj.title
  end

  def flags
    @native_obj.flags
  end

  def dup
    @native_obj.dup
  end

  def clone
    dup
  end
end