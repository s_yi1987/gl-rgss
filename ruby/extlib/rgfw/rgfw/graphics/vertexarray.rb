class RGFW::VertexArray
  include RGFW::NativeObj
  
  alias :native_initialize :initialize
  def initialize(*args)
    native_initialize(*args)
    @__clear_count = 0
  end

  alias :native_clear :clear
  def clear
    @__clear_count += 1
    native_clear
  end

  alias :native_resize :resize
  def resize(v_count)
    v_count = 0 if v_count < 0
    @__clear_count += 1    if v_count < vertex_count
    native_resize(v_count) if vertex_count != v_count
  end

  alias :native_index_operator :[]
  def [](index)
    throw IndexError if index >= vertex_count
    vertex = get_native_obj_args(:native_index_operator, false, index)
    #_mark_vertex(vertex)
    vertex
  end
  
  def dup
    RGFW::VertexArray.new(self)
  end

  def clone
    dup
  end

  private :native_initialize, :native_clear, :native_resize,
          :native_index_operator
end