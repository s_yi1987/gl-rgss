
class RGFW::SpriteVertexData
  alias :native_set_color :color=
  def color=(val)
    val = RGFW::Color.new(val) if val.is_a? RGFW::PlainColor
    native_set_color(val)
  end

  def dup
    RGFW::SpriteVertexData.new(self)
  end

  def clone
    dup
  end

  private :native_set_color
end