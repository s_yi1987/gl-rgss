
class RGFW::NativeFont

  def dup
    throw NotImplementedError
  end

  def clone
    dup
  end

end


class RGFW::Font
  attr_reader :native_obj

  def initialize(*args)
    @native_obj = RGFW::NativeFont.new(*args)
    ObjectSpace.define_finalizer(self, self.class.finalize(@native_obj))
  end

  def self.finalize(native_font)
    proc{ native_font.dispose }
  end

  def dispose
    @native_obj.dispose
  end

  def disposed?
    @native_obj.disposed?
  end

  def setup(*args)
    @native_obj.setup(*args)
  end

  def style=(val)
    @native_obj.style = val
  end

  def style
    @native_obj.style
  end

  def outline=(val)
    @native_obj.outline = val
  end

  def outline
    @native_obj.outline
  end

  def hinting=(val)
    @native_obj.hinting = val
  end

  def hinting
    @native_obj.hinting
  end

  def kerning=(val)
    @native_obj.kerning = val
  end

  def kerning
    @native_obj.kerning
  end

  def height
    @native_obj.height
  end

  def ascent
    @native_obj.ascent
  end

  def descent
    @native_obj.descent
  end

  def line_skip
    @native_obj.line_skip
  end

  def family_name
    @native_obj.family_name
  end

  def style_name
    @native_obj.style_name
  end

  def calc_text_size(str)
    @native_obj.calc_text_size(str)
  end

  def fixed_face_width?
    @native_obj.fixed_face_width?
  end

  def to_surface_text_solid(*args)
    @native_obj.to_surface_text_solid(*args)
  end

  def render_text_solid_to(*args)
    @native_obj.render_text_solid_to(*args)
  end

  def to_surface_utf8_solid(*args)
    @native_obj.to_surface_utf8_solid(*args)
  end

  def render_utf8_solid_to(*args)
    @native_obj.render_utf8_solid_to(*args)
  end

  def to_surface_unicode_solid(*args)
    @native_obj.to_surface_unicode_solid(*args)
  end

  def render_unicode_solid_to(*args)
    @native_obj.render_unicode_solid_to(*args)
  end

  def to_surface_text_shaded(*args)
    @native_obj.to_surface_text_shaded(*args)
  end

  def render_text_shaded_to(*args)
    @native_obj.render_text_shaded_to(*args)
  end

  def to_surface_utf8_shaded(*args)
    @native_obj.to_surface_utf8_shaded(*args)
  end

  def render_utf8_shaded_to(*args)
    @native_obj.render_utf8_shaded_to(*args)
  end

  def to_surface_unicode_shaded(*args)
    @native_obj.to_surface_unicode_shaded(*args)
  end

  def render_unicode_shaded_to(*args)
    @native_obj.render_unicode_shaded_to(*args)
  end

  def to_surface_text_blended(*args)
    @native_obj.to_surface_text_blended(*args)
  end

  def render_text_blended_to(*args)
    @native_obj.render_text_blended_to(*args)
  end

  def to_surface_utf8_blended(*args)
    @native_obj.to_surface_utf8_blended(*args)
  end

  def render_utf8_blended_to(*args)
    @native_obj.render_utf8_blended_to(*args)
  end

  def to_surface_unicode_blended(*args)
    @native_obj.to_surface_unicode_blended(*args)
  end

  def render_unicode_blended_to(*args)
    @native_obj.render_unicode_blended_to(*args)
  end

end