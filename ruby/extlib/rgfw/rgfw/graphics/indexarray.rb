class RGFW::IndexArray
  include RGFW::NativeObj

  alias native_indices indices
  def indices
    get_native_obj(:native_indices, false)
  end

  private :native_indices
end