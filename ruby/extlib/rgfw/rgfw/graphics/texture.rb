
#Don't access the NativeTexture's methods directly
#Use the Texture class instead

class RGFW::NativeTexture
  def dup
    RGFW::NativeTexture.new(self)
  end

  def clone
    dup
  end

  alias :color_mod= :set_color_mod
end

class RGFW::Texture
  attr_reader :native_obj

  def initialize(*args)
    if args.size == 1 && args[0].is_a?(RGFW::Texture)
      @native_obj = RGFW::NativeTexture.new(args[0].native_obj)
    else
      @native_obj = RGFW::NativeTexture.new(*args)
    end
    ObjectSpace.define_finalizer(self, self.class.finalize(@native_obj))
  end

  def self.finalize(native_texture)
    proc{ native_texture.dispose }
  end

  def setup(*args)
    @native_obj.setup(*args)
  end

  def update(*args)
    @native_obj.update(*args)
  end

  def dispose
    @native_obj.dispose
  end

  def disposed?
    @native_obj.disposed?
  end

  def window=(window)
    @window = window
    @native_obj.window = window.native_obj if window
  end

  def window
    @window = nil if @window.disposed?
    @window
  end

  def color_mod=(color)
    @native_obj.color_mod = color
  end

  def color_mod
    @native_obj.color_mod
  end

  def alpha_mod=(alpha_val)
    @native_obj.alpha_mod = alpha_val
  end

  def alpha_mod
    @native_obj.alpha_mod
  end

  def blend_mode=(blend_val)
    @native_obj.blend_mode = blend_val
  end

  def blend_mode
    @native_obj.blend_mode
  end

  def clip_rect
    @native_obj.clip_rect
  end

  def clip_rect=(val)
    @native_obj.clip_rect = val
  end

  def blit(src, src_rect, dst_rect)
    @native_obj.blit(src.native_obj, src_rect, dst_rect)
  end

  def blit_scaled(src, src_rect, dst_rect)
    @native_obj.blit_scaled(src.native_obj, src_rect, dst_rect)
  end

  def fill_rect(*args)
    @native_obj.fill_rect(*args)
  end

  def width
    @native_obj.width
  end

  def height
    @native_obj.height
  end

  def actual_width
    @native_obj.actual_width
  end

  def actual_height
    @native_obj.actual_height
  end

  def rect
    @native_obj.rect
  end

  def smooth=(val)
    @native_obj.smooth = val
  end

  def smooth?
    @native_obj.smooth?
  end

  def repeated=(val)
    @native_obj.repeated = val
  end

  def repeated?
    @native_obj.repeated?
  end

  def to_surface
    @native_obj.to_surface
  end

  def download_to(surface)
    @native_obj.download_to(surface)
  end

  def dup
    RGFW::Texture.new(self)
  end

  def clone
    dup
  end

  def self.get_valid_size(val)
    RGFW::NativeTexture.get_valid_size(val)
  end

  def self.maximum_size
    RGFW::NativeTexture.maximum_size
  end
end