
class RGFW::Surface
  def dup
    RGFW::Surface.new(self)
  end

  def clone
    dup
  end
end