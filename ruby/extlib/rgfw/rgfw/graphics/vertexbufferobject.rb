class RGFW::NativeVertexBufferObject
  def dup
    throw NotImplementedError
  end

  def clone
    dup
  end
end


class RGFW::VertexBufferObject
  attr_reader :native_obj

  def initialize(*args)
    @native_obj = RGFW::NativeVertexBufferObject.new(*args)
    ObjectSpace.define_finalizer(self, self.class.finalize(@native_obj))
  end

  def self.finalize(native_vbo)
    proc{ native_vbo.dispose }
  end

  def setup(*args)
    @native_obj.setup(*args)
  end

  def update(*args)
    @native_obj.update(*args)
  end

  def update_vertexbuffer(*args)
    @native_obj.update_vertexbuffer(*args)
  end

  def update_indexbuffer(*args)
    @native_obj.update_indexbuffer(*args)
  end

  def dispose
    @native_obj.dispose
  end

  def disposed?
    @native_obj.disposed?
  end

  def vertex_capacity
    @native_obj.vertex_capacity
  end

  def index_capacity
    @native_obj.index_capacity
  end
  
  def vertex_read_size=(val)
    @native_obj.vertex_read_size = val
  end
  
  def vertex_read_size
    @native_obj.vertex_read_size
  end
  
  def index_read_size=(val)
    @native_obj.index_read_size = val
  end
  
  def index_read_size
    @native_obj.index_read_size
  end

  def mode
    @native_obj.mode
  end

  def primitive_type=(val)
    @native_obj.primitive_type = val
  end

  def primitive_type
    @native_obj.primitive_type
  end

end