class RGFW::Vertex
  include RGFW::NativeObj

  alias :native_set_pos :position=
  def position=(val)
    set_native_obj(:native_set_pos, true, val)
  end

  alias native_position position
  def position
    get_native_obj(:native_position, true)
  end

  alias :native_set_color :color=
  def color=(val)
    val = val.to_plain if val.is_a? RGFW::Color
    set_native_obj(:native_set_color, true, val)
  end

  alias native_color color
  def color
    get_native_obj(:native_color, true)
  end

  alias :native_set_tcoords :tex_coords=
  def tex_coords=(val)
    set_native_obj(:native_set_tcoords, true, val)
  end

  alias native_tex_coords tex_coords
  def tex_coords
    get_native_obj(:native_tex_coords, true)
  end

  def native_obj_disposed?
    if @__ref_owner
      @__clear_stamp != @__ref_owner.instance_variable_get(:@__clear_count)
    else
      false
    end
  end

  def disposed?
    native_obj_disposed?
  end

  def dup
    throw RuntimeError if disposed?
    RGFW::Vertex.new(self)
  end

  def clone
    dup
  end

  private :native_position, :native_color, :native_tex_coords,
          :native_set_color, :native_set_pos, :native_set_tcoords
end