
class RGFW::PlainColor
  alias native_initialize initialize
  def initialize(*args)
    case args.size
    when 4
      native_initialize(RGFW.ensure_within_color_range(args[0].to_i),
                        RGFW.ensure_within_color_range(args[1].to_i),
                        RGFW.ensure_within_color_range(args[2].to_i),
                        RGFW.ensure_within_color_range(args[3].to_i))
    when 3
      native_initialize(RGFW.ensure_within_color_range(args[0].to_i),
                        RGFW.ensure_within_color_range(args[1].to_i),
                        RGFW.ensure_within_color_range(args[2].to_i))
    when 1
      args[0] = args[0].to_plain if args[0].is_a? RGFW::Color
      native_initialize(*args)
    else
      native_initialize(*args)
    end
  end

  alias :native_set_r :r=
  def r=(val)
    native_set_r(RGFW.ensure_within_color_range(val.to_i))#[[0, val].max, 255].min
  end

  alias :native_set_g :g=
  def g=(val)
    native_set_g(RGFW.ensure_within_color_range(val.to_i))
  end

  alias :native_set_b :b=
  def b=(val)
    native_set_b(RGFW.ensure_within_color_range(val.to_i))
  end

  alias :native_set_a :a=
  def a=(val)
    native_set_a(RGFW.ensure_within_color_range(val.to_i))
  end

  alias red r
  alias green g
  alias blue b
  alias alpha a

  alias :red= :r=
  alias :green= :g=
  alias :blue= :b=
  alias :alpha= :a=

  def dup
    RGFW::PlainColor.new(self)
  end

  def clone
    dup
  end

  private :native_initialize, :native_set_r, :native_set_g, :native_set_b, :native_set_a
end


class RGFW::Color
  alias native_initialize initialize
  def initialize(*args)
    case args.size
    when 4
      native_initialize(RGFW.ensure_within_color_range(args[0]),
                        RGFW.ensure_within_color_range(args[1]),
                        RGFW.ensure_within_color_range(args[2]),
                        RGFW.ensure_within_color_range(args[3]))
    when 3
      native_initialize(RGFW.ensure_within_color_range(args[0]),
                        RGFW.ensure_within_color_range(args[1]),
                        RGFW.ensure_within_color_range(args[2]))
    else
      native_initialize(*args)
    end
  end

  alias native_set set
  def set(*args)
    case args.size
    when 4
      native_set(RGFW.ensure_within_color_range(args[0]),
                 RGFW.ensure_within_color_range(args[1]),
                 RGFW.ensure_within_color_range(args[2]),
                 RGFW.ensure_within_color_range(args[3]))
    when 3
      native_set(RGFW.ensure_within_color_range(args[0]),
                 RGFW.ensure_within_color_range(args[1]),
                 RGFW.ensure_within_color_range(args[2]))
    else
      native_set(*args)
    end
  end

  alias :native_set_r :r=
  def r=(val)
    native_set_r(RGFW.ensure_within_color_range(val))
  end

  alias :native_set_g :g=
  def g=(val)
    native_set_g(RGFW.ensure_within_color_range(val))
  end

  alias :native_set_b :b=
  def b=(val)
    native_set_b(RGFW.ensure_within_color_range(val))
  end

  alias :native_set_a :a=
  def a=(val)
    native_set_a(RGFW.ensure_within_color_range(val))
  end

  alias red r
  alias green g
  alias blue b
  alias alpha a

  alias :red= :r=
  alias :green= :g=
  alias :blue= :b=
  alias :alpha= :a=

  def dup
    RGFW::Color.new(self)
  end

  def clone
    dup
  end

  private :native_set, :native_set_r, :native_set_g, :native_set_b, :native_set_a
end