
class RGFW::LibMain
  def dup
    throw NotImplementedError
  end

  def clone
    dup
  end
end

module RGFW
  @main = LibMain.new

  def self.init
    res = @main.init
    throw RuntimeError("Failed to initialize RGFW: " + last_error) if !res
    res
  end

  def self.ticks
    @main.ticks
  end

  def self.performance_counter
    @main.performance_counter
  end

  def self.performance_frequency
    @main.performance_frequency
  end

  def self.delay(ms)
    @main.delay(ms)
  end

  def self.last_error
    @main.last_error
  end

  def self.last_font_error
    @main.last_font_error
  end
end