class RGFW::Vector2i
  def dup
    RGFW::Vector2i.new(self)
  end

  def clone
    dup
  end
end

class RGFW::Vector2u
  def dup
    RGFW::Vector2u.new(self)
  end

  def clone
    dup
  end
end

class RGFW::Vector2f
  def dup
    RGFW::Vector2f.new(self)
  end

  def clone
    dup
  end
end