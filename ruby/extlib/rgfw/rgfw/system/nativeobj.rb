module RGFW::NativeObj
  def set_native_obj(sym, check_disposed, *args)
    throw RuntimeError if check_disposed && native_obj_disposed?
    method(sym).call(*args)
  end

  def get_native_obj(sym, check_disposed)
    throw RuntimeError if check_disposed && native_obj_disposed?
    native_obj = method(sym).call
    return native_obj if !native_obj
    native_obj.instance_variable_set(:@__ref_owner, self)
    native_obj
  end

  def get_native_obj_args(sym, check_disposed, *args)
    throw RuntimeError if check_disposed && native_obj_disposed?
    native_obj = method(sym).call(*args)
    return native_obj if !native_obj
    native_obj.instance_variable_set(:@__ref_owner, self)
    native_obj
  end

  def native_obj_disposed?
    throw NotImplementedError
  end

  private :set_native_obj, :get_native_obj, :get_native_obj_args
end