
class RGFW::KeyboardEvent
  include RGFW::NativeObj

  alias native_keysym keysym
  def keysym
    get_native_obj(:native_keysym, false)
  end

  private :native_keysym
end


class RGFW::Event
  include RGFW::NativeObj

  alias native_window window
  def window
    get_event_struct(:native_window)
  end

  alias native_key key
  def key
    get_event_struct(:native_key)
  end

  alias native_edit edit
  def edit
    get_event_struct(:native_edit)
  end

  alias native_text text
  def text
    get_event_struct(:native_text)
  end

  alias native_motion motion
  def motion
    get_event_struct(:native_motion)
  end

  alias native_button button
  def button
    get_event_struct(:native_button)
  end

  alias native_wheel wheel
  def wheel
    get_event_struct(:native_wheel)
  end

  alias native_jaxis jaxis
  def jaxis
    get_event_struct(:native_jaxis)
  end

  alias native_jball jball
  def jball
    get_event_struct(:native_jball)
  end

  alias native_jhat jhat
  def jhat
    get_event_struct(:native_jhat)
  end

  alias native_jbutton jbutton
  def jbutton
    get_event_struct(:native_jbutton)
  end

  alias native_jdevice jdevice
  def jdevice
    get_event_struct(:native_jdevice)
  end

  alias native_caxis caxis
  def caxis
    get_event_struct(:native_caxis)
  end

  alias native_cbutton cbutton
  def cbutton
    get_event_struct(:native_cbutton)
  end

  alias native_cdevice cdevice
  def cdevice
    get_event_struct(:native_cdevice)
  end

  alias native_quit quit
  def quit
    get_event_struct(:native_quit)
  end

  alias native_user user
  def user
    get_event_struct(:native_user)
  end

  alias native_tfinger tfinger
  def tfinger
    get_event_struct(:native_tfinger)
  end

  alias native_mgesture mgesture
  def mgesture
    get_event_struct(:native_mgesture)
  end

  alias native_dgesture dgesture
  def dgesture
    get_event_struct(:native_dgesture)
  end

  def get_event_struct(struct_sym)
    get_native_obj(struct_sym, false)
  end

  def dup
    RGFW::Event.new(self)
  end

  def clone
    dup
  end

  private :native_window, :native_key, :native_edit, :native_text, :native_motion, :native_button, :native_wheel,
          :native_jaxis, :native_jball, :native_jhat, :native_jbutton, :native_jdevice, :native_caxis, :native_cbutton,
          :native_cdevice, :native_quit, :native_user, :native_tfinger, :native_mgesture, :native_dgesture
end