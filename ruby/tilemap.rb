class Tilemap
  TILE_SIZE = 32

  PASSABLE_TILE   = 0x00
  IMPASSABLE_TILE = 0x01
  OVER_TILE       = 0x10

  A1 = [
    [18,17,14,13], [ 2,14,17,18], [13, 3,17,18], [ 2, 3,17,18],
    [13,14,17, 7], [ 2,14,17, 7], [13, 3,17, 7], [ 2, 3,17, 7],
    [13,14, 6,18], [ 2,14, 6,18], [13, 3, 6,18], [ 2, 3, 6,18],
    [13,14, 6, 7], [ 2,14, 6, 7], [13, 3, 6, 7], [ 2, 3, 6, 7],
    [16,17,12,13], [16, 3,12,13], [16,17,12, 7], [12, 3,16, 7],
    [10, 9,14,13], [10, 9,14, 7], [10, 9, 6,13], [10, 9, 6, 7],
    [18,19,14,15], [18,19, 6,15], [ 2,19,14,15], [ 2,19, 6,15],
    [18,17,22,21], [ 2,17,22,21], [18, 3,22,21], [ 2, 3,21,22],
    [16,19,12,15], [10, 9,22,21], [ 8, 9,12,13], [ 8, 9,12, 7],
    [10,11,14,15], [10,11, 6,15], [18,19,22,23], [ 2,19,22,23],
    [16,17,20,21], [16, 3,20,21], [ 8,11,12,15], [ 8, 9,20,21],
    [16,19,20,23], [10,11,22,23], [ 8,11,20,23], [ 0, 1, 4, 5]
  ]

  A1POS = [
    [0, 0], [0, TILE_SIZE * 3], [TILE_SIZE * 6, 0], [TILE_SIZE * 6,TILE_SIZE * 3],
    [TILE_SIZE * 8, 0], [TILE_SIZE * 14, 0], [TILE_SIZE * 8,TILE_SIZE * 3], [TILE_SIZE * 14, TILE_SIZE * 3],
    [0, TILE_SIZE * 6], [TILE_SIZE * 6,TILE_SIZE * 6], [0, TILE_SIZE * 9], [TILE_SIZE * 6,TILE_SIZE * 9],
    [TILE_SIZE * 8,TILE_SIZE * 6], [TILE_SIZE * 14,TILE_SIZE * 6], [TILE_SIZE * 8,TILE_SIZE * 9], [TILE_SIZE * 14,TILE_SIZE * 9]
  ]

  A1E = [
    [0,1,6,7],[0,1,4,5],[2,3,6,7],[1,2,5,6]
  ]

  A3 = [
    [5, 6, 9, 10], [4, 5, 8, 9], [1,2, 5, 6], [0,1, 4, 5],
    [6, 7, 10,11], [4, 7, 8,11], [2,3, 6, 7], [0,3, 4, 7],
    [9, 10,13,14], [8, 9,12,13], [1,2,13,14], [0,1,12,13],
    [10,11,14,15], [8,11,12,13], [2,3,14,15], [0,3,12,15]
  ]


  @@cached_tile_rects = {}

  attr_accessor :bitmaps
  attr_reader   :map_data
  attr_accessor :flash_data
  attr_accessor :flags
  attr_accessor :viewport
  attr_accessor :visible
  attr_reader   :ox
  attr_reader   :oy


  def initialize(viewport = nil)
    @bitmaps = []
    @viewport = viewport
    @visible = true
    @ox = 0
    @oy = 0
    @anim_count = 0
    @disposed = false
    vbo_cls = RGFW::VertexBufferObject
    @anim_vbo_lists = [
      [vbo_cls.new, vbo_cls.new, vbo_cls.new, vbo_cls.new, vbo_cls.new],
      [vbo_cls.new, vbo_cls.new, vbo_cls.new, vbo_cls.new, vbo_cls.new],
      [vbo_cls.new, vbo_cls.new, vbo_cls.new, vbo_cls.new, vbo_cls.new]
    ]
    @upper_vbo_lists = [
      [vbo_cls.new, vbo_cls.new],
      [vbo_cls.new, vbo_cls.new]
    ]
    @layers = [TilemapLayer.new, TilemapLayer.new, TilemapLayer.new]
    @layers[0].z = 0
    @layers[0].viewport = @viewport
    @layers[1].z = 100
    @layers[1].viewport = @viewport
    @layers[2].z = 200
    @layers[2].viewport = @viewport
  end

  def dispose
    for vbos in @anim_vbo_lists
      for vbo in vbos
        vbo.dispose if vbo
      end
    end
    for vbos in @upper_vbo_lists
      for vbo in vbos
        vbo.dispose if vbo
      end
    end
    for layer in @layers
      layer.dispose
    end
    @disposed = true
  end

  def disposed?
    @disposed
  end

  def update
    frame_mult = @need_animation ? 3 : 1
    @anim_count = (@anim_count + 1) % (frame_mult * 30)
    @layers[0].bitmaps = @bitmaps
    @layers[0].vbos = @anim_vbo_lists[@anim_count / 30]
  end

  def refresh
    return if @map_data.nil? || @flags.nil?
    generate_anim_layers
    generate_upper_layers
  end

  def generate_anim_layers
    v_arr = RGFW::TileVertexArray.new
    vertex_array_lists = []
    if need_animated_layer?
      @need_animation = true
      vertex_array_lists.push([v_arr,     v_arr.dup, v_arr.dup, v_arr.dup, v_arr.dup])
      vertex_array_lists.push([v_arr.dup, v_arr.dup, v_arr.dup, v_arr.dup, v_arr.dup])
      vertex_array_lists.push([v_arr.dup, v_arr.dup, v_arr.dup, v_arr.dup, v_arr.dup])
    else
      @need_animation = false
      vertex_array_lists.push([v_arr, v_arr.dup, v_arr.dup, v_arr.dup, v_arr.dup])
    end
    collect_anim_tiles(vertex_array_lists)
    setup_vbos(@anim_vbo_lists, vertex_array_lists)
    @layers[0].bitmaps = @bitmaps
    @layers[0].vbos = @anim_vbo_lists[0]
    vertex_array_lists.clear
  end

  def collect_anim_tiles(vertex_array_lists)
    @@non_anim_autotiles ||= [2, 3]
    for x in 0..@map_data.xsize - 1
      for y in 0..@map_data.ysize - 1
        id = @map_data[x, y, 0]
        autotile = (id - 2048) / 48
        tileset_bitmap_index = bitmap_for_autotile(autotile)
        should_animate = !@@non_anim_autotiles.include?(autotile)
        for i in 0..vertex_array_lists.size - 1
          vertex_arrays = vertex_array_lists[i]
          if tileset_bitmap_index
            frame = should_animate ? i : 0
            vertex_array = vertex_arrays[tileset_bitmap_index]
            collect_anim_tile(vertex_array, x, y, 0, frame) if id.between?(2048, 2815)
            collect_anim_tile(vertex_array, x, y, 0) if id.between?(2816, 8191)
          end
          collect_A5_tile(vertex_arrays[4], x, y, 0) if id.between?(1536, 1663)
        end
        id = @map_data[x, y, 1]
        autotile = (id - 2048) / 48
        tileset_bitmap_index = bitmap_for_autotile(autotile)
        if tileset_bitmap_index
          should_animate = !@@non_anim_autotiles.include?(autotile)
          for i in 0..vertex_array_lists.size - 1
            frame = should_animate ? i : 0
            vertex_array = vertex_array_lists[i][tileset_bitmap_index]
            collect_anim_tile(vertex_array, x, y, 1, frame) if id.between?(2048, 2815)
            collect_anim_tile(vertex_array, x, y, 1) if id.between?(2816, 4351)
          end
        end#if tileset_bitmap_index
      end
    end
  end

  def setup_vbos(vbo_lists, vertex_array_lists)
    for i in 0..vertex_array_lists.size - 1
      vbos = vbo_lists[i]
      vertex_arrays = vertex_array_lists[i]
      for j in 0..vertex_arrays.size - 1
        vbo = vbos[j]
        vertex_array = vertex_arrays[j]
        if vertex_array.vertex_count > 0
          vbo.setup(vertex_array, RGFW::VBO_STATIC_DRAW)
          vbo.primitive_type = vertex_array.primitive_type
          vertex_array.clear
        else
          vbo.dispose
        end
      end
    end
  end

  def bitmap_for_autotile(autotile)
    return 0 if autotile.between?(0, 15)
    return 1 if autotile.between?(16, 47)
    return 2 if autotile.between?(48, 79)
    return 3 if autotile.between?(80, 127)
  end

  def collect_anim_tile(vertex_array, x, y, z, frame = 0)
    pos = RGFW::Transformable.new
    rect_entries = get_anim_layer_rects(x, y, z, frame)
    for rect_entry in rect_entries
      pos.set_position((x * TILE_SIZE) + rect_entry[1], (y * TILE_SIZE) + rect_entry[2])
      vertex_array.append(rect_entry[0], pos.transform)
    end
  end

  def collect_A5_tile(vertex_array, x, y, z)
    id = @map_data[x, y, z]
    adjusted_id = id - 1536
    key = [:a5, adjusted_id]
    tile_rect = @@cached_tile_rects[key]
    if !tile_rect
      tr_x = TILE_SIZE * (adjusted_id % 8)
      tr_y = TILE_SIZE * ((adjusted_id % 128) / 8)
      tile_rect = Rect.new(tr_x, tr_y, TILE_SIZE, TILE_SIZE)
      @@cached_tile_rects[key] = tile_rect
    end
    pos = RGFW::Transform.new
    pos.translate(x * TILE_SIZE, y * TILE_SIZE)
    vertex_array.append(tile_rect, pos)
  end

  def get_anim_layer_rects(x, y, z, frame = 0)
    id = @map_data[x, y, z]
    rects = nil
    if id.between?(2048, 2815) || id.between?(2816, 4351)
      rects = get_A1_vertices(id, frame)
    elsif id.between?(4352, 5887)
      rects = get_A3_vertices(id)
    elsif id.between?(5888, 8191)
      rects = get_A4_vertices(id)
    end
    rects
  end

  def get_A1_vertices(id, frame = 0)
    autotile = (id - 2048) / 48
    @@waterfall_autotiles ||= [5, 7, 9, 11, 13, 15]
    return get_waterfall_vertices(id, frame) if @@waterfall_autotiles.include?(autotile)
    tileset_bitmap_index = bitmap_for_autotile(autotile)
    case tileset_bitmap_index
    when 0
      atile_x = A1POS[autotile][0]
      atile_y = A1POS[autotile][1]
    when 1
      atile_x = (TILE_SIZE * 2) * ((autotile - 16) % 8)
      atile_y = (TILE_SIZE * 3) * ((autotile - 16) / 8)
    when 2
      atile_x = (TILE_SIZE * 2) * ((autotile - 48) % 8)
      atile_y = (TILE_SIZE * 2) * ((autotile - 48) / 8)
    when 3
      atile_x = (TILE_SIZE * 2) * ((autotile - 80) % 8)
      atile_y = (TILE_SIZE * 3) * ((((autotile - 80) / 8) + 1) / 2) + (TILE_SIZE * 2) * (((autotile - 80) / 8) / 2)
    end
    atile_x += (TILE_SIZE * 2) * frame

    index = (id - 2048) % 48
    get_autotile_rects(A1[index], atile_x, atile_y)
  end

  def get_waterfall_vertices(id, frame = 0)
    autotile = (id - 2048) / 48
    index = (id - 2048) % 48
    atile_x = A1POS[autotile][0]
    atile_y = A1POS[autotile][1] + (TILE_SIZE * frame)
    get_autotile_rects(A1E[index], atile_x, atile_y)
  end

  def get_autotile_rects(autotile_data, atile_x, atile_y)
    key = [autotile_data, atile_x, atile_y]
    tile_rects = @@cached_tile_rects[key]
    return tile_rects if tile_rects

    tile_rects = create_autotile_rects(autotile_data, atile_x, atile_y)
    @@cached_tile_rects[key] = tile_rects
    tile_rects
  end

  def create_autotile_rects(autotile_data, atile_x, atile_y)
    autotile_rects = []
    src_rect = Rect.new(0, 0, TILE_SIZE / 2, TILE_SIZE / 2)
    for i in 0..3
      src_rect.x = (TILE_SIZE / 2) * (autotile_data[i] % 4) + atile_x
      src_rect.y = (TILE_SIZE / 2) * (autotile_data[i] / 4) + atile_y
      autotile_rects.push([src_rect.dup, (i % 2) * TILE_SIZE / 2, (i / 2) * TILE_SIZE / 2])
    end
    autotile_rects
  end

  def get_A3_vertices(id)
    autotile = (id - 2048) / 48
    tileset_bitmap_index = bitmap_for_autotile(autotile)
    case tileset_bitmap_index
    when 0
      x2 = (TILE_SIZE * 2) * ((autotile) % 8)
      y2 = (TILE_SIZE * 3) * ((autotile) / 8)
    when 1
      x2 = (TILE_SIZE * 2) * ((autotile - 16) % 8)
      y2 = (TILE_SIZE * 3) * ((autotile - 16) / 8)
    when 2
      x2 = (TILE_SIZE * 2) * ((autotile - 48) % 8)
      y2 = (TILE_SIZE * 2) * ((autotile - 48) / 8)
    when 3
      x2 = (TILE_SIZE * 2) * ((autotile - 80) % 8)
      y2 = (TILE_SIZE * 3) * ((((autotile - 80) / 8) + 1) / 2) + (TILE_SIZE * 2) * (((autotile - 80) / 8) / 2)
    end
    index = (id - 2048) % 48

    if !A3[index]
      autotile_data = A1[index]
    else
      autotile_data = A3[index]
    end
    get_autotile_rects(autotile_data, x2, y2)
  end

  def get_A4_vertices(id)
    autotile = (id - 2048) / 48
    case autotile
    when 80..87
      get_A1_vertices(id)
    when 96..103
      get_A1_vertices(id)
    when 112..119
      get_A1_vertices(id)
    else
      get_A3_vertices(id)
    end
  end

  def need_animated_layer?
    return false if !@map_data
    for x in 0..@map_data.xsize - 1
      for y in 0..@map_data.ysize - 1
        if @map_data[x,y,0].between?(2048, 2815) ||
           @map_data[x,y,1].between?(2048, 2815)
          return true
        end
      end
    end
    false
  end

  def generate_upper_layers
    v_arr = RGFW::TileVertexArray.new
    vertex_array_lists = [
      [v_arr,     v_arr.dup],
      [v_arr.dup, v_arr.dup]
    ]
    for x in 0..@map_data.xsize - 1
      for y in 0..@map_data.ysize - 1
        next if @map_data[x, y, 2] == 0
        list_index  = @flags[@map_data[x, y, 2]] & OVER_TILE == 0 ? 0 : 1
        v_arr_index = (5 + @map_data[x, y, 2] / 256) == 5 ? 0 : 1
        vertex_array = vertex_array_lists[list_index][v_arr_index]
        collect_upper_tile(vertex_array, x, y)
      end
    end
    setup_vbos(@upper_vbo_lists, vertex_array_lists)
    tileset_bitmaps = [@bitmaps[5], @bitmaps[6]]
    @layers[1].vbos = @upper_vbo_lists[0]
    @layers[1].bitmaps = tileset_bitmaps
    @layers[2].vbos = @upper_vbo_lists[1]
    @layers[2].bitmaps = tileset_bitmaps
    vertex_array_lists.clear
  end

  def collect_upper_tile(vertex_array, x, y)
    tile_rect = get_upper_layer_rect(x, y)
    pos = RGFW::Transform.new
    pos.translate(x * TILE_SIZE, y * TILE_SIZE)
    vertex_array.append(tile_rect, pos)
  end

  def get_upper_layer_rect(x, y)
    n = @map_data[x, y, 2] % 256
    key = [:bc, n]
    tile_rect = @@cached_tile_rects[key]
    if !tile_rect
      tile_rect = Rect.new(TILE_SIZE * ((n % 8) + (8 * (n / 128))),
                           TILE_SIZE * ((n % 128) / 8),
                           TILE_SIZE, TILE_SIZE)
      @@cached_tile_rects[key] = tile_rect
    end
    tile_rect
  end

  def map_data=(data)
    return if @map_data == data
    @map_data = data
    refresh
  end

  def flags=(data)
    @flags = data
    refresh
  end

  def ox=(value)
    @ox = value
    for layer in @layers
      layer.ox = @ox
    end
  end

  def oy=(value)
    @oy = value
    for layer in @layers
      layer.oy = @oy
    end
  end
end
