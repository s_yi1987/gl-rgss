#Weird variables

module Graphics

  class FPSLimiter
  
    def initialize(desired_fps)
      @last_tick_count = RGSS.performance_counter
      @tick_frequency  = RGSS.performance_frequency
      @tick_freq_ms = @tick_frequency / 1000
    
      set_desired_fps(desired_fps)
    
      @adj_last = RGSS.performance_counter
      @adj_ideal_diff = 0
      @adj_reset_flag = false
    end
    
    def set_desired_fps(desired_fps)
      @ticks_per_frame = @tick_frequency / desired_fps
      @desired_fps = desired_fps
      @ms_limit = 1000.0 / @desired_fps
    end
    
    def reset_frame_adjust
      @adj_reset_flag = true
    end
    
    def delay
      tick_delta = RGSS.performance_counter - @last_tick_count
      to_delay = @ticks_per_frame - tick_delta
      to_delay -= @adj_ideal_diff
      
      to_delay = 0 if to_delay < 0
      
      delay_ticks(to_delay)
      
      tick_now = RGSS.performance_counter
      @last_tick_count = tick_now
      
      tick_diff = tick_now - @adj_last
      @adj_last = tick_now
      
      @adj_ideal_diff = tick_diff - @ticks_per_frame + @adj_ideal_diff
      if @adj_reset_flag
        @adj_ideal_diff = 0
        @adj_reset_flag = false
      end
    end
    
    def delay_ticks(ticks)
      return if ticks <= 0 
      
      ms = ticks.to_f / @tick_freq_ms
      ms *= 0.4 if (ms / @ms_limit) >= 0.6
      
      before_sleep_counter = RGSS.performance_counter
      sleep(ms / 1000)
      
      sleep_amount_ticks = RGSS.performance_counter - before_sleep_counter
      sleep_diff = ticks - sleep_amount_ticks
      
      if sleep_diff > 0
        wait_start_counter = RGSS.performance_counter
        while true
          wait_diff = RGSS.performance_counter - wait_start_counter
          break if wait_diff >= sleep_diff
        end
      end
    end
  
  end

  FPS_COUNT               = 30
  @frame_rate             = 60
  @frame_count            = 0
  @frame_count_recent     = 0
  @real_fps               = 0
  @fps_ticks              = 0
  @show_fps               = false
  @brightness             = 255
  @freeze                 = false                # or true?

  class << self
    attr_reader :width, :height
    attr_reader :screen
    attr_reader :brightness
    attr_reader :show_fps
    attr_accessor :frame_rate, :frame_count

    attr_reader :real_fps

    def init(title, x = RGFW::WINDOWPOS_CENTERED, y = x, width = @width, height = @height)
      @fps_limiter = FPSLimiter.new(@frame_rate)
      @fps_limiter.reset_frame_adjust
      
      @vertex_array  = RGFW::SpriteVertexData.new

      setup_screen(title, x, y, width, height)
      #@ticks_start = RGSS.ticks
      @initialized ||= true
      @render_states = RGFW::RenderStates.new
    end

    def setup_screen(title, x, y, width, height)
      return if initialized?
      @screen = RGFW::RenderWindow.new(title, x, y, width, height)
      @screen.vertical_sync_enabled = false
      resize_screen(width, height)
    end

    def resize_screen(width, height, center = true)
      @width  = width
      @height = height
      @graphics_render_target = Bitmap.new(width, height) if @screen
      if initialized?
        @screen.set_size(width, height)
        view = RGFW::View.new
        view.reset(RGFW::FloatRect.new(0, 0, width, height))
        @screen.view = view
        @screen.default_view = view
        center_screen if center
      end
    end

    def center_screen
      position_screen(RGFW::WINDOWPOS_CENTERED, RGFW::WINDOWPOS_CENTERED)
    end

    def position_screen(x, y)
      return if !initialized?
      @screen.set_position(x, y)
    end

    def initialized?
      @initialized
    end

    def screen_title=(title)
      @screen.title = title if initialized?
    end

    def screen_title
      @screen.title if initialized?
    end

    def show_fps=(show_fps)
      if show_fps
        self.screen_title = "#{RGSS.title} - #{@real_fps}fps"
      else
        self.screen_title = RGSS.title
      end
      @show_fps = show_fps
    end

    def update
      if @show_fps && @fps != @real_fps
        self.screen_title = "#{RGSS.title} - #{@real_fps}fps"
        @fps = @real_fps
      end
      RGSS.update

      draw_resources
      present
    end

    def draw_resources
      if @old_resources != RGSS.resources # Maybe here can make a dirty mark
        RGSS.resources.sort!
        @old_resources = RGSS.resources.clone
      end

      unless @frozen # redraw only when !frozen
        @screen.render_target = @graphics_render_target.texture
        @screen.clear
        RGSS.resources.each { |resource| resource.draw }
      end
      @vertex_array.texture_rect = @graphics_render_target.rect

      @render_states.texture = @graphics_render_target.texture
      @screen.render_target = nil
      @screen.clear
      @screen.draw(@vertex_array, @render_states)
    end

    def present
      @fps_limiter.delay
      @screen.present

      @frame_count_recent += 1
      if @frame_count_recent >= FPS_COUNT
        @frame_count_recent = 0
        now                 = RGSS.ticks
        @real_fps           = FPS_COUNT * 1000 / (now - @fps_ticks)
        @fps_ticks          = now
      end
    end

    def wait(duration)
      duration.times { update }
    end

    def fadeout(duration)
      step = 255 / duration
      duration.times { |i| brightness = 255 - i * step; update }
      brightness = 0
    end

    def fadein(duration)
      step = 255 / duration
      duration.times { |i| brightness = i * step; update }
      brightness = 255
    end

    def freeze
      @frozen = true
    end

    def transition(duration = 10, filename = nil, vague = 40)
      @frozen = false
      brightness = 255
      return


      puts "SHOULDN'T BE HERE"
      if filename.nil? #
        imgmap = Array.new(@width) { Array.new(@height) { 255 } }
      else
        b = Bitmap.new(filename)
        tmp_arr = [0, 1]
        imgmap = Array.new(@width) {|x|
          Array.new(@height) {|y|
            tmp_arr[0] = b.get_pixel(x, y).red
            tmp_arr.max
          }
        }
      end

      new_frame = Bitmap.new(@width, @height)
      new_frame.fill_rect(nil, Color::BLACK)

      RGSS.resources.sort!
      @old_resources = RGSS.resources.clone
      RGSS.resources.each { |resource| resource.draw(new_frame) }

      picmap = new_frame.surface.dup
      picmap_color     = Color.new
      transition_color = Color.new

      alpha_array = [0, 255]
      @width.times { |x| @height.times { |y|
          if imgmap[x][y] != 0
            picmap_color.set(picmap.get_pixel(x, y, false))
            alpha_array[0] = 255 / (duration / (255.0 / imgmap[x][y]))
            transition_color.set(picmap_color.red, picmap_color.green, picmap_color.blue, alpha_array.min)
            #TODO : alpha will be 255 after many render.it's different fron RPG Maker
          else
            transition_color.set(picmap_color.red, picmap_color.green, picmap_color, 255)
          end
          new_frame.surface.set_pixel(x, y, transition_color)
        }
      }
      new_frame.texture.update(new_frame.surface)

      maker = Bitmap.new(@width, @height) # create pre-render layout
      maker.fill_rect(nil, Color::BLACK)
      maker.blt(0, 0, @graphics_render_target, @graphics_render_target.rect)

      vertex_array = RGFW::SpriteVertexData.new(maker.rect)
      duration.times { |i|
        @screen.clear
        maker.blt(0, 0, new_frame, new_frame.rect) # alpha
        @screen.draw(vertex_array, RGFW::Transform::Identity, RGFW::BLEND_ALPHA, maker.texture)
        @screen.present
      }
                                                                                                     # TODO: free
      @frozen = false
      brightness = 255
      update
    end

    def snap_to_bitmap
      @graphics_render_target.dup # free
    end

    def frame_reset

    end

    def play_movie(filename)

    end

    def brightness=(brightness)
      @brightness = brightness < 0 ? 0 : brightness > 255 ? 255 : brightness
      @screen.brightness = brightness.to_f / 255
      @brightness
    end

  end

end
