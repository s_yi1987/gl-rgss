class Window
  include RGSS::Drawable

  # Refers to the bitmap (Bitmap) used as a window skin.
  #
  # Skin specifications are nearly identical to those in the previous version (VX). Resource standards: See the detailed information on window skins.
  attr_accessor :windowskin

  # Refers to the bitmap (Bitmap) used for the window's contents.
  attr_accessor :contents

  # The cursor box (Rect).
  #
  # Specifies a rectangle with coordinates based on the window's contents.
  attr_accessor :cursor_rect

  # Refers to the viewport (Viewport) associated with the window.
  attr_accessor :viewport

  # The cursor's blink status. If TRUE, the cursor is blinking. The default is TRUE.
  attr_accessor :active

  # The visibility of scrolling arrows. If TRUE, the arrows are visible. The default is TRUE.
  attr_accessor :arrows_visible

  # The pause graphic's visibility. This is a symbol that appears in the message window when waiting for the player to press a button. If TRUE, the graphic is visible. The default is FALSE.
  attr_accessor :pause

  # The window's x-coordinate.
  attr_accessor :x

  # The window's y-coordinate.
  attr_accessor :y

  # The window's width.
  attr_accessor :width

  # The window's height.
  attr_accessor :height

  # The window's z-coordinate. The larger the value, the closer to the player the window will be displayed.
  #
  # If multiple objects share the same z-coordinate, the more recently created object will be displayed closest to the player.
  #
  # The default is 100 (RGSS3).
  attr_accessor :z

  # The x-coordinate of the starting point of the window's contents. Change this value to scroll the window's contents.
  #
  # Also affects the cursor. (RGSS3)
  attr_accessor :ox

  # The y-coordinate of the starting point of the window's contents. Change this value to scroll the window's contents.
  #
  # Also affects the cursor. (RGSS3)
  attr_accessor :oy

  # The size of the padding between the window's frame and contents. The default value is 12. (RGSS3)
  attr_accessor :padding

  # The padding for the bottom. Must be set after padding because it is changed along with it.
  attr_accessor :padding_bottom

  # The window's opacity (0-255). Out-of-range values are automatically corrected. The default value is 255.
  attr_accessor :opacity

  # The window background's opacity (0-255). Out-of-range values are automatically corrected. The default value is 192 (RGSS3).
  attr_accessor :back_opacity

  # The opacity of the window's contents (0-255). Out-of-range values are automatically corrected. The default value is 255.
  attr_accessor :contents_opacity

  # The openness of the window (from 0 to 255). Out-of-range values are automatically corrected.
  #
  # By changing this value in stages from 0 (completely closed) to 255 (completely open), it is possible to create an animation of the window opening and closing. If the openness is less than 255, the contents of the window will not be displayed. The default value is 255.
  attr_accessor :openness

  # The color (Tone) of the window's background.
  attr_accessor :tone

  @@background = {}
  @@border     = {}
  @@tone       = {}
  @@window_viewport = Viewport.new
  @@window_viewport.z = 2000

  def initialize(x = 0, y = 0, width = 0, height = 0)
    @x                = x
    @y                = y
    @z                = 100
    @ox               = 0
    @oy               = 0
    @width            = width
    @height           = height
    @tone             = Tone.new
    @contents         = Bitmap.new(32, 32)
    @cursor_rect      = Rect.new
    @back_opacity     = 192
    @opacity          = 255
    @contents_opacity = 255
    @active           = true
    @openness         = 255
    @padding          = 12
    @padding_bottom   = 12
    @cursor_status    = 0
    @visible          = true
    @curcos_flash     = 0

    @vertex_buffer_obj = RGFW::VertexBufferObject.new
    @vertex_array  = RGFW::SpriteVertexData.new
    @transformable = RGFW::Transformable.new
    @render_states = RGFW::RenderStates.new
    super(@@window_viewport)
  end

  def update
    if active
      @cursor_status = (@cursor_status + 4) % 511
    else
      @cursor_status = 0
    end
  end

  def move(x, y, width, height)
    @x      = x
    @y      = y
    @width  = width
    @height = height
  end

  def open?
    openness == 255
  end

  def close?
    openness == 0
  end

  def openness=(openness)
    @openness = openness < 0 ? 0 : openness > 255 ? 255 : openness
  end

  def draw
    return if close?
    base_x = @x - @ox
    base_y = @y - @oy

    old_clip_rect = Graphics.screen.clip_rect
    revert_clip_rect = false
    if viewport
      viewport_rect = viewport.rect
      if viewport_rect != old_clip_rect
        Graphics.screen.clip_rect = viewport_rect
        revert_clip_rect = true
      end
      base_x += viewport.rect.x - viewport.ox
      base_y += viewport.rect.y - viewport.oy
    end
    base_y += @height * (255 - @openness) / 255 / 2 if @openness < 255
    
    #don't forget about alpha_mod later
    draw_bg(base_x, base_y)
    draw_border(base_x, base_y) if opacity > 0
    
    if open?
      draw_contents(base_x, base_y) if contents_opacity > 0
      draw_cursor(base_x, base_y) if cursor_rect.width > 0 && cursor_rect.height > 0
    end
    Graphics.screen.clip_rect = old_clip_rect if revert_clip_rect
  end

  def draw_bg(x, y)
    can_draw = opacity > 0 && back_opacity > 0 && @height * @openness / 255 - 8 > 0
    return if !can_draw
    
    width  = @width - 8
    width = width <= 0 ? 1 : width

    height = @height * @openness / 255 - 8
    height = height <= 0 ? 1 : height
    
    draw_bg1(x, y, width, height)
    draw_bg2(x, y, width, height)
  end

  def draw_bg1(x, y, width, height)
    wskin_size = 64
    scalefactor_x = width.to_f / wskin_size
    scalefactor_y = height.to_f / wskin_size
    
    toned_bg = apply_tone
    
    @transformable.set_scale(scalefactor_x, scalefactor_y)
    @transformable.set_position(x + 4, y + 4)
    
    @vertex_array.texture_rect = Rect.new(0, 0, wskin_size, wskin_size)
    @render_states.transform = @transformable.transform
    @render_states.texture   = toned_bg.texture
    
    @transformable.set_scale(1, 1)
    #draw, apply tone shader here, I guess
    Graphics.screen.draw(@vertex_array, @render_states)
  end

  def draw_bg2(x, y, width, height)
    wskin_x = 0 #x1
    wskin_y = 64 #y1
    wskin_size = 64 #w1, h1

    bg2_vertices = va_tiled(
      Rect.new(0, 0, width, height),
      Rect.new(wskin_x, wskin_y, wskin_size, wskin_size))
    
    prepare_vbo_with_vertices(bg2_vertices)
    
    @transformable.set_position(x + 4, y + 4)
    @render_states.transform = @transformable.transform
    @render_states.texture = @windowskin.texture

    Graphics.screen.draw(@vertex_buffer_obj, @render_states)
  end
  
  def va_tiled(bounds_rect, tile_rect)
    @@va_tiled_cache ||= {}
    
    tiled_varray = @@va_tiled_cache[[bounds_rect.to_s, tile_rect.to_s]]
    return tiled_varray if tiled_varray
    
    tiled_varray = RGFW::TileVertexArray.new
    tsf = RGFW::Transformable.new
    
    xnow = bounds_rect.x
    while xnow <= bounds_rect.x + bounds_rect.width
      ynow = bounds_rect.y
      while ynow <= bounds_rect.y + bounds_rect.height
        tsf.set_position(xnow, ynow)
        tile_rect_copy = tile_rect.dup
        if tile_rect_copy.width + xnow > bounds_rect.x + bounds_rect.width
          diff = (tile_rect_copy.width + xnow) - (bounds_rect.x + bounds_rect.width)
          tile_rect_copy.width -= diff
        end
        if tile_rect_copy.height + ynow > bounds_rect.y + bounds_rect.height
          diff = (tile_rect_copy.height + ynow) - (bounds_rect.y + bounds_rect.height)
          tile_rect_copy.height -= diff
        end
        tiled_varray.append(tile_rect_copy, tsf.transform)
        ynow += tile_rect.height
      end
      xnow += tile_rect.width
    end
    @@va_tiled_cache[[bounds_rect.to_s, tile_rect.to_s]] = tiled_varray
    tiled_varray
  end
  
  def prepare_vbo_with_vertices(vertices)
    vertex_capacity = @vertex_buffer_obj.vertex_capacity < vertices.vertex_count ?
      vertices.vertex_count :
      @vertex_buffer_obj.vertex_capacity
      
    index_capacity = @vertex_buffer_obj.index_capacity < vertices.index_count ?
      vertices.index_count :
      @vertex_buffer_obj.index_capacity
      
    if vertex_capacity != @vertex_buffer_obj.vertex_capacity ||
      index_capacity != vertices.index_count
       @vertex_buffer_obj.setup(vertex_capacity, index_capacity, RGFW::VBO_DYNAMIC_DRAW)
    end
    @vertex_buffer_obj.update(vertices)
    @vertex_buffer_obj.primitive_type   = vertices.primitive_type
    @vertex_buffer_obj.vertex_read_size = vertices.vertex_count
    @vertex_buffer_obj.index_read_size  = vertices.index_count
  end
  
  def apply_tone
    tone = @tone
    rst  = @@tone[[@windowskin.surface, tone.red, tone.green, tone.blue, tone.gray]]
    return rst if rst
    rst = @windowskin.copy_region(0, 0, 64, 64)
    gg  = tone.gray / 256

    c = Color.new
    max = [0, 0]
    min = [0, 255]
    for x in 0...rst.width
      for y in 0...rst.height
        c.set(rst.surface.get_pixel(x, y))
        g = ((c.red * 38 + c.green * 75 + c.blue * 15) / 128) % 256

        max[0]  = tone.red + c.red + (g - c.red) * gg
        min[0]  = max.max
        c.red   = min.min #[[tone.red + c.red + (g - c.red) * gg, 0].max, 255].min

        max[0]  = tone.green + c.green + (g - c.green) * gg
        min[0]  = max.max
        c.green = min.min #[[tone.green + c.green + (g - c.green) * gg, 0].max, 255].min

        max[0]  = tone.blue + c.blue + (g - c.blue) * gg
        min[0]  = max.max
        c.blue  = min.min #[[tone.blue + c.blue + (g - c.blue) * gg, 0].max, 255].min
        rst.surface.set_pixel(x, y, c)
      end
    end
    rst.texture.update(rst.surface)
    @@tone[[@windowskin.surface, tone.red, tone.green, tone.blue, tone.gray]] = rst
    rst
  end
  
  def draw_border(x, y)
    width  = @width
    height = @height * @openness / 255
    
    corner_varray = border_corner_varray(width, height)
    line_varray   = border_line_varray(width, height)
    
    @transformable.set_position(x, y)
    @render_states.texture   = @windowskin.texture
    @render_states.transform = @transformable.transform
    
    prepare_vbo_with_vertices(corner_varray)
    Graphics.screen.draw(@vertex_buffer_obj, @render_states)
    
    prepare_vbo_with_vertices(line_varray)
    Graphics.screen.draw(@vertex_buffer_obj, @render_states)
  end
  
  def border_corner_varray(width, height)
    tsf = RGFW::Transformable.new
    corner_varray = RGFW::TileVertexArray.new
    tl_rect = Rect.new(64, 0, 16, 16)
    tr_rect = Rect.new(128 - 16, 0, 16, 16)
    bl_rect = Rect.new(64, 64 - 16, 16, 16)
    br_rect = Rect.new(128 - 16, 64 - 16, 16, 16)
    
    corner_varray.append(tl_rect)
    
    tsf.set_position(width - 16, 0)
    corner_varray.append(tr_rect, tsf.transform)
    
    tsf.set_position(0, height - 16)
    corner_varray.append(bl_rect, tsf.transform)
    
    tsf.set_position(width - 16, height - 16)
    corner_varray.append(br_rect, tsf.transform)
    
    corner_varray
  end
  
  def border_line_varray(width, height)
    line_varray = RGFW::TileVertexArray.new
    
    bounds_rect = Rect.new(0, 16, 16, height - 32)
    tile_rect   = Rect.new(64, 16, 32, 32)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(width - 16, 16, 16, height - 32)
    tile_rect.set(128 - 16, 16, 32, 32)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(16, 0, width - 32, 16)
    tile_rect.set(64 + 16, 0, 32, 32)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(16, height - 16, width - 32, 16)
    tile_rect.set(64 + 16, 64 - 16, 32, 32)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    line_varray
  end
  
  def draw_contents(x, y)
    @vertex_array.texture_rect = Rect.new(0, 0, @width - padding * 2, @height - padding - padding_bottom)
    @transformable.set_position(x + padding, y + padding)

    @render_states.transform = @transformable.transform
    @render_states.texture = @contents.texture
    Graphics.screen.draw(@vertex_array, @render_states)
  end
  
  def draw_cursor(x, y)
    width  = cursor_rect.width
    height = cursor_rect.height
    
    corner_varray = cursor_corner_varray(width, height)
    prepare_vbo_with_vertices(corner_varray)
    
    @render_states.texture = @windowskin.texture
    @transformable.set_position(x + cursor_rect.x + padding, y + cursor_rect.y + padding)
    @render_states.transform = @transformable.transform
    
    Graphics.screen.draw(@vertex_buffer_obj, @render_states)
    
    line_varray = cursor_line_varray(width, height)
    prepare_vbo_with_vertices(line_varray)
    
    Graphics.screen.draw(@vertex_buffer_obj, @render_states)
  end
  
  def cursor_corner_varray(width, height)
    tsf = RGFW::Transformable.new
    corner_varray = RGFW::TileVertexArray.new
    tl_rect = Rect.new(64, 64, 8, 8)
    tr_rect = Rect.new(96 - 8, 64, 8, 8)
    bl_rect = Rect.new(64, 96 - 8, 8, 8)
    br_rect = Rect.new(96 - 8, 96 - 8, 8, 8)
    
    corner_varray.append(tl_rect)
    
    tsf.set_position(width - 8, 0)
    corner_varray.append(tr_rect, tsf.transform)
    
    tsf.set_position(0, height - 8)
    corner_varray.append(bl_rect, tsf.transform)
    
    tsf.set_position(width - 8, height - 8)
    corner_varray.append(br_rect, tsf.transform)
    
    corner_varray
  end
  
  def cursor_line_varray(width, height)
    line_varray = RGFW::TileVertexArray.new
    
    bounds_rect = Rect.new(0, 8, 8, height - 16)
    tile_rect   = Rect.new(64, 64 + 8, 8, 8)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(width - 8, 8, 8, height - 16)
    tile_rect.set(96 - 8, 64 + 8, 8, 8)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(8, 0, width - 16, 8)
    tile_rect.set(64 + 8, 64, 8, 8)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(8, height - 8, width - 16, 8)
    tile_rect.set(64 + 8, 96 - 8, 8, 8)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    bounds_rect.set(8, 8, width - 16, height - 16)
    tile_rect.set(64 + 8, 64 + 8, 16, 16)
    line_varray.append(va_tiled(bounds_rect, tile_rect))
    
    line_varray
  end

end