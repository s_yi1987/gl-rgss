Color = RGFW::Color

class Color
  BLACK = Color.new(0, 0, 0, 255)
  WHITE = Color.new(255, 255, 255, 255)
  TRANSPARENT = Color.new(0, 0, 0, 0)

  def ==(other) # :nodoc:
    same_color? other
  end

  def ===(other) # :nodoc:
    same_color? other
  end

  def eql?(other) # :nodoc:
    same_color? other
  end

  def same_color?(other) # :nodoc:
    raise TypeError.new("can't convert #{other.class} into Color") unless other.is_a? Color
    return red == other.red &&
           green == other.green &&
           blue == other.blue &&
           alpha == other.alpha
  end

  def _dump(depth = 0) # :nodoc:
    [red, green, blue, alpha].pack("D*")
  end

  def self._load(string) # :nodoc:
    new(*string.unpack("D*")) #fix by zh99998
  end

  def to_s # :nodoc:
    "(#{red}, #{green}, #{blue}, #{alpha})"
  end

end