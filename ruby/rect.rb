Rect = RGFW::Rect

class Rect

  def == other
    if other.is_a? Rect
      same_position?(other) && same_size?(other)
    else
      raise TypeError.new("Can't convert #{other.class} to Rect")
    end
  end

  def same_position?(other)
    x == other.x && y == other.y
  end

  def same_size?(other)
    width == other.width && height == other.height
  end

  def _dump(limit)
    [x, y, width, height].pack("iiii")
  end

  def self._load(string)
    new(*string.unpack("iiii"))
  end

  def to_s
    "(#{x}, #{y}, #{width}, #{height})"
  end

end