class TilemapLayer
  include RGSS::Drawable

  attr_reader :opacity
  attr_accessor :ox, :oy
  attr_accessor :vbos
  attr_accessor :bitmaps

  def initialize(viewport = nil)
    @bitmap = nil
    @x  = 0
    @y  = 0
    @z  = 0
    @ox = 0
    @oy = 0
    @opacity  = 255
    @disposed = false
    @visible  = true
    @transformable = RGFW::Transformable.new
    @render_states = RGFW::RenderStates.new
    super(viewport)
  end

  def opacity=(val)
    @opacity = RGFW.ensure_within_color_range(val)
  end

  def draw
    return unless @vbos && !@vbos.empty? && @bitmaps && !@bitmaps.empty?
    pos_x = 0
    pos_y = 0

    old_clip_rect = Graphics.screen.clip_rect
    revert_clip_rect = false
    if viewport
      viewport_rect = viewport.rect
      if viewport_rect != old_clip_rect
        Graphics.screen.clip_rect = viewport_rect
        revert_clip_rect = true
      end
      pos_x += viewport.rect.x - viewport.ox
      pos_y += viewport.rect.y - viewport.oy
    end

    @transformable.set_origin(@ox, @oy)
    @transformable.set_position(pos_x, pos_y)
    @render_states.transform = @transformable.transform
    for i in 0..@vbos.size - 1
      vbo = @vbos[i]
      next if vbo.disposed?
      @render_states.texture = @bitmaps[i].texture
      Graphics.screen.draw(vbo, @render_states)
    end
    Graphics.screen.clip_rect = old_clip_rect if revert_clip_rect
  end
end