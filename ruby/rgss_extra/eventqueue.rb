class EventQueue
  def poll
    @event ||= RGFW::Event.new
    if RGFW::Event.poll_event(@event)
      @event.dup
    else
      nil
    end
  end
end