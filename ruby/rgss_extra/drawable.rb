module RGSS
  module Drawable
    @@current_drawable_id = 0

    attr_accessor :x, :y, :viewport, :created_at
    attr_reader :z, :visible, :drawable_id

    def initialize(viewport = nil)
      @created_at  = RGSS.ticks
      @viewport    = viewport
      @drawable_id = @@current_drawable_id
      @@current_drawable_id += 1
      self.visible = @visible
    end

    def viewport=(viewport)
      @viewport    = viewport
      self.visible = @visible
    end

    def >(v)
      return false if @viewport.nil? && v.viewport
      if v.viewport
        return false if @viewport.z < v.viewport.z
        if @viewport.z == v.viewport.z
          return false if @viewport.created_at < v.viewport.created_at
          return false if @viewport.created_at == v.viewport.created_at &&
                          @viewport.viewport_id < v.viewport.viewport_id
        end
      end

      return false if @z < v.z
      if @z == v.z
        return false if @y < v.y
        if @y == v.y
          return false if @created_at < v.created_at
          return false if @created_at == v.created_at && @drawable_id < v.drawable_id
        end
      end
      true
    end

    def <=>(v)
      return 1 if self > v
      return -1
    end

    def z=(z)
      return if z == @z
      @z = z

      #self.visible = true if @visible && !@disposed
    end

    def y=(y)
      return if y == @y
      @y = y
      # RGSS.resources.sort
      #self.visible = true if @visible && !@disposed
    end

    def disposed?
      @disposed
    end

    def dispose
      @disposed = true
      RGSS.resources.delete self
    end

    def visible=(visible)
      #if @visible
      #  RGSS.resources.delete self
      #end
      @visible = visible
      if !@visible
        RGSS.resources.delete self
      elsif !RGSS.resources.include? self
        RGSS.resources << self
        #   RGSS.resources.sort
      end
=begin
      if @visible
        RGSS.resources.each_with_index { |resource, index|

          #TODO: ????
          if resource.viewport
            resource_viewport_z = resource.viewport.z
            resource_viewport_y = resource.viewport.y
          else
            resource_viewport_z = 0
            resource_viewport_y = 0
          end
          if self.viewport
            self_viewport_z = self.viewport.z
            self_viewport_y = self.viewport.y
          else
            self_viewport_z = 0
            self_viewport_y = 0
          end

          return RGSS.resources.insert(index-1, self) if (
          if self_viewport_z == resource_viewport_z
            if self_viewport_y == resource_viewport_y
              if (self.viewport.nil? and resource.viewport) or (self.viewport and resource.viewport and self.viewport.created_at < resource.viewport.created_at)
                true
              elsif (self.viewport and resource.viewport.nil?) or (self.viewport and resource.viewport and self.viewport.created_at > resource.viewport.created_at)
                false
              elsif self.z == resource.z
                if self.y == resource.y
                  self.created_at < resource.created_at
                else
                  self.y < resource.y
                end
              else
                self.y < resource.y
              end
            else
              self_viewport_y < resource_viewport_y
            end
          else
            self_viewport_z < resource_viewport_z
          end
          )

        }
        RGSS.resources += [self]
      end
=end
    end

    def draw(destination = nil)
      raise NotImplementedError
    end

  end
end