# The following built-in functions are defined in RGSS.

module RGSS
  @resources = []
  @load_path = []
  @event_queue = EventQueue.new

  class << self
    # ??
    attr_accessor :title

    # get_file??????????????
    attr_accessor :load_path

    # get_file???????????????????????
    attr_accessor :load_ext

    # ?????????Drawable???
    attr_accessor :resources

    def init
      RGFW.init
      Graphics.init(@title)
    end

    def ticks
      RGFW.ticks
    end

    def performance_counter
      RGFW.performance_counter
    end

    def performance_frequency
      RGFW.performance_frequency
    end

    def delay(ms)
      RGFW.delay(ms)
    end

    def last_error
      RGFW.last_error
    end

    def last_font_error
      RGFW.last_font_error
    end

    # ?load_path????????????????Autoload_Extname???????????? .png, .jpg, .gif, .bmp, .ogg, .wma, .mp3, .wav, .mid
    #
    # ?Audio?Bitmap???????
    #
    # ???????????filename??

    def get_file(filename)
      ([nil] + RGSS.load_path).each do |directory|
        ([''] + load_ext).each do |extname|
          path = File.expand_path filename + extname, directory
          if File.exist? path
            return path
          end
        end
      end
      filename
    end

    # ????????Graphics.update?Input.update???????

    def update
      while event = @event_queue.poll
        case event.type
        when RGFW::QUIT
          exit
        when RGFW::KEYDOWN, RGFW::KEYUP
          Input.events << event
        else
          #Log.debug "unhandled event: #{event.type}"
        end
      end
    end

    def load_settings(filename)
      #Load Game.ini
      $RGSS_CONFIG = IniFile.load('Game.ini', encoding: "GBK")['Game']

      #Set title
      @title = $RGSS_CONFIG['Title']

      #Detect RGSS version
      $RGSS_VERSION = if index = ['.rxdata', '.rvdata' '.rvdata2'].index(File.extname($RGSS_CONFIG['Scripts']))
        index + 1
      elsif $RGSS_CONFIG['Library'] && $RGSS_CONFIG['Library'] =~ /RGSS(\d)\d\d\w?\.dll/i
        $1.to_i
      else
        puts "Warning: Can't detect RGSS version!"
        1
      end

      #Set screen size
      if $RGSS_VERSION == 1
        Graphics.resize_screen(640, 480)
      else
        Graphics.resize_screen(544, 416)
      end

      #Load RTP on all platform
      if $RGSS_VERSION == 1
        [$RGSS_CONFIG['RTP1'], $RGSS_CONFIG['RTP2'], $RGSS_CONFIG['RTP3']].each do |rtp|
          RGSS.load_path << ENV["RGSS_RTP_#{rtp}"]
        end
      else
        RGSS.load_path << ENV["RGSS#{$RGSS_VERSION}_RTP_#{$RGSS_CONFIG['RTP']}"]
      end

      #Load RTP on windows
      if RUBY_PLATFORM['mingw'] or RUBY_PLATFORM['mswin']
        require 'win32/registry'
        registry = Win32::Registry::HKEY_LOCAL_MACHINE
        if $RGSS_VERSION == 1
          registry.open('Software\Enterbrain\RGSS\RTP') do |reg|
            [$RGSS_CONFIG['RTP1'], $RGSS_CONFIG['RTP2'], $RGSS_CONFIG['RTP3']].each do |rtp|
              (RGSS.load_path << reg[rtp].force_encoding(Encoding.default_external).encode('UTF-8')) rescue nil
            end
          end
        else
          registry.open("Software\\Enterbrain\\RGSS#{$RGSS_VERSION}\\RTP") do |reg|
            (RGSS.load_path << reg[$RGSS_CONFIG['RTP']].force_encoding(Encoding.default_external).encode('UTF-8')) rescue nil
          end
        end
      end
    end
  end

  self.load_ext  = ['.png', '.jpg', '.gif', '.bmp', '.ogg', '.wma', '.mp3', '.wav', '.mid']
  self.load_path = []
end

# Evaluates the provided block one time only.
#
# Detects a reset within a block with a press of the F12 key and returns to the beginning if reset.
#  rgss_main { SceneManager.run }

def rgss_main
  RGSS.init
  begin
    yield
  rescue RGSSReset
    RGSS.resources.clear
    retry
  end
end

# Stops script execution and only repeats screen refreshing. Defined for use in script introduction.
#
# Equivalent to the following.
#
#  loop { Graphics.update }

def rgss_stop

end

# Loads the data file indicated by filename and restores the object.
#
#  $data_actors = load_data("Data/Actors.rvdata2")
#
# This function is essentially the same as:
#
#  File.open(filename, "rb") { |f|
#    obj = Marshal.load(f)
#  }
#
# However, it differs in that it can load files from within encrypted archives.

def load_data(filename)
  File.open(filename, "rb") { |f|
    obj = Marshal.load(f)
  }
end

# Saves the object obj to the data file indicated by filename.
#
#  save_data($data_actors, "Data/Actors.rvdata2")
#
# This function is the same as:
#  File.open(filename, "wb") { |f|
#    Marshal.dump(obj, f)
#  }

def save_data(obj, filename)
  File.open(filename, "wb") { |f|
    Marshal.dump(obj, f)
  }
end

# Outputs the arguments to the message box. If a non-string object has been supplied as an argument, it will be converted into a string with to_s and output.
#
# Returns nil.
#
# <b>(Not Implemented in OpenRGSS)</b>
def msgbox(*args)
  if RUBY_PLATFORM['mingw'] or RUBY_PLATFORM['mswin']
    require 'dl'
    @@messagebox ||= DL::CFunc.new(DL.dlopen('user32')['MessageBoxA'], DL::TYPE_LONG, 'MessageBox')
    @@messagebox.call([0, args.collect { |arg| arg.to_s }.join("\n"), RGSS.title, 0].pack('L!ppL!').unpack('L!*'))
  else
    puts args
  end
end

# Outputs obj to the message box in a human-readable format. Identical to the following code (see Object#inspect):
#
#  msgbox obj.inspect, "\n", obj2.inspect, "\n", ...
#
# Returns nil.
#
# <b>(Not Implemented in OpenRGSS)</b>
def msgbox_p(*args)
  msgbox args.collect { |obj| obj.inspect }.join("\n")
end
