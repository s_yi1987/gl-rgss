# The bitmap class. Bitmaps represent images.
#
# Sprites (Sprite) and other objects must be used to display bitmaps onscreen.

class Bitmap
  attr_accessor :font,  :text
  attr_reader   :texture

  # :call-seq:
  #  Bitmap.new(filename)
  #  Bitmap.new(width, height)
  #
  # Loads the graphic file specified in filename or size and creates a bitmap object.
  #
  # Also automatically searches files included in RGSS-RTP and encrypted archives. File extensions may be omitted.

  def initialize(width, height = nil)
    setup_draw_data(width, height)
    @font = Font.new
  end

  #private
  def setup_draw_data(width, height = nil)
    if !width.is_a? Numeric
      arg = width.is_a?(String) ? RGSS.get_file(width) : width
      arg = arg.surface if arg.is_a? Bitmap
      @surface = RGFW::Surface.new(arg)
    else
      if width <= 0 || height <= 0
        raise ArgumentError("Both width and height has to be larger than 0")
      end
      @surface = RGFW::Surface.new(width, height)
    end
    @texture = RGFW::Texture.new(@surface)
    @texture.window = Graphics.screen

    @download_pixels = false
  end

  # Frees the bitmap. If the bitmap has already been freed, does nothing.

  def dispose
    @surface.dispose

    if !RGFW.has_active_gl_context?
      res = Graphics.screen.set_active
      throw RuntimeError("Failed to activate GL context for disposal of Texture") if !res
    end
    @texture.dispose
  end

  # Returns true if the bitmap has been freed.

  def disposed?
    @surface.disposed? && @texture.disposed?
  end

  # Gets the bitmap width.

  def width
    @surface.width
  end

  # Gets the bitmap height.

  def height
    @surface.height
  end

  # Gets the bitmap rectangle (Rect).

  def rect
    Rect.new(0, 0, width, height)
  end

  # Performs a block transfer from the src_bitmap box src_rect (Rect) to the specified bitmap coordinates (x, y).
  #
  # opacity can be set from 0 to 255.

  def blt(x, y, src_bitmap, src_rect, opacity = 255)
    dest_rect = Rect.new(x, y, src_rect.width, src_rect.height)
    stretch_blt(dest_rect, src_bitmap, src_rect, opacity)
  end

  # Performs a block transfer from the src_bitmap box src_rect (Rect) to the specified bitmap box dest_rect (Rect).
  #
  # opacity can be set from 0 to 255.

  def stretch_blt(dest_rect, src_bitmap, src_rect, opacity = 255)
    throw RuntimeError("Can't blit with a disposed Bitmap") if disposed?

    src_texture    = src_bitmap.is_a?(Bitmap) ? src_bitmap.texture : src_bitmap
    old_blend_mode = src_texture.blend_mode
    old_alpha_mod  = src_texture.alpha_mod

    src_texture.blend_mode = RGFW::BLEND_ALPHA
    src_texture.alpha_mod  = opacity

    if dest_rect.same_size? src_rect
      @texture.blit(src_texture, src_rect, dest_rect)
    else
      @texture.blit_scaled(src_texture, src_rect, dest_rect)
    end

    src_texture.blend_mode = old_blend_mode
    src_texture.alpha_mod  = old_alpha_mod
    @download_pixels = true
  end

  # Clears the entire bitmap.

  def clear
    clear_rect(nil)
  end

  # Clears this bitmap box or (x, y, width, height) or rect (Rect).

  def clear_rect(x, y = nil, width = nil, height = nil)
    rect = nil
    if x.is_a?(Rect) || x.nil?
      rect = x
    else
      rect = Rect.new(x, y, width, height)
    end
    fill_rect(rect, Color::TRANSPARENT)
  end

  # :call-seq:
  # fill_rect(x, y, width, height, color)
  # fill_rect(rect, color)
  #
  # Fills the bitmap box (x, y, width, height) or rect (Rect) with color (Color).

  def fill_rect(x, y, width = nil, height = nil, color = nil)
    rect = nil
    if x.is_a?(Rect) || x.nil?
      rect  = x
      color = y
    else
      rect = Rect.new(x, y, width, height)
    end
    @texture.fill_rect(rect, color)
    @download_pixels = true
  end

  # :call-seq:
  # gradient_fill_rect(x, y, width, height, color1, color2[, vertical])
  # gradient_fill_rect(rect, color1, color2[, vertical])
  #
  # Fills in this bitmap box (x, y, width, height) or rect (Rect) with a gradient from color1 (Color) to color2 (Color).
  #
  # Set vertical to true to create a vertical gradient. Horizontal gradient is the default.

  def gradient_fill_rect(x, y, width, height = false, color1 = nil, color2 = nil, vertical = false) #TODO: Make it faster with custom set render target
    if x.is_a? Rect
      rect     = x
      color1   = y
      color2   = width
      vertical = height
      x        = rect.x
      y        = rect.y
      width    = rect.width
      height   = rect.height
    end
    old_view = Graphics.screen.view
    old_clip_rect = Graphics.screen.clip_rect
    old_render_target = Graphics.screen.render_target
    Graphics.screen.render_target = @texture
    fill_color = Color.new
    transformable = RGFW::Transformable.new

    varray_rect = Rect.new(0, 0, vertical ? width : 1, vertical ? 1 : height)
    vertex_array = RGFW::SpriteVertexData.new(varray_rect)

    render_states = RGFW::RenderStates.new
    render_states.blend_mode = RGFW::BLEND_NONE

    loop_amount = vertical ? height : width
    loop_amount.times do |i|
      fill_color.set(color1.red + (color2.red - color1.red) * i / loop_amount,
          color1.green + (color2.green - color1.green) * i / loop_amount,
          color1.blue  + (color2.blue - color1.blue) * i / loop_amount,
          color1.alpha + (color2.alpha - color1.alpha) * i / loop_amount
      )
      vertex_array.color = fill_color
      x_pos = vertical ? x : x + i
      y_pos = vertical ? y + i : y
      transformable.set_position(x_pos, y_pos)
      render_states.transform = transformable.transform
      Graphics.screen.draw(vertex_array, render_states)
    end
    Graphics.screen.render_target = old_render_target
    Graphics.screen.clip_rect = old_clip_rect
    Graphics.screen.view = old_view
    @download_pixels = true
  end

  # Gets the color (Color) at the specified pixel (x, y).

  def get_pixel(x, y, get_color = true)
    update_surface
    if get_color
      @surface.get_pixel_color(x, y)
     else
      @surface.get_pixel(x, y)
    end
  end

  def update_surface
    return unless @download_pixels
    @texture.download_to(@surface)
    @download_pixels = false
  end

  # Sets the specified pixel (x, y) to color (Color).

  def set_pixel(x, y, color)
    fill_rect(Rect.new(x, y, 1, 1), color)
  end

  # Changes the bitmap's hue within 360 degrees of displacement.
  #
  # This process is time-consuming. Furthermore, due to conversion errors, repeated hue changes may result in color loss.

  def hue_change(hue)

  end

  # Applies a blur effect to the bitmap. This process is time consuming.

  def blur

  end

  # Applies a radial blur to the bitmap. angle is used to specify an angle from 0 to 360. The larger the number, the greater the roundness.
  #
  # division is the division number (from 2 to 100). The larger the number, the smoother it will be. This process is very time consuming.

  def radial_blur(angle, division)

  end

  # Draws the string str in the bitmap box (x, y, width, height) or rect (Rect).
  #
  # If str is not a character string object, it will be converted to a character string using the to_s method before processing is performed.
  #
  # If the text length exceeds the box's width, the text width will automatically be reduced by up to 60 percent.
  #
  # Horizontal text is left-aligned by default. Set align to 1 to center the text and to 2 to right-align it. Vertical text is always centered.
  #
  # As this process is time-consuming, redrawing the text with every frame is not recommended.

  def draw_text(x, y, width = 0, height = nil, str = nil, align = 0)
    if x.is_a? Rect
      rect  = x
      str   = y
      align = width

      x      = rect.x
      y      = rect.y
      width  = rect.width
      height = rect.height
    end

    str = str.to_s
    text_size_rect = text_size(str)
    if align == 2
      x += width - text_size_rect.width
    elsif align == 1
      x += (width - text_size_rect.width) / 2
    end

    #tmp = @font.surface.render_blended_utf8
    tmp = RGFW::Surface.new
    @font.entity.render_utf8_blended_to(tmp, str, @font.color)

    @@text_texture ||= RGFW::Texture.new
    final_w = @@text_texture.width
    final_h = @@text_texture.height

    if tmp.width > final_w
      final_w = tmp.width
      setup_needed = true
    end
    if tmp.height > final_h
      final_h = tmp.height
      setup_needed = true
    end
    @@text_texture.setup(final_w, final_h) if setup_needed
    @@text_texture.update(tmp)
    blt(x, y, @@text_texture, tmp.rect)
    tmp.dispose
  end

  # Gets the box (Rect) used when drawing the string str with the draw_text method. Does not include the outline portion (RGSS3) and the angled portions of italicized text.
  #
  # If str is not a character string object, it will be converted to a character string using the to_s method before processing is performed.

  def text_size(str)
    str = str.to_s if !str.is_a? String
    @font.entity.calc_text_size(str)
  end

  def draw(*args)
    @texture.draw(*args)
  end

  def surface
    update_surface
    @surface
  end

  def dup
    b = Bitmap.new(self)
    b.font = @font
    b
  end

  def clone
    dup
  end

  def copy_region(*args)
    case args.size
    when 1
      rect = *args
    when 4
      rect = Rect.new(*args)
    else
      raise ArgumentError("Invalid Bitmap copy region!")
    end
    bitmap_copy = Bitmap.new(rect.width, rect.height)
    bitmap_copy.blt(0, 0, self, rect)
    bitmap_copy
  end

  def color_mod=(color)
    @surface.color_mod = color
    @texture.color_mod = color
  end

  def alpha_mod=(mod)
    @surface.alpha_mod = mod
    @texture.alpha_mod = mod
  end

  def blend_mode=(mode)
    @surface.blend_mode = mode
    @texture.blend_mode = mode
  end

  private :setup_draw_data
end